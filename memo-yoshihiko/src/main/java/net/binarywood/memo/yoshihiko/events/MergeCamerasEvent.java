/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.events;

import java.util.HashSet;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.event.EventTarget;
import net.binarywood.memo.common.model.Camera;

/**
 * An event for merging two cameras
 *
 * @author Stefanie Wiegand
 */
public class MergeCamerasEvent extends ActionEvent {
	private static final long serialVersionUID = 1L;

	private final Camera newCamera;
	private final Set<Camera> oldCameras;

	/**
	 * Create a new MergeCamerasEvent
	 *
	 * @param source the event source
	 * @param target the event target
	 * @param newCam the new, merged camera
	 * @param oldCams the old cameras that are merged into the new camera
	 */
	public MergeCamerasEvent(Object source, EventTarget target, Camera newCam, Set<Camera> oldCams) {
		super(source, target);
		this.newCamera = newCam;
		if (oldCams!=null) {
			this.oldCameras = oldCams;
		} else {
			this.oldCameras = new HashSet<>();
		}
	}

	/**
	 * Get the new camera
	 *
	 * @return the new camera
	 */
	public Camera getNewCamera() {
		return newCamera;
	}

	/**
	 * Get the old cameras
	 *
	 * @return the old cameras
	 */
	public Set<Camera> getOldCameras() {
		return oldCameras;
	}

}
