/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */
package net.binarywood.memo.yoshihiko.components;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.model.OwnerHistory;
import net.binarywood.memo.common.model.TimezoneHistory;
import net.binarywood.memo.common.spec.AComponent;
import net.binarywood.memo.common.spec.AController;
import net.binarywood.memo.yoshihiko.controller.CameraManagerController;
import net.binarywood.memo.yoshihiko.events.ClearCamerasEvent;
import net.binarywood.memo.yoshihiko.events.MergeCamerasEvent;
import net.binarywood.memo.yoshihiko.events.ShowMediaEvent;
import net.binarywood.memo.yoshihiko.events.UpdateCameraEvent;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This component provides a plug-in camera manager. It can import/export cameras from XML and/or database and perform
 * operations on cameras via the CameraTile component.
 *
 * @author Stefanie Wiegand
 */
public class CameraManagerPane extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(CameraManagerPane.class);

	private boolean showMediaButtons;

	@FXML
	private ImageView helpImageView;
	@FXML
	private ScrollPane cameraTilesScrollPane;
	@FXML
	private VBox cameraTilesVbox;
	@FXML
	private Button clearCamerasButton;
	@FXML
	private VBox xmlVBox;
	@FXML
	private Button xmlImportButton;
	@FXML
	private Button xmlExportButton;
	@FXML
	private VBox dbVBox;
	@FXML
	private Button dbSaveButton;
	@FXML
	private Button dbClearButton;
	@FXML
	private Button dbImportButton;
	@FXML
	private Button dbExportButton;

	/**
	 * Create a new CameraMamagerPane
	 *
	 * @param controller the controller to handle the component
	 */
	public CameraManagerPane(AController controller) {
		super(controller);

		addTooltip(helpImageView, "Editing cameras is potentially dangerous.\n"
				+ "It will permanently modify photo metadata and - if not carefully used - \n"
				+ "might mess up photos. It is however useful in case of third party software\n"
				+ "messing up EXIF data or to manually add serial numbers to cameras\n"
				+ "that don't have one.\n");

		//set image from resources
		helpImageView.setImage(new Image(getClass().getClassLoader().getResourceAsStream(MemoOptions.ICON_HELP)));

		//TODO: make DB panel visible
		dbVBox.setVisible(false);
		dbVBox.setManaged(false);

		addEventHandlers();
	}

	/**
	 * Add event handlers to catch merge cameras event from cameratile
	 */
	private void addEventHandlers() {

		cameraTilesVbox.addEventHandler(ActionEvent.ANY, (ActionEvent event) -> {

			//merge event
			if (event.getClass().equals(MergeCamerasEvent.class)) {

				Set<Camera> toMerge = new HashSet<>();
				cameraTilesVbox.getChildrenUnmodifiable().stream().map(n -> (CameraTile) n)
					.filter(ct -> ct.isSelected()).forEach(ct -> toMerge.add(ct.getCamera()));

				//only merge if there is more than one camera
				if (toMerge.size() > 1) {

					//collect all makes
					Set<String> makes = new HashSet<>();
					toMerge.stream().forEach(c -> {
						if (c.getMake() != null && !c.getMake().isEmpty()) {
							makes.add(c.getMake());
						}
					});
					if (makes.isEmpty()) {
						makes.add(MemoOptions.UNKNOWN);
					}

					//collect all models
					Set<String> models = new HashSet<>();
					toMerge.stream().forEach(c -> {
						if (c.getModel() != null && !c.getModel().isEmpty()) {
							models.add(c.getModel());
						}
					});
					if (models.isEmpty()) {
						models.add(MemoOptions.UNKNOWN);
					}

					//collect all serial numbers
					Set<String> serials = new HashSet<>();
					toMerge.stream().forEach(c -> serials.add(c.getSerialNumber()));
					if (serials.isEmpty()) {
						serials.add("");
					}

					//show dialog for the user to choose which values the merged camera should have
					Dialog<HashMap<String, String>> dialog = new Dialog<>();
					dialog.setTitle("Merge cameras");
					dialog.setHeaderText("Please select the values for the merged camera. You can modify it later");
					//use MemO CSS for dialog
					dialog.getDialogPane().getStylesheets().add(
							getClass().getClassLoader().getResource(MemoOptions.CSS_COMMON).toExternalForm()
					);

					// Set the button types.
					ButtonType mergeButtonType = new ButtonType("Merge", ButtonData.OK_DONE);
					dialog.getDialogPane().getButtonTypes().addAll(mergeButtonType, ButtonType.CANCEL);

					//create a new grid
					GridPane grid = new GridPane();
					grid.setHgap(10);
					grid.setVgap(10);
					grid.setPadding(new Insets(20, 150, 10, 10));

					//build comboboxes for make/model/serial
					ComboBox<String> makesBox = new ComboBox<>();
					makes.stream().forEach(make -> makesBox.getItems().add(make));
					makesBox.setValue(makes.iterator().next());

					ComboBox<String> modelsBox = new ComboBox<>();
					models.stream().forEach(model -> modelsBox.getItems().add(model));
					modelsBox.setValue(models.iterator().next());

					ComboBox<String> serialsBox = new ComboBox<>();
					serials.stream().forEach(serial -> serialsBox.getItems().add(serial));
					//set a non-empty value as default for the serial
					for (String s : serials) {
						if (s != null && !s.isEmpty()) {
							serialsBox.setValue(s);
							break;
						}
					}

					//put the boxes and labels into the grid
					grid.add(new Label("Make:"), 0, 0);
					grid.add(makesBox, 1, 0);
					grid.add(new Label("Model:"), 0, 1);
					grid.add(modelsBox, 1, 1);
					grid.add(new Label("Serial number:"), 0, 2);
					grid.add(serialsBox, 1, 2);
					dialog.getDialogPane().setContent(grid);

					// Convert the result to the desired data structure
					dialog.setResultConverter(dialogButton -> {
						if (dialogButton == mergeButtonType) {
							HashMap<String, String> result = new HashMap<>();
							result.put("make", makesBox.getValue());
							result.put("model", modelsBox.getValue());
							result.put("serial", serialsBox.getValue());
							return result;
						}
						return null;
					});

					//show the dialog
					Optional<HashMap<String, String>> result = dialog.showAndWait();

					//user has closed the dialog
					result.ifPresent((HashMap<String, String> r) -> {

						//use user input for new, merged camera
						Camera c = new Camera();
						c.setMake(r.get("make"));
						c.setModel(r.get("model"));
						c.setSerialNumber(r.get("serial"));
						c.makeIdentifier();
						logger.debug("Merging cameras into {}", c.getIdentifier());

						//get histories from cameras to merge
						TimezoneHistory settingsHistory = new TimezoneHistory();
						TimezoneHistory locationHistory = new TimezoneHistory();
						OwnerHistory ownerHistory = new OwnerHistory();
						//merge them
						toMerge.stream().forEach(cam -> {
							settingsHistory.merge(cam.getSettingsHistory());
							locationHistory.merge(cam.getLocationHistory());
							ownerHistory.merge(cam.getOwnerHistory());
						});
						//set the merged history to the merged camera
						c.setSettingsHistory(settingsHistory);
						c.setLocationHistory(locationHistory);
						c.setOwnerHistory(ownerHistory);

						//remove old cameras
						Set<CameraTile> toDelete = new HashSet<>();
						Set<Camera> oldCams = new HashSet<>();
						cameraTilesVbox.getChildrenUnmodifiable().stream().map(n -> (CameraTile) n)
							.filter(ct -> ct.isSelected()).forEach(ct -> {
								toDelete.add(ct);
								oldCams.add(ct.getCamera().copy());
							});
						toDelete.stream().forEach(ct -> {
							cameraTilesVbox.getChildren().remove(ct);
							getController().getCameras().remove(ct.getCamera().getIdentifier());
						});

						//add new camera
						try {
							getController().getCameras().put(c.getIdentifier(), c);
							cameraTilesVbox.getChildren().add(new CameraTile(c, getController()));
						} catch (IllegalArgumentException e) {
							logger.error("Could not add duplicate CameraTile for camera {}", c.getIdentifier(), e);
						}

						//send event to parent for updating cameras and all photos that need to be modified
						//(write tags) if there are any photos
						if (mainContainer.getParent() != null) {
							logger.debug("Sending merge event to {}", mainContainer.getParent());
							fireEvent(new MergeCamerasEvent(cameraTilesVbox, mainContainer.getParent(), c, oldCams));
						}

					});
				} else {
					//inform the user that a merge is not possible
					(controller.createAlert(AlertType.INFORMATION, "Merge not possible",
						"Only one camera is selected", "Please select more than one camera to merge")).showAndWait();
				}
			} else if (event.getClass().equals(ShowMediaEvent.class)) {
				//just pass this event on
				fireEvent(new ShowMediaEvent(
					cameraTilesVbox, mainContainer.getParent(), ((ShowMediaEvent) event).getCamera()
				));
			} else {
				logger.info("Caught unknown event {}", event);
			}
			event.consume();
		});
	}

	/**
	 * Clear all cameras
	 *
	 * @param event the event that triggered this
	 */
	@FXML
	private void clearCameras(ActionEvent event) {

		//remove UI components
		cameraTilesVbox.getChildren().clear();
		getController().clearCameras();

		//send event to controller to remove cameras from model
		if (mainContainer.getParent() != null) {
			logger.debug("Sending clear event to {}", mainContainer.getParent());
			this.fireEvent(new ClearCamerasEvent(this, mainContainer.getParent()));
		}
		event.consume();
	}

	/**
	 * Import cameras from an XML config file
	 *
	 * @param event the event that triggered this
	 */
	@FXML
	private void importXML(ActionEvent event) {

		//let the user pick a XML camera config file to import
		FileChooser fc = new FileChooser();
		fc.setTitle("Choose camera config XML file");
		fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("XML configuration file", "xml"));
		File file = fc.showOpenDialog(getController().getStage());

		//do it
		if (file != null) {
			getController().importXML(file.getAbsolutePath());
		}

		//update display
		refresh();
		event.consume();
	}

	/**
	 * Export all cameras to an XML file
	 *
	 * @param event the event that triggered this
	 */
	@FXML
	private void exportXML(ActionEvent event) {

		//let the user pick a destination to export an XML camera config file
		FileChooser fc = new FileChooser();
		fc.setTitle("Save camera config XML file");
		fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("XML configuration file", "xml"));
		fc.setInitialFileName("cameras_" + (new DateTime()).toString(MediaItem.safeFileDateFormat) + ".xml");
		File file = fc.showSaveDialog(getController().getStage());

		//do it
		if (file != null) {
			getController().exportXML(file.getAbsolutePath());
			//make alert
			Alert a = controller.createAlert(Alert.AlertType.CONFIRMATION, "Exported cameras",
				"Your cameras were exported", "The export file was saved to " + file.getAbsolutePath());
			a.showAndWait();
		}
		event.consume();
	}

	/**
	 * Store all cameras in the connected database
	 *
	 * @param event the event that triggered this
	 */
	@FXML
	private void saveToDB(ActionEvent event) {
		getController().savetoKB();
		event.consume();
	}

	/**
	 * Clear the connected db
	 *
	 * @param event the event that triggered this
	 */
	@FXML
	private void clearDB(ActionEvent event) {
		getController().clearKB();
		event.consume();
	}

	/**
	 * load cameras from the connected db
	 *
	 * @param event the event that triggered this
	 */
	@FXML
	private void loadFromDB(ActionEvent event) {
		getController().loadfromKB();
		event.consume();
	}

	/**
	 * Create a dump of the current database in XML format
	 *
	 * @param event the event that triggered this
	 */
	@FXML
	private void exportDB(ActionEvent event) {
		//TODO: let user select the path
		//getController().exportKB(path);
		event.consume();
	}

	/**
	 * Refresh cameras by clearing and reloading the display
	 */
	public void refresh() {

		//clear
		while (cameraTilesVbox.getChildren().size()>0) {
			try {
				cameraTilesVbox.getChildren().remove(0);
			} catch (IllegalArgumentException e) {
				//results in an IllegalArgumentException:
				//Children: duplicate children added: parent = VBox[id=cameraTilesVbox]
				//deliberately ignoring exception here and forcing to clear "manually"
			}
		}

		//re-add cameras
		getController().getCameras().values().stream().forEach(c -> {
			CameraTile ct = new CameraTile(c, getController());
			if (!cameraTilesVbox.getChildrenUnmodifiable().contains(ct)) {
				logger.debug("Adding camera tile for {}", c.getIdentifier());
				try {
					cameraTilesVbox.getChildren().add(ct);
				} catch (IllegalArgumentException e) {
					logger.error("Could not add camera tile for camera {}, it appears to exist already",
						c.getIdentifier(), e);
				}
			}
		});

		//show media buttons if they were shown before
		setMediaButtonsVisibility(showMediaButtons);
	}

	/**
	 * Send update event in case Yohihiko is running in embedded mode
	 *
	 * @param oldCamera the camera to change
	 * @param newCamera what to change it to
	 */
	public void updateCamera(Camera oldCamera, Camera newCamera) {
		//send event to parent (if it's run as embedded camera manager) for updating cameras and all photos that
		//need to be modified (write tags) if there are any photos
		if (mainContainer.getParent() != null) {
			logger.debug("Sending update event to {}", mainContainer.getParent());
			this.fireEvent(new UpdateCameraEvent(this, mainContainer.getParent(), oldCamera, newCamera));
		}
	}

	/**
	 * Show the XML area
	 *
	 * @param visible true to show, false to hide
	 */
	public void showXMLArea(boolean visible) {
		xmlVBox.setVisible(visible);
		xmlVBox.setManaged(visible);
	}

	/**
	 * Show the database area
	 *
	 * @param visible true to show, false to hide
	 */
	public void showDBArea(boolean visible) {
		dbVBox.setVisible(visible);
		dbVBox.setManaged(visible);
	}

	/**
	 * Display the buttons for showing media taken by that camera
	 *
	 * @param visible whether to show the buttons or not
	 */
	public void setMediaButtonsVisibility(boolean visible) {

		showMediaButtons = visible;
		cameraTilesVbox.getChildren().stream().forEach(n -> ((CameraTile) n).toggleMediaButton(showMediaButtons));
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public final CameraManagerController getController() {
		return (CameraManagerController) controller;
	}

	public int getNumCameraTiles() {
		return cameraTilesVbox.getChildrenUnmodifiable().size();
	}

}
