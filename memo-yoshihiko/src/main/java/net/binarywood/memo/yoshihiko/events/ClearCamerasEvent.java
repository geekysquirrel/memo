/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.events;

import javafx.event.ActionEvent;
import javafx.event.EventTarget;

/**
 * An event for clearing the camera manager
 *
 * @author Stefanie Wiegand
 */
public class ClearCamerasEvent extends ActionEvent {
	private static final long serialVersionUID = 1L;

	/**
	 * Create a new clear event
	 *
	 * @param source the event source
	 * @param target the event target
	 */
	public ClearCamerasEvent(Object source, EventTarget target) {
		super(source, target);
	}

}
