/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.presenter;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import net.binarywood.memo.common.components.QuickMenu;
import net.binarywood.memo.common.spec.APresenter;
import net.binarywood.memo.yoshihiko.components.CameraManagerPane;
import net.binarywood.memo.yoshihiko.controller.CameraManagerController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Empty window containing a camera manager pane
 *
 * @author Stefanie Wiegand
 */
public class CameraManagerPresenter extends APresenter {

	private static final Logger logger = LoggerFactory.getLogger(CameraManagerPresenter.class);
	private CameraManagerPane cmp;

	@FXML
	private AnchorPane mainPane;

	/**
	 * Adds a simple menu to the camera manager presenter
	 */
	public void addMenu() {
		QuickMenu quickMenu = new QuickMenu(controller);
		AnchorPane.setTopAnchor(quickMenu, 10.0);
		AnchorPane.setRightAnchor(quickMenu, 0.0);
		AnchorPane.setLeftAnchor(quickMenu, 0.0);
		mainPane.getChildren().add(quickMenu);
	}

	/**
	 * Add a camera manager pane to the presenter using the existing controller
	 */
	public void addCameraManagerPane() {

		cmp = new CameraManagerPane(controller);
		//hide media buttons because there are no photos by default
		cmp.setMediaButtonsVisibility(false);

		//anchor new pane to parent container
		AnchorPane.setTopAnchor(cmp, 40.0);
		AnchorPane.setRightAnchor(cmp, 0.0);
		AnchorPane.setBottomAnchor(cmp, 0.0);
		AnchorPane.setLeftAnchor(cmp, 0.0);
		//actually attach it
		mainPane.getChildren().add(cmp);
	}

	@Override
	public void refresh() {
		cmp.refresh();
	}

	// Getters/Setters ////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get the camera manager pane
	 *
	 * @return the camera manager pane
	 */
	public CameraManagerPane getCameraManagerPane() {
		return cmp;
	}

	@Override
	public CameraManagerController getController() {
		return (CameraManagerController) controller;
	}

	public AnchorPane getMainPane() {
		return mainPane;
	}

}
