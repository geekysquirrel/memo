/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko;

/**
 * Main class for running CameraManager as a stand-alone application
 *
 * @author Stefanie Wiegand
 */
public class Main {

	private Main() {}

	/**
	 * Do it!
	 *
	 * @param args the arguments - not currently used
	 */
	public static void main(String[] args) {
		new Thread(new CameraManager()).start();
	}
}
