/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko;

import javafx.stage.Stage;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.AApplication;
import net.binarywood.memo.yoshihiko.controller.CameraManagerController;
import net.binarywood.memo.yoshihiko.presenter.CameraManagerPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Camera manager app
 *
 * @author Stefanie Wiegand
 */
public class CameraManager extends AApplication {

	private static final Logger logger = LoggerFactory.getLogger(CameraManager.class);
	//deliberately override so that the getter returns an object of the correct type
	private CameraManagerController controller;

	/**
	 * Create a new instance of the Yohihiko application
	 */
	public CameraManager() {
		super();
		init("fxml/CameraManagerScene.fxml", MemoOptions.ICON_CAMERA);
		//TODO: load from file, db or pic directory? where to save to?
	}

	@Override
	public void start(Stage stage) throws Exception {

		//make a new controller for this application
		controller = new CameraManagerController();
		super.controller = controller;

		//start the application
		super.start(stage);
		controller.showStage(this);

		//add menu buttons
		controller.getPresenter().addMenu();

		//add a new camera manager pane
		controller.getPresenter().addCameraManagerPane();

		//apply the previously selected theme
		controller.applyTheme();
	}

	@Override
	public void run() {
		CameraManager.launch();
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public CameraManagerController getController() {
		return controller;
	}

	/**
	 * Get the presenter for this application
	 *
	 * @return the presenter, cast to the correct class
	 */
	public CameraManagerPresenter getPresenter() {
		return controller.getPresenter();
	}
}
