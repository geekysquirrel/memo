/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.components;

import java.util.HashSet;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.spec.AComponent;
import net.binarywood.memo.common.spec.AController;
import net.binarywood.memo.yoshihiko.controller.CameraController;
import net.binarywood.memo.yoshihiko.events.MergeCamerasEvent;
import net.binarywood.memo.yoshihiko.events.ShowMediaEvent;
import net.binarywood.memo.yoshihiko.presenter.CameraPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is a tile, representing a single camera which can be selected to merge with other cameras.
 *
 * @author Stefanie Wiegand
 */
public class CameraTile extends AComponent implements Comparable<CameraTile> {

	private static final Logger logger = LoggerFactory.getLogger(CameraTile.class);

	private static final String CSS_CLASS_SELECTED = "selected";

	private final Camera camera;
	private boolean selected;

	@FXML
	private Label makeLabel;
	@FXML
	private Label modelLabel;
	@FXML
	private Label snLabel;
	@FXML
	private Button editButton;
	@FXML
    private Button mediaButton;
	@FXML
	private HBox mainHbox;
	@FXML
	private Label cameraIdLabel;

	/**
	 * Create a new camera tile
	 *
	 * @param c the camera that it represents
	 * @param parentController the controller of the parent UI element
	 */
	public CameraTile(Camera c, AController parentController) {
		super(parentController);
		mainContainer.setId(mainContainer.getId() + "_" + c.getIdentifier());
		camera = c;
		controller = new CameraController();
		controller.setParentController(parentController);

		//unselected by default
		selected = false;

		//set labels
		cameraIdLabel.setText(camera.getIdentifier());
		makeLabel.setText(camera.getMake());
		modelLabel.setText(camera.getModel());
		snLabel.setText(camera.getSerialNumber());
	}

	@FXML
	private void editCamera(ActionEvent event) {

		//start a new camera scene; set up controller/presenter for it
		CameraController cc = new CameraController();
		CameraPresenter p = (CameraPresenter) controller.showDialog(
			"fxml/CameraScene.fxml", "Edit camera " + camera.getIdentifier(), cc, controller.getParentController()
		);
		cc.setPresenter(p);
		p.setController(cc);
		p.setCamera(camera);
		event.consume();
	}

	/**
	 * Toggle the visibility of the show media button
	 *
	 * @param visible true for visible, false for invisible
	 */
	public void toggleMediaButton(boolean visible) {
		mediaButton.setVisible(visible);
		mediaButton.setManaged(visible);
	}

	@FXML
    void showMedia(ActionEvent event) {

		//send event to open popup with media recorded with this camera
		if (mainContainer.getParent()!=null) {
			logger.debug("Sending show media event to {}", mainContainer.getParent());
			fireEvent(new ShowMediaEvent(this, mainContainer.getParent(), camera));
		}
		event.consume();
    }

	@FXML
	private void selectCamera(MouseEvent event) {

		//show context menu on right click
		if (event.getButton().equals(MouseButton.SECONDARY)) {
			if (selected) {
				//build merge context menu
				MenuItem merge = new MenuItem("Merge cameras...");
				//if merging is clicked
				merge.setOnAction((ActionEvent e) -> {
					HashSet<String> toMerge = new HashSet<>();
					//check all cameras that are currently selected
					mainHbox.getParent().getParent().getChildrenUnmodifiable().stream().forEach(n -> {
						Node c = ((Pane) n).getChildren().get(0);
						//only consider selected cameras for merge action
						if (c.getStyleClass().contains(CSS_CLASS_SELECTED)) {
							//read id from hidden label
							String cameraID = ((Labeled) ((Pane) c).getChildren().get(1)).getText();
							toMerge.add(cameraID);
							c.getStyleClass().remove(CSS_CLASS_SELECTED);
						}
					});
					mergeCameras(e, toMerge);
				});
				ContextMenu cm = new ContextMenu(merge);
				cm.show(mainHbox, event.getScreenX(), event.getScreenY());
			}
		//for left click toggle selected (i.e. select/deselect)
		} else {
			if (selected) {
				mainHbox.getStyleClass().remove(CSS_CLASS_SELECTED);
				selected = false;
			} else {
				mainHbox.getStyleClass().add(CSS_CLASS_SELECTED);
				selected = true;
			}
		}
		event.consume();
	}

	/**
	 * Forward a merge cameras event to the parent controller that will take action and display the emrge dialog
	 *
	 * @param e the event that triggered this
	 * @param cameras the cameras to merge
	 */
	private void mergeCameras(ActionEvent e, HashSet<String> cameras) {

		//send event to parent controller
		if (mainContainer.getParent()!=null) {
			logger.debug("Sending merge event to {}", mainContainer.getParent());
			fireEvent(new MergeCamerasEvent(this, mainContainer.getParent(), null, null));
		}
		e.consume();
	}

	@Override
	public int compareTo(CameraTile ct) {
		return this.getCamera().getIdentifier().compareTo(ct.getCamera().getIdentifier());
	}

	@Override
	public boolean equals(Object o) {
		if (!o.getClass().equals(CameraTile.class)) {
			return false;
		} else {
			return this.getCamera().getIdentifier().compareTo(((CameraTile) o).getCamera().getIdentifier())==0;
		}
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 29 * hash + Objects.hashCode(this.camera);
		return hash;
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	public boolean isSelected() {
		return selected;
	}

	public Camera getCamera() {
		return camera;
	}
}
