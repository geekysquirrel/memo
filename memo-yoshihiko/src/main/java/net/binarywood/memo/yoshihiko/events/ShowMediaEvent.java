/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.events;

import javafx.event.ActionEvent;
import javafx.event.EventTarget;
import net.binarywood.memo.common.model.Camera;

/**
 * An event for displaying camera media.
 *
 * @author Stefanie Wiegand
 */
public class ShowMediaEvent extends ActionEvent {
	private static final long serialVersionUID = 1L;

	private final Camera camera;

	/**
	 * Create a new camera update event
	 *
	 * @param source the event source
	 * @param target the event target
	 * @param cam the old camera
	 */
	public ShowMediaEvent(Object source, EventTarget target, Camera cam) {
		super(source, target);
		this.camera = cam;
	}

	/**
	 * Get the new camera
	 *
	 * @return the new camera
	 */
	public Camera getCamera() {
		return camera;
	}

}
