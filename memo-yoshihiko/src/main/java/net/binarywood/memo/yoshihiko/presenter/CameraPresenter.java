/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.presenter;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import net.binarywood.memo.common.components.TimezoneHistoryEntry;
import net.binarywood.memo.common.events.DeleteTimezoneHistoryEntryEvent;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.model.OwnerHistory;
import net.binarywood.memo.common.model.TimezoneHistory;
import net.binarywood.memo.common.spec.APresenter;
import net.binarywood.memo.yoshihiko.components.DataEntry;
import net.binarywood.memo.yoshihiko.controller.CameraController;
import net.binarywood.memo.yoshihiko.controller.CameraManagerController;
import net.binarywood.memo.yoshihiko.events.MergeCamerasEvent;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class for the single camera window. The Controller will be a CameraController
 and the parent a CameraManagerController.
 *
 * @author Stefanie Wiegand
 */
public class CameraPresenter extends APresenter {

	private static final Logger logger = LoggerFactory.getLogger(CameraPresenter.class);

	private Camera camera;
	private Camera original;	//need to keep this to save modified camera to parent presenter

	@FXML
	private TextField serialNoTextfield;
	@FXML
	private Label makeLabel;
	@FXML
	private Label modelLabel;
	@FXML
	private Label idLabel;
	@FXML
	private Button reloadButton;
	@FXML
	private Button applyButton;
	@FXML
	private Button saveAndCloseButton;
	@FXML
	private Button cancelButton;
	@FXML
	private VBox settingsHistoryVBox;
	@FXML
	private VBox locationHistoryVBox;
	@FXML
	private VBox ownerHistoryVBox;
	@FXML
	private Accordion historyAccordion;
	@FXML
	private TitledPane settingsHistoryTitledPane;
	@FXML
	private TitledPane locationHistoryTitledPane;
	@FXML
	private TitledPane ownerHistoryTitledPane;
	@FXML
	private Label statusLabel;
	@FXML
	private ImageView settingsHistoryHelpImage;
	@FXML
	private ImageView locationHistoryHelpImage;
	@FXML
	private ImageView ownerHistoryHelpImage;

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		super.initialize(url, rb);

		// Handle TextField text changes.
		serialNoTextfield.textProperty().addListener((observable, oldValue, newValue) -> {
			if (camera!=null && newValue!=null) {
				camera.setSerialNumber(newValue.trim().replace(" ", "_").replace("/", "-"));
				camera.makeIdentifier();
				idLabel.setText(camera.getIdentifier());
				statusLabel.setText("");
			}
		});

		//set help image icon
		setImage(settingsHistoryHelpImage, MemoOptions.ICON_HELP);
		setImage(locationHistoryHelpImage, MemoOptions.ICON_HELP);
		setImage(ownerHistoryHelpImage, MemoOptions.ICON_HELP);

		//attach tooltips
		setTooltip(settingsHistoryHelpImage, "The date/time given here is considered to be in the timezone specified,"
			+ " i.e. local time.", null);
		setTooltip(locationHistoryHelpImage, "The date/time given here is considered to be in the timezone specified,"
			+ " i.e. local time.", null);
		setTooltip(ownerHistoryHelpImage, "The date/time given here is considered to be in UTC as owner changes don't"
			+ " keep a timezone reference.", null);

		historyAccordion.setExpandedPane(settingsHistoryTitledPane);

		//delete settings history entry
		settingsHistoryTitledPane.addEventHandler(ActionEvent.ANY, (ActionEvent event) -> {
			if (event.getClass().equals(DeleteTimezoneHistoryEntryEvent.class)) {
				//check if there is more than one entry left
				if (settingsHistoryVBox.getChildren().size()<2) {
					logger.debug("Last entry cannot be deleted!");
				} else {
					deleteTimezoneEntry(
						((DeleteTimezoneHistoryEntryEvent) event).getTimezoneHistoryEntry(), settingsHistoryVBox
					);
				}
			}
			event.consume();
		});

		//delete location history entry
		locationHistoryVBox.addEventHandler(ActionEvent.ANY, (ActionEvent event) -> {
			if (event.getClass().equals(DeleteTimezoneHistoryEntryEvent.class)) {
				//check if there is more than one entry left
				if (locationHistoryVBox.getChildren().size()<2) {
					logger.debug("Last entry cannot be deleted!");
				} else {
					deleteTimezoneEntry(
						((DeleteTimezoneHistoryEntryEvent) event).getTimezoneHistoryEntry(), locationHistoryVBox
					);
				}
			}
			event.consume();
		});

	}

	@FXML
	private void addTimezoneToHistory(ActionEvent event) {

		TimezoneHistoryEntry tze = new TimezoneHistoryEntry();
		if (locationHistoryVBox.getChildren().size()<=1) {
			tze.disableDeleteButton();
		}
		settingsHistoryVBox.getChildren().add(tze);
		if (settingsHistoryVBox.getChildren().size()>1) {
			settingsHistoryVBox.getChildren().stream().forEach(de ->
				((TimezoneHistoryEntry) de).enableDeleteButton()
			);
		}
		statusLabel.setText("");
		event.consume();
	}

	@FXML
	private void addLocationToHistory(ActionEvent event) {
		TimezoneHistoryEntry tze = new TimezoneHistoryEntry();
		if (locationHistoryVBox.getChildren().size()<=1) {
			tze.disableDeleteButton();
		}
		locationHistoryVBox.getChildren().add(tze);
		if (locationHistoryVBox.getChildren().size()>1) {
			locationHistoryVBox.getChildren().stream().forEach(de ->
				((TimezoneHistoryEntry) de).enableDeleteButton()
			);
		}
		statusLabel.setText("");
		event.consume();
	}

	@FXML
	private void addOwnerToHistory(ActionEvent event) {
		DataEntry den = new DataEntry(getController());
		if (ownerHistoryVBox.getChildren().size()<=1) {
			den.enableDeleteButton(false);
		}
		ownerHistoryVBox.getChildren().add(den);
		if (ownerHistoryVBox.getChildren().size()>1) {
			ownerHistoryVBox.getChildren().stream().forEach(de -> ((DataEntry) de).enableDeleteButton(true));
		}
		statusLabel.setText("");
		event.consume();
	}

	@FXML
	private void reload(ActionEvent event) {
		refresh();
		statusLabel.setText("Reloaded camera");
		event.consume();
	}

	@FXML
	private void save(ActionEvent event) {

		//timezone settings history
		TimezoneHistory settings = new TimezoneHistory();
		settingsHistoryVBox.getChildren().stream().forEach(sEntry ->
			settings.add(
				new DateTime(((TimezoneHistoryEntry) sEntry).getTimestamp()),
				DateTimeZone.forID(((TimezoneHistoryEntry) sEntry).getTimeZone())
			)
		);
		camera.setSettingsHistory(settings);

		//timeone location history
		TimezoneHistory locations = new TimezoneHistory();
		locationHistoryVBox.getChildren().stream().forEach(locEntry ->
			locations.add(
				new DateTime(((TimezoneHistoryEntry) locEntry).getTimestamp()),
				DateTimeZone.forID(((TimezoneHistoryEntry) locEntry).getTimeZone())
			)
		);
		camera.setLocationHistory(locations);

		//owner history
		OwnerHistory owners = new OwnerHistory();
		ownerHistoryVBox.getChildren().stream().forEach(oEntry ->
			owners.add(new DateTime(((DataEntry) oEntry).getTimestamp()), ((DataEntry) oEntry).getData())
		);
		camera.setOwnerHistory(owners);

		//remove "old" camera in case of id change and add this copy of the camera instead
		getController().getParentController().getCameras().remove(original.getIdentifier());
		//update media items!
		Set<Camera> oldCams = new HashSet<>();
		oldCams.add(camera);
		oldCams.add(original);
		getController().getParentController().getPresenter().getCameraManagerPane().fireEvent(
			new MergeCamerasEvent(
				getController().getParentController().getPresenter().getCameraManagerPane(),
				getController().getParentController().getPresenter().getCameraManagerPane().getParent(),
				camera, oldCams
			)
		);
		//add updated camera
		if (!((CameraManagerController) controller.getParentController()).getCameras().containsValue(camera)) {
			((CameraManagerController) controller.getParentController())
				.getCameras().put(camera.getIdentifier(), camera);
		}

		//refresh self
		refresh();
		//refresh camera manager
		controller.getParentController().getPresenter().refresh();

		statusLabel.setText("Saved camera configuration");
		event.consume();
	}

	@FXML
	private void saveAndClose(ActionEvent event) {
		save(event);
		controller.getStage().close();
	}

	@FXML
	private void discardAndClose(ActionEvent event) {
		controller.getStage().close();
		event.consume();
	}

	@Override
	public void refresh() {

		if (camera!=null) {
			makeLabel.setText(camera.getMake());
			modelLabel.setText(camera.getModel());
			serialNoTextfield.setText(camera.getSerialNumber());
			idLabel.setText(camera.getIdentifier());

			settingsHistoryVBox.getChildren().clear();
			camera.getSettingsHistory().entrySet().stream().forEach(entry ->
				settingsHistoryVBox.getChildren().add(
					new TimezoneHistoryEntry(entry.getKey(), (DateTimeZone)entry.getValue())
				)
			);

			locationHistoryVBox.getChildren().clear();
			camera.getLocationHistory().entrySet().stream().forEach(entry ->
				locationHistoryVBox.getChildren().add(
					new TimezoneHistoryEntry(entry.getKey(), (DateTimeZone)entry.getValue())
				)
			);

			ownerHistoryVBox.getChildren().clear();
			camera.getOwnerHistory().entrySet().stream().forEach(entry ->
				ownerHistoryVBox.getChildren().add(new DataEntry(getController(), entry.getKey(), entry.getValue()))
			);

			statusLabel.setText("");
		}
	}

	/**
	 * Delete a timezone entry from the UI
	 *
	 * @param te the entry to delete
	 * @param fromContainer the box to delete it from (the settings or the location box)
	 */
	public void deleteTimezoneEntry(TimezoneHistoryEntry te, VBox fromContainer) {
		fromContainer.getChildren().remove(te);
		if (fromContainer.getChildren().size() == 1) {
			TimezoneHistoryEntry t = (TimezoneHistoryEntry) fromContainer.getChildren().get(0);
			t.disableDeleteButton();
		}
		statusLabel.setText("Entry deleted");
	}

	/**
	 * Delete a owner entry from the UI
	 *
	 * @param de the entry to delete
	 */
	public void deleteOwnerEntry(DataEntry de) {
		ownerHistoryVBox.getChildren().remove(de);
		if (ownerHistoryVBox.getChildren().size() == 1) {
			DataEntry d = (DataEntry) ownerHistoryVBox.getChildren().get(0);
			d.enableDeleteButton(false);
		}
		statusLabel.setText("Entry deleted");
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////

	/**
	 * Get the currently displayed camera
	 *
	 * @return the camera
	 */
	public Camera getCamera() {
		return camera;
	}

	/**
	 * Set the currently displayed camera
	 *
	 * @param camera the camera
	 */
	public void setCamera(Camera camera) {
		this.camera = camera;
		original = camera.copy();
		refresh();
	}

	public VBox getSettingsHistoryVBox() {
		return settingsHistoryVBox;
	}

	public VBox getLocationHistoryVBox() {
		return locationHistoryVBox;
	}

	public VBox getOwnerHistoryVBox() {
		return ownerHistoryVBox;
	}

	@Override
	public CameraController getController() {
		return (CameraController) controller;
	}

}
