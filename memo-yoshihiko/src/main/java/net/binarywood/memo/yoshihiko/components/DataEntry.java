/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.components;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import net.binarywood.memo.common.spec.AComponent;
import net.binarywood.memo.yoshihiko.controller.CameraController;
import net.binarywood.memo.yoshihiko.presenter.CameraPresenter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents an entry in a camera history.
 * This can be anything (free text), that is described in the data text field.
 *
 * @author Stefanie Wiegand
 */
public class DataEntry extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(DataEntry.class);
	private CameraPresenter presenter;

	@FXML
	private TextField data;
	@FXML
	private DatePicker date;
	@FXML
	private TextField time;
	@FXML
	private Button deleteButton;

	/**
	 * Create a new component for capturing/displaing a data entry
	 *
	 * @param controller the controller of the camera to which this entry belongs
	 */
	public DataEntry(CameraController controller) {
		super(controller);
	}

	/**
	 * Create a new component for capturing/displaing a data entry
	 *
	 * @param controller the controller of the camera to which this entry belongs
	 * @param dt the timestamp of the data entry
	 * @param data the data to display
	 */
	public DataEntry(CameraController controller, DateTime dt, Object data) {

		//init
		this(controller);
		DecimalFormat df = new DecimalFormat("00");

		//prepare UI elements
		this.data.setText(data.toString());
		date.setValue(LocalDate.of(dt.year().get(), Month.of(dt.monthOfYear().get()), dt.dayOfMonth().get()));
		time.setText(df.format(dt.hourOfDay().get()) + ":"
					+ df.format(dt.minuteOfHour().get()) + ":"
					+ df.format(dt.secondOfMinute().get()));
	}

	@FXML
	private void deleteDataEntry(ActionEvent event) {

		//TODO: use events
		String source = ((Node) event.getSource()).getParent().getParent().getParent().getParent().getParent()
				.getParent().getParent().getParent().getParent().getParent().getId();

		//if the source of the event is the owner history UI element
		if ("ownerHistoryTitledPane".equalsIgnoreCase(source)) {
			//check if there is more than one entry left
			if (presenter.getSettingsHistoryVBox().getChildren().size()<2) {
				logger.debug("Last entry cannot be deleted!");
			} else {
				presenter.deleteOwnerEntry(this);
			}
		//ignore
		} else {
			logger.debug("Invalid source for OwnerEntry delete action: {}", source);
		}

		event.consume();
	}

	/**
	 * Enable/disable the delete button
	 *
	 * @param enabled true for enabling, false for disabling
	 */
	public void enableDeleteButton(boolean enabled) {
		deleteButton.setDisable(!enabled);
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Create an (epoch) timestamp from the date/time input fields
	 *
	 * @return the UNIX timestamp
	 */
	public long getTimestamp() {
		LocalDate ld = date.getValue();
		String[] t = time.getText().split(":");
		long l = 0L;
		//date/time are UTC
		try {
			DateTime dt = new DateTime(ld.getYear(), ld.getMonthValue(), ld.getDayOfMonth(),
				Integer.valueOf(t[0]), Integer.valueOf(t[1]), Integer.valueOf(t[2]), DateTimeZone.UTC);
			l = dt.getMillis();
		} catch (NullPointerException e) {
			logger.debug("Invalid datetime given; using \"0\" epoch (01/01/1970 00:00:00 GMT)");
			//deliberately not printing/rethrowing debug-level exception
		}
		return l;
	}

	public String getData() {
		return data.getText();
	}

}
