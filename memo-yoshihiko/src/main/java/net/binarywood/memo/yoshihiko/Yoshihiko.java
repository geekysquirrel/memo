/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko;

import java.io.File;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MediaItem;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class provides static methods to manage cameras and do related tasks
 *
 * @author Stefanie Wiegand
 */
public class Yoshihiko {

	private static final Logger logger = LoggerFactory.getLogger(Yoshihiko.class);
	private static final String XML_TIMEZONE_CHANGE = "timezoneChange";
	private static final String XML_OWNER_CHANGE = "ownerChange";
	private static final String XML_DATE = "date";
	private static final String XML_CHANGED_INTO = "changedInto";
	private static final String XML_SERIAL_NO = "serialNo";

	//hide the public constructor
	private Yoshihiko() {}

	/**
	 * Import a camera history XML file into memory
	 *
	 * @param path the absolute path of the file
	 * @return the imported cameras
	 */
	public static SortedMap<String, Camera> importXML(String path) {
		return importXML(path, null);
	}

	/**
	 * Import a camera history XML file into memory operating a whitelist
	 *
	 * @param path the absolute path of the file
	 * @param whitelist the cameras to import from the xml file
	 * @return the imported cameras
	 */
	public static SortedMap<String, Camera> importXML(String path, Map<String, Camera> whitelist) {

		SortedMap<String, Camera> cameras = new TreeMap<>();

		try {
			logger.debug("Loading camera settings config file {}", path);
			XMLConfiguration config = new XMLConfiguration(path);

			NodeList files = config.getDocument().getElementsByTagName("camera");
			for (int i = 0; i < files.getLength(); i++) {
				Node file = files.item(i);
				String id = file.getAttributes().getNamedItem("id").getNodeValue();

				//create new camera
				Camera cam = new Camera();
				//set camera properties
				cam.setIdentifier(id);
				cam.setMake(file.getAttributes().getNamedItem("make").getNodeValue());
				cam.setModel(file.getAttributes().getNamedItem("model").getNodeValue());
				if (file.getAttributes().getNamedItem(XML_SERIAL_NO)!=null) {
					cam.setSerialNumber(file.getAttributes().getNamedItem(XML_SERIAL_NO).getNodeValue());
				} else {
					cam.setSerialNumber("");
				}

				//check if whitelisting is used
				NodeList tags = file.getChildNodes();
				if (whitelist==null || whitelist.containsKey(cam.getIdentifier())) {
					cameras.put(id, cam);
				}

				//find histories in DOM
				for (int j = 0; j < tags.getLength(); j++) {
					Node tag = tags.item(j);
					String tagName = tag.getNodeName();
					NodeList entries;

					switch (tagName) {

						case "settingsHistory":
							entries = tag.getChildNodes();
							for (int k = 0; k < entries.getLength(); k++) {
								Node entry = entries.item(k);
								//add all setting changes
								if (XML_TIMEZONE_CHANGE.equals(entry.getNodeName())) {
									String dateChanged = entry.getAttributes().getNamedItem(XML_DATE).getNodeValue();
									String changedInto = entry.getAttributes().getNamedItem(XML_CHANGED_INTO).getNodeValue();

									//parse datetime as UTC - will be displayed in the given timezone later
									DateTime datetime = MediaItem.noXSDUTCReadFormat.parseDateTime(dateChanged);
									datetime = datetime.withZoneRetainFields(DateTimeZone.UTC);
									cameras.get(id).addTimezoneSetting(datetime, DateTimeZone.forID(changedInto));
								}
							}
							break;

						case "locationHistory":
							entries = tag.getChildNodes();
							for (int k = 0; k < entries.getLength(); k++) {
								Node entry = entries.item(k);
								//add all location changes
								if (XML_TIMEZONE_CHANGE.equals(entry.getNodeName())) {
									String dateChanged = entry.getAttributes().getNamedItem(XML_DATE).getNodeValue();
									String changedInto = entry.getAttributes().getNamedItem(XML_CHANGED_INTO).getNodeValue();

									//parse datetime as UTC - will be displayed in the given timezone later
									DateTime datetime = MediaItem.noXSDUTCReadFormat.parseDateTime(dateChanged);
									datetime = datetime.withZoneRetainFields(DateTimeZone.UTC);
									cameras.get(id).addLocation(datetime, DateTimeZone.forID(changedInto));
								}
							}
							break;

						case "ownerHistory":
							entries = tag.getChildNodes();
							for (int k = 0; k < entries.getLength(); k++) {
								Node entry = entries.item(k);
								//add all location changes
								if (XML_OWNER_CHANGE.equals(entry.getNodeName())) {
									String dateChanged = entry.getAttributes().getNamedItem(XML_DATE).getNodeValue();
									String changedInto = entry.getAttributes().getNamedItem(XML_CHANGED_INTO).getNodeValue();

									//parse datetime as UTC
									DateTime datetime = MediaItem.noXSDUTCReadFormat.parseDateTime(dateChanged);
									datetime = datetime.withZoneRetainFields(DateTimeZone.UTC);
									cameras.get(id).addOwner(datetime, changedInto);
								}
							}
							break;

						default:
							//ignore
					}
				}
			}
		} catch (ConfigurationException e) {
			logger.error("Error loading camera settings xml file {}", path, e);
			cameras = null;
		} catch (IllegalArgumentException e) {
			logger.error("Could not finish loading xml file {}; invalid entries contained", path, e);
			cameras = null;
		}
		return cameras;
	}

	/**
	 * Export an xml file with the given cameras
	 *
	 * @param path where to export to
	 * @param cameras the cameras to include in the exported XML file
	 */
	public static void exportXML(String path, Map<String, Camera> cameras) {

		if (cameras.isEmpty()) {
			logger.warn("No cameras to export, won't save empty file {}", path);
			return;
		}

		DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder icBuilder;
		try {
			icBuilder = icFactory.newDocumentBuilder();
			Document doc = icBuilder.newDocument();
			Element rootElement = doc.createElement("cameras");
			doc.appendChild(rootElement);

			for (Camera c : cameras.values()) {
				Element cam = doc.createElement("camera");
				cam.setAttribute("id", c.getIdentifier());
				cam.setAttribute("make", c.getMake());
				cam.setAttribute("model", c.getModel());
				cam.setAttribute(XML_SERIAL_NO, c.getSerialNumber());

				//TODO: aliases -> also add to data model
				Element als = doc.createElement("aliases");
				cam.appendChild(als);

				Element sh = doc.createElement("settingsHistory");
				for (Map.Entry<DateTime, Object> e: c.getSettingsHistory().entrySet()) {
					Element tzc = doc.createElement(XML_TIMEZONE_CHANGE);
					tzc.setAttribute(XML_DATE, e.getKey().toString(MediaItem.noXSDUTCReadFormat));
					tzc.setAttribute(XML_CHANGED_INTO, e.getValue().toString());
					sh.appendChild(tzc);
				}
				cam.appendChild(sh);

				Element lh = doc.createElement("locationHistory");
				for (Map.Entry<DateTime, Object> e: c.getLocationHistory().entrySet()) {
					Element tzc = doc.createElement(XML_TIMEZONE_CHANGE);
					tzc.setAttribute(XML_DATE, e.getKey().toString(MediaItem.noXSDUTCReadFormat));
					tzc.setAttribute(XML_CHANGED_INTO, e.getValue().toString());
					lh.appendChild(tzc);
				}
				cam.appendChild(lh);

				Element oh = doc.createElement("ownerHistory");
				for (Map.Entry<DateTime, Object> e: c.getOwnerHistory().entrySet()) {
					Element oc = doc.createElement(XML_OWNER_CHANGE);
					oc.setAttribute(XML_DATE, e.getKey().toString(MediaItem.noXSDUTCReadFormat));
					oc.setAttribute(XML_CHANGED_INTO, e.getValue().toString());
					oh.appendChild(oc);
				}
				cam.appendChild(oh);

				rootElement.appendChild(cam);
			}

			// output DOM XML to console
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			DOMSource source = new DOMSource(doc);
			StreamResult output = new StreamResult(new File(path));
			transformer.transform(source, output);
			logger.info("Saved camera config file to {}", path);

		} catch (ParserConfigurationException | DOMException | TransformerFactoryConfigurationError
				| IllegalArgumentException | TransformerException e) {
			logger.error("Could not write import location config file to {}", path, e);
		}
	}

}
