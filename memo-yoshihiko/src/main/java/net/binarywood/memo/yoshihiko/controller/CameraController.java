/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.controller;

import javafx.stage.Stage;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.spec.AController;
import net.binarywood.memo.yoshihiko.presenter.CameraPresenter;

/**
 * This class edits one single camera.
 *
 * @author Stefanie Wiegand
 */
public class CameraController extends AController {

	private Camera camera;

	/**
	 * Create a new camera controller instance
	 */
	public CameraController() {
		super();
	}

	/**
	 * Create a new camera controller for a given stage
	 *
	 * @param stage the stage
	 */
	public CameraController(Stage stage) {
		super(stage);
	}

	/**
	 * Load a camera into the controller
	 *
	 * @param camera the camera
	 */
	public void loadCamera(Camera camera) {
		this.camera = camera;
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////

	@Override
	public CameraPresenter getPresenter() {
		return (CameraPresenter) presenter;
	}

	/**
	 * Get the currently managed camera
	 *
	 * @return the camera
	 */
	public Camera getCamera() {
		return camera;
	}

	@Override
	public CameraManagerController getParentController() {
		return (CameraManagerController) parentController;
	}

}
