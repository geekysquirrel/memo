/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.events;

import javafx.event.ActionEvent;
import javafx.event.EventTarget;
import net.binarywood.memo.common.model.Camera;

/**
 * An event for updating a camera
 *
 * @author Stefanie Wiegand
 */
public class UpdateCameraEvent extends ActionEvent {
	private static final long serialVersionUID = 1L;

	private final Camera newCamera;
	private final Camera oldCamera;

	/**
	 * Create a new camera update event
	 *
	 * @param source the event source
	 * @param target the event target
	 * @param oldCam the old camera
	 * @param newCam the new, updated camera
	 */
	public UpdateCameraEvent(Object source, EventTarget target, Camera oldCam, Camera newCam) {
		super(source, target);
		this.oldCamera = oldCam;
		this.newCamera = newCam;
	}

	/**
	 * Get the new camera
	 *
	 * @return the new camera
	 */
	public Camera getNewCamera() {
		return newCamera;
	}

	/**
	 * Get the old camera
	 *
	 * @return the old camera
	 */
	public Camera getOldCamera() {
		return oldCamera;
	}

}
