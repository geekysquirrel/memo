/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.yoshihiko.controller;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import javafx.scene.control.Alert;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.spec.AController;
import net.binarywood.memo.yoshihiko.Yoshihiko;
import net.binarywood.memo.yoshihiko.presenter.CameraManagerPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides an overview of cameras. It can load information from various sources,
 * allows in-memory manipulation and writes back to various sources.
 *
 * @author Stefanie Wiegand
 */
public class CameraManagerController extends AController {

	private static final Logger logger = LoggerFactory.getLogger(CameraManagerController.class);

	private SortedMap<String, Camera> cameras;

	/**
	 * Create a new controller for the camera manager
	 */
	public CameraManagerController() {
		super();
		cameras = new TreeMap<>();
	}

	/**
	 * Remove all cameras from the manager window
	 */
	public void clearCameras() {
		cameras.clear();
	}

	/**
	 * Import a camera history XML file into memory
	 *
	 * @param path the absolute path of the file
	 */
	public void importXML(String path) {
		importXML(path, null);
	}

	/**
	 * Import a camera history XML file into memory operating a whitelist
	 *
	 * @param path the absolute path of the file
	 * @param whitelist the cameras to import from the xml file
	 */
	public void importXML(String path, Map<String, Camera> whitelist) {

		if (cameras==null) {
			Alert a = createAlert(Alert.AlertType.ERROR, "Loading XML file failed",
				"Could not finish loading the XML file provided", "There were invalid entries contained.");
			a.showAndWait();
		} else {
			SortedMap<String, Camera> cams = Yoshihiko.importXML(path, whitelist);
			cameras.putAll(cams!=null?cams:(new TreeMap<>()));
		}
	}

	/**
	 * Export an xml file containing the cameras' settings histories
	 *
	 * @param path where to export to
	 */
	public void exportXML(String path) {
		Yoshihiko.exportXML(path, cameras);
	}

	/**
	 * Load cameras from database
	 */
	public void loadfromKB() {
		//TODO: implement
		logger.error("This has not yet been implemented");
	}

	/**
	 * Save cameras to database
	 */
	public void savetoKB() {
		//TODO: implement
		logger.error("This has not yet been implemented");
	}

	/**
	 * Clear (i.e. empty) database
	 */
	public void clearKB() {
		//TODO: implement
		logger.error("This has not yet been implemented");
	}

	/**
	 * Export cameras from DB to XML
	 *
	 * @param path the path at which the XML file is to be saved
	 */
	public void exportKB(String path) {
		//TODO: implement
		logger.error("This has not yet been implemented");
	}

	/**
	 * Add a camera to the manager
	 *
	 * @param camera the new camera
	 */
	public void addCamera(Camera camera) {
		cameras.put(camera.getIdentifier(), camera);
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////

	@Override
	public CameraManagerPresenter getPresenter() {
		return (CameraManagerPresenter) presenter;
	}

	/**
	 * Get all currently managed cameras
	 *
	 * @return a map of the cameras, ordereb by ID
	 */
	public Map<String, Camera> getCameras() {
		return cameras;
	}

	/**
	 * Set the list of managed cameras, overriding the old list without checking.
	 *
	 * @param cameras the new cameras
	 */
	public void setCameras(SortedMap<String, Camera> cameras) {
		this.cameras = cameras;
	}

}
