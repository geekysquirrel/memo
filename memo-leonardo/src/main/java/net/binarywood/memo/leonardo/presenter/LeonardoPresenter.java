/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.leonardo.presenter;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.StringConverter;
import net.binarywood.memo.adrian.PhotoNormaliser;
import net.binarywood.memo.common.JSLogger;
import net.binarywood.memo.common.components.AMediaItemComponent;
import net.binarywood.memo.common.components.MediaItemLine;
import net.binarywood.memo.common.components.MediaItemTile;
import net.binarywood.memo.common.components.ThemeButton;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.model.ShortFile;
import net.binarywood.memo.common.spec.APresenter;
import net.binarywood.memo.leonardo.controller.LeonardoController;
import net.binarywood.memo.leonardo.controller.LeonardoSettingsController;
import net.binarywood.memo.leonardo.controller.MediaDirectoryTreeItem;
import net.binarywood.memo.yoshihiko.CameraManager;
import netscape.javascript.JSObject;
import org.controlsfx.control.RangeSlider;
import org.controlsfx.control.SegmentedButton;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Stefanie Wiegand
 */
public class LeonardoPresenter extends APresenter {

	private final Logger logger = LoggerFactory.getLogger(LeonardoPresenter.class);

	private Pane mediaItemContainer;
	private Map<String, SyncStatus> syncStatus;

	//query section
    @FXML private ListView<String> camerasListView;
	@FXML private ListView<String> tagsListView;
	@FXML private VBox timeRangeVBox;
	@FXML private Label rangeLabelFrom;
	@FXML private Label rangeLabelTo;
	private RangeSlider rangeSlider;
	@FXML private TextArea queryTextArea;
	@FXML private Button executeQueryButton;
	//tab section (left)
	@FXML private TabPane tabPane;
	@FXML private Tab fileTreeTab;
	@FXML private Tab tagTreeTab;
	@FXML private Button toggleSelectionButton;
	@FXML private ImageView toggleSelectionImage;
	@FXML private AnchorPane selectionPanel;
	@FXML private TreeView<File> fileTreeView;
	private ContextMenu syncMenu;
	@FXML private TreeItem<File> currentlySelectedDirectory;
	@FXML private TreeView<String> tagTreeView;
	// filter section (right)
	@FXML private Button toggleFilterButton;
	@FXML private ImageView toggleFilterImage;
	@FXML private AnchorPane filterPanel;
	@FXML private VBox filterVBox;
	@FXML private CheckBox normalisedCheckbox;
	@FXML private CheckBox geotaggedCheckbox;
	@FXML private CheckBox stereoCheckbox;
	@FXML private CheckBox videoCheckbox;
	@FXML private CheckBox taggedCheckbox;
	private SegmentedButton segmentedButton;
	// main section
	@FXML private SplitPane splitPane;
	private Map<Integer, Float> dividerPositions;
	@FXML private ScrollPane mediaItemScrollPane;
	// toggle buttons
	@FXML private ToggleGroup viewToggleGroup;
	@FXML private ToggleButton listViewToggleButton;
	@FXML private ToggleButton mapViewToggleButton;
	@FXML private ToggleButton gridViewToggleButton;
	//status bar (bottom)
	@FXML private ToolBar toolBar;
	@FXML private Label statusLabel;

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		super.initialize(url, rb);

		//set up query inputs
		camerasListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tagsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		queryTextArea.setText("SELECT ?item ?path WHERE {\n" +
		"	?item rdf:type :MediaItem .\n" +
		"	?item :pathOnDisk ?path .\n" +
		"	?item memo:hasAutomaticTag memo:shallowDepthOfField .\n" +
		"}");

		//split pane dividers
		dividerPositions = new HashMap<>();
		dividerPositions.put(0, 0.2f);
		dividerPositions.put(1, 0.8f);
		splitPane.getDividers().get(0).positionProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			float rounded = Math.round(newValue.doubleValue()*100)/100.0f;
			if (rounded>0.0f && rounded<1.0f && rounded!=dividerPositions.get(0)) {
				//logger.debug("New rounded double value: {}, previously {}", rounded, dividerPositions.get(0));
				dividerPositions.put(0, rounded);
			}
		});
		splitPane.getDividers().get(1).positionProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			float rounded = Math.round(newValue.doubleValue()*100)/100.0f;
			if (rounded>0.0f && rounded<1.0f && rounded!=dividerPositions.get(1)) {
				//logger.debug("New rounded double value: {}, previously {}", rounded, dividerPositions.get(1));
				dividerPositions.put(1, rounded);
			}
		});
	}

	/**
	 * Initialise this presenter, load information from the store for display purposes and set up event handlers.
	 * This is separate from the init method as it requires the controller to be set up.
	 */
	public void setup() {

		getSyncStatus();
		buildFileTree();

		//set up synchronisation menu
		syncMenu = new ContextMenu();
		MenuItem syncThis = new MenuItem("Sync this");
		syncThis.setOnAction((ActionEvent event) -> {
			if (mediaItemContainer!=null) {
				mediaItemContainer.setDisable(true);
			}
			getController().addMediaItemsToStore(String.valueOf(syncMenu.getUserData()), false);
			if (mediaItemContainer!=null) {
				mediaItemContainer.setDisable(false);
			}
			event.consume();
		});
		MenuItem syncAll = new MenuItem("Sync this and all subs");
		syncAll.setOnAction((ActionEvent event) -> {
			getController().addMediaItemsToStore(String.valueOf(syncMenu.getUserData()), true);
			event.consume();
		});
		syncMenu.getItems().addAll(syncThis, syncAll);

		//add theme button
		ThemeButton tb = new ThemeButton();
		tb.setController(controller);
		toolBar.getItems().add(tb);

		//set up flash filter
		ToggleButton b1 = new ToggleButton("flash");
		b1.setOnAction((ActionEvent event) -> {
			toggleFilters();
			event.consume();
		});
		ToggleButton b2 = new ToggleButton("no flash");
		b2.setOnAction((ActionEvent event) -> {
			toggleFilters();
			event.consume();
		});
		segmentedButton = new SegmentedButton(b1, b2);
		filterVBox.getChildren().add(segmentedButton);

		refreshQuerySection();

		//add event handler for resizing
		getController().getStage().getScene().heightProperty().addListener(
			(ObservableValue<? extends Number> obs, Number old, Number current) -> {
				logger.debug("Height changed: " + current);
				mediaItemContainer.setPrefHeight(mediaItemScrollPane.getHeight());
			}
		);
	}

	/**
	 * Get the status of all directories from the store
	 */
	private void getSyncStatus() {

		//TODO: extend for out-of sync (e.g. count photos to compare with existing dir)

		syncStatus = new HashMap<>();
		//get all parent directories from
		if (getController()!=null) {
			List<Map<String, String>> result = getController().getStore().querySelect(
				"SELECT DISTINCT ?parent WHERE { ?mi memo:parentDirectory ?parent }"
			);
			result.forEach(res -> syncStatus.put(res.get("parent"), SyncStatus.SYNCED));
		}
	}

	/**
	 * Set up the file tree view using the root directory specified in the settings/properties
	 */
	private void buildFileTree() {

		//set up file treeview
		MediaDirectoryTreeItem root;
		if (syncStatus.containsKey(MemoOptions.get("path.picDirectory"))) {
			root = new MediaDirectoryTreeItem(new ShortFile(MemoOptions.get("path.picDirectory")), syncStatus.get(MemoOptions.get("path.picDirectory")));
		} else {
			root = new MediaDirectoryTreeItem(new ShortFile(MemoOptions.get("path.picDirectory")), SyncStatus.NOTSYNCED);
		}
		fileTreeView.setRoot(root);
		fileTreeView.setShowRoot(true);
		root.getChildren(syncStatus);
	}

	// Actions ////////////////////////////////////////////////////////////////////////////////////

	@FXML
	private void quit(ActionEvent event) {

		getController().getStore().disconnect();
		getController().getStage().close();
		event.consume();
	}

	@FXML
	private void selectDirectoryForDisplay(MouseEvent event) {

		//left mouse
		if (event.getButton().equals(MouseButton.PRIMARY)) {
			TreeItem<File> selectedItem = fileTreeView.getSelectionModel().getSelectedItem();
			if (selectedItem!=null && selectedItem!=currentlySelectedDirectory) {
				currentlySelectedDirectory = selectedItem;
				String fullpath = currentlySelectedDirectory.getValue().getAbsolutePath();
				logger.debug("Showing directory {}", fullpath);

				getController().loadMediaItems(fullpath, syncStatus.get(fullpath));
				statusLabel.setText("Showing " + getController().getMediaItems().size() + " media items");
				showSelectedView();
			}
			syncMenu.setUserData(null);
			syncMenu.hide();
		//right click: show context menu
		} else if (event.getButton().equals(MouseButton.SECONDARY)) {
			MediaDirectoryTreeItem item = (MediaDirectoryTreeItem) fileTreeView.getSelectionModel().getSelectedItem();
			logger.info("Context menu requested for {}", item.getValue().getAbsolutePath());
			syncMenu.setUserData(item.getValue().getAbsolutePath());
			syncMenu.show(fileTreeView, event.getScreenX(), event.getScreenY());
		}
		event.consume();
	}

	@FXML
	private void executeComfyQuery(ActionEvent event) {

		clearPhotos();
		getController().getMediaItems().clear();

		StringBuilder b = new StringBuilder("SELECT ?item ?path WHERE {\n" +
		"	?item rdf:type :MediaItem .\n" +
		"	?item memo:hasIFD ?ifd .\n" +
		"	?item :pathOnDisk ?path .\n");

		//filter by camera
		ObservableList<String> cams = camerasListView.getSelectionModel().getSelectedItems();
		if (!cams.isEmpty()) {
			b.append("	?item memo:shotWithCamera ?cam .\n\tFILTER(?cam IN ( ");
			for (String cam: cams) {
				b.append("<" + MemoOptions.get("store.graphURI") + cam + ">, ");
			}
			b.delete(b.length()-2, b.length());
			b.append(")).\n");
		}

		//filter by tag
		ObservableList<String> tags = tagsListView.getSelectionModel().getSelectedItems();
		if (!tags.isEmpty()) {
			b.append("	?item ?tagProp ?tag .\n\t?tagProp rdfs:subPropertyOf* memo:hasTag .\n\tFILTER(?tag IN ( ");
			for (String tag: tags) {
				b.append("<" + MemoOptions.get("store.memoURI") + tag + ">, ");
			}
			b.delete(b.length()-2, b.length());
			b.append(")).\n");
		}


		//date slider (only if used)
		if (rangeSlider.getLowValue()>rangeSlider.getMin() || rangeSlider.getHighValue()<rangeSlider.getMax()) {
			//TODO: distinguish between UTC and local time?
			String low = MediaItem.noXSDReadFormat.print((long) rangeSlider.getLowValue()*1000);
			String high = MediaItem.noXSDReadFormat.print((long) rangeSlider.getHighValue()*1000);
			b.append("	?item :hasDateTime ?dateTime .\n");
			b.append("	?dateTime :localDateTime ?local .\n");
			b.append("	FILTER (?local >= \"" + low + "\"^^xsd:dateTime) .\n");
			b.append("	FILTER (?local <= \"" + high + "\"^^xsd:dateTime) .\n");
		}

		//finalise
		b.append("}");
		String query = b.toString();
		logger.debug(query);

		//execute
		if (!query.isEmpty()) {
			List<Map<String, String>> result = getController().getStore().querySelect(query);
			setStatus("Found " + result.size() + " media item" + ((result.size()==1)?"":"s"));
			result.stream().filter(mediaItem -> mediaItem.containsKey("path")).forEachOrdered(mediaItem -> {
				try {
					getController().getMediaItems().put(mediaItem.get("path"), new MediaItem(mediaItem.get("path"), false, true, false));
				} catch (FileNotFoundException ex) {
					logger.error("Could not load media item {}", mediaItem.get("path"), ex);
				}
			});
			if (getController().getMediaItems().isEmpty()) {
				logger.info("No media items found :(");
			}
			showSelectedView();
		}
		event.consume();
	}

	@FXML
	private void executeQuery(ActionEvent event) {

		clearPhotos();
		getController().getMediaItems().clear();

		String query = queryTextArea.getText().trim();
		if (query.trim().startsWith("SELECT")) {
			List<Map<String, String>> result = getController().getStore().querySelect(queryTextArea.getText());
			setStatus("Found " + result.size() + " media item" + ((result.size()==1)?"":"s"));
			for (Map<String, String> mediaItem: result) {
				if (mediaItem.containsKey("path")) {
					try {
						getController().getMediaItems().put(mediaItem.get("path"), new MediaItem(mediaItem.get("path"), false, true, false));
					} catch (FileNotFoundException ex) {
						logger.error("Could not load media item {}", mediaItem.get("path"), ex);
					}
				}
			}
			if (getController().getMediaItems().isEmpty()) {
				logger.info("No media items found :(");
			}
			showSelectedView();
		} else {
			logger.warn("Only SELECT SPARQL queries are supported");
		}
		event.consume();
	}

	@FXML
	private void refreshTree(Event event) {
		refreshTab();
		event.consume();
	}

	@FXML
	private void switchView(ActionEvent event) {

		showSelectedView();
		event.consume();
	}

	/**
	 * Refresh the tab in the left-hand side tab pane, that is active (no need to refresh inactive tabs
	 */
	public void refreshTab() {

		//update the selected view to reflect sync status
		if (tabPane.getSelectionModel().getSelectedItem().equals(fileTreeTab)) {
			logger.debug("Refreshing file view");
			getSyncStatus();
			buildFileTree();
		} else if (tabPane.getSelectionModel().getSelectedItem().equals(tagTreeTab)) {
			//TODO: implement
			logger.debug("Refreshing tag view");
		}
	}

	/**
	 * Redraw the filters for the comfy query section using the latest data from the store
	 */
	public void refreshQuerySection() {

		//get cameras
		List<Map<String, String>> cams = getController().getStore().querySelect(
			"SELECT * WHERE { ?cam a memo:Camera .\n"
			+ "?p memo:shotWithCamera ?cam .\n}", MemoOptions.get("store.graphURI"));
		ObservableList<String> camList =  FXCollections.observableArrayList();
		for (Map<String, String> cam: cams) {
			String camstring = cam.get("cam").replace(MemoOptions.get("store.graphURI"), "");
			if (!camList.contains(camstring)) {
				camList.add(camstring);
			}
		}
		camerasListView.setItems(camList.sorted());

		//get tags
		List<Map<String, String>> tags = getController().getStore().querySelect(
			"SELECT * WHERE {\n\t?tag a ?t .\n"
			+ "\t?t rdfs:subClassOf ?parentTag .\n"
			+ "\t?parentTag rdfs:subClassOf* memo:Tag .\n"
			+ "\t?p ?prop ?tag .\n"
			+ "\t?prop rdfs:subPropertyOf* memo:hasTag .\n}");
		ObservableList<String> tagList =  FXCollections.observableArrayList();
		for (Map<String, String> tag: tags) {
			String tagstring = tag.get("tag").replace(MemoOptions.get("store.memoURI"), "");
			if (!tagList.contains(tagstring)) {
				tagList.add(tagstring);
			}
		}
		tagsListView.setItems(tagList.sorted());
		tagTreeView.setRoot(new TreeItem<>("root"));
		tagTreeView.setShowRoot(false);
		tagList.sorted().forEach(tag -> tagTreeView.getRoot().getChildren().add(new TreeItem<>(tag)));

		//get time range
		List<Map<String, String>> result = getController().getStore().querySelect(
			"SELECT (MIN(?o) as ?min) (MAX(?o) as ?max) (MIN(?date) as ?minDate) (MAX(?date) as ?maxDate) WHERE {\n" +
			"	?p a memo:MediaItem .\n" +
			"	?p memo:hasDateTime ?dt.\n" +
			"	OPTIONAL {\n" +
			"		?dt ?ts ?o.\n" +
			"		FILTER (?ts IN (memo:localTimestamp, memo:utcTimestamp))\n" +
			"	}\n" +
			"	OPTIONAL {\n" +
			"		?dt ?t ?date .\n" +
			"		FILTER (?t IN (memo:localDateTime, memo:utcDateTime))\n" +
			"	}\n" +
			"}", MemoOptions.get("store.graphURI"));

		if (result!=null && !result.isEmpty()) {
			Map<String, String> res = result.iterator().next();

			//TODO: check the correct formatter is used here
			DateTime minU = MediaItem.noXSDReadFormat.parseDateTime(res.get("minDate"));
			DateTime maxU = DateTime.parse(res.get("maxDate"), MediaItem.noXSDReadFormat);
			DateTime min = new DateTime(Long.parseLong(res.get("min"))*1000);
			DateTime max = new DateTime(Long.parseLong(res.get("max"))*1000);

			long minD = (minU!=null?minU:min).getMillis()/1000;
			long maxD = (maxU!=null?maxU:max).getMillis()/1000;
			long diff = maxD-minD;

			//set up range slider
			rangeSlider = new RangeSlider(minD, maxD, minD, maxD);
			rangeSlider.setOrientation(Orientation.HORIZONTAL);
			rangeSlider.setShowTickMarks(true);
			rangeSlider.setShowTickLabels(true);
			rangeSlider.setBlockIncrement(1);
			rangeSlider.setLabelFormatter(new StringConverter<Number>() {
				private final DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy\nHH:mm:ss");
				@Override
				public String toString(Number object) {
					return (new DateTime(object.longValue()*1000)).toString(formatter);
				}
				@Override
				public Number fromString(String string) {
					return formatter.parseMillis(string)/1000;
				}
			});
			rangeSlider.setMajorTickUnit(diff/7);
			rangeSlider.setMinorTickCount(4);
			rangeSlider.setSnapToTicks(false);
			timeRangeVBox.getChildren().add(rangeSlider);
			timeRangeVBox.setVisible(true);
			timeRangeVBox.setManaged(true);
			rangeLabelFrom.setText(rangeSlider.getLabelFormatter().toString(minD).replace("\n", " "));
			rangeLabelTo.setText(rangeSlider.getLabelFormatter().toString(maxD).replace("\n", " "));

			//set slider values when changed
			rangeSlider.lowValueProperty().addListener((ObservableValue<? extends Number> obs, Number o, Number n) -> {
				rangeLabelFrom.setText(rangeSlider.getLabelFormatter().toString(n).replace("\n", " "));
			});
			rangeSlider.highValueProperty().addListener((ObservableValue<? extends Number> obs, Number o, Number n) -> {
				rangeLabelTo.setText(rangeSlider.getLabelFormatter().toString(n).replace("\n", " "));
			});

		} else {
			timeRangeVBox.setVisible(false);
			timeRangeVBox.setManaged(false);
		}
	}

	// Render UI //////////////////////////////////////////////////////////////////////////////////

	/**
	 * Show the view which has been selected by the user using the toggle buttons (may be none)
	 */
	private void showSelectedView() {

		logger.debug("Showing selected view...");
		controller.getCurrentScene().setCursor(Cursor.WAIT);
		Platform.runLater(() -> controller.getCurrentScene().setCursor(Cursor.WAIT));

		if (gridViewToggleButton!=null && gridViewToggleButton.isSelected()) {
			showGrid();
		} else if (listViewToggleButton!=null && listViewToggleButton.isSelected()) {
			showList();
		} else if (mapViewToggleButton!=null && mapViewToggleButton.isSelected()) {
			showMap();
		} else {
			mediaItemScrollPane.setContent(null);
		}
		Platform.runLater(() -> controller.getCurrentScene().setCursor(Cursor.DEFAULT));
		controller.getCurrentScene().setCursor(Cursor.DEFAULT);
		logger.debug("Finished showing selected view");
	}

	/**
	 * Render a grid for all photos in the selected directory
	 */
	public void showGrid() {

		logger.info("Showing grid");

		//create flow pane
		mediaItemContainer = new FlowPane();
		mediaItemContainer.setFocusTraversable(false);
		mediaItemContainer.setId("canvas");
		mediaItemContainer.setPadding(new Insets(20.0));
		mediaItemContainer.setPrefHeight(770);
		((FlowPane) mediaItemContainer).setRowValignment(VPos.CENTER);
		((FlowPane) mediaItemContainer).setColumnHalignment(HPos.LEFT);
		((FlowPane) mediaItemContainer).setPrefWrapLength(400.0);
		((FlowPane) mediaItemContainer).setAlignment(Pos.TOP_LEFT);
		((FlowPane) mediaItemContainer).setOrientation(Orientation.HORIZONTAL);
		((FlowPane) mediaItemContainer).setHgap(20.0);
		((FlowPane) mediaItemContainer).setVgap(20.0);

		mediaItemScrollPane.setContent(mediaItemContainer);
		mediaItemScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

		getController().getMediaItems().values().stream().forEach((MediaItem mediaItem) -> {
			MediaItemTile tile = new MediaItemTile(mediaItem, getController());
			if (tile.isLoaded()) {
				mediaItemContainer.getChildren().add(tile);
			} else {
				logger.error("Could not add tile for media item {}", mediaItem.getFilename());
			}
		});
	}

	/**
	 * Render a list for all photos in the selected directory
	 */
	public void showList() {

		logger.info("Showing list");
		mediaItemContainer = new VBox();
		mediaItemContainer.setFocusTraversable(false);
		mediaItemContainer.setId("canvas");
		mediaItemContainer.setPadding(new Insets(10.0));
		mediaItemContainer.setPrefHeight(770);
		((VBox) mediaItemContainer).setFillWidth(true);
		((VBox) mediaItemContainer).setAlignment(Pos.TOP_LEFT);
		((VBox) mediaItemContainer).setSpacing(5.0);

		mediaItemScrollPane.setContent(mediaItemContainer);
		mediaItemScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

		getController().getMediaItems().values().stream().forEach((MediaItem mediaItem) -> {
			//TODO: for each mediaItem verify checksum and flag if mediaItem is different
			MediaItemLine line = new MediaItemLine(mediaItem, getController());
			mediaItemContainer.getChildren().add(line);
			if (!line.isLoaded()) {
				logger.error("Could not add tile for media item {}", mediaItem.getFilename());
			}
		});
	}

	/**
	 * Show all photos in the selected directory on a map
	 */
	public void showMap() {

		//get (min/max) coordinates
		Double longMin = null;
		Double longMax = null;
		Double latMin = null;
		Double latMax = null;
		for (MediaItem mi: getController().getMediaItems().values()) {
			if (mi.isGeotagged()) {

				Double lon = mi.getLongitudeNum();
				if (lon!=null) {
					if (longMin==null || lon<longMin) {
						longMin = lon;
					}
					if (longMax==null || lon>longMax) {
						longMax = lon;
					}
				}

				Double lat = mi.getLatitudeNum();
				if (lat!=null) {
					if (latMin==null || lat<latMin) {
						latMin = lat;
					}
					if (latMax==null || lat>latMax) {
						latMax = lat;
					}
				}
			}
		}
		final Double laMin = latMin;
		final Double loMin = longMin;
		final Double laMax = latMax;
		final Double loMax = longMax;
		final Integer zoomFactor;
		final Double laCentre;
		final Double loCentre;

		//calculate centre of map
		if (laMin!=null && laMax!=null && loMin!=null && loMax!=null) {
			logger.info("Displaying coordinates {} to {} latitude and {} to {} longitude", latMin, latMax, longMin, longMax);
			laCentre = (latMin+latMax)/2;
			loCentre = (longMin+longMax)/2;
			logger.info("Centering map at {} {}", laCentre, loCentre);

			//calculate zoom level:
			//--use widest distance between coordinates in degrees
			Double laDist = latMax - latMin;
			laDist = laDist>=0?laDist:laDist*-1;
			Double loDist = longMax - longMin;
			loDist = loDist>=0?loDist:loDist*-1;
			Double maxDist = Math.max(laDist, loDist);
			//calculate what zoom level would still cover that distance
			Double degree = 360.0;
			int i = 0;
			while (degree>maxDist && i<10) {
				degree = degree/2;
				i++;
			}
			zoomFactor = i;
		} else {
			logger.info("No coordinates found, displaying empty map");
			zoomFactor = 2;
			laCentre = 0.0;
			loCentre = 0.0;
		}

		//create a new web view and load the map html page
		mediaItemContainer = new AnchorPane();
		WebView map = new WebView();
		final WebEngine engine = map.getEngine();

		engine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) ->
		{
			JSObject window = (JSObject) engine.executeScript("window");
			window.setMember("leonardo", getController());
			window.setMember("java", new JSLogger());
			engine.executeScript("console.log = function(message)\n"
					+ "{\n"
					+ "    java.log(message);\n"
					+ "};");
		});

		logger.debug("WebView version: {}", engine.getUserAgent());

		engine.setJavaScriptEnabled(true);
		engine.getLoadWorker().stateProperty().addListener((ObservableValue<? extends State> ov, State oldState, State newState) -> {
			if (newState == State.SUCCEEDED) {

				//engine.executeScript("init()");
				logger.debug("Map loaded!");

				//center map
				engine.executeScript("jumpTo(" + laCentre + "," + loCentre + "," + zoomFactor + ")");

				//add media items
				for (MediaItem mi: getController().getMediaItems().values()) {
					if (mi.isGeotagged()) {
						logger.debug("Adding marker for media item {}", mi.getFilename());
						logger.debug("{}", System.getProperty("user.dir") + File.separator + mi.getThumbnail());
						engine.executeScript("addMarker('" + mi.getPath() + "'," + mi.getLongitudeNum() + ","
								+ mi.getLatitudeNum() + ",'" + mi.getFilename() + "','"
								+ System.getProperty("user.dir") + File.separator + mi.getThumbnail()+ "')");
					} else {
						//TODO: add thingy to drag and drop on the map
					}
				}
				//TODO: count number of displayed media items and show in info bar
			}
		});
		engine.load(getClass().getClassLoader().getResource("map/map.html").toExternalForm());

		//make map as high as the scroll pane
		mediaItemContainer.setPrefHeight(mediaItemScrollPane.getHeight());
		mediaItemContainer.getChildren().add(map);
		AnchorPane.setTopAnchor(map, 0.0);
		AnchorPane.setRightAnchor(map, 0.0);
		AnchorPane.setBottomAnchor(map, 0.0);
		AnchorPane.setLeftAnchor(map, 0.0);

		//display without the scroll bar
		mediaItemScrollPane.setContent(mediaItemContainer);
		mediaItemScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
	}

	/**
	 * Apply the filters that are selected in the filter panel to the currently displayed media items
	 */
	@FXML
	private void toggleFilters() {

		//TODO: update statusbar

		if (mediaItemContainer!=null && !mediaItemContainer.getChildren().isEmpty()) {

			String toggleText;
			if (((Labeled) segmentedButton.getToggleGroup().getSelectedToggle())!=null) {
				toggleText = ((Labeled) segmentedButton.getToggleGroup().getSelectedToggle()).getText();
			} else {
				toggleText = "";
			}

			mediaItemContainer.getChildren().stream().map(n -> (AMediaItemComponent) n).forEach(tile -> {

				//checkboxes
				if ((tile.getMediaItem().isNormalised()|| !normalisedCheckbox.isSelected())
					&& (tile.getMediaItem().isGeotagged() || !geotaggedCheckbox.isSelected())
					&& (tile.getMediaItem().isStereo()|| !stereoCheckbox.isSelected())
					&& (tile.getMediaItem().isVideo()|| !videoCheckbox.isSelected())
					&& (tile.getMediaItem().getFlash()!=null && (
							(tile.getMediaItem().getFlash() && "flash".equals(toggleText))
							|| (!tile.getMediaItem().getFlash() && "no flash".equals(toggleText))
							|| ("".equals(toggleText))
						))
					//TODO: set isTagged flag
				) {
					tile.show();
				} else {
					tile.hide();
				}
			});
		}
	}

	@FXML
	private void toggleSelectionPanel(ActionEvent event) {

		selectionPanel.setManaged(!selectionPanel.isManaged());
		selectionPanel.setVisible(!selectionPanel.isVisible());
		setupSplitPane();
		event.consume();
	}

	@FXML
	private void toggleFilterPanel(ActionEvent event) {

		filterPanel.setManaged(!filterPanel.isManaged());
		filterPanel.setVisible(!filterPanel.isVisible());
		setupSplitPane();
		event.consume();
	}

	/**
	 * Resize the left and right panel by setting the divider positions in the split panel
	 */
	private void setupSplitPane() {

		if (selectionPanel.isVisible()) {
			toggleSelectionImage.setRotate(90.0);
			splitPane.setDividerPosition(0, dividerPositions.get(0));
		} else {
			toggleSelectionImage.setRotate(270.0);
			splitPane.setDividerPosition(0, 0.0);
		}

		if (filterPanel.isVisible()) {
			toggleFilterImage.setRotate(270.0);
			splitPane.setDividerPosition(1, dividerPositions.get(1));
		} else {
			toggleFilterImage.setRotate(90.0);
			splitPane.setDividerPosition(1, 1.0);
		}
	}

	@FXML
    void updateDividerPositions(DragEvent event) {
		logger.debug("{}", splitPane.getDividerPositions());
    }

	/**
	 * Remove all photos from the view
	 */
	public void clearPhotos() {

		if (mediaItemScrollPane!=null) {
			mediaItemScrollPane.setContent(null);
		}
	}

	// Dialogs ////////////////////////////////////////////////////////////////////////////////////

	@FXML
	private void showCamerasDialog(ActionEvent event) {

		//doesn't have a parent controller
		controller.showDialog(new CameraManager(), null);
		event.consume();
	}

	@FXML
	private void showPeopleDialog(ActionEvent event) {
		//TODO: implement
		controller.createAlert(Alert.AlertType.INFORMATION, "Under construction", "Coming soon...",
			"This feature has not yet been implemented. Check back later.").showAndWait();
		event.consume();
	}

	@FXML
	private void showNormaliserDialog(ActionEvent event) {

		//doesn't have a parent controller
		controller.showDialog(new PhotoNormaliser(), null);
		event.consume();
	}

	@FXML
	private void showStereocardsDialog(ActionEvent event) {
		//TODO: implement
		controller.createAlert(Alert.AlertType.INFORMATION, "Under construction", "Coming soon...",
			"This feature has not yet been implemented. Check back later.").showAndWait();
		event.consume();
	}

	@FXML
	private void showOptionsDialog(ActionEvent event) {

		APresenter presenter = controller.showDialog("fxml/LeonardoSettingsScene.fxml", "Leonardo settings", new LeonardoSettingsController(), controller);
		presenter.refresh();
		event.consume();
	}

	@FXML
	private void showAboutDialog(ActionEvent event) {

		controller.showDialog("fxml/AboutScene.fxml", "About MemO", controller, controller);
		event.consume();
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////

	@Override
	public LeonardoController getController() {
		return (LeonardoController) controller;
	}

	@Override
	public void setStatus(String status) {
		statusLabel.setText(status);
	}

	/**
	 * These are the status options for a directory.
	 *
	 * SYNCED:		The directory is synced with the store
	 * OUTOFSYNC:	The directory was synced previously but either the files or the store have changed since
	 * NOTSYNCED:	There is no record of this directory in the database
	 */
	public enum SyncStatus {
		SYNCED, OUTOFSYNC, NOTSYNCED;
	}
}
