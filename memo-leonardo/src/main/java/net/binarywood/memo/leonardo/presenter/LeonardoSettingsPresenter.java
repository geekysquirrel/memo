/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.leonardo.presenter;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.APresenter;
import net.binarywood.memo.leonardo.controller.LeonardoSettingsController;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Stefanie Wiegand
 */
public class LeonardoSettingsPresenter extends APresenter implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(LeonardoSettingsPresenter.class);

	@FXML private ChoiceBox<String> storeTypeChoiceBox;
	@FXML private TextArea storeStatsTextArea;
	@FXML private Button storeImportButton;
	@FXML private Button storeExportButton;
	@FXML private Button storeClearButton;
	@FXML private TextField pictureDirTextField;
	@FXML private Slider thumbnailHeightSlider;
	@FXML private Slider thumbnailWidthSlider;
	@FXML private Button saveButton;
	@FXML private Button saveAndCloseButton;
	@FXML private Button discardAndCloseButton;

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		pictureDirTextField.setText(MemoOptions.get("path.picDirectory"));

		thumbnailHeightSlider.setValue(Double.valueOf(MemoOptions.get("display.thumbnailHeight")));
		thumbnailWidthSlider.setValue(Double.valueOf(MemoOptions.get("display.thumbnailWidth")));
	}

	@Override
	public void refresh() {

		//refresh store info
		storeStatsTextArea.setText(getController().getParentController().getStoreInfo());
		//also refresh tab view in main app
		getController().getParentController().getPresenter().refreshTab();
	}

	@FXML
	private void importStore(ActionEvent event) {

		FileChooser fc = new FileChooser();
		fc.setTitle("Choose import file location");
		fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("RDF files", "RDF"));

		File file = fc.showOpenDialog(controller.getStage());
		if (file!=null) {
			String path = file.getAbsolutePath();
			//windows-specific fix - there seem to be problems with capital drive letters
			String location = path.replaceFirst(path.substring(0,1), path.substring(0,1).toLowerCase());
			getController().getParentController().getStore().importLocalDocument(
				location, MemoOptions.get("store.graphURI"), MemoOptions.get("store.graphURI"), RDFFormat.RDFXML
			);
			(controller.createAlert(Alert.AlertType.CONFIRMATION, "Imported store", "Your store was imported.",
				"The file " + file.getAbsolutePath() + " (" + file.length() + " bytes) was imported to the store")).showAndWait();
			refresh();
		} else {
			logger.error("Could not import store");
		}
		event.consume();
	}

	@FXML
	private void exportStore(ActionEvent event) {

		FileChooser fc = new FileChooser();
		fc.setTitle("Choose export file location");
		fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("RDF ontology files", "RDF"));

		File file = fc.showSaveDialog(controller.getStage());
		if (file!=null) {
			String path = file.getAbsolutePath();
			//windows-specific fix - there seem to be problems with capital drive letters
			String location = path.replaceFirst(path.substring(0,1), path.substring(0,1).toLowerCase());
			getController().getParentController().exportStore(location);
			(controller.createAlert(Alert.AlertType.CONFIRMATION, "Exported store", "Your store was exported.",
				"The file (" + file.length() + " bytes) was saved to " + file.getAbsolutePath())).showAndWait();
			refresh();
		} else {
			logger.error("Could not export store");
		}
		event.consume();
	}

	@FXML
	private void clearStore(ActionEvent event) {

		getController().getParentController().clearStore();
		refresh();
		event.consume();
	}

	@FXML
	private void save(ActionEvent event) {

		MemoOptions.set("path.picDirectory", pictureDirTextField.getText());
		MemoOptions.set("display.thumbnailHeight", String.valueOf(thumbnailHeightSlider.getValue()));
		MemoOptions.set("display.thumbnailWidth", String.valueOf(thumbnailWidthSlider.getValue()));
		MemoOptions.save();
		refresh();
		event.consume();
	}

	@FXML
	private void saveAndClose(ActionEvent event) {

		save(event);
		controller.getStage().close();
	}

	@FXML
	private void discardAndClose(ActionEvent event) {

		controller.getStage().close();
		event.consume();
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////
	
	@Override
	public LeonardoSettingsController getController() {
		return (LeonardoSettingsController) controller;
	}
}
