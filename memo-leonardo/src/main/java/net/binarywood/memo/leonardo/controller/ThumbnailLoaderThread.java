/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.leonardo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sw on 22/01/14.
 */
public class ThumbnailLoaderThread implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(getClass());

//	private ThumbnailPanel thumbnailPanel;
//
//	public ThumbnailLoaderThread(ThumbnailPanel thumbnailPanel) {
//		this.thumbnailPanel = thumbnailPanel;
//	}

	@Override
	public void run() {

//		ImageIcon thumbnailIcon = null;
//
//		String thumbnailPath = PicManOptions.getProperty("path.thumbnailDirectory")
//				+ thumbnailPanel.getPhoto().getPath();
//		File thumbnail = new File(thumbnailPath);
//		if (thumbnail.exists()) {
//			thumbnailIcon = new ImageIcon(thumbnail.getPath());
//		} else {
//
//			try {
//
//				BufferedImage img = ImageIO.read(new File(thumbnailPanel.getPhoto().getPath()));
//				BufferedImage thumb =  Scalr.resize(img, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_HEIGHT,
//						Integer.valueOf(PicManOptions.getProperty("display.thumbnailHeight")));
//
//				File outputfile = new File(thumbnailPath);
//				outputfile.mkdirs();
//				outputfile.createNewFile();
//				ImageIO.write(thumb, "jpg", outputfile);
//
//				thumbnailIcon = new ImageIcon(thumb);
//
//			} catch (IOException | IllegalArgumentException | ImagingOpException e) {
//				logger.error("Error creating thumbnail for photo {}", thumbnailPanel.getPhoto().getPath(), e);
//			}
//		}
		//thumbnailPanel.setThumbnail(thumbnailIcon);
		//thumbnailPanel.add(new WebImage(thumbnailPanel.getThumbnail()), BorderLayout.CENTER);
	}
}
