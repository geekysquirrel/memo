/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.leonardo;

/**
 * Startup hook for Leonardo. Starts the application in a new thread.
 *
 * @author Stefanie Wiegand
 */
public class Main {

	private Main() {}

	/**
	 * Run Leonardo on a new thread
	 *
	 * @param args none
	 */
	public static void main(String[] args) {
		new Thread(new Leonardo()).start();
	}
}
