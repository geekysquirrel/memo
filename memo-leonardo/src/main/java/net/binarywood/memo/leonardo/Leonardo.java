/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.leonardo;

import javafx.stage.Stage;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.AApplication;
import net.binarywood.memo.leonardo.controller.LeonardoController;
import org.eclipse.rdf4j.repository.RepositoryLockedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents the MemO main application, displaying photos and providing the tools to modify, sort and
 * query them. It uses the other MemO modules to achieve this.
 *
 * @author Stefanie Wiegand
 */
public class Leonardo extends AApplication implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(Leonardo.class);

	/**
	 * Creates a new instance of the application
	 */
	public Leonardo() {
		super();
		init("fxml/LeonardoScene.fxml", MemoOptions.ICON_CAMERA);
	}

	@Override
	public void start(Stage stage) throws Exception {

		try {
			controller = new LeonardoController();
			super.controller = controller;
			super.start(stage);

			controller.showStage(this);
			getController().getPresenter().setup();
		} catch (RepositoryLockedException e) {
			logger.error("Another instance of MemO Leonardo is alreday running!", e);
		}
	}

	@Override
	public void run() {
		Leonardo.launch();
	}

	@Override
	public LeonardoController getController() {
		return (LeonardoController) controller;
	}
}
