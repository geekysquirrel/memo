/*
 * Copyright 2016 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.leonardo.controller;

import java.io.File;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import net.binarywood.memo.common.model.ShortFile;
import net.binarywood.memo.leonardo.presenter.LeonardoPresenter;
import org.apache.commons.io.filefilter.DirectoryFileFilter;

/**
 * Tree item for directories where the dir is a short file (e.g. only renders its name when printed) and hidden
 * directories are not displayed.
 */
public class MediaDirectoryTreeItem extends TreeItem<File> {

	/**
	 * Create tree item with icon to indicate its status
	 *
	 * @param value the directory it represents
	 * @param status the status of this directory
	 */
	public MediaDirectoryTreeItem(File value, LeonardoPresenter.SyncStatus status) {
		super(value);
		String icon;
		switch (status) {
			case SYNCED:
				icon = "synced";
				break;
			case OUTOFSYNC:
				icon = "outofsync";
				break;
			default:
				icon = "notsynced";
		}
		Image image = new Image(
			"file:" + getClass().getClassLoader().getResource("icons/" + icon + ".png").getPath(),
			16.0, 16.0, true, true
		);
		setGraphic(new ImageView(image));
	}

	private boolean isLeaf;
	private boolean isFirstTimeChildren = true;
	private boolean isFirstTimeLeaf = true;

	/**
	 * Get all children of this tree item
	 *
	 * @param status the synchronisation status of all tree items
	 * @return a list of tree items representing the children
	 */
	public ObservableList<TreeItem<File>> getChildren(Map<String, LeonardoPresenter.SyncStatus> status) {
		if (isFirstTimeChildren) {
			isFirstTimeChildren = false;
			super.getChildren().setAll(buildChildren(this, status));
			this.setExpanded(true);
		}
		return super.getChildren();
	}

	@Override
	public boolean isLeaf() {

		if (isFirstTimeLeaf) {
			isFirstTimeLeaf = false;
			File f = getValue();
			isLeaf = true;
			//only directories with no subdirectories are leaves
			if (f.list(DirectoryFileFilter.DIRECTORY)!=null && f.list(DirectoryFileFilter.DIRECTORY).length>0) {
				isLeaf = false;
			}
		}
		return isLeaf;
	}

	/**
	 * Recursively build a tree of non-hidden directories, using theis sync status to visualize it in the GUI
	 *
	 * @param TreeItem the item for which to build a sub-tree
	 * @param syncStatus the sync status of all directories in the store
	 * @return the sub-tree
	 */
	private ObservableList<TreeItem<File>> buildChildren(TreeItem<File> TreeItem, Map<String, LeonardoPresenter.SyncStatus> syncStatus) {

		File f = TreeItem.getValue();
		if (f != null && f.isDirectory()) {
			File[] files = f.listFiles();
			if (files != null) {
				ObservableList<TreeItem<File>> children = FXCollections.observableArrayList();

				//list all non-hidden directories
				for (File childFile : files) {
					if (childFile.isDirectory() && !childFile.isHidden()) {
						ShortFile sf = new ShortFile(childFile.getAbsolutePath());
						LeonardoPresenter.SyncStatus status = LeonardoPresenter.SyncStatus.NOTSYNCED;
						if (syncStatus!=null && !syncStatus.isEmpty() && syncStatus.containsKey(childFile.getAbsolutePath())) {
							status = syncStatus.get(childFile.getAbsolutePath());
						}

						//check children of this node
						TreeItem<File> node = new MediaDirectoryTreeItem(sf, status);
						for (File childchild: childFile.listFiles()) {
							if (childchild.isDirectory() && !childchild.isHidden()) {
								//recurse
								node.getChildren().setAll(buildChildren(node, syncStatus));
							}
						}
						children.add(node);
					}
				}
				return children.sorted();
			}
		}
		return FXCollections.emptyObservableList();
	}
}
