/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.leonardo.controller;

import net.binarywood.memo.common.spec.AController;
import net.binarywood.memo.leonardo.presenter.LeonardoSettingsPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class controls the settings window
 *
 * @author Stefanie Cox
 */
public class LeonardoSettingsController extends AController {

	private static final Logger logger = LoggerFactory.getLogger(LeonardoSettingsController.class);

	/**
	 * Creates a new settings controller
	 */
	public LeonardoSettingsController() {
		super();
	}

	// Getters ////////////////////////////////////////////////////////////////////////////////////

	@Override
	public LeonardoSettingsPresenter getPresenter() {
		return (LeonardoSettingsPresenter) presenter;
	}

	@Override
	public LeonardoController getParentController() {
		return (LeonardoController) parentController;
	}
}
