/*
 * Copyright 2017 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

var map, view, popup, popupElement, markerStyle, markers, markerLayer, mapLayer;

/**
 * Initialise the map, defining all universally applicable variables and options.
 * This creates a new map, centered on 0/0.
 */
function init(){

	console.log("Initialising map...");

	// init custom icon
	markerStyle = new ol.style.Style({
		image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
			//center the icon and move it so that it actually points to the coords with its pointy thing
			anchor: [0.5, 36],
			anchorXUnits: 'fraction',
			anchorYUnits: 'pixels',
			src: 'pin.png',
			opacity: 0.9
		}))
	});

	//use OSM map source
	mapLayer = new ol.layer.Tile({
		source: new ol.source.OSM()
	});

	//init an empty feature layer
	markers = new ol.source.Vector({
		features: []
	});
	markerLayer = new ol.layer.Vector({
		source: markers,
		style: markerStyle
	});

	//create the view (required for jumping to coordinates later)
	view = new ol.View({
		center: [0, 0],
		zoom: 2,
		minZoom: 2,
		maxZoom: 20
	});

	//init the map, showing the two defined layers
	map = new ol.Map({
		interactions: ol.interaction.defaults().extend([new app.Drag()]),
		layers: [mapLayer, markerLayer],
		target: document.getElementById('map'),
		view: view,
		restrictedExtent: [0.0,0.0,0.0,0.0],
		controls: [
			new ol.control.Attribution(),
			new ol.control.Zoom(),
			new ol.control.ZoomSlider(),
			new ol.control.ScaleLine(),
			new ol.control.ZoomToExtent(),
			new ol.control.MousePosition({
				coordinateFormat: ol.coordinate.createStringXY(3),
				projection: 'EPSG:4326',
				undefinedHTML: '&nbsp;'
			})
		]
	});

	//create one popup for all markers
	popupElement = document.getElementById('popup');
	popup = new ol.Overlay({
		element: popupElement,
		positioning: 'auto',
		stopEvent: false,
		offset: [0, -25]
	});
	map.addOverlay(popup);

	//add event listeners:
	//--right click event
	map.getViewport().addEventListener('contextmenu', function (evt) {
		evt.preventDefault();
		//TODO: add a marker here? which one?
		console.log('right click');
	});

	console.log("Initialising map done.");

	//debug
	//jumpTo(43.091461111111116, 14.742301388888889, 5);
	//addMarker("c", 25.131894444444445, 35.33615, "Crete", "pin.png");
	//addMarker("b", 4.352708333333333, 50.84677222222223, "Brussels", "pin.png");
}

/**
 * Center the map in a given location and zoom in/out
 *
 * @param {double} lon the longitude
 * @param {double} lat the latitude
 * @param {double} zoom the zoom level (between 2 and 20)
 */
function jumpTo(lon, lat, zoom) {
	console.log('Jumping to ' + lat + ", " + lon + ", zoom: " + zoom);
	view.setCenter(ol.proj.fromLonLat([lat, lon]));
	view.setZoom(zoom);
}

/**
 * Add a marker to the marker layer
 *
 * @param {string} id the ID of the marker
 * @param {double} lon the longitude
 * @param {double} lat the latitude
 * @param {string} tooltip the tooltip for the element
 * @param {string} thumbnail the thumbnail to be displayed in the hover bubble or nothing
 */
function addMarker(id, lon, lat, tooltip, thumbnail) {

	if (typeof tooltip === 'undefined') {
		tooltip = id;
	}

	//create a new marker
	var marker = new ol.Feature({
		id: id,
		geometry: new ol.geom.Point(ol.proj.fromLonLat([lon, lat])),
		name: tooltip,
		thumbnail: thumbnail
	});
	marker.setStyle(markerStyle);

	//add the marker to the feature layer
	markers.addFeature(marker);

	//TODO: click event?
	marker.on('click', function (e) {
		console.log(e.originalEvent.button);
	});

}

/**
 * This method calls a Java connector
 *
 * @param {string} id the id (= full path) of the media item to be changed
 * @param {double} lat the new latitude
 * @param {double} lon the new longitude
 */
function updateMediaItem(id, lat, lon) {

	if (typeof leonardo !== 'undefined') {
		leonardo.updateMediaItem(id, lat, lon);
	} else {
		console.log('Could not connect to Java bridge; not updating coordinates for item ' + id);
	}
}

/**
 * Global drag and drop functions.
 * Based on http://openlayers.org/en/v3.2.1/examples/drag-features.html
 */
window.app = {};
var app = window.app;

/**
 * A drag and drop application
 *
 * @constructor
 * @extends {ol.interaction.Pointer}
 */
app.Drag = function() {

  ol.interaction.Pointer.call(this, {
    handleDownEvent: app.Drag.prototype.handleDownEvent,
    handleDragEvent: app.Drag.prototype.handleDragEvent,
    handleMoveEvent: app.Drag.prototype.handleMoveEvent,
    handleUpEvent: app.Drag.prototype.handleUpEvent
  });

  /**
   * The current coordinates
   *
   * @type {ol.Pixel}
   * @private
   */
  this.coordinate_ = null;

  /**
   * The feature currently being processed
   *
   * @type {ol.Feature}
   * @private
   */
  this.feature_ = null;

  /**
   * Whether a hover action is currently being done
   *
   * @type {boolean}
   * @private
   */
  this.hover_ = false;
};
ol.inherits(app.Drag, ol.interaction.Pointer);

/**
 * Handles the mouse down event
 *
 * @param {ol.MapBrowserEvent} evt Map browser event.
 * @return {boolean} `true` to start the drag sequence.
 */
app.Drag.prototype.handleDownEvent = function (evt) {

	this.hover_ = false;

	var map = evt.map;

	//get the feature
	var feature = map.forEachFeatureAtPixel(evt.pixel,
		function (feature) {
			return feature;
		}
	);

	//if there was a feature
	if (feature) {
		//use the feature's start coordinates and mark the feature as being processed
		this.coordinate_ = evt.coordinate;
		this.feature_ = feature;
		console.log('Selecting marker ' + feature.H.id + ' (' + feature.H.name + ')');
	}

	return !!feature;
};

/**
 * Handles the drag event following mouse down
 *
 * @param {ol.MapBrowserEvent} evt Map browser event.
 */
app.Drag.prototype.handleDragEvent = function (evt) {

	this.hover_ = false;

	//get new coordinates
	var deltaX = evt.coordinate[0] - this.coordinate_[0];
	var deltaY = evt.coordinate[1] - this.coordinate_[1];

	//calculate movement
	var geometry = /** @type {ol.geom.SimpleGeometry} */ (this.feature_.getGeometry());
	geometry.translate(deltaX, deltaY);

	//write new coordinates back
	this.coordinate_[0] = evt.coordinate[0];
	this.coordinate_[1] = evt.coordinate[1];
};

/**
 * "Normal" move event on map without dragging anything
 *
 * @param {ol.MapBrowserEvent} evt Event.
 */
app.Drag.prototype.handleMoveEvent = function (evt) {

	var feature = map.forEachFeatureAtPixel(evt.pixel,
		function (feature) {
			return feature;
		}
	);

	//distinguish between different move events
	//--drag
	if (this.feature_) {
		this.hover_ = false;
		//show "fist" pointer
		map.getTarget().style.cursor = 'move';
		//get rid of the popover
		$(popupElement).popover('destroy');
	//--hover
	} else if (feature) {
		//set cursor to pointer (hand)
		map.getTarget().style.cursor = 'pointer';

		if (this.hover_ !== true) {

			//console.log('Showing popover for marker ' + feature.H.id + ' (' + feature.H.name + ')');

			//display a tooltip containing the feature's name
			var coordinates = feature.getGeometry().getCoordinates();
			popup.setPosition(coordinates);

			$(popupElement).popover({
				'placement': 'auto',
				'html': true,
				'content': '<span class="thumbnailHeader">' + feature.get('name') + '</span>' +
							//make image in any case
							'<img class="thumbnail" alt="' + feature.get('name') + '" ' +
							//add source if it was set
							((typeof feature.tooltip !== undefined)?'src="' + feature.get('thumbnail') + '"':'')
							+ ' />'
			});
			$(popupElement).popover('show');
		}
		this.hover_ = true;
	//--move mouse
	} else {
		this.hover_ = false;
		//set cursor to normal (arrow)
		map.getTarget().style.cursor = '';
		//hide the popup
		$(popupElement).popover('destroy');
	}

	//check the cursor:
	if (this.cursor_) {
		var element = map.getTargetElement();

		//--if it's on top of a feature
		if (feature) {
			//--use the "hand" cursor
			if (element.style.cursor !== this.cursor_) {
				this.previousCursor_ = element.style.cursor;
				element.style.cursor = this.cursor_;
			}
		//--else
		} else if (this.previousCursor_ !== undefined) {
			//--unset (use default cursor)
			element.style.cursor = this.previousCursor_;
			this.previousCursor_ = undefined;
		}
	}
};

/**
 * Drop the currently dragged feature
 *
 * @param {ol.MapBrowserEvent} evt Map browser event.
 * @return {boolean} `false` to stop the drag sequence.
 */
app.Drag.prototype.handleUpEvent = function (evt) {

	this.hover_ = false;

	var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');

	console.log('Dropped marker ' + this.feature_.H.id + ' (' + this.feature_.H.name + ') at '
				+ lonlat[0] + ', ' + lonlat[1]);

	updateMediaItem(this.feature_.H.id, lonlat[1], lonlat[0]);

	//unset feature and coordinates
	this.coordinate_ = null;
	this.feature_ = null;
	return false;
};
