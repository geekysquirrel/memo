/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.sherlock.test;

import java.io.File;
import net.binarywood.memo.sherlock.controller.SherlockAnalyser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a utility/test file which illustrates the usage of the SherlockAnalyser class.
 * It is used to analyse batched of photos and help gain insights about the tagging.
 *
 * @author Stefanie Wiegand
 */
public class SherlockAnalyserTest {

	private static final Logger logger = LoggerFactory.getLogger(SherlockAnalyserTest.class);
	private static SherlockAnalyser sa;

	public static void main(String[] args) {

		sa = new SherlockAnalyser();

		doClassification("testpictures_classifier", "memo-runtime.ttl");
		analyseFeatures("testpictures_features_stef", "memo-features.ttl");
	}

	/**
	 * Load testpictures, classify and query
	 *
	 * @param directoryName the name of the directory which contains the test pictures
	 * @param modelName the name of the model to be used
	 */
	public static void doClassification(String directoryName, String modelName) {
		//generate path using the test resources path
		String testPath = SherlockAnalyserTest.class.getClassLoader().getResource(directoryName).getPath();
		//run the classification
		sa.classifyPhotos(testPath, testPath + File.separator + modelName);
		//query the classified model
		sa.doQuery();
	}

	/**
	 * Load testpictures, classify and run analysis
	 *
	 * @param directoryName the name of the directory which contains the test pictures
	 * @param modelName the name of the model to be used
	 */
	public static void analyseFeatures(String directoryName, String modelName) {

		String testPath = SherlockAnalyserTest.class.getClassLoader().getResource(directoryName).getPath();
		String testFile = testPath + File.separator + modelName;
		sa.importFeatureDataset(testPath, testFile);
		sa.doFeatureAnalysis(testFile);
	}

}
