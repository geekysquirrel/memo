/*
 * Copyright 2016 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */
package net.binarywood.memo.sherlock.store;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.binarywood.memo.common.MemoUtil;
import net.binarywood.memo.common.model.MemoOptions;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.query.UpdateExecutionException;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.rdfxml.util.RDFXMLPrettyWriter;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import org.slf4j.LoggerFactory;
import org.topbraid.spin.model.Argument;
import org.topbraid.spin.system.SPINModuleRegistry;
import org.topbraid.spin.util.JenaUtil;

/**
 * This class provides an interface to the RDF4j framework (formerly sesame).
 * It saves the state of a repository, with connections made just while operations are executed on the repo.
 * It also provides methods to load SPIN templates from the store and execute them directly on the store
 * (as opposed to in-memory). See http://docs.rdf4j.org/programming
 */
public final class Store extends AStoreWrapper {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Store.class);

	private Repository repo;
	private RepositoryConnection con;
	private ValueFactory factory;

	/**
	 * Create a new store with the directory specified in the store properties file
	 */
	public Store() {
		this(MemoOptions.get("store.dir"));
	}

	/**
	 * Create a new store with the persistence directory given
	 *
	 * @param dir where to persist the store
	 */
	public Store(String dir) {
		super();

		File dataDir = new File(dir);
		repo = new SailRepository(new NativeStore(dataDir, "spoc,posc,cosp,cspo"));
		repo.initialize();

		factory = SimpleValueFactory.getInstance();
	}

	// General ////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void connect() {
		con = repo.getConnection();
	}

	@Override
	public void disconnect() {

		if (con==null) {
			return;
		}

		if (con.isOpen()) {
			con.close();
		}
		con = null;
	}

	@Override
	public boolean isConnected() {
		return con != null && con.isOpen();
	}

	@Override
	public void loadNamespaces() {

		connect();
		try (RepositoryResult<Namespace> result = repo.getConnection().getNamespaces()) {
			while (result.hasNext()) {
				Namespace ns = result.next();
				prefixURIMap.put(ns.getPrefix(), ns.getName());
			}
		} finally {
			disconnect();
		}
		factory = repo.getValueFactory();
	}

	@Override
	public void clear() {

		connect();
		con.clear();
		disconnect();
		prefixURIMap = new HashMap<>();
	}

	@Override
	public long size() {

		connect();
		long size = con.size();
		disconnect();
		return size;
	}

	@Override
	public void export(String path, RDFFormat format) {
		export(null, path, format);
	}

	@Override
	public void shutdown() {

		super.shutdown();
		repo.shutDown();
		repo = null;
	}

	@Override
	public Model createOntologyModel(String baseURI, String comment, String ... imports) {

		ModelBuilder builder = new ModelBuilder();
		String base = baseURI.substring(0, baseURI.length()-1);

		getPrefixURIMap().entrySet().forEach(ns -> builder.setNamespace(ns.getKey(), ns.getValue()));

		builder.setNamespace("", base);
		builder.add(factory.createIRI(base), RDF.TYPE, OWL.ONTOLOGY);
		builder.add(factory.createIRI(base), RDFS.COMMENT, comment);

		for (String imp: imports) {
			builder.add(factory.createIRI(base), OWL.IMPORTS, factory.createIRI(imp.substring(0, imp.length()-1)));
		}

		return builder.build();
	}

	// Manage graphs //////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean graphExists(String graphURI) {

		boolean found = false;
		connect();
		try (RepositoryResult<Resource> result = con.getContextIDs()) {
			while (result.hasNext()) {
				Resource graph = result.next();
				if (graph.stringValue().equals(graphURI)) {
					found = true;
					break;
				}
			}
		} finally {
			disconnect();
		}

		return found;
	}

	@Override
	public void clearGraph(String graphURI) {

		if (graphURI!=null) {
			connect();
			try {
				con.clear(getFactory().createIRI(graphURI));
				logger.info("Cleared graph <{}>", graphURI);
			} catch (RepositoryException e) {
				logger.error("Could not clear graph <{}>", graphURI, e);
			} finally {
				disconnect();
			}
		} else {
			connect();
			try {
				con.clear((Resource) null);
				logger.info("Cleared all triples that are not in a graph");
			} catch (RepositoryException e) {
				logger.error("Could not clear triples that are not in a graph", e);
			} finally {
				disconnect();
			}
		}
	}

	@Override
	public long size(String graphURI) {

		connect();
		long size = con.size(getFactory().createIRI(graphURI));
		disconnect();
		return size;
	}

	// Querying ///////////////////////////////////////////////////////////////////////////////////
	@Override
	public List<Map<String, String>> querySelect(String sparql) {

		connect();

		TupleQuery tupleQuery;
		try {
			tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, getSPARQLPrefixes() + sparql);
		} catch (MalformedQueryException | RepositoryException e) {
			tupleQuery = null;
			logger.error("Could not execute SELECT:\n{}", getSPARQLPrefixes() + sparql, e);
		}

		//persist result in here. Picked list instead of set because the results may have duplicates.
		//for no duplicates, the SPARQL "DISTINCT" keyword can be used
		List<Map<String, String>> results = new ArrayList<>();

		if (tupleQuery!=null) {
			try (TupleQueryResult result = tupleQuery.evaluate()) {
				while (result.hasNext()) {
					HashMap<String, String> row = new HashMap<>();
					BindingSet bindingSet = result.next();
					bindingSet.getBindingNames().forEach(binding -> {
						Value v = bindingSet.getValue(binding);
						row.put(binding, v.stringValue());
					});
					results.add(row);
				}
			}
		}
		disconnect();

		return results;
	}

	@Override
	public List<Map<String, String>> querySelect(String sparql, String graphURI) {
		return querySelect(sparql.replace("WHERE", "FROM <" + graphURI + "> WHERE"));
	}

	@Override
	public Model queryConstruct(String sparql) {

		connect();
		Model m = null;
		try {
			m = Repositories.graphQuery(repo, getSPARQLPrefixes() + sparql, r -> QueryResults.asModel(r));
		} catch (MalformedQueryException | QueryEvaluationException | RepositoryException e) {
			logger.error("Could not execute CONSTRUCT:\n{}", getSPARQLPrefixes() + sparql, e);
		} finally {
			disconnect();
		}
		return m;
	}

	@Override
	public Model queryConstruct(String sparql, String graphURI) {
		return queryConstruct(sparql.replaceFirst("WHERE", "FROM <" + graphURI + "> WHERE"));
	}

	@Override
	public Model queryDescribe(String sparql) {

		connect();
		Model m = null;
		try {
			m = Repositories.graphQuery(repo, getSPARQLPrefixes() + sparql, r -> QueryResults.asModel(r));
		} catch (MalformedQueryException | QueryEvaluationException | RepositoryException e) {
			logger.error("Could not execute DESCRIBE:\n{}", getSPARQLPrefixes() + sparql, e);
		} finally {
			disconnect();
		}
		return m;
	}

	@Override
	public Model queryDescribe(String sparql, String graphURI) {
		return queryDescribe(sparql.replace("WHERE", "FROM <" + graphURI + "> WHERE"));
	}

	@Override
	public Boolean queryAsk(String sparql) {

		Boolean result = null;
		connect();
		BooleanQuery query = con.prepareBooleanQuery(getSPARQLPrefixes() + sparql);
		try {
			result = query.evaluate();
		} catch (QueryEvaluationException e) {
			logger.error("Could not execute ASK:\n{}", getSPARQLPrefixes() + sparql, e);
		} finally {
			disconnect();
		}
		return result;
	}

	@Override
	public Boolean queryAsk(String sparql, String graphURI) {

		//the where clause is optional for ASK queries
		if (sparql.toUpperCase().contains("WHERE")) {
			return queryAsk(sparql.replace("WHERE", "FROM <" + graphURI + "> WHERE"));
		} else {
			return queryAsk(sparql.replace("ASK", "ASK { GRAPH <" + graphURI + ">") + " }");
		}
	}

	@Override
	public void update(String sparql) {

		connect();
		Update update = con.prepareUpdate(getSPARQLPrefixes() + sparql);
		try {
			update.execute();
		} catch (UpdateExecutionException e) {
			logger.error("Could not execute UPDATE:\n{}", getSPARQLPrefixes() + sparql, e);
		} finally {
			disconnect();
		}
	}

	@Override
	public void update(String sparql, String graphURI) {

		if (graphURI!=null) {
			//insert data is a shorthand form without a where clause and does not support "with"
			if (sparql.toUpperCase().startsWith("INSERT DATA")) {
				update(sparql.replace("INSERT DATA {", "INSERT DATA { GRAPH <" + graphURI + "> {") + " }");
			//delete data is a shorthand form without a where clause and does not support "with"
			} else if (sparql.toUpperCase().startsWith("DELETE DATA")) {
				update(sparql.replace("DELETE DATA {", "DELETE DATA { GRAPH <" + graphURI + "> {") + " }");
			//the with clause is applicable to delete, insert and delete/insert updates
			} else {
				update("WITH <" + graphURI + "> " + sparql);
			}
		} else {
			logger.debug("No graph given, update will be executed without a graph");
			update(sparql);
		}
	}

	// SPIN ///////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method to translate a sesame model from the store into a jena model to execute SPIN rules
	 *
	 * @param graphURI the graph to translate
	 * @return the model containing all the triples from the graph
	 */
	private OntModel createJenaModel(String graphURI) {

		// Create jena model
		OntModel model = ModelFactory.createOntologyModel();
		JenaUtil.initNamespaces(model.getGraph());

		//get statements from store
		connect();
		List<Map<String, String>> content = null;
		try {
			//get all triples from the store including the types of subject and object
			content = querySelect("SELECT * WHERE {\n" +
				"	?s ?p ?o .\n" +
				"	BIND (IF(ISBLANK(?s),\"blank\",IF(ISIRI(?s),\"iri\",IF(ISLITERAL(?s),\"literal\",\"unknown\"))) as ?st)\n" +
				"	BIND (IF(ISBLANK(?o),\"blank\",IF(ISIRI(?o),\"iri\",IF(ISLITERAL(?o),DATATYPE(?o),\"unknown\"))) as ?ot)\n" +
				"}", graphURI);

			logger.debug("Loaded all content from graph <{}>", graphURI);
		} catch (RepositoryException e) {
			logger.error("Could not load statements from triple store", e);
		}

		//translate sesame model to jena model, statement by statement
		if (content!=null) {
			for (Map<String, String> s: content) {

				if (s==null) {
					continue;
				}

				//This may be a URI or blank node
				org.apache.jena.rdf.model.Resource subject = null;
				if ("iri".equals(s.get("st"))) {
					subject = model.createResource(s.get("s"));
				} else if ("blank".equals(s.get("st"))) {
					//workaround for blank node identifiers: create URI in MemO namespace
					subject = model.createResource(new AnonId(s.get("s")));
				} else {
					logger.warn("Unknown subject type: {}", s.get("st"));
				}
				//the predicate should always be URI
				Property predicate = ResourceFactory.createProperty(s.get("p"));
				//This may be a URI, literal or blank node
				RDFNode object = null;
				if ("iri".equals(s.get("ot"))) {
					object = model.createResource(s.get("o"));
				} else if ("blank".equals(s.get("ot"))) {
					object = model.createResource(new AnonId(s.get("o")));
				} else if (s.get("ot")!=null && !"unknown".equals(s.get("ot"))) {
					XSDDatatype dt;
					if (s.get("ot").endsWith("double") || s.get("ot").endsWith("float")) {
						dt = XSDDatatype.XSDdouble;
					} else if (s.get("ot").endsWith("int") || s.get("ot").endsWith("integer")) {
						dt = XSDDatatype.XSDint;
					} else if (s.get("ot").endsWith("boolean")) {
						dt = XSDDatatype.XSDboolean;
					} else if (s.get("ot").endsWith("long")) {
						dt = XSDDatatype.XSDlong;
					} else if (s.get("ot").endsWith("short")) {
						dt = XSDDatatype.XSDshort;
					} else if (s.get("ot").toLowerCase().endsWith("datetime")) {
						dt = XSDDatatype.XSDdateTime;
					} else if (s.get("ot").toLowerCase().endsWith("date")) {
						dt = XSDDatatype.XSDdate;
					} else if (s.get("ot").toLowerCase().endsWith("time")) {
						dt = XSDDatatype.XSDtime;
					} else {
						dt = XSDDatatype.XSDstring;
					}
					object = ResourceFactory.createTypedLiteral(s.get("o"), dt);
				} else {
					logger.warn("Unknown object type: {}", s.get("ot"));
				}

				if (subject!=null && predicate!=null && object!=null) {
					model.add(ResourceFactory.createStatement(subject, predicate, object));
				} else {
					logger.warn("Could not add statement {} {} {}", s.get("s"), s.get("p"), s.get("o"));
				}
			}
		}
		disconnect();

		model.loadImports();

		logger.debug("Transformed contents of <{}> into {} Jena statements", graphURI, model.size());

		return model;
	}

	/**
	 * Get all templates that are in the given graph
	 *
	 * @param graphURI the URI of the graph or null to get all templates in the repo
	 * @return a set of all retrieved templates
	 */
	public Map<String, String> getAllTemplates(String graphURI) {

		long start = System.currentTimeMillis();
		Map<String, String> templates = new HashMap<>();
		SPINModuleRegistry.get().init();

		OntModel model = createJenaModel(graphURI);

		//use SPIN API to load templates and functions (which might appear in templates)
		SPINModuleRegistry.get().registerAll(model, null);
		//only use templates that have a body
		SPINModuleRegistry.get().getTemplates().stream().filter(t -> t!=null && t.getBody()!=null).forEachOrdered(t -> {
			Map<String, Argument> aMap = t.getArgumentsMap();

			//add arguments to template
			if (aMap!=null && !aMap.isEmpty()) {
				for (Map.Entry<String, Argument> a : aMap.entrySet()) {
					//TODO: add argument to map
				}
			}
			templates.put(t.getURI(), t.getBody().toString());
		});

		logger.debug("Loaded {} SPIN templates from <{}> in {}",
			templates.size(), graphURI, MemoUtil.getTimeStr(System.currentTimeMillis() - start));

		return templates;
	}

	/**
	 * Run a template on a graph
	 *
	 * @param templateURI the URI of the template to run. It must have been loaded previously
	 * @param arguments a map or arguments for the template
	 * @param templates the set containing the template we want to execute
	 * @param graphURI the URI of the graph on which to run the template
	 * @return the resulting model, containing inferred triples or triples to remove, depending on the template.
			it will not contain the allTemplates of updates, i.e. triples that already *have been* added/removed.
	 */
	public Model runTemplate(String templateURI, Map<String, String> arguments, Map<String, String> templates, String graphURI) {

		Model resultModel = new LinkedHashModel();

		for (Map.Entry<String, String> t : templates.entrySet()) {

			if (t.getKey().equals(templateURI)) {

				logger.debug("Running template <{}>", t.getKey());

				long start = System.currentTimeMillis();

				String msg = "";
				if (t.getValue() != null) {

					//this will contain the actual query as fired, considering all arguments and optimisations
					//it doesn not include the prefixes - these will be added later
					String query = t.getValue();

					try {
						//TODO: check if arguments are required by the template
						//replace arguments
						if (arguments != null) {
							logger.debug("Arguments: {}", arguments);
							for (Map.Entry<String, String> arg : arguments.entrySet()) {
								query = query.replace("?" + arg.getKey(), arg.getValue());
							}
						}

						//doit - depending on the query type
						//--construct triples and add them to the allTemplates but don't add them to the store
						if (t.getValue().toLowerCase().startsWith("construct")) {
							connect();
							try {
								//run query on graph
								query = getSPARQLPrefixes() + query.replaceFirst("WHERE", "FROM <" + graphURI + "> WHERE");
								resultModel = QueryResults.asModel(con.prepareGraphQuery(query).evaluate());
							} catch (MalformedQueryException | QueryEvaluationException | RepositoryException e) {
								logger.error("Could not execute CONSTRUCT:\n{}", query, e);
							} finally {
								disconnect();
							}
							msg = resultModel.size() + " new triples constructed";
						} else if (t.getValue().toLowerCase().startsWith("delete")) {
							//TODO: DELETE/INSERT , see https://www.w3.org/TR/2013/REC-sparql11-update-20130321/#deleteInsert
							long before = con.size();
							update(getSPARQLPrefixes() + "WITH <" + graphURI + "> " + query);
							long diff = before - con.size();
							msg = diff + " triples deleted";
						} else if (t.getValue().toLowerCase().startsWith("insert")) {
							long before = con.size();
							update(getSPARQLPrefixes() + "WITH <" + graphURI + "> " + query);
							long diff = con.size() - before;
							msg = diff + " triples inserted";
						//} else if (t.getValue().toLowerCase().startsWith("with")) {
							//TODO: "WITH" (WITH <g1> DELETE { a b c } INSERT { x y z } WHERE { ... })
						} else {
							logger.warn("Unknown query type for template:\n{}", getSPARQLPrefixes() + query);
						}

					} catch (MalformedQueryException | QueryEvaluationException | RepositoryException e) {
						logger.error("Error running template <{}>: {}", t.getKey(), query, e);
					}
				} else {
					logger.error("Template <{}> has no body, can't run", t.getKey());
				}

				logger.info("Finished running template <{}> in {}. {}.",
					t.getKey(), MemoUtil.getTimeStr(System.currentTimeMillis() - start), msg);
				//all done, there can't be another template with the same URI
				break;
			}
		}

		return resultModel;
	}

	/**
	 * Run all templates (subclasses of a given template class) by individually loading and executing them.
	 *
	 * @param templateClass the URI of the template class of which to load the templates
	 * @param templates all templates (need to be loaded before this is called)
	 * @param graphURI the URI against which to run the templates
	 * @return the inference model (all inferred triples)
	 * @see net.binarywood.memo.sherlock.store.Store#getAllTemplates(String graphURI)
	 */
	public Model runTemplatesOfClass(String templateClass, Map<String, String> templates, String graphURI) {

		Model infModel = new LinkedHashModel();

		if (templates==null || templates.isEmpty()) {
			logger.warn("No templates were previously loaded. Use getAllTemplates(...) to load them.");
			return infModel;
		}

		logger.info("Running all templates of class <{}>", templateClass);

		//get all templates of parent template class
		List<Map<String, String>> allTemplates = querySelect("SELECT * WHERE { ?tmp rdfs:subClassOf* <" + templateClass + "> . }");

		if (allTemplates==null || allTemplates.isEmpty()) {
			logger.info("No templates found for class <{}>", templateClass);
			return infModel;
		}

		//this will collect all the templates' URIs
		Set<String> tmps = new HashSet<>();
		allTemplates.forEach(row -> tmps.add(row.get("tmp")));

		//filter templates
		Map<String, String> applicableTemplates = new HashMap<>();
		templates.entrySet().forEach(template -> {
			//filter by classname
			if (tmps.contains(template.getKey())) {
				applicableTemplates.put(template.getKey(), template.getValue());
			}
		});
		logger.info("{} template(s) loaded from parent class <{}>", applicableTemplates.size(), templateClass);

		//run templates
		applicableTemplates.entrySet().forEach(template -> {
			if (template.getValue() != null) {
				//run template without any arguments
				infModel.addAll(runTemplate(template.getKey(), null, templates, graphURI));
			} else {
				logger.warn("Skipping template <{}>, template body is null", template.getKey());
			}
		});
		logger.info("Finished running templates, {} new triples constructed", infModel.size());

		return infModel;
	}

	// Actions on graph ///////////////////////////////////////////////////////////////////////////

	@Override
	public void importLocalDocument(String path, String baseURI, String graphURI, RDFFormat format) {

		long start = System.currentTimeMillis();
		connect();
		try {
			con.add(new File(path), baseURI, format, getFactory().createIRI(graphURI));
			logger.info("Imported local document <{}>, format {} into graph <{}> in {} using baseURI <{}>",
				path, format, graphURI, MemoUtil.getTimeStr(System.currentTimeMillis() - start), baseURI);

		} catch (IOException | RDFParseException | RepositoryException ex) {
			logger.error("Could not import local document <{}> into store", path, ex);
		} finally {
			disconnect();
		}
		loadNamespaces();
	}

	@Override
	public void importRemoteDocument(String url, String baseURI, String graphURI, RDFFormat format) {

		long start = System.currentTimeMillis();
		connect();
		try {
			con.add(new URL(url), baseURI, format, getFactory().createIRI(graphURI));
			logger.info("Imported remote document <{}>, format {} into graph <{}> in {} using baseURI <{}>",
				url, format, graphURI, MemoUtil.getTimeStr(System.currentTimeMillis() - start), baseURI);
		} catch (IOException | RDFParseException | RepositoryException ex) {
			logger.error("Could not import remote document <{}> into store", url, ex);
		} finally {
			disconnect();
		}
		loadNamespaces();
	}

	@Override
	public void storeStatement(Statement stmt, String graphURI) {

		connect();
		try {
			con.add(stmt, (graphURI!=null)?getFactory().createIRI(graphURI):((Resource) null));
		} catch (RepositoryException e) {
			logger.error("Could not add statement {} to graph <{}>", stmt, graphURI, e);
		} finally {
			disconnect();
		}
	}

	@Override
	public void removeStatement(Statement stmt, String graphURI) {

		connect();
		try {
			con.remove(stmt, (graphURI!=null)?getFactory().createIRI(graphURI):((Resource) null));
		} catch (RepositoryException e) {
			logger.error("Could not remove statement {} from graph <{}>", stmt, graphURI, e);
		} finally {
			disconnect();
		}
	}

	@Override
	public void storeModel(Model m, String graphURI) {

		long start = System.currentTimeMillis();
		connect();
		try {
			con.add(m, (graphURI!=null)?getFactory().createIRI(graphURI):((Resource) null));
			logger.info("Stored model (size {}) in <{}> in {}",
				m.size(), graphURI, MemoUtil.getTimeStr(System.currentTimeMillis() - start));
		} catch (RepositoryException e) {
			logger.error("Could not add model to graph <{}>", graphURI, e);
		} finally {
			disconnect();
		}
	}

	@Override
	public void removeModel(Model m, String graphURI) {

		long start = System.currentTimeMillis();
		connect();
		try {
			con.remove(m, (graphURI!=null)?getFactory().createIRI(graphURI):((Resource) null));
			logger.info("Removed model (size {}) from <{}> in {}",
				m.size(), graphURI, MemoUtil.getTimeStr(System.currentTimeMillis() - start));
		} catch (RepositoryException e) {
			logger.error("Could not remove model from graph <{}>", graphURI, e);
		} finally {
			disconnect();
		}
	}

	@Override
	public void export(String graphURI, String path, RDFFormat format) {

		long start = System.currentTimeMillis();

		//get graph
		connect();
		Model model = new LinkedHashModel();
		RepositoryResult<Statement> result = null;
		String graphStr = graphURI!=null?"graph <" + graphURI + ">":"the entire repository";
		try {
			if (graphURI!=null) {
				result = con.getStatements(null, null, null, getFactory().createIRI(graphURI));
			} else {
				result = con.getStatements(null, null, null);
			}
			while (result.hasNext()) {
				model.add(result.next());
			}
		} catch (RepositoryException e) {
			logger.error("Could not retrieve triples from store to export", e);
		} finally {
			disconnect();
		}

		//write to file
		try {

			RDFWriter writer;
			if (format.equals(RDFFormat.RDFXML)) {
				writer = new RDFXMLPrettyWriter(new FileOutputStream(path));
			} else {
				writer = Rio.createWriter(format, new FileOutputStream(path));
			}

			writer.startRDF();

			//use namespaces
			prefixURIMap.entrySet().forEach(ns -> writer.handleNamespace(ns.getKey(), ns.getValue()));
			if (graphURI!=null) {
				writer.handleNamespace("", graphURI);
			}

			//add statements
			model.forEach(st -> writer.handleStatement(st));

			writer.endRDF();
			logger.info("Exported {} containing {} triples to file <{}> in {} using format {}",
					graphStr, model.size(), path, MemoUtil.getTimeStr(System.currentTimeMillis() - start), format);
		} catch (RDFHandlerException | FileNotFoundException e) {
			logger.error("Could not save the contents of {} to file <{} using format {}", graphStr, path, format, e);
		}
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////

	/**
	 * Get a value factory
	 *
	 * @return the factory which can be used for creating IRIs etc.
	 */
	public ValueFactory getFactory() {

		if (factory==null) {
			factory = repo.getValueFactory();
		}
		return factory;
	}
}
