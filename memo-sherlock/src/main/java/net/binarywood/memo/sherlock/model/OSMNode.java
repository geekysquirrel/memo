/**
 * (c) Jens Kübler
 * This software is public domain
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Modified by Stefanie Wiegand
 *
 */

package net.binarywood.memo.sherlock.model;

import java.util.Map;

/**
 * A node on OpenStreetMap
 *
 * @author Jens Kübler, Stefanie Wiegand
 */
public class OSMNode {

	private String id;

	private String lat;

	private String lon;

	private final Map<String, String> tags;

	private String version;

	/**
	 * Create a node from the given parameters
	 *
	 * @param id the OSM ID
	 * @param latitude the node's lat
	 * @param longitude	the node's lon
	 * @param version the version of this node
	 * @param tags OSM tags attached to this node
	 */
	public OSMNode(String id, String latitude, String longitude, String version, Map<String, String> tags) {
		this.id = id;
		this.lat = latitude;
		this.lon = longitude;
		this.tags = tags;
		this.version = version;
	}

	@Override
	public String toString() {
		String string =  id + ": " + lat + ", " + lon + " {version " + version + "}";
		string = tags.entrySet().stream().map(
			tag -> "\n - " + tag.getKey() + ": " + tag.getValue()
		).reduce(string, String::concat);
		return string;
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
