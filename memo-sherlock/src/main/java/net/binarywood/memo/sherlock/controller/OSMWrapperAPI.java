/**
 * (c) Jens Kübler
 * This software is public domain
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Modified by Stefanie Wiegand
 *
 */

package net.binarywood.memo.sherlock.controller;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.binarywood.memo.sherlock.model.OSMNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * An OpenStreetMap API for read access
 *
 * @author Jens Kübler, Stefanie Wiegand
 */
public class OSMWrapperAPI {

	private static final Logger logger = LoggerFactory.getLogger(OSMWrapperAPI.class);

	private static final String OVERPASS_API = "http://www.overpass-api.de/api/interpreter";
	private static final String OPENSTREETMAP_API_06 = "http://www.openstreetmap.org/api/0.6/";

	private OSMWrapperAPI() {}

	/**
	 * Retrieve a node from OSM
	 *
	 * @param nodeId the node's ID
	 * @return the node
	 * @throws IOException ?
	 * @throws ParserConfigurationException ?
	 * @throws SAXException ?
	 */
	public static OSMNode getNode(String nodeId) throws IOException, ParserConfigurationException, SAXException {
		String string = "http://www.openstreetmap.org/api/0.6/node/" + nodeId;
		URL osm = new URL(string);
		HttpURLConnection connection = (HttpURLConnection) osm.openConnection();

		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document document = docBuilder.parse(connection.getInputStream());
		List<OSMNode> nodes = getNodes(document);
		if (!nodes.isEmpty()) {
			return nodes.iterator().next();
		}
		return null;
	}

	/**
	 *
	 * @param lon the longitude
	 * @param lat the latitude
	 * @param vicinityRange bounding box in this range
	 * @return the xml document containing the queries nodes
	 * @throws IOException ?
	 * @throws SAXException ?
	 * @throws ParserConfigurationException ?
	 */
	private static Document getXML(double lon, double lat, double vicinityRange)
			throws IOException, SAXException, ParserConfigurationException {

		DecimalFormat format = new DecimalFormat("##0.0000000", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		String left = format.format(lat - vicinityRange);
		String bottom = format.format(lon - vicinityRange);
		String right = format.format(lat + vicinityRange);
		String top = format.format(lon + vicinityRange);

		String string = OPENSTREETMAP_API_06 + "map?bbox=" + left + "," + bottom + "," + right + "," + top;
		URL osm = new URL(string);
		HttpURLConnection connection = (HttpURLConnection) osm.openConnection();

		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		return docBuilder.parse(connection.getInputStream());
	}

	/**
	 * Get nodes from an XML document
	 *
	 * @param xmlDocument the document
	 * @return a list of openstreetmap nodes extracted from xml
	 */
	public static List<OSMNode> getNodes(Document xmlDocument) {
		List<OSMNode> osmNodes = new ArrayList<>();

		Node osmRoot = xmlDocument.getFirstChild();
		NodeList osmXMLNodes = osmRoot.getChildNodes();
		for (int i = 1; i < osmXMLNodes.getLength(); i++) {
			Node item = osmXMLNodes.item(i);
			if ("node".equals(item.getNodeName())) {
				NamedNodeMap attributes = item.getAttributes();
				NodeList tagXMLNodes = item.getChildNodes();
				Map<String, String> tags = new HashMap<>();
				for (int j = 1; j < tagXMLNodes.getLength(); j++) {
					Node tagItem = tagXMLNodes.item(j);
					NamedNodeMap tagAttributes = tagItem.getAttributes();
					if (tagAttributes != null) {
						tags.put(tagAttributes.getNamedItem("k").getNodeValue(), tagAttributes.getNamedItem("v")
								.getNodeValue());
					}
				}
				Node namedItemID = attributes.getNamedItem("id");
				Node namedItemLat = attributes.getNamedItem("lat");
				Node namedItemLon = attributes.getNamedItem("lon");
				Node namedItemVersion = attributes.getNamedItem("version");

				String id = namedItemID.getNodeValue();
				String latitude = namedItemLat.getNodeValue();
				String longitude = namedItemLon.getNodeValue();
				String version = "0";
				if (namedItemVersion != null) {
					version = namedItemVersion.getNodeValue();
				}

				osmNodes.add(new OSMNode(id, latitude, longitude, version, tags));
			}

		}
		return osmNodes;
	}

	/**
	 * Get all nodes in a given vicinity
	 *
	 * @param lat latitude
	 * @param lon longitude
	 * @param vicinityRange the bounding box
	 * @return a list of OSM nodes within the vicinity
	 * @throws IOException ?
	 * @throws SAXException ?
	 * @throws ParserConfigurationException ?
	 */
	public static List<OSMNode> getOSMNodesInVicinity(double lat, double lon, double vicinityRange)
			throws IOException, SAXException, ParserConfigurationException {
		return OSMWrapperAPI.getNodes(getXML(lon, lat, vicinityRange));
	}

	/**
	 *	Execute an overpass API query
	 *
	 * @param query the overpass query
	 * @return the nodes in the formulated query
	 * @throws IOException ?
	 * @throws ParserConfigurationException ?
	 * @throws SAXException ?
	 */
	public static Document getNodesViaOverpass(String query)
			throws IOException, ParserConfigurationException, SAXException {
		String hostname = OVERPASS_API;
		String queryString = query;

		URL osm = new URL(hostname);
		HttpURLConnection connection = (HttpURLConnection) osm.openConnection();
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

		try (DataOutputStream printout = new DataOutputStream(connection.getOutputStream())) {
			printout.writeBytes("data=" + URLEncoder.encode(queryString, "utf-8"));
			printout.flush();
		}

		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		return docBuilder.parse(connection.getInputStream());
	}

	/**
	 * main method that simply reads some nodes
	 *
	 * @param args command line arguments (unused)
	 */
	public static void main(String[] args) {
		logger.debug("Starting test");

		try {
			//bounding box
			List<OSMNode> osmNodesInVicinity = getOSMNodesInVicinity(49, 8.3, 0.005);
			osmNodesInVicinity.stream().forEach(osmNode -> logger.debug("{}", osmNode));

			//overpass query
			List<OSMNode> result = getNodes(getNodesViaOverpass("(node(51.249,7.148,51.251,7.152);<;);out;"));
			result.stream().forEach(osmNode -> logger.debug("{}", osmNode));
		} catch (IOException | SAXException | ParserConfigurationException e) {
			logger.error("Could not execute OSM wrapper test", e);
		}
	}

}
