/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */
package net.binarywood.memo.sherlock.controller;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.binarywood.memo.common.MemoUtil;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.sherlock.store.Store;
import org.apache.jena.vocabulary.XSD;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class does the hard semantic work. It serves as a store I/O as well as a reasoning engine.
 *
 * @author Stefanie Wiegand
 */
public class Sherlock {

	private static final Logger logger = LoggerFactory.getLogger(Sherlock.class);

	//TODO: move to config file
	private static final String AUTOTAG = "memo:hasAutomaticTag";

	private final Store store;
	private Map<String, String> templates;

	/**
	 * Creates a new sherlock object using the standard store directory as defined in the properties file
	 */
	public Sherlock() {
		this(MemoOptions.get("store.dir"));
	}

	/**
	 * Creates a new sherlock object
	 *
	 * @param dir the store directory
	 */
	public Sherlock(String dir) {

		if (!MemoOptions.isLoaded()) {
			MemoOptions.load();
		}

		//create new store
		store = new Store(dir);
		initStore();
	}

	/**
	 * Create a Sherlock object using an existing store
	 *
	 * @param store the store
	 */
	public Sherlock(Store store) {

		this.store = store;
		initStore();
	}

	/**
	 * Initialise the store by importing the MemO ontology if applicable and setting the MemO namespaces
	 */
	private void initStore() {

		//import MemO ontology
		if (store.size(MemoOptions.get("store.memoURI")) == 0) {
			store.importLocalDocument(getClass().getClassLoader().getResource("ontologies/memo.rdf").getPath(),
					MemoOptions.get("store.memoURI"), MemoOptions.get("store.memoURI"), RDFFormat.RDFXML);
		}

		//set runtime namespace
		store.getPrefixURIMap().put("memo", MemoOptions.get("store.memoURI"));
		store.getPrefixURIMap().put("", MemoOptions.get("store.graphURI"));
		logger.debug("Using namespaces {}", store.getPrefixURIMap());
	}

	// Get data from store ////////////////////////////////////////////////////////////////////////

	/**
	 * Get all media item URIs that are currently saved in the store
	 *
	 * @return a Set of URIs, each of which stands for a media item in the store
	 */
	public Set<String> getAllMediaItemURIs() {

		Set<String> uris = new HashSet<>();
		store.querySelect("SELECT * WHERE { ?uri a memo:MediaItem }", MemoOptions.get("store.graphURI")).forEach(row ->
			uris.add(row.get("uri"))
		);
		return uris;
	}

	/**
	 * Retrieves a MediaItem from the store
	 *
	 * @param parentDir the parent directory for which to retrieve the items
	 * @param uri if set and not null, this filters the retrieved media items to effectlively only return one
	 * @return the MediaItem(s)
	 */
	public Set<MediaItem> retrieveMediaItems(String parentDir, String uri) {

		long start = System.currentTimeMillis();

		String query = "SELECT * WHERE {\n";
		//filter
		if (uri!=null) {
			query += "	BIND(<" + uri + "> AS ?uri)\n";
		}
		if (parentDir!=null) {
			query += "	BIND(\"" + parentDir + "\" AS ?parentDirectory)\n";
		}
		query += "	?uri a memo:MediaItem .\n" +
		"	?uri memo:pathOnDisk ?path .\n" +
		"	?uri memo:parentDirectory ?parentDirectory .\n" +
		"	?uri memo:filename ?filename .\n" +
		"	?uri memo:filesize ?filesize .\n" +
		"	?uri memo:checksum ?checksum .\n" +
		"	?uri memo:fileChecksum ?fileChecksum .\n" +
		"	?uri memo:dateChecksum ?dateChecksum .\n" +
		"	?uri memo:filenameChecksum ?filenameChecksum .\n" +
		"	OPTIONAL { ?uri memo:thumbnailPath ?thumbnail }\n" +
		"	?uri memo:shotWithCamera ?c .\n" +
		"	?c exif:make ?cMake .\n" +
		"	?c exif:model ?cModel .\n" +
		"	OPTIONAL { ?c memo:serialNumber ?cSerial }\n" +
		"	?uri memo:hasDateTime ?dt .\n" +
		"	OPTIONAL { ?dt memo:utcDateTime ?gpsDate }\n" +
		"	OPTIONAL { ?dt memo:localDateTime ?datetimeOriginalExif }\n" +
		"	?uri memo:hasIFD ?ifd .\n" +
		"	OPTIONAL { ?ifd memo:originalFilename ?originalFilename }\n" +
		"	OPTIONAL { ?ifd exif:isoSpeedRatings ?isoSpeed }\n" +
		"	OPTIONAL { ?ifd memo:lensInfo ?lensInfo }\n" +
		"	OPTIONAL { ?ifd exif:exposureBiasValue ?exposureBias }\n" +
		"	OPTIONAL { ?ifd exif:apertureValue ?aperture }\n" +
		"	OPTIONAL { ?ifd exif:flash ?flash }\n" +
		"	OPTIONAL { ?ifd exif:focalLength ?focalLength }\n" +
		"	OPTIONAL { ?ifd memo:focalDistance ?focalDistance }\n" +
		"	OPTIONAL { ?ifd memo:focalPlaneDiagonal ?focalPlaneDiagonal }\n" +
		"	OPTIONAL { ?ifd exif:shutterSpeedValue ?exposureTime }\n" +
		"	OPTIONAL { ?ifd memo:temperature ?temperature }\n" +
		"	OPTIONAL { ?ifd memo:lightValue ?lightValue }\n" +
		"	OPTIONAL { ?ifd memo:depthOfField ?depthOfField }\n" +
		"	OPTIONAL { ?ifd memo:focusFrom ?focusFrom }\n" +
		"	OPTIONAL { ?ifd memo:focusTo ?focusTo }\n" +
		"	OPTIONAL { ?ifd memo:fieldOfView ?fieldOfView }\n" +
		"	OPTIONAL { ?ifd exif:digitalZoomRatio ?digitalZoom }\n" +
		"	OPTIONAL { ?ifd exif:whiteBalance ?whiteBalance }\n" +
		"	OPTIONAL { ?ifd exif:userComment ?comment }\n" +
		"	OPTIONAL { ?ifd exif:imageWidth ?width }\n" +
		"	OPTIONAL { ?ifd exif:imageLength ?height }\n" +
		"	OPTIONAL { ?ifd exif:gpsLatitudeRef ?latRef }\n" +
		"	OPTIONAL { ?ifd exif:gpsLatitude ?latitude }\n" +
		"	OPTIONAL { ?ifd exif:gpsLongitudeRef ?longRef }\n" +
		"	OPTIONAL { ?ifd exif:gpsLongitude ?longitude }\n" +
		"	OPTIONAL { ?ifd exif:gpsAltitudeRef ?altRef }\n" +
		"	OPTIONAL { ?ifd exif:gpsAltitude ?altitude }\n" +
		"	OPTIONAL { ?ifd memo:timezoneID ?timezoneID }\n" +
		//TODO:	?orientationTag ?keywords
		"}";

		List<Map<String, String>> result = store.querySelect(query);

		Set<MediaItem> mediaItems = new HashSet<>();

		if (result.isEmpty()) {
			logger.warn("Could not find media items (URI <{}>) in store", uri);
			return mediaItems;
		}

		Set<String> optionals = Stream.of("thumbnail", "cSerial", "gpsDate", "datetimeOriginalExif",
			"originalFilename", "isoSpeed", "lensInfo", "exposureBias", "aperture", "flash", "focalLength",
			"focalDistance", "focalPlaneDiagonal", "exposureTime", "temperature", "lightValue", "depthOfField",
			"focusFrom", "focusTo", "fieldOfView", "digitalZoom", "whiteBalance", "comment", "width", "height",
			"latRef", "latitude", "longRef", "longitude", "altRef", "altitude", "timezoneID", "orientationTag",
			"keywords"
		).collect(Collectors.toSet());

		for (Map<String, String> row: result) {
			//set null values for all unbound variables!
			optionals.stream().filter(o -> !row.containsKey(o)).forEachOrdered(o -> row.put(o, null));
			mediaItems.add(createMediaItem(row));
		}

		logger.info("Retrieved {} media item(s) from store in {}", mediaItems.size(),
			MemoUtil.getTimeStr(System.currentTimeMillis() - start));

		return mediaItems;
	}

	// Create data ////////////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a media item given all the required information
	 *
	 * @param data the input data used to create a media item. Unset keys in the map will be interpreted as null values.
	 * @return the media item
	 */
	public MediaItem createMediaItem(Map<String, String> data) {

		MediaItem created = null;
		try {

			//calc
			MediaItem.Orientation orientation = MediaItem.Orientation.UNKNOWN;
			if (data.get("orientationTag")!=null) {
				if ("landscapeOrientation".equals(data.get("orientationTag"))) {
					orientation = MediaItem.Orientation.LANDSCAPE;
				} else if ("portraitOrientation".equals(data.get("orientationTag"))) {
					orientation = MediaItem.Orientation.PORTRAIT;
				}
			}

			created = new MediaItem(data.get("path"), data.get("cMake"), data.get("cModel"),
				data.get("cSerial"), data.get("thumbnail"), data.get("parentDirectory"), data.get("filename"),
				data.get("originalFilename"), data.get("checksum"), data.get("fileChecksum"), data.get("dateChecksum"),
				data.get("filenameChecksum"),
				data.get("filesize")!=null?Long.valueOf(data.get("filesize")):null, orientation,
				data.get("isoSpeed")!=null?Integer.valueOf(data.get("isoSpeed")):null, data.get("lensInfo"),
				data.get("exposureBias")!=null?Double.valueOf(data.get("exposureBias")):null,
				data.get("aperture")!=null?Double.valueOf(data.get("aperture")):null,
				data.get("flash")!=null?Boolean.valueOf(data.get("flash")):null,
				data.get("focalLength")!=null?Double.valueOf(data.get("focalLength")):null,
				data.get("focalDistance")!=null?Double.valueOf(data.get("focalDistance")):null,
				data.get("focalPlaneDiagonal")!=null?Double.valueOf(data.get("focalPlaneDiagonal")):null,
				data.get("exposureTime")!=null?Double.valueOf(data.get("exposureTime")):null,
				data.get("temperature")!=null?Double.valueOf(data.get("temperature")):null,
				data.get("lightValue")!=null?Double.valueOf(data.get("lightValue")):null,
				data.get("depthOfField")!=null?Double.valueOf(data.get("depthOfField")):null,
				data.get("focusFrom")!=null?Double.valueOf(data.get("focusFrom")):null,
				data.get("focusTo")!=null?Double.valueOf(data.get("focusTo")):null,
				data.get("fieldOfView")!=null?Double.valueOf(data.get("fieldOfView")):null, data.get("digitalZoom"),
				data.get("whiteBalance")!=null?Integer.valueOf(data.get("whiteBalance")):null,
				data.get("comment"), data.get("keywords"),
				data.get("width")!=null?Integer.valueOf(data.get("width")):null,
				data.get("height")!=null?Integer.valueOf(data.get("height")):null, data.get("latRef"),
				data.get("latitude"), data.get("longRef"), data.get("longitude"), data.get("altRef"),
				data.get("altitude"), data.get("gpsDate"), data.get("datetimeOriginalExif"), data.get("timezoneID"));
		} catch (NullPointerException e) {
			logger.error("Could not create media item given the data: the information is incomplete " +
				"(one or more keys are missing)", e);
		} catch (FileNotFoundException ex) {
			logger.error("The file {} as retrieved from the store could not be found on disk", data.get("path"), ex);
		}
		return created;
	}

	/**
	 * Creates a semantic model for a media item
	 *
	 * @param mi the media item
	 * @return the new model representing the media item
	 */
	public Model createModelForMediaItem(MediaItem mi) {

		ModelBuilder builder = new ModelBuilder();
		store.getPrefixURIMap().entrySet().forEach(ns -> builder.setNamespace(ns.getKey(), ns.getValue()));
		builder.namedGraph(MemoOptions.get("store.graphURI"));

		//no spaces in URIs!
		//TODO: what about same filenames in different folders? maybe hava a combination make-model-datetime-filename
		String safename = mi.getSafeName();

		//TODO: what if this changes or is renamed?
		IRI item = store.getFactory().createIRI(MemoOptions.get("store.graphURI"), "Item-" + safename);
		IRI ifd = store.getFactory().createIRI(MemoOptions.get("store.graphURI"), "IFD-" + safename);
		IRI dateTime = store.getFactory().createIRI(MemoOptions.get("store.graphURI"), "DateTime-" + safename);
		IRI cam = store.getFactory().createIRI(MemoOptions.get("store.graphURI"), "Camera-" + mi.getCamera().getIdentifier());

		//general
		builder.subject(item)
			.add(RDF.TYPE, store.getFactory().createIRI(MemoOptions.get("store.memoURI"), "MediaItem"))
			.add("memo:hasIFD", ifd)
			.add("memo:hasDateTime", dateTime)
			.add("memo:shotWithCamera", cam)

			.add("memo:pathOnDisk", mi.getPath())
			.add("memo:parentDirectory", mi.getParentDirectory())
			.add("memo:filename", mi.getFilename())
			.add("memo:checksum", mi.getChecksum())
			.add("memo:fileChecksum", mi.getFileChecksum())
			.add("memo:dateChecksum", mi.getDateChecksum())
			.add("memo:filenameChecksum", mi.getFilenameChecksum())
			.add("memo:filesize", mi.getFilesize())

			.add("memo:isNormalised", mi.isNormalised())
			.add("memo:isStereo", mi.isStereo())
			.add("memo:isVideo", mi.isVideo());
		if (mi.getThumbnail()!=null) {
			builder.add("memo:thumbnailPath", mi.getThumbnail());
		}
		if (mi.getOriginalFilename()!= null) {
			builder.add("memo:originalFilename", mi.getOriginalFilename());
		}
		//pre-reasoning tagging
		if (mi.getFilename().toLowerCase().contains("hdr")) {
			builder.add(AUTOTAG, store.getFactory().createIRI(MemoOptions.get("store.memoURI"), "hdr"));
		}
		if (mi.getOrientation() == MediaItem.Orientation.PORTRAIT) {
			builder.add(AUTOTAG, store.getFactory().createIRI(MemoOptions.get("store.memoURI"), "portraitOrientation"));
		} else if (mi.getOrientation() == MediaItem.Orientation.LANDSCAPE) {
			builder.add(AUTOTAG, store.getFactory().createIRI(MemoOptions.get("store.memoURI"), "landscapeOrientation"));
		}

		//date/time
		builder.subject(dateTime)
			.add(RDF.TYPE, store.getFactory().createIRI(MemoOptions.get("store.memoURI"), "DateTime"))
			.add("memo:localDateTime",
				store.getFactory().createLiteral(mi.getXSDDateTime(false), getStore().getFactory().createIRI(XSD.dateTime.getURI()))
			)
			.add("memo:localTimestamp", Math.round(mi.getDatetimeOriginalExif().getMillis()/1000));
		if (mi.getGpsDate() != null) {
			builder.add("memo:utcDateTime", mi.getXSDDateTime(true));
			builder.add("memo:utcTimestamp", Math.round(mi.getGpsDate().getMillis()/1000));
		}
		if (mi.getTimezoneID() != null) {
			builder.add("memo:timezoneID", mi.getTimezoneID());
		}

		builder.subject(ifd)
			.add(RDF.TYPE, store.getFactory().createIRI(store.getPrefixURIMap().get("exif"), "IFD"));
		//GPS
		if (mi.getLatitude() != null) {
			builder.add("exif:gpsLatitude", mi.getLatitude());
		}
		if (mi.getLatRef() != null) {
			builder.add("exif:gpsLatitudeRef", mi.getLatRef());
		}
		if (mi.getLongitude() != null) {
			builder.add("exif:gpsLongitude", mi.getLongitude());
		}
		if (mi.getLongRef() != null) {
			builder.add("exif:gpsLongitudeRef", mi.getLongRef());
		}
		if (mi.getAltitude()!= null) {
			builder.add("exif:gpsAltitude", mi.getAltitude());
		}
		if (mi.getAltRef()!= null) {
			builder.add("exif:gpsAltitudeRef", mi.getAltRef());
		}
		//Other metadata
		if (mi.getComment()!= null) {
			builder.add("exif:userComment", mi.getComment());
		}
		if (mi.getFlash() != null) {
			builder.add("exif:flash", mi.getFlash());
		}
		if (mi.getAperture() != null) {
			builder.add("exif:apertureValue", mi.getAperture());
		}
		if (mi.getExposureTime()!= null) {
			builder.add("exif:shutterSpeedValue", mi.getExposureTime());
		}
		if (mi.getExposureTime() != null) {
			builder.add("exif:exposureTime", mi.getExposureTime());
		}
		if (mi.getExposureBias() != null) {
			builder.add("exif:exposureBiasValue", mi.getExposureBias());
		}
		if (mi.getIsoSpeed() != null) {
			builder.add("exif:isoSpeedRatings", mi.getIsoSpeed());
		}
		if (mi.getDigitalZoom() != null) {
			builder.add("exif:digitalZoomRatio", mi.getDigitalZoom());
		}
		if (mi.getFocalLength() != null) {
			builder.add("exif:focalLength", mi.getFocalLength());
		}
		if (mi.getWhiteBalance() != null) {
			builder.add("exif:whiteBalance", mi.getWhiteBalance());
		}
		if (mi.getHeight() != null) {
			builder.add("exif:imageLength", mi.getHeight());
		}
		if (mi.getWidth() != null) {
			builder.add("exif:imageWidth", mi.getWidth());
		}
		if (mi.getTemperature() != null) {
			builder.add("memo:temperature", mi.getTemperature());
		}
		if (mi.getLightValue() != null) {
			builder.add("memo:lightValue", mi.getLightValue());
		}
		if (mi.getFieldOfView() != null) {
			builder.add("memo:fieldOfView", mi.getFieldOfView());
		}
		if (mi.getDepthOfField() != null) {
			builder.add("memo:depthOfField", mi.getDepthOfField());
		}
		if (mi.getFocusFrom() != null) {
			builder.add("memo:focusFrom", mi.getFocusFrom());
		}
		if (mi.getFocusTo() != null) {
			builder.add("memo:focusTo", mi.getFocusTo());
		}
		if (mi.getFocalDistance() != null) {
			builder.add("memo:focalDistance", mi.getFocalDistance());
		}
		if (mi.getLensInfo()!= null) {
			builder.add("memo:lensInfo", mi.getLensInfo());
		}
		if (mi.getFocalPlaneDiagonal()!= null) {
			builder.add("memo:focalPlaneDiagonal", mi.getFocalPlaneDiagonal());
		}

		Model m = builder.build();

		//add camera
		m.addAll(createModelForCamera(mi.getCamera()));

		return m;
	}

	/**
	 * Creates a semantic model for a camera
	 *
	 * @param cam the camera
	 * @return the new model representing the camera
	 */
	public Model createModelForCamera(Camera cam) {

		ModelBuilder builder = new ModelBuilder();
		store.getPrefixURIMap().entrySet().forEach(ns -> builder.setNamespace(ns.getKey(), ns.getValue()));

		builder.namedGraph(MemoOptions.get("store.graphURI"))
			.subject(store.getFactory().createIRI(MemoOptions.get("store.graphURI"), "Camera-" + cam.getIdentifier()))
				.add(RDF.TYPE, store.getFactory().createIRI(MemoOptions.get("store.memoURI"), "Camera"));

		if (cam.getMake() != null) {
			builder.add("exif:make", cam.getMake());
		}
		if (cam.getModel() != null) {
			builder.add("exif:model", cam.getModel());
		}
		if (cam.getSerialNumber()!= null) {
			builder.add("memo:serialNumber", cam.getSerialNumber());
		}

		return builder.build();
	}

	// Add data ///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Add a coll,ection of photos to sherlock
	 *
	 * @param photos the photos to add
	 */
	public void addMediaItems(Collection<MediaItem> photos) {

		//init
		if (templates==null || templates.isEmpty()) {
			//get templates for later use
			templates = store.getAllTemplates(MemoOptions.get("store.memoURI"));
		}

		//TODO: move to a separate thread

		if (!photos.isEmpty()) {
			logger.debug("Adding media items to Sherlock");
			Model m = new LinkedHashModel();

			//add media items and their cameras to a temporary model
			photos.stream().forEach(p -> m.addAll(createModelForMediaItem(p)));

			//run the templates on a temporary graph (for performance)
			String tmpGraph = "http://binarywood.net/memo-tmp#";
			store.clearGraph(tmpGraph);
			store.storeModel(m, tmpGraph);
			//--calculations need to be run first as the calculated values might be needed for the classification
			m.addAll(store.runTemplatesOfClass("http://binarywood.net/memo#CalculationTemplate", templates, tmpGraph));
			//--classify media items (assign tags)
			m.addAll(store.runTemplatesOfClass("http://binarywood.net/memo#ClassificationTemplate", templates, tmpGraph));
			//remove the temporary graph
			store.clearGraph(tmpGraph);

			//save the full model in the store
			store.storeModel(m, MemoOptions.get("store.graphURI"));
		} else {
			logger.debug("Nothing to add");
		}
	}

	// Remove data ////////////////////////////////////////////////////////////////////////////////

	/**
	 * Clear the store. If graph is null clear the whole store, otherwise clear the given graph
	 *
	 * @param graph the URI of a graph in the store
	 */
	public void clearStore(String graph) {
		if (graph == null) {
			store.clear();
		} else {
			store.clearGraph(graph);
		}
	}

	/**
	 * Remove the given item from the KB
	 *
	 * @param pic the item to remove
	 */
	public void removePhoto(MediaItem pic) {
		//TODO: implement
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * Remove the given camera from the KB
	 *
	 * @param cam the item to remove
	 */
	public void removeCamera(Camera cam) {
		//TODO: implement
		throw new UnsupportedOperationException("Not supported yet.");
	}

	// I/O ////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Export the KB to file
	 *
	 * @param path the path where to save the RDF file
	 * @param graph the URI of a named graph within the KB. Will export everything if this is null.
	 */
	public void exportStore(String path, String graph) {
		store.export(graph, path, RDFFormat.RDFXML);
	}

	/**
	 * Adds the model to the given graph in the KB. Will use default graph if null.
	 *
	 * @param model the model to add
	 * @param graph the graph to add it to
	 */
	public void addToStore(Model model, String graph) {

		if (store != null) {

			store.connect();
			try {
				store.storeModel(model, graph);
			} catch (RepositoryException e) {
				logger.error("Could not store model (size {}) in graph <{}>", model.size(), graph, e);
			} finally {
				store.disconnect();
			}
		} else {
			logger.warn("Not connected to any store, cannot add model");
		}
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////

	/**
	 * Get the store wrapper for direct manipulations
	 *
	 * @return the store wrapper object
	 */
	public Store getStore() {
		return store;
	}
}
