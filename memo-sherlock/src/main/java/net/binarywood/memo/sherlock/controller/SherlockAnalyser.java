/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.sherlock.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.binarywood.memo.common.controller.DataReader;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class does the hard semantic work. It serves as a store I/O as well as a reasoning engine.
 *
 * @author Stefanie Wiegand
 */
public class SherlockAnalyser {

	private static final Logger logger = LoggerFactory.getLogger(SherlockAnalyser.class);
	private static volatile Sherlock sherlock;

	/**
	 * Create a new sherlock analyser with a separate repository under the ".analyser" path
	 */
	public SherlockAnalyser() {
		sherlock = new Sherlock(".analyser");
	}

	/**
	 * Run the classification of all testpictures in an ontology
	 *
	 * @param testpicturesDirectory the directory in which to search for the photos
	 * @param modelPath the desired name for the output ontologies containing the classified photos
	 */
	public void classifyPhotos(String testpicturesDirectory, String modelPath) {

		if (testpicturesDirectory!=null) {
			DataReader dr = new DataReader();
			dr.read(testpicturesDirectory, true, false, false, true);
			Collection<MediaItem> photos = dr.getMediaItems().values();
			sherlock.addMediaItems(photos);
			sherlock.getStore().export(MemoOptions.get("store.graphURI"), modelPath, RDFFormat.N3);

		} else {
			logger.error("Cannot classify photos: {} is not a valid path", testpicturesDirectory);
		}
	}

	/**
	 * Create an ontology for the feature dataset. Similar to normal photo classification but adds a label
	 * (in form of a manual tag) to the photo to record which feature it belongs to.
	 *
	 * @param testpicturesDirectory the directory in which to search for the photos. Needs to have one subdirectory
	 *								per feature
	 * @param outputFile where to save the ontology
	 */
	public void importFeatureDataset(String testpicturesDirectory, String outputFile) {

		DataReader dr = new DataReader();
		dr.read(testpicturesDirectory, true, false, false, true);
		Collection<MediaItem> photos = dr.getMediaItems().values();
		sherlock.addMediaItems(photos);

		ModelBuilder builder = new ModelBuilder();
		builder.setNamespace("", MemoOptions.get("store.graphURI"))
			.setNamespace("memo", MemoOptions.get("store.memoURI"))
			.setNamespace(RDF.NS);

		builder.namedGraph(MemoOptions.get("store.graphURI"));

		//attach feature label from directory name
		photos.stream().forEach(p -> {

			String safename = p.getFilename().replace(" ", "_");
			String photo = ":Photo-" + safename;

			String[] path = p.getPath().split(File.separator);
			String label = path[path.length-2];

			builder.subject(photo).add("memo:hasManualTag", label);
		});
		//TODO: use props URI
		sherlock.getStore().storeModel(builder.build(), MemoOptions.get("store.graphURI"));
		sherlock.getStore().export("http://binarywood.net/memo-instances", outputFile, RDFFormat.N3);
	}

	/**
	 * Run the feature analysis, by
	 *	- loading an ontology into memory which contains a feature dataset
	 *	- calculate the average value for each feature and the deviation from that standard value
	 * - write the results to a CSV file to be analysed further
	 *
	 * @param inputFile the ontology model containing the feature dataset photos
	 */
	public void doFeatureAnalysis(String inputFile) {

		//get manual tags from model
		String query ="SELECT DISTINCT (STR(?tag) as ?t) WHERE {\n" +
		"	?photo a memo:Photo .\n"	+
		"	?photo memo:hasManualTag ?tag .\n" +
		"}";
		List<Map<String, String>> tagMap = sherlock.getStore().querySelect(query);
		Set<String> tags = new HashSet<>();
		tagMap.stream().forEach(tag -> tags.add(tag.get("t")));
		logger.debug("Found tags {}", tags);

		//get low level features from model
		query ="SELECT ?f WHERE {\n" +
		"	?photo a memo:Photo .\n" +
		"	?photo memo:hasIFD ?ifd .\n" +
		"	?ifd ?f ?v .\n" +
		"}";
		List<Map<String, String>> featureMap = sherlock.getStore().querySelect(query);
		Set<String> features = new HashSet<>();
		featureMap.stream().forEach(f -> features.add(f.get("f")));
		logger.debug("Found features {}", features);

		Set<String> filterFeatures = new HashSet<>(Arrays.asList(
			"http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
			"http://www.w3.org/2003/12/exif/ns#gpsLatitude",
			"http://www.w3.org/2003/12/exif/ns#gpsLatitudeRef",
			"http://www.w3.org/2003/12/exif/ns#gpsLongitude",
			"http://www.w3.org/2003/12/exif/ns#gpsLongitudeRef",
			"http://www.w3.org/2003/12/exif/ns#make",
			"http://www.w3.org/2003/12/exif/ns#model",
			"http://www.w3.org/2003/12/exif/ns#isoSpeedRatings",
			"http://www.w3.org/2003/12/exif/ns#imageLength",
			"http://www.w3.org/2003/12/exif/ns#imageWidth",
			"http://www.w3.org/2003/12/exif/ns#shutterSpeedValue",
			"http://www.w3.org/2003/12/exif/ns#flash"
		));

		//iterate over tags/features
		List<Map<String, String>> csv = new ArrayList<>();
		List<Map<String, String>> overall = new ArrayList<>();
		//0 = false, 1 = true, 2 = any
		for (int flash = 0; flash<=2; flash++) {

			for (String f: features) {
				//filter features
				if (filterFeatures.contains(f)) {
					continue;
				}

				//get overall average
				query ="SELECT DISTINCT (STR(AVG(?value)) as ?v) (STR(COUNT(?value)) as ?num) WHERE {\n" +
				"	BIND(<" + f + "> as ?f) .\n" +
				"	?photo a memo:Photo .\n"	+
				"	?photo memo:hasIFD ?ifd .\n" +
				"	?ifd ?f ?value .\n" +
				"	?ifd exif:flash ";
				if (flash==0) {
					query += "false";
				} else if (flash==1) {
					query += "true";
				} else {
					query += "?flash";
				}
				query += " .\n" +
				"}";
				List<Map<String, String>> valueMap = sherlock.getStore().querySelect(query);
				Map<String, String> first = valueMap.iterator().next();
				Map<String, String> results = new HashMap<>();
				results.put("f", f);
				results.put("fl", String.valueOf(flash));
				results.put("num", first.get("num"));
				results.put("v", first.get("v"));
				overall.add(results);
				logger.debug("OVERALL FOR {}: value={} num={}, flash={}", f, first.get("v"), first.get("num"), flash);

				for (String t: tags) {
					//query
					query ="SELECT DISTINCT (STR(AVG(?value)) as ?v) (STR(COUNT(?value)) as ?num) WHERE {\n" +
					"	BIND(<" + f + "> as ?f) .\n" +
					"	?photo a memo:Photo .\n"	+
					"	?photo memo:hasManualTag \"" + t + "\" .\n" +
					"	?photo memo:hasIFD ?ifd .\n" +
					"	?ifd ?f ?value .\n" +
					"	?ifd exif:flash ";
					if (flash==0) {
						query += "false";
					} else if (flash==1) {
						query += "true";
					} else {
						query += "?flash";
					}
					query += " .\n" +
					"}";
					valueMap = sherlock.getStore().querySelect(query);
					first = valueMap.iterator().next();
					logger.debug("{} {}: value={} num={}, flash={}", f, t, first.get("v"), first.get("num"), flash);
					results = new HashMap<>();
					results.put("f", f);
					results.put("t", t);
					results.put("fl", String.valueOf(flash));
					results.put("num", first.get("num"));
					results.put("v", first.get("v"));
					//get overall for this combination
					for (Map<String, String> r: overall) {
						if (r.get("f").equals(f) && r.get("fl").equals(String.valueOf(flash))) {
							results.put("o", r.get("v"));
							results.put("oNum", r.get("num"));
							break;
						}
					}
					csv.add(results);
				}
			}
		}

		//write csv file:
		StringBuilder sb = new StringBuilder("tag,feature,flash,num,v,oNum,o\n");
		for (Map<String, String> row: csv) {
			if (row.get("v")==null || row.get("o")==null) {
				continue;
			}
			//TODO: stddev etc.
			sb.append(row.get("t"));
			sb.append(",");
			sb.append(row.get("f"));
			sb.append(",");
			sb.append(row.get("fl"));
			sb.append(",");
			sb.append(row.get("num"));
			sb.append(",");
			sb.append(row.get("v").replace("e0", ""));
			sb.append(",");
			sb.append(row.get("oNum"));
			sb.append(",");
			sb.append(row.get("o").replace("e0", ""));
			sb.append("\n");
		}

		try {
			String outputFile = inputFile.replace(".ttl", "-results.csv");

			try (PrintWriter out = new PrintWriter(outputFile, "UTF-8")) {
				out.write(sb.toString());
				logger.info("Wrote results to {}", outputFile);
			}
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			logger.error("Could not write CSV file, printing results instead", e);
			logger.debug("{}", sb.toString());
		}
	}

	/**
	 * Query the model for all tags photos in this ontology have

	 * @return the result set the query returned or null if something went wrong
	 */
	public List<Map<String, String>> doQuery() {

		String query ="SELECT * WHERE {\n" +
		"	?photo a memo:MediaItem .\n"	+
		"	OPTIONAL {\n" +
		"		?photo ?prop ?tag .\n" +
		"		?prop rdfs:subPropertyOf* memo:hasTag .\n" +
		"	} .\n" +
		"}";
		List<Map<String, String>> result = sherlock.getStore().querySelect(query);
		logger.info("\n{}", result);

		return result;
	}

}
