/*
 * Copyright 2016 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * This file is based on https://github.com/it-innovation/EasyJena/blob/master/src/main/java/uk/ac/soton/
 * itinnovation/easyjena/core/spec/AStoreWrapper.java
 */
package net.binarywood.memo.sherlock.store;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides the basis for store wrapper implementations.
 * A store wrapper is a connection to *one* store.
 *
 * @author Stefanie Wiegand
 */
public abstract class AStoreWrapper implements IStoreWrapper {

	private static final Logger logger = LoggerFactory.getLogger(AStoreWrapper.class);

	protected Map<String, String> prefixURIMap = new HashMap<>();
	protected String sparqlPrefixes = "";

	/**
	 * It is highly recommended to call this constructor in any implementing classes' constructor.
	 * This preloads a number of commonly used prefixes for easier querying.
	 */
	protected AStoreWrapper() {

		//add a couple of standard prefixes
		prefixURIMap.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		prefixURIMap.put("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		prefixURIMap.put("xsd", "http://www.w3.org/2001/XMLSchema#");
		prefixURIMap.put("spin", "http://spinrdf.org/spin#");
		prefixURIMap.put("sp", "http://spinrdf.org/sp#");
		prefixURIMap.put("owl", "http://www.w3.org/2002/07/owl#");
		prefixURIMap.put("fn", "http://www.w3.org/2005/xpath-functions#");
		prefixURIMap.put("spl", "http://spinrdf.org/spl#");
		prefixURIMap.put("exif", "http://www.w3.org/2003/12/exif/ns#");
	}

	// General actions ////////////////////////////////////////////////////////////////////////////

	@Override
	public Object querySelect(String sparql) {
		logger.error("SELECT queries currently not supported for store of type {}", this.getClass());
		return null;
	}

	@Override
	public Object queryConstruct(String sparql) {
		logger.error("CONSTRUCT queries currently not supported for store of type {}", this.getClass());
		return null;
	}

	@Override
	public Object queryDescribe(String sparql) {
		logger.error("DESCRIBE queries currently not supported for store of type {}", this.getClass());
		return null;
	}

	@Override
	public Boolean queryAsk(String sparql) {
		logger.error("ASK queries currently not supported for store of type {}", this.getClass());
		return false;
	}

	@Override
	public void update(String sparql) {
		logger.error("UPDATE queries currently not supported for store of type {}", this.getClass());
	}

	@Override
	public void shutdown() {

		if (isConnected()) {
			disconnect();
		}
	}

	// Getters/Setters ////////////////////////////////////////////////////////////////////////////
	@Override
	public String getSPARQLPrefixes() {
		StringBuilder builder = new StringBuilder();
		prefixURIMap.entrySet().stream().filter(e -> e.getKey() != null && e.getValue() != null).forEach(
			e -> builder.append("PREFIX " + e.getKey() + ":<" + e.getValue() + ">\n")
		);
		sparqlPrefixes = builder.toString();
		return sparqlPrefixes;
	}

	@Override
	public Map<String, String> getPrefixURIMap() {
		return prefixURIMap;
	}

}

