/*
 * Copyright 2016 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * This file is based on https://github.com/it-innovation/EasyJena/blob/master/src/main/java/uk/ac/soton/
 * itinnovation/easyjena/core/spec/IStoreWrapper.java
 */
package net.binarywood.memo.sherlock.store;

import java.util.Map;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.RDFFormat;

/**
 * This interface specifies a triple store, in which ontology models can be saved for further processing.
 * It can either be a using native libraries or SPARQL endpoints for accessing the store.
 */
public interface IStoreWrapper {

	// General actions ////////////////////////////////////////////////////////////////////////////

	/**
	 * Start a connection to the store.
	 * This does not require a specific graph to be selected yet, but a store implementation
	 * may choose to preselect a default graph.
	 */
	void connect();

	/**
	 * End the connection to the store. Release all resources.
	 */
	void disconnect();

	/**
	 * Find out if there is an active connection to the store at this instant.
	 *
	 * @return whether it is connected
	 */
	boolean isConnected();

	/**
	 * Loads all prefixes used in the store and saves them to the prefixes string and namespace map.
	 * This makes it unnecessary for the user to specify all prefixes manually in each SPARQL query.
	 */
	void loadNamespaces();

	/**
	 * Clears the entire repository
	 */
	void clear();

	/**
	 * Counts all statements in the repository, all grpahs combined
	 *
	 * @return the amount of statements contained in the entire repository
	 */
	long size();

	/**
	 * Export the entire repository and save it to a file
	 *
	 * @param path the path where to save the file
	 * @param format the format of the file
	 */
	void export(String path, RDFFormat format);

	/**
	 * Shuts down the store, disconnecting if it was connected
	 */
	void shutdown();

	/**
	 * Create a new ontology model
	 *
	 * @param baseURI the baseURI for this model (including the / or # separator)
	 * @param comment a comment for the ontology
	 * @param imports all imports as they would appear in the imports section, i.e. without the separator
	 * @return the model containing the new ontology
	 */
	public Model createOntologyModel(String baseURI, String comment, String ... imports) ;

	// Graph management ///////////////////////////////////////////////////////////////////////////

	/**
	 * Checks for the existence of a given graph
	 *
	 * @param graphURI the ID of the graph to check for
	 * @return whether the graph exists
	 */
	boolean graphExists(String graphURI);

	/**
	 * Clears the store which means it is reset to its original state, containing no triples or other content.
	 *
	 * @param graphURI the graph to be cleared or null to clear all triples that are not in a graph
	 */
	void clearGraph(String graphURI);

	/**
	 * Return the amount of triples contained in the store
	 *
	 * @param graphURI the graph for which to count
	 * @return the amount of triples
	 */
	long size(String graphURI);

	// Querying ///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Executes a SELECT query and returns the results in a persistent form
	 *
	 * @param sparql the SPARQL SELECT query (doesn't need prefix statements if previously specified)
	 * @return the results of the query, in whatever form the implementing class chooses
	 */
	Object querySelect(String sparql);

	/**
	 * Executes a SELECT query on a particular graph
	 *
	 * @param sparql the SPARQL SELECT query (doesn't need prefix statements if previously specified)
	 * @param graphURI the URI of the graph on which to execute the query
	 * @return the results of the query, in whatever form the implementing class chooses
	 */
	Object querySelect(String sparql, String graphURI);

	/**
	 * Executes a CONSTRUCT query
	 *
	 * @param sparql the SPARQL CONSTRUCT query (doesn't need prefix statements if previously specified)
	 * @return the constructed triples in whatever form the implementing class chooses
	 */
	Object queryConstruct(String sparql);

	/**
	 * Executes a CONSTRUCT query on a particular graph
	 *
	 * @param sparql the SPARQL CONSTRUCT query (doesn't need prefix statements if previously specified)
	 * @param graphURI the URI of the graph on which to execute the query
	 * @return the constructed triples in whatever form the implementing class chooses
	 */
	Object queryConstruct(String sparql, String graphURI);

	/**
	 * Executes a DESCRIBE query
	 *
	 * @param sparql the SPARQL DESCRIBE query (doesn't need prefix statements if previously specified)
	 * @return the results of the query in whatever form the implementing class chooses
	 */
	Object queryDescribe(String sparql);

	/**
	 * Executes a DESCRIBE query on a particular graph
	 *
	 * @param sparql the SPARQL DESCRIBE query (doesn't need prefix statements if previously specified)
	 * @param graphURI the URI of the graph on which to execute the query
	 * @return the results of the query in whatever form the implementing class chooses
	 */
	Object queryDescribe(String sparql, String graphURI);

	/**
	 * Executes a ASK query
	 *
	 * @param sparql the SPARQL ASK query (doesn't need prefix statements if previously specified)
	 * @return the results of the query
	 */
	Boolean queryAsk(String sparql);

	/**
	 * Executes a ASK query on a particular graph
	 *
	 * @param sparql the SPARQL ASK query (doesn't need prefix statements if previously specified)
	 * @param graphURI the URI of the graph on which to execute the query
	 * @return the results of the query
	 */
	Boolean queryAsk(String sparql, String graphURI);

	/**
	 * Runs an update (e.g. INSERT, DELETE, ...) on a repository
	 *
	 * @param sparql the update to run
	 */
	void update(String sparql);

	/**
	 * Runs an update (e.g. INSERT, DELETE, ...) on a particular graph
	 *
	 * @param sparql the update to run
	 * @param graphURI the URI of the graph on which to execute the query
	 */
	void update(String sparql, String graphURI);

	// I/O ////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Imports a local document into the store
	 *
	 * @param path the absolute path of the document
	 * @param baseURI the baseURI of the ontology contained in the document
	 * @param graphURI the URI of the graph into which the document is to be saved
	 * @param format the format of the document
	 */
	void importLocalDocument(String path, String baseURI, String graphURI, org.eclipse.rdf4j.rio.RDFFormat format);

	/**
	 * Imports a remote document into the store
	 *
	 * @param url the URL of the document
	 * @param baseURI the baseURI of the ontology contained in the document
	 * @param graphURI the URI of the graph into which the document is to be saved
	 * @param format the format of the document
	 */
	void importRemoteDocument(String url, String baseURI, String graphURI, org.eclipse.rdf4j.rio.RDFFormat format);

	/**
	 * Stores a single triple in the store. Since this method should be a transaction,
	 * it is discouraged from using it for large amounts of triples.
	 *
	 * @param stmt the triple to be stored
	 * @param graphURI the URI of the graph; default graph if this is null
	 */
	void storeStatement(Statement stmt, String graphURI);

	/**
	 * Remove a specific triple from the store
	 *
	 * @param stmt the triple to remove
	 * @param graphURI the URI of the graph; default graph if this is null
	 */
	void removeStatement(Statement stmt, String graphURI);

	/**
	 * Stores a model in the store
	 *
	 * @param m the model to store
	 * @param graphURI the URI of the graph; default graph if this is null
	 */
	void storeModel(Model m, String graphURI);

	/**
	 * Removes a model from the store
	 *
	 * @param m the model to remove
	 * @param graphURI the URI of the graph; default graph if this is null
	 */
	void removeModel(Model m, String graphURI);

	/**
	 * Get the RDF representation of the contents of the repository
	 *
	 * @param graphURI the URI of the graph
	 * @param format the format for the exported file
	 * @param path the path where to save the exported file
	 */
	void export(String graphURI, String path, RDFFormat format);

	// Simple Getters /////////////////////////////////////////////////////////////////////////////

	/**
	 * Get a String containing of all the prefixes used in the store for querying as they would
	 * appear at the beginning of each SPARQL query
	 *
	 * @return the prefixes
	 */
	String getSPARQLPrefixes();

	/**
	 * Get the namespace mappings used in the store
	 *
	 * @return the namespace mapping
	 */
	Map<String, String> getPrefixURIMap();

}

