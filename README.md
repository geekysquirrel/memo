###########################
# MemO - Memory Organiser #
###########################

# Requirements
* Java8 run-time environment version 8u66 or above
* Exiftool installed on your system

# Installation

## From package
The package you downloaded should contain everything you need to run MemO.
Unpack the zip file and navigate to the root of the unzipped directory.
No installation is necessary.

## Build using maven
If you cloned or downloaded MemO using Git you can build it by navigating to the root of the MemO directory and typing

```mvn clean install```

# Usage
Double click the jar file of the module you want to run or open a command line window and execute

```java -jar memo-modulename-version.jar```

MemO can be used for different tasks around the topic of photo management. The following sections describe my workflow. There are question mark icons throughout the tool suite, which contain tooltips with (hopefully) helpful information.

Before you start, **MAKE A COPY** of your photos and videos! Never run any tool on your original data! This is essential. Any software can contain bugs and I am not responsible for any damage you do to your data.

## MemO-Yoshihiko
I keep a record of all my cameras and the cameras of friends of family, from which I possess photos. If you're a geek like me, the easiest way is to maintain an XML file. However, Yoshihiko represents a GUI for this particular task.

Run ```java -jar memo-yoshihiko-version.jar``` and you see an empty window. You can then import an XML file that you have created (with or without MemO) earlier.

Each camera has an "Edit" button that brings you to its details window. You can add a serial number (to keep different cameras of the same make/model apart) and record details relating to your camera's settings, location and owner.

Don't forget to save your XML file once you're done.

## MemO-Adrian
Adrian shows the steps required for sorting photos in the order I think makes most sense. You should try to follow the steps instead of jumpin in between them, as this may have undesired side-effects.

Select the directory you want to use. It is recommended to use the "enhance metadata" option in case your photos are missing metadata (this often happens with videos and panorama photos).

The cameras displayed are the ones found when scanning your photos. You can add information manually or import an existing XML file. Note that if your have added/modified the serial number, you have to merge the affected cameras. You can do this by selecting them (left click) and once they are selected, right-click any one of them to merge. Select the appropriate metadata in the dialog. If you're not sure which camera is which, you can click the "Media" button to show a selection of photos/videos taken with the camera.

If you have already added all information about the cameras' locations, you can continue in the following window without changing anything. I add all the settings in the camera details but leave the trip details until here as it's easier to see at a glance where the trip took you and which cameras were involved in which steps.

To align photos (e.g. if you have a camera without the date set correctly) you have to select a reference camera. You can then link cameras together using photos that were taken at the same time (which is a great way to do this!). You don't need to do this for all cameras, just the ones you want to align. If a camera is aligned, its symbol will light up.

Next you can choose to geotag your photos. Typically, the GPX files have been recorded using one of your devices, and this is the one that should be the reference camera in the previous step. Its geographic information can be used for all your other photos to geotag them if there are GPS coordinates for the time at which a photo was taken. All of this happens automatically behind the scenes.

Finally, you can decide what to do with all the information you're about to get. Again, note that your should **MAKE A COPY** of your data before doing this. You can write the information straight back into the files. This happens in a way that makes it possible to access it from other software. If you want to sort your photos, you really need to add a filename prefix as this is the only reliable way to sort data on any device (simply alphabetically using a UTC timestamp). You can choose to remove any subfolders in which you kept you data to see all photos "serialised", ordered by time. If you have added any information to your cameras, it's wise to export it here so that you can re-use it in the future.

That's it. Now press the button and watch the magic happen :)

## Memo-Sherlock
By itself, the command line tools that Sherlock provides are more for expert users. There are tools to tag photos and run an analysis, the results of which hopefully lead to more insights about the metadata. To use all these clever bits, you better ignore this module and run Leonardo.If you're feeling brave, you can dive straight into the javadoc.

## Memo-Leonardo
This is the part that keeps it all together. Leonardo provides a user interface to browse, load, manage, filter and edit photos. It is in its early stages and does not yet have all the functionality that it will hopefully have eventually. Still it's a useful tool to find photos easily by applying a number of automatic filters to all imported photos.

# Why?
Have you ever been on holidays with somebody? Taken more than one camera/phone? Been to a country with a different timezone, perhaps even crossed one or more during your holiday? Like tracking location (think geocaching, sat nav, sports,...) and look at the results?
All this generates data. Lots of it. But it's not tidy data. Some devices may be kept up to date and have the correct time, timezone etc. Others might not. After your holiday you come home and get everybody's pictures. But they're **so** not sorted. They have different filenames and timezones and can't be ordered on a timeline. So you end up processing them separately, you'll inevitable print duplicates and can't find a certain photo you remember in a reasonable time because you forgot which camera you used to take it.
Some people don't care but I do and probably you do too, otherwise you wouldn't be reading this :)

# What?
To me, my photos are my most valuable possession. They are the priceless memories I imagine I will look at when I'm old. We take many photos but very rarely we do anything with them. Digital photos are not just photos, they come with all kinds of data attached. So really they're a diary. They contain everything you need for fancy things like a map of where you've been (potentially heatmaps, network graphs or other fancy stuff) or stats about the things you photographed most, your favourite camera modes, graphs of photos by time, your imagination is the limit. This project aims at making use of that data.

**MemO** is short for "Memory Organiser" because this is what it does. I created it first for sorting photos but the deeper I dug into it the more problems with sorting i discovered. In a perfect world, the EXIF standard would record UTC timestamps. Yes you heard me, it doesn't. It's really up to developers of camera software to maintain their metadata but unfortunately support for standards is still flaky. The day that a new, improved EXIF standard is widely used, which records UTC (or whatever standard comes after it) this software will be redundant. But for now it is not and I need a solution NOW. Around this need, I started developing this toolset and keep adding bits to it that I want but cannot find in other software that I consider using.

# Complaints
This is my private software project, which I started end of 2014. I work on it as much as I can but I _do_ have a private life which is more important to me.

I released **MemO** in the hope that it might be useful to others just like it is useful to me. However, it was written with myself as a user in mind. It started as a collection of small scripts and tools but soon evolved into what it is today. I have zero interest in replacing other software. If software X has a nicer UI or software Y does this cool thing then use them. I don't want to clutter **MemO** with things I don't really need. Along the same lines, yes, it could have been written in language ABC, used technology 123 or provide interfaces for tool XYZ but I have no interest to support this. Why Java FX? Because I like it and wanted to get better at using it. The tool is deliberately designed as a Desktop application. Your photos. No cloud, synchronisation, sharing or social integration. Just organising photos. You are welcome :)

# Architecture

## MemO
This is the main project; the full monty if you like. It doesn't actually do much other than keeping it all together.

## MemO-common
This contains common classes and components that are used across the **MemO** project.

## MemO-Yoshihiko
This is the camera manager module. It can be used on its own or as part of other modules.
Named after Yoshihiko Amino. He was a Japanese historian. Many camera brands are Japanese. Historians sort things in timelines. That's all there is to it :)

## MemO-Adrian
Sorting is cool. Tidy is nice. This module will sort your pictures, no matter where they came from. It's not magic unfortunately so you'll need to feed it with some information, e.g. what the setting of your camera was, when you changed it, where you were at all or what photos were taken at the same time. While this might seem tedious, I can assure you it takes a lot longer to sort a holiday dump of say 1500 photos from 2 cameras and 3 phones manually.
Named after Adrian Monk, master of order and tidiness.

## MemO-Sherlock
That's the clever bit. It does the semantic reasoning, creates RDF models from your photos and then infers all kinds of things. Then, it's used to retrieve photos based on the tagging and inference. It could for example find out which of your photos have been taken at night in Spain. Maybe all the photos you took while flying or on the beach or all the pictures you took on Saturday afternoons.
This module is an API which is used by other parts; it doesn't come with a GUI.
Named after Sherlock Holmes obviously.

## MemO-Leonardo
This is the suite. It's reasonably pretty and does it all. Sort pictures, import/export, do queries, create stereocards,... All the tools in one place. Deliberately not in the cloud - you want to share your highly personal data with only yourself.
Named after Leonardo DaVinci, who also did everything and anything.

## MemO-Santa
To create a full release, I made this module. It aggregates all the dependencies, resources, docs and executables and packages them nicely in one big file.
Ho ho ho!

# IPR information

I used the following code in MemO:

* I adapted **file tree view** by Hugues Johnson (GPL)
* **jGPX** - Martin Jansen (http://opensource.org/licenses/BSD-3-Clause)
* **OSM wrapper API** - Jens Kuebler (Public domain)
* the store is loosely based on **EasyJena** - IT Innovation Centre (LGPL 2.1)

The following resources are dependencies:

## MemO common

* The **MemO logo** is based on the "polaroid photo with heart" picture which was designed by Freepik (Free license (with attribution)))

* The **Escher icon** was downloaded from publicdomainvectors.org (Public domain)

* All other icons come from the **gnome-colors-common** project (GPL2)

* Logging is done with **slf4j** (MIT license) and **logback** (LGPL 2.1), anhanced by **logback-prettier** (Apache license 2.0)

* The time calculations are done using **joda-time** (Apache license 2.0)

* For reading photo metadata, MemO uses **metadata-extractor** (Apache license 2.0) and **exiftool** (Perl license), aided by **im4java** (LGPL 2.1)

* The thumbnails are created with **imgscalr** (Apache license 2.0)

* Geographic location is enhanced by **geonames** (Apache license 2.0)

* The tests are done with **junit** (Eclipse Public License 1.0)

* Data is (de-)serialised with **json** (JSON license)

* Various **apache commons** packages (Apache license 2.0) are used: commons-io, commons-codec, commons-configuration, commons-collections

## MemO-Yoshihiko

All resources from MemO-common are used.

## MemO-Adrian

All resources from MemO-Yoshihiko are used.

## MemO-Sherlock

All resources from MemO-common are used.

* For feature extraction I'm planning to use **LIRE** (GPL2)

## MemO-Leonardo

All resources from MemO-Adrian and MemO-Sherlock are used.

* the double slider is from **ControlsFX** (http://opensource.org/licenses/BSD-3-Clause)
* I'm using **OpenLayers3** for the map visualisation (https://github.com/openlayers/openlayers/blob/master/LICENSE.md)
* For speed I've included copies of the following libraries:
	- jQuery 2.2.4 (jquery.org/license)
	- bootstrap (MIT license)
	- polyfill (MIT license) - this had to be loaded locally as it would not be triggered by JavaFX's built-in version of webkit

## MemO-Santa

All resources mentioned above are packaged in this module. It does not actually use any of them.

# Documentation

If this all wasn't enough for you yet and you want to dive deeper into the code, check out the javadoc for

	* [MemO-common](./memo-common/index.html)
	* [MemO-Yoshihiko](./memo-yoshihiko/index.html)
	* [MemO-Adrian](./memo-adrian/index.html)
	* [MemO-Sherlock](./memo-sherlock/index.html)
	* [MemO-Leonardo](./memo-leonardo/index.html)

