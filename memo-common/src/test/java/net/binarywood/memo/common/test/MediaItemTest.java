/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import net.binarywood.memo.common.model.MediaItem;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Just a test file for local component tests
 *
 * @author Stefanie Wiegand
 */
@RunWith(JUnit4.class)
public class MediaItemTest {

	private static final Logger logger = LoggerFactory.getLogger(MediaItemTest.class);

	@Rule public TestName name = new TestName();

	@BeforeClass
    public static void beforeClass() {}

	@Before
	public void before() {}

	@AfterClass
	public static void afterClass() {}

	public MediaItemTest() {}

	// Tests //////////////////////////////////////////////////////////////////////////////////////

	@Test
	public void testLoad() {

		logger.info("Running test {}", name.getMethodName());

		MediaItem m;
		String filename;
		URL url;
		File file;

		try {
			filename = "testpictures/PP228276.JPG";
			url = MediaItemTest.class.getClassLoader().getResource(filename);
			file = new File(url.getPath());

			//without exiftool (tests that would fail commented out. the goal is to improve the class so that
			//all these tests succeed without having to use exiftool)
			m = new MediaItem(file.getPath(), false, false, false);
			assertNotNull(m);
			assertTrue(m.getPath().endsWith("/testpictures/PP228276.JPG"));
			assertTrue(m.getParentDirectory().endsWith("/testpictures"));
			assertEquals("PP228276.JPG", m.getFilename());
			//don't test checksum as it depends on the file date which changes on each build
			//assertEquals("c94f051022d6a5e0ca839dda2a2095b4-11f4d778ab06be69da6886ac5122436--4f11d7dd7fe6dea7464dda0e780e1ea1", m.getChecksum());
			assertEquals(1913402l, (long) m.getFilesize());
			assertTrue(!m.isStereo());
			assertTrue(!m.isVideo());
			assertTrue(!m.isGeotagged());
			assertTrue(!m.isNormalised());
			assertTrue((m.getThumbnail()!=null)==m.thumbnailGenerated());
			assertTrue(m.hasLandscapeOrientation());
			assertEquals("2015-05-22T00:30:34.000+01:00", m.getDatetimeOriginalExif().toString());
			assertEquals("2015-05-22T00:30:34.000+01:00", m.getDatetimeDigitisedExif().toString());
			assertEquals("2015-05-22T00:30:34.000+01:00", m.getDatetimeExif().toString());
			assertEquals(9, m.getMetadata().getDirectoryCount());
			assertEquals(MediaItem.Orientation.LANDSCAPE, m.getOrientation());
			assertEquals(100, (int) m.getIsoSpeed());
			assertEquals(-1.0, m.getExposureBias(), 0);
			assertEquals(9.5, m.getAperture(), 0);
			assertEquals(false, m.getFlash());
			assertEquals(14.0, m.getFocalLength(), 0);
			//assertEquals(1.7976931348623157E308, m.getFocalDistance(), 0);
			//assertTrue(m.getHyperfocalDistance().isInfinite());
			assertEquals(21.6, m.getFocalPlaneDiagonal(), 0);
			assertEquals(0.001391030584669749 , m.getCircleOfConfusion(), 0);
			assertEquals(10.0, m.getExposureTime(), 0);
			//assertEquals(36.3, m.getTemperature(), 0);
			//assertEquals(4.173926931999809, m.getLightValue(), 0);
			//assertEquals(-1.0, m.getDepthOfField(), 0);
			//assertEquals(1.38, m.getFocusFrom(), 0);
			//assertEquals(1.7976931348623157E308, m.getFocusTo(), 0);
			//assertEquals(65.4, m.getFieldOfView(), 0);
			assertEquals(5300, Double.valueOf(m.getWhiteBalance()), 0);
			assertEquals("", m.getComment());
			assertEquals(3648, m.getWidth(), 0);
			assertEquals(2736, m.getHeight(), 0);

			//with exiftool
			m = new MediaItem(file.getPath(), false, false, true);
			assertNotNull(m);
			assertTrue(m.getPath().endsWith("/testpictures/PP228276.JPG"));
			assertTrue(m.getParentDirectory().endsWith("/testpictures"));
			assertEquals("PP228276.JPG", m.getFilename());
			//don't test checksum as it depends on the file date which changes on each build
			//assertEquals("c94f051022d6a5e0ca839dda2a2095b4-11f4d778ab06be69da6886ac5122436--4f11d7dd7fe6dea7464dda0e780e1ea1", m.getChecksum());
			assertEquals(1913402l, (long) m.getFilesize());
			assertTrue(!m.isStereo());
			assertTrue(!m.isVideo());
			assertTrue(!m.isGeotagged());
			assertTrue(!m.isNormalised());
			assertTrue((m.getThumbnail()!=null)==m.thumbnailGenerated());
			assertTrue(m.hasLandscapeOrientation());
			assertEquals("2015-05-22T00:30:34.000+01:00", m.getDatetimeOriginalExif().toString());
			assertEquals("2015-05-22T00:30:34.000+01:00", m.getDatetimeDigitisedExif().toString());
			assertEquals("2015-05-22T00:30:34.000+01:00", m.getDatetimeExif().toString());
			assertEquals(9, m.getMetadata().getDirectoryCount());
			assertEquals(MediaItem.Orientation.LANDSCAPE, m.getOrientation());
			assertEquals(100, (int) m.getIsoSpeed());
			assertEquals(-1.0, m.getExposureBias(), 0);
			assertEquals(9.5, m.getAperture(), 0);
			assertEquals(false, m.getFlash());
			assertEquals(14.0, m.getFocalLength(), 0);
			assertEquals(1.7976931348623157E308, m.getFocalDistance(), 0);
			assertTrue(m.getHyperfocalDistance().isInfinite());
			assertEquals(21.6, m.getFocalPlaneDiagonal(), 0);
			assertEquals(0.001391030584669749 , m.getCircleOfConfusion(), 0);
			assertEquals(10.0, m.getExposureTime(), 0);
			assertEquals(36.3, m.getTemperature(), 0);
			assertEquals(4.173926931999809, m.getLightValue(), 0);
			assertEquals(-1.0, m.getDepthOfField(), 0);
			assertEquals(1.38, m.getFocusFrom(), 0);
			assertEquals(1.7976931348623157E308, m.getFocusTo(), 0);
			assertEquals(65.4, m.getFieldOfView(), 0);
			assertEquals(5300, Double.valueOf(m.getWhiteBalance()), 0);
			assertEquals("", m.getComment());
			assertEquals(3648, m.getWidth(), 0);
			assertEquals(2736, m.getHeight(), 0);

			//TODO: from store

			filename = "testpictures/P8190014.JPG";
			url = MediaItemTest.class.getClassLoader().getResource(filename);
			file = new File(url.getPath());
			m = new MediaItem(file.getPath(), false, false, false);
			logger.debug("{}", m.getString());
			m.printAllMetadata();

		} catch (FileNotFoundException e) {
			logger.error("Could not load test file", e);
			fail("Could not load test file");
		}
	}

	@Test
	public void testParseGPS() {
		logger.info("Running test {}", name.getMethodName());

		MediaItem m;
		String filename;
		URL url;
		File file;

		try {
			filename = "testpictures/IMG_20141119_191715.jpg";
			url = MediaItemTest.class.getClassLoader().getResource(filename);
			file = new File(url.getPath());

			//with exiftool
			m = new MediaItem(file.getPath(), false, false, true);
			assertNotNull(m);

			assertEquals(50.84677222222223, m.getLatitudeNum(), 0);
			assertEquals(4.352708333333333, m.getLongitudeNum(), 0);

		} catch (FileNotFoundException e) {
			logger.error("Could not load test file", e);
			fail("Could not load test file");
		}
	}
}
