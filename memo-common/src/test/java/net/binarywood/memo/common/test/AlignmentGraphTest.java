/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.test;

import net.binarywood.memo.common.exceptions.EdgeOverrideException;
import net.binarywood.memo.common.model.AlignmentGraph;
import net.binarywood.memo.common.model.GraphNode;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Stefanie Wiegand
 */
@RunWith(JUnit4.class)
public class AlignmentGraphTest {

	private static final Logger logger = LoggerFactory.getLogger(AlignmentGraphTest.class);

	@Rule public TestName name = new TestName();

	private AlignmentGraph dwg;
	private final GraphNode n1;
	private final GraphNode n2;
	private final GraphNode n3;
	private final GraphNode n4;
	private final GraphNode n5;
	private final GraphNode n6;
	private final GraphNode n7;

	public AlignmentGraphTest() {

		n1 = new GraphNode("Node1");
		n2 = new GraphNode("Node2");
		n3 = new GraphNode("Node3");
		n4 = new GraphNode("Node4");
		n5 = new GraphNode("Node5");
		n6 = new GraphNode("Node6");
		n7 = new GraphNode("Node7");

		try {
			n1.linkToNode(n2, 1L);
			n1.linkToNode(n5, 2L);
			n2.linkToNode(n3, -3L);
			n3.linkToNode(n4, 8L);
			n3.linkToNode(n6, 16L);
			n6.linkToNode(n7, 32L);
		} catch (EdgeOverrideException e) {
			logger.warn("Graph setup might be faulty: edges are overriding each other");
		}
	}

	@Before
	public void setUp() {
		dwg = new AlignmentGraph();
	}

	// Tests //////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Test of addNode method, of class AlignmentGraph.
	 */
	@Test
	public void testAddNode() {

		logger.info("Running test {}", name.getMethodName());

		dwg.addNode(n1);
		if (dwg.isEmpty()) {
			fail("Could not add node to AlignmentGraph");
		}
	}

	/**
	 * Test of removeNode method, of class AlignmentGraph.
	 */
	@Test
	public void testRemoveNode() {

		logger.info("Running test {}", name.getMethodName());

		dwg.addNode(n1);
		if (dwg.isEmpty()) {
			fail("Could not add node to AlignmentGraph");
		}
		dwg.removeNode(n1);
		if (!dwg.isEmpty()) {
			fail("Could not remove node from AlignmentGraph");
		}
	}

	/**
	 * Test of distance method, of class AlignmentGraph.
	 */
	@Test
	public void testDistance() {

		logger.info("Running test {}", name.getMethodName());

		dwg.addNode(n1);
		dwg.addNode(n2);
		dwg.addNode(n3);
		dwg.addNode(n4);
		dwg.addNode(n5);
		dwg.addNode(n6);
		dwg.addNode(n7);

		long d1 = dwg.distance(n1.getId(), n4.getId());
		long d2 = dwg.distance(n1.getId(), n6.getId());
		long d3 = dwg.distance(n2.getId(), n7.getId());

		assertEquals("", 6L, d1);
		assertEquals("", 14L, d2);
		assertEquals("", 45L, d3);
	}

	/**
	 * Test of getNode method, of class AlignmentGraph.
	 */
	@Test
	public void testGetNode() {

		logger.info("Running test {}", name.getMethodName());

		dwg.addNode(n3);
		GraphNode result = dwg.getNode(n3.getId());
		assertEquals(n3, result);
	}

	/**
	 * Test of isEmpty method, of class AlignmentGraph.
	 */
	@Test
	public void testIsEmpty() {

		logger.info("Running test {}", name.getMethodName());
		
		if (!dwg.isEmpty()) {
			fail("New graph wasn't empty");
		}
		dwg.addNode(n4);
		dwg.removeNode(n4);
		if (!dwg.isEmpty()) {
			fail("Graph wasn't empty after removing last node");
		}
	}
}
