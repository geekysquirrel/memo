/*
 * Copyright 2016 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * This is a test suite for the MemO-common module
 *
 * @author Stefanie Wiegand
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	MediaItemTest.class,
	AlignmentGraphTest.class
})
public class TestSuite {}
