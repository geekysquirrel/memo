/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.events;

import javafx.event.ActionEvent;
import javafx.event.EventTarget;
import net.binarywood.memo.common.components.TimezoneHistoryEntry;

/**
 * An event for deleting a timezone history entry
 *
 * @author Stefanie Wiegand
 */
public class DeleteTimezoneHistoryEntryEvent extends ActionEvent {
	private static final long serialVersionUID = 1L;

	private final TimezoneHistoryEntry entry;

	/**
	 * Create a new DeleteTimezoneHistoryEntryEvent
	 *
	 * @param source the event source
	 * @param target the event target
	 * @param entry the entry to delete
	 */
	public DeleteTimezoneHistoryEntryEvent(Object source, EventTarget target, TimezoneHistoryEntry entry) {
		super(source, target);
		this.entry = entry;
	}

	/**
	 * Get the TimezoneHistoryEntry
	 *
	 * @return the entry to delete
	 */
	public TimezoneHistoryEntry getTimezoneHistoryEntry() {
		return entry;
	}

}
