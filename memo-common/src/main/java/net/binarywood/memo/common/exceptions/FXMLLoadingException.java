/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.exceptions;

/**
 * An exception in case the loading of a scene or component from an FXML file fails
 *
 * @author Stefanie Wiegand
 */
public class FXMLLoadingException extends MemoException {

	private static final long serialVersionUID = 1L;

	/**
	 * Create a new FXMLLoadingException
	 *
	 * @param msg the message to log
	 * @param cause the exception that caused this
	 */
	public FXMLLoadingException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Create a new FXMLLoadingException
	 *
	 * @param msg the message to log
	 */
	public FXMLLoadingException(String msg) {
		super(msg);
	}
}
