/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import java.util.Objects;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A camera with it's metainformation and associated histories if they exist
 *
 * @author Stefanie Wiegand
 */
public class Camera implements Comparable<Camera> {

	private static final Logger logger = LoggerFactory.getLogger(Camera.class);

	//made up of make, model and serial number
	private String identifier;
	//the make as read from the camera's media items
	private String make;
	//the model as read from the camera's media items
	private String model;
	//since this doesn't necessarily exist it can also be set by the user
	private String serialNumber;

	//who owned/used the camera?
	private OwnerHistory ownerHistory;
	//what was the setting of the camera?
	private TimezoneHistory settingsHistory;
	//in what timezone was the camera located?
	private TimezoneHistory locationHistory;

	/**
	 * Create a new, empty camera
	 */
	public Camera() {

		if (identifier==null) {
			makeIdentifier();
		}
		ownerHistory = new OwnerHistory();
		settingsHistory = new TimezoneHistory();
		locationHistory = new TimezoneHistory();
	}

	/**
	 * Creates a new camera using information from e.g. a store
	 *
	 * @param id the camera's identifier
	 * @param make the make
	 * @param model the model
	 * @param serial the serial number
	 */
	public Camera(String id, String make, String model, String serial) {

		super();
		this.identifier = id;
		this.make = make.toUpperCase();
		this.model = model;
		this.serialNumber = serial;

		//TODO: also add history from store!
	}

	// Add/remove information /////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Add a new timezone setting to the history
	 *
	 * @param changedInto the timestamp of the new setting
	 * @param newTimezone the timezone of the new setting
	 */
	public void addTimezoneSetting(DateTime changedInto, DateTimeZone newTimezone) {
		settingsHistory.add(changedInto, newTimezone);
	}

	/**
	 * Remove the timezone for this date
	 *
	 * @param dateTime the date
	 */
	public void removeTimezoneSetting(DateTime dateTime) {
		settingsHistory.remove(dateTime);
	}

	/**
	 * Add a new location entry to the history
	 *
	 * @param changedInto the timestamp of the new entry
	 * @param newTimezone the location of the new entry
	 */
	public void addLocation(DateTime changedInto, DateTimeZone newTimezone) {
		locationHistory.add(changedInto, newTimezone);
	}

	/**
	 * Remove the location for this date
	 *
	 * @param dateTime the date
	 */
	public void removeLocation(DateTime dateTime) {
		locationHistory.remove(dateTime);
	}

	/**
	 * Add a new owner to the history
	 *
	 * @param changedInto the timestamp of the new entry
	 * @param newOwner the owner of the new entry
	 */
	public void addOwner(DateTime changedInto, String newOwner) {
		ownerHistory.add(changedInto, newOwner);
	}

	/**
	 * Remove the owner for this date
	 *
	 * @param dateTime the date
	 */
	public void removeOwner(DateTime dateTime) {
		ownerHistory.remove(dateTime);
	}

	// Other //////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public int compareTo(Camera other) {
		//check if make matches
		int i = make.compareTo(other.getMake());
		if (i!=0) {
			return i;
		}
		//check if model matches
		i = model.compareTo(other.getModel());
		if (i!=0) {
			return i;
		}
		//finally check serial number: if they all match it is the same camera
		if (serialNumber!=null) {
			return serialNumber.compareTo(other.getSerialNumber());
		} else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object other) {
		return other.getClass()==this.getClass() && this.compareTo((Camera) other)==0;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 61 * hash + Objects.hashCode(this.make);
		hash = 61 * hash + Objects.hashCode(this.model);
		hash = 61 * hash + Objects.hashCode(this.serialNumber);
		return hash;
	}

	/**
	 * Merges another camera with this camera and returns the merged camera.
	 * For conflicts, the current camera's information is preferred.
	 *
	 * @param c the camera to merge with this one
	 * @return the merged camera object
	 */
	public Camera mergeCamera(Camera c) {
		Camera merged = this;

		//check for camera make and model - should be the same
		if (!this.make.equals(c.getMake()) || !this.model.equals(c.getModel())) {
			logger.warn("The camera's make and model do not match. Merging might be a bad idea...");
		}

		//use serial number if exists
		if (this.serialNumber==null) {
			merged.setSerialNumber(c.getSerialNumber());
		}

		//merge timezone history
		merged.getSettingsHistory().merge(c.getSettingsHistory());

		return merged;
	}

	/**
	 * Creates a deep copy of the camera
	 *
	 * @return the copy
	 */
	public Camera copy() {
		//create a new camera object and fill it with this camera's attributes
		Camera clone = new Camera();
		clone.setIdentifier(identifier);
		clone.setMake(make);
		clone.setModel(model);
		clone.setSerialNumber(serialNumber);
		clone.setOwnerHistory(ownerHistory);
		clone.setSettingsHistory(settingsHistory);
		clone.setLocationHistory(locationHistory);

		return clone;
	}

	/**
	 * Create the identifier for this camera using make, model and serial number
	 */
	public final void makeIdentifier() {
		identifier = "";
		//remove all spaces and forward slashes as they would be problematic when creating URIs
		if (make!=null) {
			identifier += make.trim().replace(" ", "_").replace("/", "-").toUpperCase();
		}
		if (model!=null) {
			identifier += "-" + model.trim().replace(" ", "_").replace("/", "-");
		}
		if (serialNumber!=null) {
			identifier += "-" + serialNumber.trim().replace(" ", "_").replace("/", "-");
		}
	}

	@Override
	public String toString() {
		String s = "Camera " + identifier;

		if (make != null) {
			s += ", Make: " + make;
		}
		if (model!=null) {
			s += ", Model: " + model;
		}
		if (serialNumber!=null) {
			s += ", Serial Number: " + serialNumber;
		}
		return s;
	}

	// Getters/Setters ////////////////////////////////////////////////////////////////////////////////////////////////
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
		if (this.identifier!=null) {
			this.identifier = this.identifier.trim();
		}
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
		if (this.make!=null) {
			this.make = this.make.toUpperCase().trim();
		}
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
		if (this.model!=null) {
			this.model = this.model.trim();
		}
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
		if (this.serialNumber!=null) {
			this.serialNumber = this.serialNumber.trim();
		}
	}

	public TimezoneHistory getSettingsHistory() {
		return settingsHistory;
	}

	public void setSettingsHistory(TimezoneHistory timezoneHistory) {
		this.settingsHistory = timezoneHistory;
	}

	public TimezoneHistory getLocationHistory() {
		return locationHistory;
	}

	public void setLocationHistory(TimezoneHistory locationHistory) {
		this.locationHistory = locationHistory;
	}

	public OwnerHistory getOwnerHistory() {
		return ownerHistory;
	}

	public void setOwnerHistory(OwnerHistory ownerHistory) {
		this.ownerHistory = ownerHistory;
	}

}
