/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.controller;

import net.binarywood.memo.common.spec.AController;
import net.binarywood.memo.common.spec.APresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class controls the main MemO application
 *
 * @author Stefanie Cox
 */
public class DialogTesterController extends AController {

	private static final Logger logger = LoggerFactory.getLogger(DialogTesterController.class);


	/**
	 * Creates a new leonardo controller
	 */
	public DialogTesterController() {
		super();
	}

	// Getters & Setters //////////////////////////////////////////////////////////////////////////

	@Override
	public APresenter getPresenter() {
		return presenter;
	}

}
