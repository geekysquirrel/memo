/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.controller;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import net.binarywood.memo.common.MemoUtil;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MediaItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility method to recursively load photo and camera data
 *
 * @author Stefanie Wiegand
 */
public class DataReader {

	private static final Logger logger = LoggerFactory.getLogger(DataReader.class);

	private SortedMap<String, Camera> cameras;
	private SortedMap<String, MediaItem> mediaItems;

	/**
	 * Read media files from disk.
	 *
	 * @param directory the directory to load from
	 * @param recurse whether to include subdirectories
	 * @param useWebService connect to the internet to retrieve timezone information from geotag?
	 * @param createThumbnails create thumbnails for the found pictures (provided they don't exist)?
	 * @param useExiftool whether to permit running exiftool
	 */
	public void read(String directory, boolean recurse,
			boolean useWebService, boolean createThumbnails, boolean useExiftool) {

		cameras = new TreeMap<>();
		//sort case-insensitive by file name
		mediaItems = new TreeMap<>((String o1, String o2) -> o1.toLowerCase().compareTo(o2.toLowerCase()));

		try {
			logger.info("Loading media items from {}. Recurse? {}, Webservice? {}, Thumbs? {}, Exiftool? {}",
					directory, recurse, useWebService, createThumbnails, useExiftool);

			long start = System.currentTimeMillis();
			FindMediaVisitor visitor = new FindMediaVisitor(cameras, mediaItems, null, null, 0,
					useWebService, createThumbnails, useExiftool);
			Path startDir = Paths.get(directory);
			if (recurse) {
				Files.walkFileTree(startDir, visitor);
			} else {
				Files.walkFileTree(startDir, EnumSet.of(FileVisitOption.FOLLOW_LINKS), 1, visitor);
			}
			//retrieve read information from visitor
			cameras = visitor.getCameras();
			mediaItems = visitor.getMediaItems();
			long end = System.currentTimeMillis();
			logger.info("Read {} media items in {}", mediaItems.size(), MemoUtil.getTimeStr(end - start));
		} catch (IOException e) {
			logger.error("Error loading media from " + directory, e);
		}
	}

	// Getters / Setters //////////////////////////////////////////////////////////////////////////

	/**
	 * Get a map of cameras where the key is the camers's identifier
	 *
	 * @return the cameras
	 */
	public Map<String, Camera> getCameras() {
		return cameras;
	}

	/**
	 * Get a map of mediaItems ordered by photo name
	 *
	 * @return the mediaItems
	 */
	public SortedMap<String, MediaItem> getMediaItems() {
		return mediaItems;
	}

}
