/*
 * Copyright 2017 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common;

import javafx.application.Application;
import javafx.stage.Stage;
import net.binarywood.memo.common.controller.DialogTesterController;
import net.binarywood.memo.common.spec.APresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class shows a dialog in a new window.
 */
public class DialogTester extends Application {

	private static final Logger logger = LoggerFactory.getLogger(DialogTester.class);
	private static final String DIALOG = "fxml/MediaItemScene.fxml";
	private static final DialogTesterController controller = new DialogTesterController();

	@Override
	public void start(Stage stage) throws Exception {
		try {
			APresenter presenter = controller.showDialog(DIALOG, "Dialog tester", controller, controller);

		} catch (Exception e) {
			logger.error("Could not execute DialogTester", e);
		}
	}

	/**
	 * The runnable tool for testing the dialog of which the class is defined in the DIALOG constant
	 *
	 * @param args none
	 */
	public static void main(String[] args) {
		DialogTester.launch(args);
    }
}
