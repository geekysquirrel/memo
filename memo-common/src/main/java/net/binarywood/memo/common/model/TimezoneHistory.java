/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A history for timezones
 *
 * @author Stefanie Wiegand
 */
public class TimezoneHistory extends DataHistory {

	private static final Logger logger = LoggerFactory.getLogger(TimezoneHistory.class);

	/**
	 * Get the timezone for a specific date
	 *
	 * @param date the date
	 * @return teh timezone which was in effect at the given date
	 */
	public DateTimeZone getTimezoneAtDate(DateTime date) {
		return (DateTimeZone) super.getDataAtDate(date);
	}

	/**
	 * Get the next timezone for a given date ("peek")
	 *
	 * @param date the date
	 * @return the timezone following the timezone for the given date
	 */
	public DateTimeZone getNextTimezoneForDate(DateTime date) {
		return (DateTimeZone) super.getNextDataForDate(date);
	}

	/**
	 * Get all different timezones in this history
	 *
	 * @return the timezones (no duplicates)
	 */
	public Set<DateTimeZone> getDistinctTimezones() {
		//HashSet automatically gets rid of duplicates, LinkedHashSet makes sure the order is maintained
		HashSet<DateTimeZone> timezones = new LinkedHashSet<>();
		history.values().stream().forEach(o -> timezones.add((DateTimeZone) o));
		return timezones;
	}

	/**
	 * For a given date (without timezone) give all the possible timezones from this history the date could posisbly be
	 * in. To achieve this, the date is assumed to be in all existing timezones (i.e. take the date and attach a
	 * timezone, then translate to UTC and see which timezone from the history corresponds to it).
	 *
	 * @param dt the date to check for
	 * @return the timezones that this date could be in
	 */
	public Set<DateTimeZone> getTimezonesAroundDate(DateTime dt) {
		HashSet<DateTimeZone> timezones = new HashSet<>();

		//-12/+14 are the most extreme timezones possible
		//note that while it's earlier in the -12 timezone,
		//the same date/time with this timezone would be later on a UTC timeline
		DateTime latest = dt.withZoneRetainFields(DateTimeZone.forOffsetHours(-12));
		DateTime earliest = dt.withZoneRetainFields(DateTimeZone.forOffsetHours(+14));
		//logger.debug("Earliest date: {}, Latest date: {}", earliest, latest);

		//get datetime at both borders
		timezones.add(getTimezoneAtDate(earliest));
		timezones.add(getTimezoneAtDate(latest));

		//get all the datetimes in between
		//logger.debug("Getting all timezones between {} and {}",
		//	earliest.withZone(DateTimeZone.UTC), latest.withZone(DateTimeZone.UTC));

		//start from the left(past) moving towards the right(future) on the timeline
		DateTime currentDateTime = earliest;

		for (Map.Entry<DateTime, Object> e : history.entrySet()) {

			//start at the stage in the history where the given date is
			if (!e.getKey().isBefore(earliest)) {

				//check if end of period is reached
				if (e.getKey().isAfter(latest)) {
					break;
				}

				//cache current date/time for next iteration
				currentDateTime = e.getKey();

				//add timezone
				timezones.add((DateTimeZone) e.getValue());
			}
		}
		//logger.debug("Timezones found: {}", timezones);

		return timezones;
	}

	/**
	 * Check if this history contains an entry of a given timezone between the given dates
	 *
	 * @param tz the timezone to search for
	 * @param start the left border of the search (&gt;=)
	 * @param end the right border of the search (&lt;)
	 * @return whether the timezone was found or not
	 */
	public boolean wasPresentInTimezoneBetweenDates(DateTimeZone tz, DateTime start, DateTime end) {
		//logger.debug("check whether {} was active between {} and {}", tz, start, end);
		//walk to end date in history collecting all timezones encountered
		Set<DateTimeZone> foundTimezones = new HashSet<>();
		Map.Entry<DateTime, Object> prev = null;
		for (Map.Entry<DateTime, Object> entry: getHistory().entrySet()) {
			if ((end.isEqual(entry.getKey()) || end.isAfter(entry.getKey())) &&
				(prev==null || (start.isAfter(prev.getKey())))) {
				foundTimezones.add((DateTimeZone) entry.getValue());
			}
			prev = entry;
		}
		//logger.debug("Found timezones: {} between {} and {}", foundTimezones, start, end);
		return foundTimezones.contains(tz);
	}

	/**
	 * Find the earliest date contained in this history
	 *
	 * @return the earliest datetime
	 */
	public DateTime getEarliestDate() {
		return history.firstKey();
	}

	/**
	 * Find the latest date contained in this history
	 *
	 * @return the latest datetime
	 */
	public DateTime getLatestDate() {
		return history.lastKey();
	}
}
