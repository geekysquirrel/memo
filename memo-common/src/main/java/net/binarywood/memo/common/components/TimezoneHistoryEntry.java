/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.components;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import net.binarywood.memo.common.events.DeleteTimezoneHistoryEntryEvent;
import net.binarywood.memo.common.spec.AComponent;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A single entry in a timezone history containing a timezone, date and time and a delete button to remove it. This can
 * either be used to populate it programmatically or be filled out by the user.
 *
 * @author Stefanie Wiegand
 */
public class TimezoneHistoryEntry extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(TimezoneHistoryEntry.class);

	@FXML
	private Pane timezonePlaceholder;
	//doesn't have FXML declaration because it is a custom element
	private FilteringComboBox timezone;
	@FXML
	private DatePicker date;
	@FXML
	private TextField time;
	@FXML
	private Button deleteButton;

	/**
	 * Create an empty TimezoneHistoryEntry
	 */
	public TimezoneHistoryEntry() {

		super();

		//add the timezone dropdown menu
		timezone = new FilteringComboBox(DateTimeZone.getAvailableIDs(), null);
		timezonePlaceholder.getChildren().add(timezone);
	}

	/**
	 * Create a TimezoneHistoryEntry displaying the given dateTime/time
	 *
	 * @param dateTime the date/time to populate it with
	 * @param zone the timezone in which the time should be rendered
	 */
	public TimezoneHistoryEntry(DateTime dateTime, DateTimeZone zone) {
		this();
		setDate(dateTime, zone);
		timezone.selectEntry(zone.getID());
	}

	/**
	 * Delete the entry linked to this component by sending a delete event to be caught by its controller
	 *
	 * @param event the UI event that triggered this method
	 */
	@FXML
	public void deleteEntry(ActionEvent event) {

		this.fireEvent(new DeleteTimezoneHistoryEntryEvent(this, mainContainer.getParent(), this));
		event.consume();
	}

	/**
	 * Set the given date to be displayed by the component
	 *
	 * @param dateTime the date to set
	 * @param timezone the timezone for which it should be displayed. As parsed (most likely UTC) if null
	 */
	public final void setDate(DateTime dateTime, DateTimeZone timezone) {

		//set date to date picker
		this.date.setValue(LocalDate.of(
			dateTime.year().get(), Month.of(dateTime.monthOfYear().get()), dateTime.dayOfMonth().get()
		));

		//time is given in UTC! Calculate actual local time
		DateTime local;
		if (timezone != null) {
			local = dateTime.withZone(timezone);
		} else {
			local = dateTime;
		}

		//set time to text field, use leading zeroes
		DecimalFormat df = new DecimalFormat("00");
		this.time.setText(df.format(local.hourOfDay().get()) + ":"
				+ df.format(local.minuteOfHour().get()) + ":"
				+ df.format(local.secondOfMinute().get()));
	}

	/**
	 * Disable the delete button
	 */
	public void disableDeleteButton() {
		deleteButton.setDisable(true);
	}

	/**
	 * Enable the delete button
	 */
	public void enableDeleteButton() {
		deleteButton.setDisable(false);
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieve the timestamp displayed
	 *
	 * @return the instant (unix timestamp) representing this date/time
	 */
	public long getTimestamp() {
		LocalDate ld = date.getValue();
		String[] t = time.getText().split(":");
		long l = 0L;
		//date/time are in the timezone mentioned in the entry
		try {
			DateTime dt = new DateTime(ld.getYear(), ld.getMonthValue(), ld.getDayOfMonth(),
					Integer.valueOf(t[0]), Integer.valueOf(t[1]), Integer.valueOf(t[2]),
					DateTimeZone.forID(timezone.getValue()));
			l = dt.getMillis();
		} catch (Exception e) {
			logger.debug("Invalid datetime given; using \"0\" epoch (01/01/1970 00:00:00 GMT)");
		}
		return l;
	}

	/**
	 * Get the currently selected timezone from the dropdown menu
	 *
	 * @return the timesone ID or null if non was selected
	 */
	public String getTimeZone() {
		return timezone.getValue();
	}

}
