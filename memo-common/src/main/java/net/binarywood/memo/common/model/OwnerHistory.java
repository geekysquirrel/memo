/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A history for owners of a camera
 *
 * @author Stefanie Wiegand
 */
public class OwnerHistory extends DataHistory {

	private static final Logger logger = LoggerFactory.getLogger(OwnerHistory.class);

	//TODO: create proper class for owner rather than just using the name

	/**
	 * Get the owner of a camera at the given date
	 *
	 * @param date the date for which to get the owner
	 * @return the owner
	 */
	public String getOwnerAtDate(DateTime date) {
		return (String) super.getDataAtDate(date);
	}

}
