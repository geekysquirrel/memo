/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.exif.makernotes.CanonMakernoteDirectory;
import com.drew.metadata.exif.makernotes.NikonType2MakernoteDirectory;
import com.drew.metadata.exif.makernotes.OlympusCameraSettingsMakernoteDirectory;
import com.drew.metadata.exif.makernotes.OlympusEquipmentMakernoteDirectory;
import com.drew.metadata.exif.makernotes.OlympusMakernoteDirectory;
import com.drew.metadata.exif.makernotes.PanasonicMakernoteDirectory;
import com.drew.metadata.iptc.IptcDirectory;
import com.drew.metadata.jpeg.JpegDirectory;
import com.drew.metadata.xmp.XmpDirectory;
import com.sun.javafx.collections.ObservableMapWrapper;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import javafx.collections.ObservableMap;
import javax.imageio.ImageIO;
import net.binarywood.memo.common.MemoUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.geonames.Timezone;
import org.geonames.WebService;
import org.im4java.core.ETOperation;
import org.im4java.core.ExiftoolCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.process.ArrayListOutputConsumer;
import org.imgscalr.Scalr;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A MediaItem is either a mediaItem or video
 */
public class MediaItem {

	private static final Logger logger = LoggerFactory.getLogger(MediaItem.class);

	private static final String TIMEZONE_ID_PREFIX = "TimezoneID:";
	//if no height is specified in the properties file go with this
	private static final int THUMBNAIL_FALLBACK_HEIGHT = 180;
	//the height of the thumbnail used to calculate the colour information
	private static final int COLOUR_THUMBNAIL_HEIGHT = 10;

	//observable map for all exif properties that are meant to be editable by the user
	private ObservableMap<String, String> exif;

	//the camera this media item has been recorded with
	private Camera camera;
	//includes filename
	private final String path;
	//thumbnail path - null if the mediaItem doesn't (yet) have a thumbnail
	private String thumbnail;
	//the parent dir
	private String parentDirectory;
	//the filename (with file extension)
	private String filename;
	//the original filename (record it when normalising)
	private String originalFilename;
	//the md5 checksum of the mediaItem
	private String checksum;
	private String fileChecksum;
	private String dateChecksum;
	private String filenameChecksum;
	//the size in bytes of this media item
	private Long filesize;
	//the actual file
	private File file;

	// FLAGS //////////////////////////////////////////////////////////////////////////////////////////////////////////
	//only use exiftool if information cannot be retrieved with other means (because it's slow)
	private boolean useExiftool;
	private boolean useWebService;
	private boolean createThumbnail;
	//descriptive flags
	private boolean stereo = false;
	private boolean video = false;
	private boolean geotagged = false;
	private boolean landscapeOrientation = false;
	private boolean normalised = false;
	private boolean thumbnailGenerated = false;

	// DATE/TIMESTAMPS (in order of importance) ///////////////////////////////////////////////////////////////////////
	//GPS Time-Stamp - 15:43:36 UTC / GPS Date Stamp - 2012:03:27	This is always UTC
	private DateTime gpsDate;
	//exif creation date (local time)
	private DateTime datetimeOriginalExif;
	//time of creating the digital file - same as datetimeoriginal since we're only handling digital photos for now
	private DateTime datetimeDigitisedExif;
	//exif modification date (local time)
	private DateTime datetimeExif;
	//the time the file was created - not meaningful as file could have been extracted/downloaded
	private DateTime creationDateFile;
	//this is always local time of the machine that modified it.
	private DateTime modificationDateFile;
	//a String representation of the timezone ID for jodatime, see http://joda-time.sourceforge.net/timezones.html
	private String timezoneID;

	// METADATA ///////////////////////////////////////////////////////////////////////////////////////////////////////
	//the metadata as read from the file
	private Metadata metadata;

	//Orientation
	private String orientationString;
	private Orientation orientation;
	//ISO Speed Ratings
	private String isoSpeedString;
	private Integer isoSpeed;
	//manufacturer information about the lens used
	private String lensInfo;
	//Exposure Bias Value
	private String exposureBiasString;
	private Double exposureBias;
	//Aperture Value / F-Number
	private String apertureString;
	private Double aperture;
	//Flash
	private String flashString;
	private Boolean flash;
	//Focal Length / Focal Length 35
	private String focalLengthString;
	private Double focalLength;
	//the distance of the focus in metres
	private Double focalDistance;
	//see https://en.wikipedia.org/wiki/Hyperfocal_distance
	private Double hyperfocalDistance;
	//focal plane diagonal
	private Double focalPlaneDiagonal;
	private Double circleOfConfusion;
	//Shutter Speed Value / Exposure Time
	private String shutterSpeed;
	private Double exposureTime;
	//Temperature
	private Double temperature;
	//how light it was when the mediaItem was taken. should be around 10
	private Double lightValue;
	//the length of the field in which the camera is focused. Infinity will be -1
	private Double depthOfField;
	private Double focusFrom;
	private Double focusTo;
	//the angle which is captured in the photo
	private Double fieldOfView;
	//Digital Zoom Ratio
	private String digitalZoom;
	//White Balance / White Balance Mode
	private String whiteBalanceString;
	private Integer whiteBalance;
	//User Comment
	private String comment;
	//keywords; might contain tags
	private String keywords;
	//in pixels
	private Integer width;
	//in pixels
	private Integer height;
	//GPS Latitude Ref - N
	private String latRef;
	//GPS Latitude - 50.0° 57.0' 49.0000000000083"
	private String latitude;
	//GPS Longitude Ref - W/West
	private String longRef;
	//GPS Longitude - -1.0° 25.0' 30.000000000000213"
	private String longitude;
	//GPS Altitude Ref - 0: above sea level, 1: below sea level
	private String altRef;
	//GPS Altitude - 0 metres
	private String altitude;

	// CONSTANTS //////////////////////////////////////////////////////////////////////////////////////////////////////
	//TODO: use new Java8 time instead of Joda-time
	//-- exif formats are for reading/writing exif using exiftool
	public static final DateTimeFormatter exifDateTimeFormat = DateTimeFormat.forPattern("yyyy:MM:dd HH:mm:ss");
	public static final DateTimeFormatter exifDateFormat = DateTimeFormat.forPattern("yyyy:MM:dd");
	public static final DateTimeFormatter exifTimeFormat = DateTimeFormat.forPattern("HH:mm:ss");
	//-- used for writing for UI purposes
	public static final DateTimeFormatter dateFormatWithoutTimezone = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
	public static final DateTimeFormatter dateFormatWithTimezone = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss z");
	//-- safe format for using dates in filenames
	public static final DateTimeFormatter safeFileDateFormat = DateTimeFormat.forPattern("yyyy-MM-dd_HH-mm-ss");
	//-- used for parsing file attribute dates
	public static final DateTimeFormatter fileReadFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");
	//-- formats for XSD reading/writing
	public static final DateTimeFormatter xsdUTCReadFormat = DateTimeFormat.forPattern("\"yyyy-MM-dd'T'HH:mm:ss'Z\"^^xsd:dateTime'");
	public static final DateTimeFormatter xsdUTCWriteFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z^^xsd:dateTime'");
	public static final DateTimeFormatter noXSDReadFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.S");
	public static final DateTimeFormatter noXSDUTCReadFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
	public static final DateTimeFormatter xsdWriteFormat = DateTimeFormat.forPattern("\"yyyy-MM-dd'T'HH:mm:ss.S'\"^^xsd:dateTime'");
	public static final DateTimeFormatter xsdReadFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.S'^^xsd:dateTime'");

	/**
	 * "Common" constructor
	 *
	 * @param path the path of the media item
	 * @throws FileNotFoundException if the file does not exist
	 */
	private MediaItem(String path) throws FileNotFoundException {

		this.path = path;
		this.filename = path.substring(path.lastIndexOf("/")+1);
		this.parentDirectory = path.substring(0, path.lastIndexOf("/"));

		//get the media item file
		file = new File(path);

		//stop right here
		if (!file.exists()) {
			throw new FileNotFoundException("MediaItem " + path + " doesn't exist, skipping...");
		} else {
			loadFileAttributes();
		}

		this.exif = new ObservableMapWrapper<>(new HashMap<>());
	}

	/**
	 * Create a MediaItem by reading its information from the given path
	 * @param path the absolute path to read from
	 * @param useWebService whether to use a webservice to obtain timezone information
	 * @param createThumbnail whether to generate a thumbnail for this item
	 * @param useExiftool whether to permit running exiftool
	 * @throws java.io.FileNotFoundException if the file could not be found at the given loaction
	 */
	public MediaItem(String path, boolean useWebService, boolean createThumbnail, boolean useExiftool)
			throws FileNotFoundException {

		this(path);

		this.useExiftool = useExiftool;
		this.useWebService = useWebService;
		this.createThumbnail = createThumbnail;

		//load media related metadata
		camera = new Camera();
		createChecksum();
		loadMetadata(file);
		autocomplete();
	}

	/**
	 * Construct a media item "manually".
	 * This method is supposed to be called when retrieving a media item from the store.
	 *
	 * @param path as retrieved from the store
	 * @param cMake as retrieved from the store
	 * @param cModel as retrieved from the store
	 * @param cSerial as retrieved from the store
	 * @param thumbnail as retrieved from the store
	 * @param parentDirectory as retrieved from the store
	 * @param filename as retrieved from the store
	 * @param originalFilename as retrieved from the store
	 * @param checksum as retrieved from the store
	 * @param fileChecksum as retrieved from the store
	 * @param dateChecksum as retrieved from the store
	 * @param filenameChecksum as retrieved from the store
	 * @param filesize as retrieved from the store
	 * @param orientation as retrieved from the store
	 * @param isoSpeed as retrieved from the store
	 * @param lensInfo as retrieved from the store
	 * @param exposureBias as retrieved from the store
	 * @param aperture as retrieved from the store
	 * @param flash as retrieved from the store
	 * @param focalLength as retrieved from the store
	 * @param focalDistance as retrieved from the store
	 * @param focalPlaneDiagonal as retrieved from the store
	 * @param exposureTime as retrieved from the store
	 * @param temperature as retrieved from the store
	 * @param lightValue as retrieved from the store
	 * @param depthOfField as retrieved from the store
	 * @param focusFrom as retrieved from the store
	 * @param focusTo as retrieved from the store
	 * @param fieldOfView as retrieved from the store
	 * @param digitalZoom as retrieved from the store
	 * @param whiteBalance as retrieved from the store
	 * @param comment as retrieved from the store
	 * @param keywords tags as retrieved from the store; separated by semicolons
	 * @param width as retrieved from the store
	 * @param height as retrieved from the store
	 * @param latRef as retrieved from the store
	 * @param latitude as retrieved from the store
	 * @param longRef as retrieved from the store
	 * @param longitude as retrieved from the store
	 * @param altRef as retrieved from the store
	 * @param altitude as retrieved from the store
	 * @param gpsDate as retrieved from the store
	 * @param datetimeOriginalExif as retrieved from the store
	 * @param timezoneID as retrieved from the store
	 *
	 * @throws FileNotFoundException if the file found in the store can not be found on disk
	 */
	public MediaItem(String path, String cMake, String cModel, String cSerial, String thumbnail,
		String parentDirectory, String filename, String originalFilename, String checksum, String fileChecksum,
		String dateChecksum, String filenameChecksum, Long filesize, Orientation orientation, Integer isoSpeed,
		String lensInfo, Double exposureBias, Double aperture, Boolean flash, Double focalLength, Double focalDistance,
		Double focalPlaneDiagonal, Double exposureTime, Double temperature, Double lightValue, Double depthOfField,
		Double focusFrom, Double focusTo, Double fieldOfView, String digitalZoom, Integer whiteBalance, String comment,
		String keywords, Integer width, Integer height, String latRef, String latitude, String longRef, String longitude,
		String altRef, String altitude, String gpsDate, String datetimeOriginalExif, String timezoneID)
		throws FileNotFoundException {

		this(path);

		//set actions:
		useWebService = false;
		createThumbnail = true;
		useExiftool = false;

		this.camera = new Camera(null, cMake, cModel, cSerial);

		this.thumbnail = thumbnail;
		this.parentDirectory = parentDirectory;
		this.filename = filename;
		this.originalFilename = originalFilename;
		this.checksum = checksum;
		this.fileChecksum = fileChecksum;
		this.dateChecksum = dateChecksum;
		this.filenameChecksum = filenameChecksum;
		this.filesize = filesize;
		this.orientation = orientation;
		this.isoSpeed = isoSpeed;
		this.lensInfo = lensInfo;
		this.exposureBias = exposureBias;
		this.aperture = aperture;
		this.flash = flash;
		this.focalLength = focalLength;
		this.focalDistance = focalDistance;
		this.focalPlaneDiagonal = focalPlaneDiagonal;
		this.exposureTime = exposureTime;
		this.temperature = temperature;
		this.lightValue = lightValue;
		this.depthOfField = depthOfField;
		this.focusFrom = focusFrom;
		this.focusTo = focusTo;
		this.fieldOfView = fieldOfView;
		this.digitalZoom = digitalZoom;
		this.whiteBalance = whiteBalance;
		this.comment = comment;
		this.keywords = keywords;
		this.width = width;
		this.height = height;
		this.latRef = latRef;
		this.latitude = latitude;
		this.longRef = longRef;
		this.longitude = longitude;
		this.altRef = altRef;
		this.altitude = altitude;

		this.gpsDate = gpsDate!=null?noXSDReadFormat.parseDateTime(gpsDate):null;
		this.datetimeOriginalExif = datetimeOriginalExif!=null?noXSDReadFormat.parseDateTime(datetimeOriginalExif):null;
		//TODO: change when the tag is used in MemO
		this.datetimeDigitisedExif = this.datetimeOriginalExif;
		this.timezoneID = timezoneID;

		autocomplete();
		setFlags();
	}

	@Override
	public boolean equals(Object other) {


		if (!other.getClass().equals(getClass())) {
			return false;
		}
		MediaItem o = (MediaItem) other;
		logger.debug("{} equals {}?", this.getFilename(), o.getFilename());

		//divide in blocks and conquer

		//check if the file has changed
		if (!checksum.equals(o.getChecksum())) {

			//file was renamed
			if (!filenameChecksum.equals(o.getFilenameChecksum())) {
				logger.debug("The file {} was renamed", filename);
				//TODO: only renamed? or moved? are the other checksums still ok?
				//		this.path
				//		this.parentDirectory
				//		this.originalFilename
			}

			//file was modified: metadata added/removed/modified, image modified
			if (!fileChecksum.equals(o.getFileChecksum())) {
				logger.debug("The file {} itself has changed ({} to {})", filename, filesize, o.getFilesize());
				//TODO: are the other checksums still ok?
			}

			//file was cut and pasted (same file, same name only new date)
			if (!dateChecksum.equals(o.getDateChecksum())) {
				logger.debug("The file {} was cut/copied and pasted", filename);
				//TODO: are the other checksums still ok?
			}

			return false;
		}

		//check dates
		if (!datetimeOriginalExif.equals(o.getDatetimeOriginalExif())
			|| (gpsDate!=null && o.getGpsDate()!=null && !gpsDate.equals(o.getGpsDate()))) {
			logger.debug("The file {} has different dates", filename);
			return false;
		}

		//check camera
		if (!camera.equals(o.getCamera())) {
			logger.debug("The file {} has a different camera ({} vs. {})", filename, camera, o.getCamera());
			return false;
		}

		//check if thumbnail is up to date
		if (!thumbnail.equals(o.getThumbnail())) {
			logger.debug("The file {} has a different thumbnail ({} vs. {})", filename, thumbnail, o.getThumbnail());
			return false;
		}

		boolean equals;
		try {
			//check metadata
			equals = //orientation==o.getOrientation() &&
				isoSpeed.equals(o.getIsoSpeed()) && latitude.equals(o.getLatitude())
				&& aperture.equals(o.getAperture()) && focalLength.equals(o.getFocalLength())
				&& focalDistance.equals(o.getFocalDistance()) && focalPlaneDiagonal.equals(o.getFocalPlaneDiagonal())
				&& lensInfo.equals(o.getLensInfo()) && exposureTime.equals(o.getExposureTime())
				&& temperature.equals(o.getTemperature()) && lightValue.equals(o.getLightValue())
				&& depthOfField.equals(o.getDepthOfField()) && focusFrom.equals(o.getFocusFrom())
				&& focusTo.equals(o.getFocusTo()) && fieldOfView.equals(o.getFieldOfView())
				&& digitalZoom.equals(o.getDigitalZoom()) && whiteBalance.equals(o.getWhiteBalance())
				&& comment.equals(o.getComment()) && keywords.equals(o.getKeywords()) && width.equals(o.getWidth())
				&& height.equals(o.getHeight()) && latRef.equals(o.getLatRef()) && exposureBias.equals(o.getExposureBias())
				&& longRef.equals(o.getLongRef()) && longitude.equals(o.getLongitude()) && altRef.equals(o.getAltRef())
				&& altitude.equals(o.getAltitude()) && timezoneID.equals(o.getTimezoneID()) && flash.equals(o.getFlash());
		} catch (NullPointerException e) {
			equals = false;
		}

		if (equals) {
			logger.debug("{} and {} are the same", filename, o.getFilename());
		} else {
			logger.debug("The file {} has different metadata", filename);
		}

		return equals;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 29 * hash + Objects.hashCode(this.camera);
		hash = 29 * hash + Objects.hashCode(this.thumbnail);
		hash = 29 * hash + Objects.hashCode(this.filename);
		hash = 29 * hash + Objects.hashCode(this.checksum);
		hash = 29 * hash + Objects.hashCode(this.fileChecksum);
		hash = 29 * hash + Objects.hashCode(this.dateChecksum);
		hash = 29 * hash + Objects.hashCode(this.filenameChecksum);
		hash = 29 * hash + Objects.hashCode(this.filesize);
		hash = 29 * hash + Objects.hashCode(this.gpsDate);
		hash = 29 * hash + Objects.hashCode(this.datetimeOriginalExif);
		hash = 29 * hash + Objects.hashCode(this.timezoneID);
		hash = 29 * hash + Objects.hashCode(this.orientation);
		hash = 29 * hash + Objects.hashCode(this.isoSpeed);
		hash = 29 * hash + Objects.hashCode(this.lensInfo);
		hash = 29 * hash + Objects.hashCode(this.exposureBias);
		hash = 29 * hash + Objects.hashCode(this.aperture);
		hash = 29 * hash + Objects.hashCode(this.flash);
		hash = 29 * hash + Objects.hashCode(this.focalLength);
		hash = 29 * hash + Objects.hashCode(this.focalDistance);
		hash = 29 * hash + Objects.hashCode(this.focalPlaneDiagonal);
		hash = 29 * hash + Objects.hashCode(this.exposureTime);
		hash = 29 * hash + Objects.hashCode(this.temperature);
		hash = 29 * hash + Objects.hashCode(this.lightValue);
		hash = 29 * hash + Objects.hashCode(this.depthOfField);
		hash = 29 * hash + Objects.hashCode(this.focusFrom);
		hash = 29 * hash + Objects.hashCode(this.focusTo);
		hash = 29 * hash + Objects.hashCode(this.fieldOfView);
		hash = 29 * hash + Objects.hashCode(this.digitalZoom);
		hash = 29 * hash + Objects.hashCode(this.whiteBalance);
		hash = 29 * hash + Objects.hashCode(this.comment);
		hash = 29 * hash + Objects.hashCode(this.keywords);
		hash = 29 * hash + Objects.hashCode(this.width);
		hash = 29 * hash + Objects.hashCode(this.height);
		hash = 29 * hash + Objects.hashCode(this.latRef);
		hash = 29 * hash + Objects.hashCode(this.latitude);
		hash = 29 * hash + Objects.hashCode(this.longRef);
		hash = 29 * hash + Objects.hashCode(this.longitude);
		hash = 29 * hash + Objects.hashCode(this.altRef);
		hash = 29 * hash + Objects.hashCode(this.altitude);
		return hash;
	}

	// Calculate, generate etc. ///////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Calculate all the parts that don't need explicit input
	 */
	private void autocomplete() {

		tidyUpMetadata();
		if (useExiftool) {
			getMissingMetadata();
		}
		fixCameraMetadata();
		calculateCompositeValues();
		setFlags();
		if (useWebService) {
			retrieveTimezone();
		}
		//TODO: set mediaItem's colour even if using old thumbnail
		if (createThumbnail) {
			createThumbnail();
		}

		//finally make sure there is a date set
		if (datetimeOriginalExif==null) {
			//get datetime from somewhere else: modification date is a better shot here as
			//creation date changes when a file is copied; e.g. from a camera to disk
			datetimeOriginalExif = new DateTime(modificationDateFile);
		}
	}

	/**
	 * Make sure all information is neat and tidy across different types of media items
	 */
	private void tidyUpMetadata() {

		//flash
		if (flash==null && flashString!=null) {
			if (flashString.contains("fired")) {
				flash = true;
			} else if (flashString.contains("did not fire")) {
				flash = false;
			} else {
				logger.debug("Unknown flash value {}", flashString);
			}
		}

		//comment
		//TODO: what if this contains other stuff?
		if (comment!=null && comment.contains(TIMEZONE_ID_PREFIX)) {
			timezoneID = comment.substring(comment.indexOf(TIMEZONE_ID_PREFIX) + TIMEZONE_ID_PREFIX.length());
		}

		//focal length
		if (focalLength==null && focalLengthString!=null) {
			focalLength = Double.valueOf(focalLengthString.replace("mm", "").trim());
		}

		//aperture
		if (aperture==null && apertureString!=null) {
			aperture = Double.valueOf(apertureString.toUpperCase().replaceAll("F/", ""));
		}

		//exposure compensation
		if (exposureBias==null && exposureBiasString!=null) {
			exposureBias = Double.valueOf(exposureBiasString.replace("\"", "").replace("EV", "").trim());
		}

		//aperture
		if (whiteBalance==null && whiteBalanceString!=null) {
			if (whiteBalanceString.toUpperCase().contains("AUTO")) {
				whiteBalance = -1;
			} else {
				whiteBalance = Integer.valueOf(whiteBalanceString.toUpperCase().replaceAll("K", "").trim());
			}
		}

		//iso
		if (isoSpeed==null && isoSpeedString!=null) {
			if (isoSpeedString.toUpperCase().contains(MemoOptions.UNKNOWN)) {
				isoSpeed = null;
			} else if (isoSpeedString.toUpperCase().contains("AUTO")) {
				isoSpeed = -1;
			} else {
				isoSpeed = Integer.valueOf(isoSpeedString.toLowerCase().replace("iso", "").replace(":", "").trim());
			}
		}
	}

	/**
	 * Checks for missing metadata and - if any is found - runs exiftool te get it
	 */
	private void getMissingMetadata() {

		//check for important values
		//--original date/time
		if (datetimeOriginalExif==null) {
			useExiftool = true;
		}
		//--iso speed
		//TODO: need to find another solution here!
//		if (isoSpeed==null) {
//			useExiftool = true;
//		}
		//don't run for light value as this can be calculated

		//as a backup read using exiftool directly
		if (useExiftool) {
			runExiftool();
		}
	}

	/**
	 * Write camera related metadata into the camera object and generate an identifier
	 */
	private void fixCameraMetadata() {

		//fallback: UNKNOWN identifier
		if (camera.getMake()==null) {
			camera.setMake(MemoOptions.UNKNOWN);
		}
		if (camera.getModel()==null) {
			camera.setModel(MemoOptions.UNKNOWN);
		}
		if (MemoOptions.UNKNOWN.equals(camera.getMake()) && MemoOptions.UNKNOWN.equals(camera.getModel())) {
			camera.setIdentifier(MemoOptions.UNKNOWN);
		} else if (camera.getIdentifier()==null || camera.getIdentifier().isEmpty() || MemoOptions.UNKNOWN.equals(camera.getIdentifier())) {
			//generate camera identifier
			camera.makeIdentifier();
		}
	}

	/**
	 * Calculate composite values, that are implicitly contained in the metadata
	 */
	private void calculateCompositeValues() {

		//infinity cleanup
		if (focalDistance!=null && focalDistance<0) {
			focalDistance = Double.MAX_VALUE;
		}
		if (focusFrom!=null && focusFrom<0) {
			focusFrom = Double.MAX_VALUE;
		}
		if (focusTo!=null && focusTo<0) {
			focusTo = Double.MAX_VALUE;
		}

		// orientation
		orientation = Orientation.UNKNOWN;
		boolean useSize = false;
		boolean invert = false;
		if (orientationString==null) {
			useSize = true;
		} else {
			//parse orientation string
			if (orientationString.toLowerCase().contains("horizontal")) {
				orientation = Orientation.LANDSCAPE;
			} else if (orientationString.toLowerCase().contains("vertical")) {
				orientation = Orientation.PORTRAIT;
			} else if (orientationString.toLowerCase().contains("rotate 180")) {
				useSize = true;
			} else if (orientationString.toLowerCase().contains("rotate 90")
					|| orientationString.toLowerCase().contains("rotate 270")) {
				useSize = true;
				invert = true;
			}
		}

		//calculate from image size; possibly apply rotation
		if (useSize && width!=null && height!=null) {
			if (width>height || (width<height && invert)) {
				orientation = Orientation.LANDSCAPE;
			} else if (height>width|| (height<width && invert)) {
				orientation = Orientation.PORTRAIT;
			}
		}

		//get exposure time from shutter speed
		if (shutterSpeed!=null) {
			String shSp = shutterSpeed.replace("sec", "").trim();

			//translate shutter speed: remove "sec" from value (e.g. 1/103sec)
			if (shSp.contains("/")) {
				double a = Double.parseDouble(shSp.split("/")[0]);
				String bString = shSp.split("/")[1];
				double b = Double.parseDouble(bString);
				exposureTime = a/b;
				shutterSpeed = String.valueOf((int) (Math.round(exposureTime)));
			//double or integer: 0.04 sec or 60 sec
			} else {
				exposureTime = Double.valueOf(shSp);
			}
		}

		//calculate lightvalue
		if (isoSpeed!=null && aperture!=null && exposureTime!=null && exposureBias!=null
				&& isoSpeed>=0) {

			//EV = log2(aperture^2/exposureTime), see http://www.scantips.com/lights/evchart.html
			Double ev = Math.log(Math.pow(aperture, 2)/exposureTime) / Math.log(2);
			//consider exposure compensation
			Double evEffective = ev - exposureBias;

			//LV = ev / (isoSpeed/100)
			//http://pentaxforums.com/forums/55-photography-articles/88197-excel-2003-lv-light-value-calculator.html
			lightValue = evEffective / (Double.valueOf(isoSpeed)/100.0);
		}

		//circle of confusion (see exiftool code, Exif.pm)
		if (focalPlaneDiagonal!=null) {
			circleOfConfusion = Math.sqrt(24.0*24.0+36.0*36.0)/(focalPlaneDiagonal * 1440.0);
		}

		//hyperfocal distance: If the lens is focused at this, then everything from half that distance to infinity
		//will be acceptably sharp in the photo
		if (hyperfocalDistance==null && focalDistance!=null && circleOfConfusion!=null && aperture!=null) {
			hyperfocalDistance = Math.pow(focalDistance, 2)/aperture * circleOfConfusion + focalDistance;
		}

		//field of view
		if (fieldOfView==null && aperture!=null && circleOfConfusion!=null && focalLength!=null) {
			//TODO
		}

		//focal length
		//distance in metres = FocusPosition * FocalLength / 1000

		//TODO: calculate more tags
		//see http://search.cpan.org/dist/Image-ExifTool/lib/Image/ExifTool/TagNames.pod (Composite Tags)
	}

	/**
	 * Set the boolean flags for this media item by evaluating its properties
	 */
	public final void setFlags() {

		if (latitude!=null && longitude!=null) {
			geotagged = true;
		}
		String lowerFilename = filename.toLowerCase();
		if (lowerFilename.endsWith(".mpo")) {
			stereo = true;
		}
		//check if it's a video
		for (String ft: MemoOptions.getVideoTypes()) {
			if (lowerFilename.endsWith(ft)) {
				video = true;
				break;
			}
		}
		//set orientationString
		if (width!=null && height!=null) {
			landscapeOrientation = width>height;
		} else {
			//TODO: get elsewhere?
			logger.debug("Could not calculate orientation for media item {}. orientationString: {}, dimensions: {}x{}",
					filename, orientationString, width, height);
		}
		//check for thumbnail
		File thumb = new File(MemoOptions.get("path.thumbnailDirectory") + path);
		if (thumb.exists()) {
			thumbnail = thumb.getPath();
		} else {
			thumbnail = null;
		}
		//check if normalised
		boolean normalisedChecker = true;
		//--all dates set and matching?
		if (gpsDate==null || datetimeOriginalExif==null || datetimeDigitisedExif==null || datetimeExif==null
				|| !datetimeOriginalExif.equals(datetimeDigitisedExif)) {
			normalisedChecker = false;
			//logger.debug("Some dates are missing and/or don't match, mediaItem {} is not normalised", filename);
		} else {
			//--check if logically correct
			if (comment!=null && !comment.isEmpty()) {
				String tzid = comment.substring(comment.indexOf(TIMEZONE_ID_PREFIX) + TIMEZONE_ID_PREFIX.length());
				DateTimeZone tz = null;
				boolean found = false;
				for (int i = 0; i<tzid.length(); i++) {
					String toTest = tzid.substring(0, tzid.length()-i);
					tz = DateTimeZone.forID(toTest);
					if (tz!=null) {
						found = true;
						break;
					}
				}
				if (found) {
					//get utc date
					DateTime dtUTC = gpsDate.withZoneRetainFields(DateTimeZone.UTC);
					//attach found timezone to local date
					DateTime dtLocal = datetimeOriginalExif.withZoneRetainFields(tz);
					//translate local date to utc
					DateTime dtLocalAsUTC = dtLocal.withZone(DateTimeZone.UTC);

					//compare: timezone+datetime must correspond to correct UTC
					if (!dtUTC.equals(dtLocalAsUTC)) {
						normalisedChecker = false;
						logger.debug("UTC and local time with timezone don't match, media item {} is not normalised",
							filename);
					}
				}

			} else {
				normalisedChecker = false;
				logger.debug("No timezone found in user comment, media item {} is not normalised", filename);
			}
		}
		normalised = normalisedChecker;

		if (thumbnail!=null) {
			thumbnailGenerated = true;
		}
	}

	/**
	 * Queries a web service to get the timezone ID for this media item's coordinates
	 */
	private void retrieveTimezone() {
		if (geotagged) {
			try {
				//get timezone from geonames webservice
				//TODO: make configurable
				WebService.setUserName("memo");
				double lat = (Double.valueOf(latitude.substring(0, latitude.indexOf("°"))))
					+ (Double.valueOf(latitude.substring(latitude.indexOf("°")+1, latitude.indexOf("'")))/60.0)
					+ (Double.valueOf(latitude.substring(latitude.indexOf("'")+1, latitude.indexOf("\"")))/3600.0);
				double lon = (Double.valueOf(longitude.substring(0, longitude.indexOf("°"))))
					+ (Double.valueOf(longitude.substring(longitude.indexOf("°")+1, longitude.indexOf("'")))/60.0)
					+ (Double.valueOf(longitude.substring(longitude.indexOf("'")+1, longitude.indexOf("\"")))/3600.0);
				//negative numbers replace reference letters
				if ("s".equalsIgnoreCase(latRef)) {
					lat = lat * -1;
				}
				if ("w".equalsIgnoreCase(longRef)) {
					lon = lon * -1;
				}

				//perform integrity check
				if ("0.0".equals(String.valueOf(lat)) && "0.0".equals(String.valueOf(lon))) {
					//there is a tiny tiny chance that someone manages to take a picture at EXACTLY 0/0
					//but since this is in the middle of the ocean it's highly unlikely that that would ever happen
					//in which case the photo would be nautical time and given in UTC anyway :)
					logger.warn("Invalid coordinates! GPS timestamp: {}", gpsDate);
				} else {

					Timezone zone = WebService.timezone(lat, lon);

					//get timezone id
					if (zone != null) {
						if (!zone.getTimezoneId().isEmpty()) {
							timezoneID = zone.getTimezoneId();
						}

						if (timezoneID!=null && !timezoneID.isEmpty()) {
							logger.debug("Timzone for coordinates {}: {}", lat + " " + lon, timezoneID);

							//set proper timezone in local date
							if (datetimeOriginalExif!=null) {
								datetimeOriginalExif = datetimeOriginalExif.withZoneRetainFields(
										DateTimeZone.forID(timezoneID)
								);
							}
						}
					} else {
						logger.warn("Could not retrieve timezone for coordinates {} {} from webservice", lat, lon);
					}
				}

			} catch (Exception e) {
				logger.debug("Could not retrieve timezone from coordinates", e);
			}
		}
	}

	/**
	 * Creates a thumbnail for this media item. The size is defined in MemoOptions
	 *
	 * @see MemoOptions
	 */
	public final void createThumbnail() {

		//first check if a thumbnail already exists
		File t = new File(MemoOptions.get("path.thumbnailDirectory") + path);
		if (t.isFile()) {
			thumbnail = t.getAbsolutePath();
		//if not try to generate one
		} else {
			//photo
			if (!isVideo()) {
				try {
					//scale original mediaItem down
					BufferedImage img = ImageIO.read(new File(path));
					int h;
					if (MemoOptions.getProps().containsKey("display.thumbnailHeight")) {
						h = Double.valueOf(MemoOptions.get("display.thumbnailHeight")).intValue();
					} else {
						h = THUMBNAIL_FALLBACK_HEIGHT;
					}
					BufferedImage thumb =  Scalr.resize(img, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_HEIGHT, h);

					//save in thumbnail dir
					File outputfile = new File(MemoOptions.get("path.thumbnailDirectory") + path);
					outputfile.mkdirs();
					outputfile.createNewFile();
					ImageIO.write(thumb, "jpg", outputfile);
					logger.debug("Created thumbnail {}", outputfile);

					//while we're at it get colours
					Map<Integer, Integer> colours;
					// make it even smaller
					thumb = Scalr.resize(img, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_HEIGHT, COLOUR_THUMBNAIL_HEIGHT);
					colours = getColoursFromThumnail(thumb);
					Color predominantColour = getPredominantColour(colours);

					if (predominantColour!=null) {
						//TODO: do something with it
						//logger.debug("Predominant colour (hue): {}", Color.RGBtoHSB(predominantColour.getRed(),
						//		predominantColour.getGreen(), predominantColour.getBlue(), null)[0]*360);

					}

				} catch (IllegalArgumentException e) {
					logger.debug("Skipping invalid file {} for thumbnail creation", filename);
				} catch (IOException | ImagingOpException e) {
					logger.error("Error creating thumbnail for photo {}", filename, e);
				}
			//video
			} else {
				try {
					logger.debug("opening video {}", path);
					//TODO: implement video thumbnail creation

				} catch (Exception e) {
					logger.error("Could not create thumbnail for video", e);
				}
			}
		}
	}

	/**
	 * Generate a map of contained colours from a thumbnail
	 *
	 * @param thumb the thumbnail to use
	 * @return a frequency map of all colours found in the image
	 */
	private Map<Integer, Integer> getColoursFromThumnail(BufferedImage thumb) {
		Map<Integer, Integer> colours = new HashMap<>();

		try {
			//iterate over each pixel in the thumbnail and get the colour
			for (int i=0; i<thumb.getHeight(); i++) {
				for (int j=0; j<thumb.getWidth(); j++) {
					int colourValue = thumb.getRGB(j, i);
					if (colours.containsKey(colourValue)) {
						colours.put(colourValue, colours.get(colourValue) + 1);
					} else {
						colours.put(colourValue, 1);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Could not read colour from photo", e);
		}
		return colours;
	}

	/**
	 * Find one of the most used colours in a colour frequency map
	 *
	 * @param colours the input map
	 * @return the most frequently used colour
	 */
	private Color getPredominantColour(Map<Integer, Integer> colours) {

		Color predominantColour = null;
		//find the most used colour
		Integer max = Collections.max(colours.values());
		//scan all colours to get one of the most used colours (if there are more than 1)
		for (Entry<Integer, Integer> e: colours.entrySet()) {
			if (e.getValue().equals(max)) {
				predominantColour =  new Color(e.getKey());
				break;
			}
		}
		return predominantColour;
	}

	/**
	 * Gets the datetime formatted for XSD.
	 * @param utc utc or local time?
	 * @return the requested date formatted as xsd string
	 */
	public String getXSDDateTime(boolean utc) {
		String date = null;
		try {
			if (utc) {
				date = gpsDate.toString(noXSDReadFormat);
			} else {
				date = datetimeOriginalExif.toString(noXSDReadFormat);
			}
		} catch (Exception e) {
			logger.warn("Could not format date ({} or {})", gpsDate, datetimeOriginalExif, e);
		}
		return date;
	}

	/**
	 * Parse a date/time string found in the metadata.
	 *
	 * @param dateTimeString the String to parse
	 * @return the parsed date
	 */
	private DateTime parseDateTimeFromString(String dateTimeString, DateTimeFormatter formatter) {

		try {
			//check if 24:xx:xx appears in the timestamp and replace it with 00:xx:xx
			return DateTime.parse(dateTimeString.replace(" 24:", " 00:"), formatter);
		} catch (Exception e) {
			logger.warn("Could not parse date {}, defaulting to unix epoch", dateTimeString, e);
			return DateTime.parse("1970:01:01 00:00:00", formatter);
		}
	}

	// Strings for printing ///////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a String representation for this media item, which contains the most important things, such as
	 * the filename, dates and camera info
	 *
	 * @return a (multi-line) String
	 */
	@Override
	public String toString() {

		StringBuilder bld = new StringBuilder();

		//build heading frame with correct length
		bld.append("\n##");
		for (int i=0; i<this.getFilename().length(); i++) {
			bld.append("#");
		}
		bld.append("##\n");
		String frame = bld.toString();

		//make frame
		String photo = frame + "# " + this.getFilename() + " #" + frame;

		//append other information
		photo += getFileInfo();
		photo += getCameraInfo();

		return photo;
	}

	/**
	 * A method to print the entire media item
	 *
	 * @return a string containing all non-internal fields and their values if any
	 */
	public String getString() {

		StringBuilder info = new StringBuilder("########## " + super.toString() + " ##########\n");
		for (Field f: getClass().getDeclaredFields()) {
			try {
				StringBuilder tabs = new StringBuilder("\t");
				for (int i=3-(f.getName().length()/8); i>0; i--) {
					tabs.append("\t");
				}
				info.append(f.getName() + tabs.toString() + ":" + f.get(this) + " (" + f.getGenericType().getTypeName() + ")\n");
			} catch (IllegalArgumentException | IllegalAccessException ex) {
				logger.debug("Could not find field {} in {} {}", f.getName(), getClass().getSimpleName(), path, ex);
			}
		}
		return info.toString();
	}

	/**
	 * Create a String which contains basic information about this file
	 *
	 * @return the created string
	 */
	private String getFileInfo() {
		String fileInfo = "";
		if (filesize!=null) {
			fileInfo += "### Image metadata ###\n";

			if (camera!=null) {
				fileInfo += "CameraID:\t\t" + camera.getIdentifier() + "\n";
			}
			if (filesize!=null) {
				fileInfo += "Size:\t\t\t" + getFilesizeString() + "\n";
			}
			if (originalFilename!=null) {
				fileInfo += "Original filename:\t" + originalFilename + "\n";
			}
			if (gpsDate!=null) {
				fileInfo += "GPS date:\t\t" + gpsDate + "\n";
			}
			if (datetimeOriginalExif!=null) {
				fileInfo += "Exif DateTimeOriginal:\t" + datetimeOriginalExif + "\n";
			}
		}
		return fileInfo;
	}

	/**
	 * Get information about the camera which captured this media item
	 *
	 * @return the camera info string
	 */
	private String getCameraInfo() {
		String cameraInfo = "";
		if (camera!=null && "".equals(camera.toString())) {
			cameraInfo += "### Camera info ###\n" + camera.toString();
		}
		return cameraInfo;
	}

	/**
	 * Make the filesize human readable
	 *
	 * @return the formatted file size string
	 */
	private String getFilesizeString() {

		if (filesize<1000) {
			return filesize + " B";
		} else if (filesize<1000000) {
			return Math.round(filesize/1000) + " kB";
		} else {
			return (new DecimalFormat("#####.##")).format(filesize / 1000000d) + " MB";
		}
	}

	/**
	 * Print all metadata contained in this MediaItem
	 */
	public void printAllMetadata() {

		if (metadata!=null) {
			for (Directory dir: metadata.getDirectories()) {
				dir.getTags().stream().forEach(tag ->
					logger.info("{} ({})", tag.toString(), dir.getDescription(tag.getTagType()))
				);
			}
		} else {
			logger.warn("Metadata has not yet been loaded for {}", path);
		}
	}

	// Load metadata //////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Load file attributes (dates) from the media item file
	 */
	private void loadFileAttributes() {

		filesize = file.length();
		BasicFileAttributes attributes;

		try {
			attributes = Files.readAttributes(Paths.get(path), BasicFileAttributes.class);
			modificationDateFile = DateTime.parse(attributes.lastModifiedTime().toString(), fileReadFormat);
			creationDateFile = DateTime.parse(attributes.creationTime().toString(), fileReadFormat);
		} catch (IOException e) {
			logger.warn("Couldn't read attributes of file {}", path, e);
		}
	}

	/**
	 * Generate the checksums for this media item
	 */
	private void createChecksum() {

		//retrieve file metadata
		try {
			//algorithm: md5 (for speed)
			MessageDigest md = MessageDigest.getInstance("MD5");
			 //get checksum of binary file (in case exif/photo has changed)
			try (
				InputStream is = Files.newInputStream(Paths.get(path))) {
				fileChecksum = DigestUtils.md5Hex(is);
			}
			//get checksum for filename (in case it has changed)
			//this is OK to use getBytes as all hashes will be relative to the host system anyway
			filenameChecksum = new BigInteger(md.digest(filename.getBytes(Charset.forName("UTF-8")))).toString(16);
			dateChecksum = new BigInteger(md.digest(creationDateFile.toString(safeFileDateFormat).getBytes(Charset.forName("UTF-8")))).toString(16);

			//concat full checksum (hex)
			checksum = fileChecksum + "-" + filenameChecksum + "-" + dateChecksum;
		} catch (NoSuchAlgorithmException | IOException e) {
			logger.debug("Could not create checksum for media item {}", path, e);
		}
	}

	/**
	 * Read the metadata from the file
	 *
	 * @param mediaItem the file
	 */
	private void loadMetadata(File mediaItem) {

		try {
			metadata = ImageMetadataReader.readMetadata(mediaItem);

			if (metadata!=null) {
				Iterator<Directory> it = metadata.getDirectories().iterator();
				while (it.hasNext()) {
					Directory dir = it.next();
					if (dir!=null) {
						processMetadataDirectory(dir);
					}
				}
			}
		} catch (ImageProcessingException e) {
			//deliberatly not printing exception here
			useExiftool = true;
			logger.debug("Exiftool required because of image processing exception", e);
		} catch (IOException e) {
			logger.error("Error reading file {}", path, e);
		}
	}

	/**
	 * Run exiftool to retrieve metadata that cannot be retrieved from the file directly
	 */
	private void runExiftool() {

		logger.debug("Running exiftool on {} for additional metainformation", this.getFilename());

		//don't run again!
		useExiftool = false;

		//prepare exiftool
		ExiftoolCmd exif = new ExiftoolCmd();
		ETOperation op = new ETOperation();
		op.addImage(this.path);
		op.fast();
		op.json();

		//decide which tags to retrieve
		//--always get dates (in order of importance. the first found tag will be used)
		op.getTags(new String[] {"datetimeoriginal", "CreateDate", "creation_time", "ModifyDate"});
		//--make (in the order of importance. the first found tag will be used)
		if (camera.getMake()==null || camera.getIdentifier()==null
				|| MemoOptions.UNKNOWN.equalsIgnoreCase(camera.getMake())) {
			op.getTags("make", "maker");
		}
		//--model
		if (camera.getModel()==null || camera.getIdentifier()==null
				|| MemoOptions.UNKNOWN.equalsIgnoreCase(camera.getModel())) {
			op.getTags("model");
		}
		//--serial number
		if (camera.getSerialNumber()==null) {
			op.getTags("CameraSerialNumber", "SerialNumber");
		}
		//--height/width
		if (height==null) {
			op.getTags("ImageHeight");
		}
		if (width==null) {
			op.getTags("ImageWidth");
		}
		//--iso
		if (isoSpeed==null) {
			op.getTags("ISO");
		}
		//--temperature
		if (temperature==null) {
			op.getTags("SensorTemperature");
		}
		//--lightValue
		if (lightValue==null) {
			op.getTags("LightValue");
		}
		//--FOV
		if (fieldOfView==null) {
			op.getTags("FOV");
		}
		//--DOF
		if (depthOfField==null) {
			op.getTags("DOF");
		}
		//focal distance
		if (focalDistance==null) {
			op.getTags("FocusDistance");
		}
		if (originalFilename==null) {
			op.getTags("OriginalFilename");
		}
		//get video specific tags
		op.getTags("Duration", "MediaDuration", "mediacreatedate", "mediamodifydate");
		String videoDuration = null;
		String videoDate = null;

		//do it!
		try {
			ArrayListOutputConsumer consumer = new ArrayListOutputConsumer();
			exif.setOutputConsumer(consumer);
			//mute exiftool
			PrintStream originalStream = MemoUtil.muteSystemOut();
			//run exiftool
			exif.run(op);
			//replace dummy stream with original stream
			System.setOut(originalStream);

			//join lines
			StringBuilder json = new StringBuilder();
			Iterator<String> it = consumer.getOutput().iterator();
			while (it.hasNext()) {
				json.append(it.next());
			}
			//parse json (this will be a json array with one object containing the result set)
			JSONArray array = new JSONArray(json.toString());
			if (!array.isNull(0)) {
				JSONObject j = array.getJSONObject(0);

				//look at retrieved values and put into data structure
				for (String key: j.keySet()) {
					String value = String.valueOf(j.get(key));

					if (datetimeOriginalExif==null && value.contains(":") &&
						("datetimeoriginal".equalsIgnoreCase(key)
							|| "CreateDate".equalsIgnoreCase(key)
							|| "creation_time".equalsIgnoreCase(key)
							|| "ModifyDate".equalsIgnoreCase(key)
							)) {

						logger.debug("Missing original date in file {}", filename);

						String createdate = value.trim();
						if (!"0000:00:00 00:00:00".equals(createdate) && !"0000-00-00 00:00:00".equals(createdate)) {
							datetimeOriginalExif = parseDateTimeFromString(createdate, exifDateTimeFormat);
						}
					} else if ("make".equalsIgnoreCase(key)) {
						camera.setMake(value.substring(value.indexOf(":")+1).trim());
					} else if ("model".equalsIgnoreCase(key)) {
						camera.setModel(value.substring(value.indexOf(":")+1).trim());
					} else if (key.toLowerCase().contains("serialnumber")) {
						String sn = value.substring(value.indexOf(":")+1).trim();
						if (sn.length()>0 && camera.getSerialNumber()==null) {
							camera.setSerialNumber(sn);
						}
					} else if ("ImageHeight".equalsIgnoreCase(key)) {
						height = Integer.valueOf(value.trim());
					} else if ("ImageWidth".equalsIgnoreCase(key)) {
						width = Integer.valueOf(value.trim());
					} else if ("ISO".equals(key)) {
						isoSpeedString = value.toLowerCase().replace("iso", "").replace(":", "").trim();
					} else if (key.toLowerCase().contains("temperature")) {
						try {
							temperature = Double.valueOf(value.toLowerCase().replace("c", "").replace("f", "").trim());
						} catch (NumberFormatException e) {
							logger.warn("Could not convert temperature {} to double", value);
							temperature = null;
						}
					} else if ("LightValue".equals(key)) {
						lightValue = Double.valueOf(value.trim());
					} else if ("FOV".equals(key)) {
						//e.g. 6.2 deg (0.16 m)
						String fov = value;
						if (fov.contains("(") && fov.contains(")")) {
							//TODO: this must be something else!
//							//use focal distance info
//							String fd = fov.substring(fov.indexOf("(") + 1, fov.indexOf(")")).replace("m", "").trim();
//							focalDistance = Double.valueOf(fd);
//							//strip focal distance from fov
							fov = fov.substring(0, fov.indexOf("("));
						}
						fieldOfView = Double.valueOf(fov.toLowerCase().replace("deg", "").trim());
					} else if ("DOF".equals(key)) {
						//e.g. 7.08 m (5.42 - 12.51)
						String dof = value.toLowerCase().replace("inf", "-1");
						if (dof.contains("(") && dof.contains(")")) {
							//use focal distance info
							String range = dof.substring(dof.indexOf("(") + 1, dof.indexOf(")")).replace("m", "").trim();
							String[] ran = range.split(" - ");
							if (ran.length==2) {
								focusFrom = Double.valueOf(ran[0]);
								focusTo = Double.valueOf(ran[1]);
							}
							//strip focal distance from fov
							dof = dof.substring(0, dof.indexOf("("));
						}
						//infinity
						depthOfField = Double.valueOf(dof.toLowerCase().replace("m", "").trim());
					} else if ("FocusDistance".equals(key)) {
						//e.g. 1.52 m
						String fd = value.toLowerCase().replace("inf", "-1");
						focalDistance = Double.valueOf(fd.replace("m", "").trim());
					} else if ("OriginalFilename".equals(key)) {
						originalFilename = value.trim();
					} else if ("Duration".equals(key) || "MediaDuration".equals(key)) {
						videoDuration = value.trim();
					} else if ("MediaCreateDate".equals(key) || "MediaModifyDate".equals(key)) {
						videoDate = value.trim();
					}
				}
			}

			//special procedure for videos
			if (videoDate!=null) {
				//video date will be UTC (those buggers!!!)
				DateTime vidDate = DateTime.parse(videoDate, exifDateTimeFormat);
				Long duration = null;
				if (videoDuration!=null) {
					if (videoDuration.contains("s")) {
						videoDuration = videoDuration.replace("s", "").trim();
						Double dd = Double.valueOf(videoDuration)*1000;
						duration = Math.round(dd);
						logger.debug("duration in ms: {}", duration);
					} else if (videoDuration.contains(":")) {
						DateTime vTime = DateTime.parse(videoDuration, exifTimeFormat);
						DateTime refTime = DateTime.parse("00:00:00", exifTimeFormat);
						DateTime vidDur = new DateTime(vTime);
						DateTime refDate = new DateTime(refTime);
						duration = vidDur.minus(refDate.getMillis()).getMillis();
					} else {
						logger.warn("Cannot handle duration format {}", videoDuration);
					}

					if (duration!=null) {
						vidDate = vidDate.minus(duration);
					}
				}
				setGpsDate(vidDate);
			}

		} catch (IOException | InterruptedException | IM4JavaException e) {
			logger.error("Error getting date tags from media item {} using exiftool", this.getFilename(), e);
		}

		//tidy up again in case some new strings were found
		tidyUpMetadata();
	}

	/**
	 * Read information from one metadata directory from a media item
	 *
	 * @param dir the metadata directory to process
	 */
	private void processMetadataDirectory(Directory dir) {

		//logger.debug("Found directory {} of type {}", dir, dir.getClass());
		switch (dir.getClass().getSimpleName()) {
			case "ExifIFD0Directory":
				readExifIFD0Directory(dir);
				break;
			case "ExifSubIFDDirectory":
				readExifSubIFDDirectory(dir);
				break;
			case "JpegDirectory":
				readJpegDirectory(dir);
				break;
			case "GpsDirectory":
				readGpsDirectory(dir);
				break;
			case "IptcDirectory":
				readIptcDirectory(dir);
				break;
			case "XmpDirectory":
				readXmpDirectory(dir);
				break;
			case "IccDirectory":
				readIccDirectory(dir);
				break;
			case "NikonType2MakernoteDirectory":
				readNikonType2MakernoteDirectory(dir);
				break;
			case "OlympusMakernoteDirectory":
				readOlympusMakernoteDirectory(dir);
				break;
			case "OlympusEquipmentMakernoteDirectory":
				readOlympusEquipmentDirectory(dir);
				break;
			case "OlympusCameraSettingsMakernoteDirectory":
				readOlympusCameraSettingsDirectory(dir);
				break;
			case "CanonMakernoteDirectory":
				readCanonMakernoteDirectory(dir);
				break;
			case "PanasonicMakernoteDirectory":
				readPanasonicMakernoteDirectory(dir);
				break;
			default:
				//TODO: comment in for more medatdata dirs
				//logger.debug("Unknown metadata directory found: {}, {}. Skipping...", dir.getClass().getSimpleName(), dir);
		}

	}

	/**
	 * Get camera information
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readExifIFD0Directory(Directory dir) {

		//don't use dateTime as it is only the last time the exif data was modified
		if (dir.getDescription(ExifIFD0Directory.TAG_DATETIME)!=null) {
			datetimeExif = parseDateTimeFromString(dir.getDescription(ExifIFD0Directory.TAG_DATETIME), exifDateTimeFormat);
		}

		//orientation
		if (dir.getDescription(ExifIFD0Directory.TAG_ORIENTATION)!=null) {
			orientationString = dir.getDescription(ExifIFD0Directory.TAG_ORIENTATION);
		}

		//make
		if (dir.getDescription(ExifIFD0Directory.TAG_MAKE)!=null) {
			camera.setMake(dir.getDescription(ExifIFD0Directory.TAG_MAKE));
		}

		//model
		if (dir.getDescription(ExifIFD0Directory.TAG_MODEL)!=null) {
			camera.setModel(dir.getDescription(ExifIFD0Directory.TAG_MODEL));
		}
	}

	/**
	 * Get picture taking conditions, camera settings etc
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readExifSubIFDDirectory(Directory dir) {

		ExifSubIFDDirectory sd = (ExifSubIFDDirectory) dir;

		//datetime original
		if (sd.containsTag(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL)) {
			DateTime oDate = parseDateTimeFromString(sd.getDescription(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL),
				exifDateTimeFormat);
			datetimeOriginalExif = new DateTime(oDate);
		}

		//datetime digitised
		if (sd.containsTag(ExifSubIFDDirectory.TAG_DATETIME_DIGITIZED)) {
			DateTime dDate = parseDateTimeFromString(sd.getDescription(ExifSubIFDDirectory.TAG_DATETIME_DIGITIZED),
					exifDateTimeFormat);
			datetimeDigitisedExif = new DateTime(dDate);
		}

		//Flash?
		if (sd.containsTag(ExifSubIFDDirectory.TAG_FLASH)) {
			flashString = sd.getDescription(ExifSubIFDDirectory.TAG_FLASH);
		}

		//Comment field to get timezone
		if (sd.containsTag(ExifSubIFDDirectory.TAG_USER_COMMENT)) {
			comment = sd.getDescription(ExifSubIFDDirectory.TAG_USER_COMMENT);
		}

		//Serial number
		if (sd.containsTag(ExifSubIFDDirectory.TAG_BODY_SERIAL_NUMBER)) {
			camera.setSerialNumber(sd.getDescription(ExifSubIFDDirectory.TAG_BODY_SERIAL_NUMBER));
		}

		//Focal length
		if (sd.containsTag(ExifSubIFDDirectory.TAG_FOCAL_LENGTH)) {
			focalLengthString = sd.getDescription(ExifSubIFDDirectory.TAG_FOCAL_LENGTH);
		} else if (sd.containsTag(ExifSubIFDDirectory.TAG_35MM_FILM_EQUIV_FOCAL_LENGTH)) {
			//TODO: does this need conversion?
			focalLengthString = sd.getDescription(ExifSubIFDDirectory.TAG_35MM_FILM_EQUIV_FOCAL_LENGTH);
		}

		//Aperture/F-Number
		if (sd.containsTag(ExifSubIFDDirectory.TAG_APERTURE)) {
			apertureString = sd.getDescription(ExifSubIFDDirectory.TAG_APERTURE);
		} else if (sd.containsTag(ExifSubIFDDirectory.TAG_FNUMBER)) {
			apertureString = sd.getDescription(ExifSubIFDDirectory.TAG_FNUMBER);
		}

		//Shutter Speed
		if (sd.containsTag(ExifSubIFDDirectory.TAG_SHUTTER_SPEED)) {
			shutterSpeed = sd.getDescription(ExifSubIFDDirectory.TAG_SHUTTER_SPEED);
		} else if (sd.containsTag(ExifSubIFDDirectory.TAG_EXPOSURE_TIME)) {
			shutterSpeed = sd.getDescription(ExifSubIFDDirectory.TAG_EXPOSURE_TIME);
		}

		//Exposure compensation
		if (sd.containsTag(ExifSubIFDDirectory.TAG_EXPOSURE_BIAS)) {
			exposureBiasString = sd.getDescription(ExifSubIFDDirectory.TAG_EXPOSURE_BIAS);
		}

		//ISO
		if (sd.containsTag(ExifSubIFDDirectory.TAG_ISO_EQUIVALENT)) {
			isoSpeedString = sd.getDescription(ExifSubIFDDirectory.TAG_ISO_EQUIVALENT);
		}

		//focal plane diagonal
		if (focalPlaneDiagonal==null && sd.containsTag(ExifSubIFDDirectory.TAG_FOCAL_PLANE_X_RESOLUTION)
				&& sd.containsTag(ExifSubIFDDirectory.TAG_FOCAL_PLANE_Y_RESOLUTION)) {

			//in inches. great!
			String strX = sd.getDescription(ExifSubIFDDirectory.TAG_FOCAL_PLANE_X_RESOLUTION);
			String strY = sd.getDescription(ExifSubIFDDirectory.TAG_FOCAL_PLANE_Y_RESOLUTION);

			if (strX!=null && strX.contains("inches") && strY!=null && strY.contains("inches")) {
				strX = strX.replace("inches", "");
				strY = strY.replace("inches", "");
				double x1 = Double.parseDouble(strX.substring(0, strX.indexOf("/")));
				double x2 = Double.parseDouble(strX.substring(strX.indexOf("/")+1, strX.length()))/1000;
				double x = x1/x2*25.4;
				double y1 = Double.parseDouble(strY.substring(0, strY.indexOf("/")));
				double y2 = Double.parseDouble(strY.substring(strY.indexOf("/")+1, strY.length()))/1000;
				double y = y1/y2*25.4;
				double diagonal = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

				//TODO: check
//				focalPlaneDiagonal = Math.round(diagonal*10.0)/10.0;
//				logger.debug("{}x{}->{} {} {}", strX, strY, focalPlaneDiagonal, camera.getMake(), camera.getModel());
			} else {
				logger.debug("Unknown focal plane resolution: {} x {}", strX, strY);
			}
		}
	}

	/**
	 * Get image width and height
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readJpegDirectory(Directory dir) {

		//width
		if (width==null && dir.containsTag(JpegDirectory.TAG_IMAGE_WIDTH)) {
			String tmp = dir.getDescription(JpegDirectory.TAG_IMAGE_WIDTH);
			if (tmp!=null) {
				width = Integer.valueOf(tmp.substring(0,tmp.indexOf(" ")));
			}
		}

		//height
		if (height==null && dir.containsTag(JpegDirectory.TAG_IMAGE_HEIGHT)) {
			String tmp = dir.getDescription(JpegDirectory.TAG_IMAGE_HEIGHT);
			if (tmp!=null) {
				height = Integer.valueOf(tmp.substring(0,tmp.indexOf(" ")));
			}
		}
	}

	/**
	 * Get GPS info
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readGpsDirectory(Directory dir) {

		//latitude
		if (dir.containsTag(GpsDirectory.TAG_LATITUDE)) {
			String lat = dir.getDescription(GpsDirectory.TAG_LATITUDE);
			if (!"0° 0' 0\"".equals(lat)) {
				latitude = lat;
				if (dir.containsTag(GpsDirectory.TAG_LATITUDE_REF)) {
					latRef = dir.getDescription(GpsDirectory.TAG_LATITUDE_REF);
				}
			}
		}

		//longitude
		if (dir.containsTag(GpsDirectory.TAG_LONGITUDE)) {
			String lon = dir.getDescription(GpsDirectory.TAG_LONGITUDE);
			if (!"0° 0' 0\"".equals(lon)) {
				longitude = lon;
				if (dir.containsTag(GpsDirectory.TAG_LONGITUDE_REF)) {
					longRef = dir.getDescription(GpsDirectory.TAG_LONGITUDE_REF);
				}
			}
		}

		//altitide
		if (dir.containsTag(GpsDirectory.TAG_ALTITUDE)) {
			altitude = dir.getDescription(GpsDirectory.TAG_ALTITUDE);
			if (dir.containsTag(GpsDirectory.TAG_ALTITUDE_REF)) {
				altRef = dir.getDescription(GpsDirectory.TAG_ALTITUDE_REF);
				//transform into number for writing:
				if (altRef.toLowerCase().contains("below")) {
					altRef = "1";
				//assume standard to be above sea level
				} else {
					altRef = "0";
				}
			}
		}

		//get GPS date
		if (dir.containsTag(GpsDirectory.TAG_DATE_STAMP) &&
				dir.containsTag(GpsDirectory.TAG_TIME_STAMP) &&
				dir.getDescription(GpsDirectory.TAG_DATE_STAMP) != null &&
				dir.getDescription(GpsDirectory.TAG_TIME_STAMP) != null &&
				!dir.getDescription(GpsDirectory.TAG_DATE_STAMP).isEmpty() &&
				!dir.getDescription(GpsDirectory.TAG_TIME_STAMP).isEmpty()) {

			String tmp = dir.getDescription(GpsDirectory.TAG_DATE_STAMP) + " " +
					dir.getDescription(GpsDirectory.TAG_TIME_STAMP);

			//parse date
			try {
				//"normal" date
				Date gDate = (new SimpleDateFormat("yyyy:MM:dd HH:mm:ss z")).parse(tmp);
				gpsDate = new DateTime(gDate.getTime(), DateTimeZone.UTC);
			} catch (ParseException e) {
				try {
					//date with milliseconds (e.g. normalised)
					Date gDate = (new SimpleDateFormat("yyyy:MM:dd HH:mm:ss.SS z")).parse(tmp);
					gpsDate = new DateTime(gDate.getTime(), DateTimeZone.UTC);
				} catch (ParseException e2) {
					logger.error("Could not parse date {}", tmp, e2);
				}
			}
		}
	}

	/**
	 * Get keywords, program version etc
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readIptcDirectory(Directory dir) {

		dir.getTags().stream().forEach(tag -> {
			if (tag.getTagType() == IptcDirectory.TAG_KEYWORDS) {
				keywords = tag.getDescription();
			}

			/* TODO: parse IPTC tag
			Originating Program - e.g. Shotwell
			Program Version - 0.15.0
			country, location, content,...
			*/
		});
	}

	/**
	 * Serial number & dc tags by shotwell
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readXmpDirectory(Directory dir) {

		//serial number
		if (dir.containsTag(XmpDirectory.TAG_CAMERA_SERIAL_NUMBER) && camera.getSerialNumber()==null) {
			camera.setSerialNumber(dir.getDescription(XmpDirectory.TAG_CAMERA_SERIAL_NUMBER));
		}

		//TODO: dc tags set by shotwell or similar
		XmpDirectory xd = (XmpDirectory) dir;
		xd.getXmpProperties().entrySet().stream().forEach(tag -> {
			//logger.debug(tag.toString());
		});
	}

	/**
	 * Colour related metadata
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readIccDirectory(Directory dir) {

		dir.getTags().stream().forEach(tag -> {
			//logger.debug(tag.toString());
			/* TODO:
			maybe for saving colour values?
			*/
		});
	}

	/**
	 * Read Nikon maker notes
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readNikonType2MakernoteDirectory(Directory dir) {

		if (camera.getSerialNumber()==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_CAMERA_SERIAL_NUMBER)) {
			camera.setSerialNumber(dir.getDescription(NikonType2MakernoteDirectory.TAG_CAMERA_SERIAL_NUMBER));
		}

		if (camera.getSerialNumber()==null
				&& dir.containsTag(NikonType2MakernoteDirectory.TAG_CAMERA_SERIAL_NUMBER_2)) {
			camera.setSerialNumber(dir.getDescription(NikonType2MakernoteDirectory.TAG_CAMERA_SERIAL_NUMBER_2));
		}

		//ISO
		if (isoSpeedString==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_ISO_1) &&
				dir.getDescription(NikonType2MakernoteDirectory.TAG_ISO_1)!=null) {
			isoSpeedString = dir.getDescription(NikonType2MakernoteDirectory.TAG_ISO_1);
		}
		if (isoSpeedString==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_ISO_INFO) &&
				dir.getDescription(NikonType2MakernoteDirectory.TAG_ISO_INFO)!=null) {
			isoSpeedString = dir.getDescription(NikonType2MakernoteDirectory.TAG_ISO_INFO);
		}
		if (isoSpeedString==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_ISO_REQUESTED) &&
				dir.getDescription(NikonType2MakernoteDirectory.TAG_ISO_REQUESTED)!=null) {
			isoSpeedString = dir.getDescription(NikonType2MakernoteDirectory.TAG_ISO_REQUESTED);
		}
		if (isoSpeedString==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_ISO_MODE) &&
				dir.getDescription(NikonType2MakernoteDirectory.TAG_ISO_MODE)!=null) {
			isoSpeedString = dir.getDescription(NikonType2MakernoteDirectory.TAG_ISO_MODE);
		}

		if (lensInfo==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_LENS)) {
			lensInfo = dir.getDescription(NikonType2MakernoteDirectory.TAG_LENS);
		}

		if (exposureBiasString==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_LENS)) {
			exposureBiasString = dir.getDescription(NikonType2MakernoteDirectory.TAG_FLASH_EXPOSURE_COMPENSATION);
		}

		if (aperture==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_LENS_STOPS)) {
			aperture = Double.valueOf(dir.getDescription(NikonType2MakernoteDirectory.TAG_LENS_STOPS));
		}

		if (flash==null && dir.containsTag(NikonType2MakernoteDirectory.TAG_FLASH_USED)) {
			flash = !"Flash Not Used".equals(dir.getDescription(NikonType2MakernoteDirectory.TAG_FLASH_USED));
		}

		/* TODO:
		White Balance RB Coefficients - 452/256 367/256 256/256 256/256
		Light source - NATURAL
		*/

	}

	/**
	 * Read Olympus maker notes
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readOlympusMakernoteDirectory(Directory dir) {

		OlympusMakernoteDirectory od = (OlympusMakernoteDirectory) dir;

		//TODO: illegally sized directory: wait for metadata-extractor fix
//		Iterator iterator = od.getErrors().iterator();
//		while (iterator.hasNext()) {
//			logger.debug(iterator.next().toString());
//		}
//		logger.debug("{}", od);
//		od.getTags().stream().forEach(tag -> {
//			logger.debug(tag.toString());
//		});

		if (od.containsTag(OlympusMakernoteDirectory.TAG_FOCUS_DISTANCE) && focalDistance==null
				&& od.getDescription(OlympusMakernoteDirectory.TAG_FOCUS_DISTANCE)!=null) {
			focalDistance = Double.valueOf(od.getDescription(OlympusMakernoteDirectory.TAG_FOCUS_DISTANCE));
		}

		if (od.containsTag(OlympusMakernoteDirectory.TAG_ISO_VALUE) && isoSpeedString==null
				&& od.getDescription(OlympusMakernoteDirectory.TAG_ISO_VALUE)!=null) {
			isoSpeedString = od.getDescription(OlympusMakernoteDirectory.TAG_ISO_VALUE);
		}
	}

	/**
	 * Read Olympus maker notes
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readOlympusEquipmentDirectory(Directory dir) {

		OlympusEquipmentMakernoteDirectory od = (OlympusEquipmentMakernoteDirectory) dir;

		//TODO:
		//[Olympus Equipment] Lens Serial Number - AC7221486
		//[Olympus Equipment] Lens Model - OLYMPUS M.14-150mm F4.0-5.6 II

		if (od.containsTag(OlympusEquipmentMakernoteDirectory.TAG_LENS_TYPE)
				&& od.getDescription(OlympusEquipmentMakernoteDirectory.TAG_LENS_TYPE)!=null) {
			lensInfo = od.getDescription(OlympusEquipmentMakernoteDirectory.TAG_LENS_TYPE);
		}

		if (od.containsTag(OlympusEquipmentMakernoteDirectory.TAG_SERIAL_NUMBER) && camera.getSerialNumber()==null
				&& od.getDescription(OlympusEquipmentMakernoteDirectory.TAG_SERIAL_NUMBER)!=null) {
			camera.setSerialNumber(od.getDescription(OlympusEquipmentMakernoteDirectory.TAG_SERIAL_NUMBER));
		} else if (od.containsTag(OlympusEquipmentMakernoteDirectory.TAG_INTERNAL_SERIAL_NUMBER) && camera.getSerialNumber()==null
				&& od.getDescription(OlympusEquipmentMakernoteDirectory.TAG_INTERNAL_SERIAL_NUMBER)!=null) {
			camera.setSerialNumber(od.getDescription(OlympusEquipmentMakernoteDirectory.TAG_INTERNAL_SERIAL_NUMBER));
		}

		//don't check if this is set already - this has priority over the calculated value
		if (od.containsTag(OlympusEquipmentMakernoteDirectory.TAG_FOCAL_PLANE_DIAGONAL)
				&& od.getDescription(OlympusEquipmentMakernoteDirectory.TAG_FOCAL_PLANE_DIAGONAL)!=null) {
			String val = od.getDescription(OlympusEquipmentMakernoteDirectory.TAG_FOCAL_PLANE_DIAGONAL);
			focalPlaneDiagonal = Double.valueOf(val.replace("mm", "").trim());
		}

	}

	/**
	 * Read Olympus maker notes
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readOlympusCameraSettingsDirectory(Directory dir) {

		OlympusCameraSettingsMakernoteDirectory od = (OlympusCameraSettingsMakernoteDirectory) dir;

//		logger.debug("{}", od);
//		od.getTags().stream().forEach(tag -> {
//			logger.debug(tag.toString());
//		});

		//TODO:
//20:26:57.785 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Exposure Mode - Program / Shutter speed priority
//20:26:57.785 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Exposure Shift - 0
//20:26:57.786 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Macro Mode - Off
//20:26:57.786 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Focus Mode - Single AF; S-AF, Imager AF / MF
//20:26:57.786 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] AF Areas - (115/255,156/255)-(138/255,176/255)
//20:26:57.786 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] AF Point Selected - (49%,65%) (49%,65%)
//20:26:57.787 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Flash Mode - Off / Fill-in
//20:26:57.789 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Scene Mode - Standard
//20:26:57.792 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Art Filter - Off; 0; 0; 0
//20:26:57.792 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Tone Level - -31999; Highlights 0; -7; 7; -31998; Shadows 0; -7; 7; 0; 0; 0; 0
//20:26:57.793 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Drive Mode - Single Shot / AE Bracketing, Shot 2
//20:26:57.793 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Panorama Mode - Off
//20:26:57.794 [DEBUG] MediaItem:1595 - [Olympus Camera Settings] Date Time UTC - 0000:00:00 00:00:00

		if (od.containsTag(OlympusCameraSettingsMakernoteDirectory.TagWhiteBalanceTemperature) && whiteBalanceString==null
				&& od.getDescription(OlympusCameraSettingsMakernoteDirectory.TagWhiteBalanceTemperature)!=null) {
			whiteBalanceString = od.getDescription(OlympusCameraSettingsMakernoteDirectory.TagWhiteBalanceTemperature);
		}

		//check this
//		if (od.containsTag(OlympusCameraSettingsMakernoteDirectory.TagFocusProcess) && focalDistance==null
//				&& od.getDescription(OlympusCameraSettingsMakernoteDirectory.TagFocusProcess)!=null) {
//			//match a number in the string
//			Pattern intsOnly = Pattern.compile("\\d+");
//			Matcher makeMatch = intsOnly.matcher(od.getDescription(OlympusCameraSettingsMakernoteDirectory.TagFocusProcess));
//			if (makeMatch.find()) {
//				focalDistance = Double.valueOf(makeMatch.group())/100;
//			}
//		}

	}

	/**
	 * Read Canon maker notes
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readCanonMakernoteDirectory(Directory dir) {

		if (dir.containsTag(CanonMakernoteDirectory.TAG_CANON_SERIAL_NUMBER) &&
				camera.getSerialNumber()==null) {
			camera.setSerialNumber(dir.getDescription(CanonMakernoteDirectory.TAG_CANON_SERIAL_NUMBER));
		}

	}

	/**
	 * Read Panasonic maker notes
	 *
	 * @param dir the metadata directory to process (this should already be of the correct class!)
	 */
	private void readPanasonicMakernoteDirectory(Directory dir) {

		if (dir.containsTag(PanasonicMakernoteDirectory.TAG_INTERNAL_SERIAL_NUMBER) &&
				dir.getDescription(PanasonicMakernoteDirectory.TAG_INTERNAL_SERIAL_NUMBER)!=null &&
				camera.getSerialNumber()==null) {
			camera.setSerialNumber(dir.getDescription(PanasonicMakernoteDirectory.TAG_INTERNAL_SERIAL_NUMBER));
		}
	}

	// Getters & Setters //////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieve a map containing all potentially editable values
	 * @return
	 */
	public Map<String, Object> getEditableValues() {

		Map<String, Object> editable = new HashMap<>();

		editable.put("gpsDate", gpsDate);
		editable.put("datetimeOriginalExif", datetimeOriginalExif);
		editable.put("datetimeDigitisedExif", datetimeDigitisedExif);
		editable.put("datetimeExif", datetimeExif);
		editable.put("creationDateFile", creationDateFile);
		editable.put("modificationDateFile", modificationDateFile);
		editable.put("timezoneID", timezoneID);
		editable.put("orientationString", orientationString);
		editable.put("orientation", orientation);
		editable.put("isoSpeedString", isoSpeedString);
		editable.put("isoSpeed", isoSpeed);
		editable.put("lensInfo", lensInfo);
		editable.put("exposureBiasString", exposureBiasString);
		editable.put("exposureBias", exposureBias);
		editable.put("apertureString", apertureString);
		editable.put("aperture", aperture);
		editable.put("flashString", flashString);
		editable.put("flash", flash);
		editable.put("focalLengthString", focalLengthString);
		editable.put("focalLength", focalLength);
		editable.put("focalDistance", focalDistance);
		editable.put("hyperfocalDistance", hyperfocalDistance);
		editable.put("focalPlaneDiagonal", focalPlaneDiagonal);
		editable.put("circleOfConfusion", circleOfConfusion);
		editable.put("shutterSpeed", shutterSpeed);
		editable.put("exposureTime", exposureTime);
		editable.put("temperature", temperature);
		editable.put("lightValue", lightValue);
		editable.put("depthOfField", depthOfField);
		editable.put("focusFrom", focusFrom);
		editable.put("focusTo", focusTo);
		editable.put("fieldOfView", fieldOfView);
		editable.put("digitalZoom", digitalZoom);
		editable.put("whiteBalanceString", whiteBalanceString);
		editable.put("whiteBalance", whiteBalance);
		editable.put("comment", comment);
		editable.put("keywords", keywords);
		editable.put("width", width);
		editable.put("height", height);
		editable.put("latRef", latRef);
		editable.put("latitude", latitude);
		editable.put("longRef", longRef);
		editable.put("longitude", longitude);
		editable.put("altRef", altRef);
		editable.put("altitude", altitude);

		return editable;
	}

	/**
	 * Generates a name which is safe to use as a URI.
	 * Currently this is a human readable part (filename without spaces) and the checksum
	 *
	 * @return the generated safe name
	 */
	public String getSafeName() {
		return getFilename().replace(" ", "_") + "-" + checksum;
	}

	public String getPath() {
		return path;
	}

	public String getFilename() {
		return filename;
	}

	public String getParentDirectory() {
		return parentDirectory;
	}

	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public DateTime getGpsDate() {
		return gpsDate;
	}

	public DateTime getDatetimeOriginalExif() {
		return datetimeOriginalExif;
	}

	public DateTime getDatetimeDigitisedExif() {
		return datetimeDigitisedExif;
	}

	public DateTime getDatetimeExif() {
		return datetimeExif;
	}

	public DateTime getCreationDateFile() {
		return creationDateFile;
	}

	public DateTime getModificationDateFile() {
		return modificationDateFile;
	}

	public String getTimezoneID() {
		return timezoneID;
	}

	public String getKeywords() {
		return keywords;
	}

	public boolean isStereo() {
		return stereo;
	}

	public boolean isVideo() {
		return video;
	}

	public boolean isGeotagged() {
		return geotagged;
	}

	public boolean isNormalised() {
		return normalised;
	}

	public boolean hasThumbnail() {
		return thumbnailGenerated;
	}

	public boolean hasLandscapeOrientation() {
		return orientation!=null?(orientation==Orientation.LANDSCAPE):landscapeOrientation;
	}

	public String getLatitude() {
		return latitude;
	}

	public Double getLatitudeNum() {
		return MemoUtil.convertCoordinates(latitude, latRef);
	}

	public Double getLongitudeNum() {
		return MemoUtil.convertCoordinates(longitude, longRef);
	}

	public String getLongitude() {
		return longitude;
	}

	public String getLatRef() {
		return latRef;
	}

	public String getLongRef() {
		return longRef;
	}

	public String getAltitude() {
		return altitude;
	}

	public String getAltRef() {
		return altRef;
	}

	/**
	 * Retrieves the checksum which depends on:
	 *
	 * - the file itself (to make sure it has not changed)
	 * - the filename (in case it was renamed)
	 * - the file creation date (if it was cut and pasted over the current file from elsewhere)
	 *
	 * @return the checksum string (50 characters long)
	 */
	public String getChecksum() {
		return checksum;
	}

	public String getFileChecksum() {
		return fileChecksum;
	}

	public String getDateChecksum() {
		return dateChecksum;
	}

	public String getFilenameChecksum() {
		return filenameChecksum;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	/**
	 * This represents the ISO speed. -1 means automatic ISO
	 *
	 * @return the ISO speed or null if unknown
	 */
	public Integer getIsoSpeed() {
		return isoSpeed;
	}

	public Double getAperture() {
		return aperture;
	}

	public Double getExposureBias() {
		return exposureBias;
	}

	public Boolean getFlash() {
		return flash;
	}

	public Double getFocalLength() {
		return focalLength;
	}

	public Double getExposureTime() {
		return exposureTime;
	}

	public String getDigitalZoom() {
		return digitalZoom;
	}

	/**
	 * This represents the white balance. -1 means automatic
	 *
	 * @return the white balance or null if unknown
	 */
	public Integer getWhiteBalance() {
		return whiteBalance;
	}

	public Integer getWidth() {
		return width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setGpsDate(DateTime gpsDate) {
		this.gpsDate = gpsDate;
	}

	public void setLocalDate(DateTime localDate) {
		this.datetimeOriginalExif = localDate;
	}

	public void setLatRef(String latRef) {
		this.latRef = latRef;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongRef(String longRef) {
		this.longRef = longRef;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}

	public void setAltRef(String altRef) {
		this.altRef = altRef;
	}

	public Double getTemperature() {
		return temperature;
	}

	public Double getLightValue() {
		return lightValue;
	}

	public Double getFocalDistance() {
		return focalDistance;
	}

	public Double getDepthOfField() {
		return depthOfField;
	}

	public Double getFieldOfView() {
		return fieldOfView;
	}

	public Double getFocusFrom() {
		return focusFrom;
	}

	public Double getFocusTo() {
		return focusTo;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public Long getFilesize() {
		return filesize;
	}

	/**
	 * Find out whether a thumbnail has been created for this media item.
	 * Note that this does not necessarily mean the thumbnail still exist.
	 *
	 * @return true if it has been generated, false if not
	 */
	public boolean thumbnailGenerated() {
		return thumbnailGenerated;
	}

	public String getComment() {
		return comment;
	}

	public boolean isUseExiftool() {
		return useExiftool;
	}

	public boolean isUseWebService() {
		return useWebService;
	}

	public boolean isCreateThumbnail() {
		return createThumbnail;
	}

	public boolean isThumbnailGenerated() {
		return thumbnailGenerated;
	}

	public Metadata getMetadata() {
		return metadata;
	}

	public Double getHyperfocalDistance() {
		return hyperfocalDistance;
	}

	public Double getFocalPlaneDiagonal() {
		return focalPlaneDiagonal;
	}

	public Double getCircleOfConfusion() {
		return circleOfConfusion;
	}

	public String getLensInfo() {
		return lensInfo;
	}

	// ENUMS //////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * A media item's orientationString.
	 * It is a known issue that videos might change orientationString while recording.
	 */
	public enum Orientation {

		UNKNOWN(0),
		PORTRAIT(1),
		LANDSCAPE(2);

		private final int numVal;

		Orientation(int numVal) {
			this.numVal = numVal;
		}

		public int getNumVal() {
			return numVal;
		}

		@Override
		public String toString() {
			return this.name();
		}
	}

}
