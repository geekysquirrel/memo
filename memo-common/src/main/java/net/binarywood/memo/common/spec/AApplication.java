/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.spec;

import java.io.File;
import java.net.URL;
import java.util.MissingResourceException;
import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a JavaFX application
 *
 * @author Stefanie Wiegand
 */
public abstract class AApplication extends Application implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(AApplication.class);

	protected AController controller;

	protected String fxmlFile;
	protected String iconFile;

	/**
	 * Create a new instance of this application
	 */
	public AApplication() {
		super();
	}

	/**
	 * Initialise the application with its FXML and icon file
	 *
	 * @param fxmlFile the FXML file for this application to use, the path is relative to the classpath
	 * @param iconFile the icon for this application
	 */
	public final void init(String fxmlFile, String iconFile) {

		URL fxmlURL = getClass().getClassLoader().getResource(fxmlFile);
		if (fxmlURL==null || !(new File(fxmlURL.getPath())).exists()) {
			throw new MissingResourceException("FXML file " + fxmlFile + " could not be found on the classpath",
				getClass().getName(), fxmlFile);
		}
		this.fxmlFile = fxmlFile;
		this.iconFile = iconFile;
	}

	@Override
	public void start(Stage stage) throws Exception {
		logger.info("Starting {}", this.getClass().getSimpleName());
		controller.setStage(stage);
	}

	// Getters ////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get the controller of this application
	 *
	 * @return the controller for this application
	 */
	public AController getController() {
		return controller;
	}

	/**
	 * Get this application's FXML file
	 * @return the location of the FXML file
	 */
	public String getFxmlFile() {
		return fxmlFile;
	}

	/**
	 * Get this application's icon
	 * @return the location of the icon
	 */
	public String getIconFile() {
		return iconFile;
	}

}
