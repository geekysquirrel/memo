/*
 * Copyright 2017 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.presenter;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Callback;
import javafx.util.Pair;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.spec.APresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a separate dialog to show details about a media item, including a preview of the item.
 */
public class MediaItemPresenter extends APresenter implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(MediaItemPresenter.class);
	private static final double ZOOM_FACTOR = 1.2;

	private MediaItem mediaItem;

	private ImageView imageView;
	private MediaView mediaView;
	private boolean mediaPlaying;

	@FXML
	private ScrollPane scrollPane;
	@FXML
	private StackPane zoomPane;
	@FXML
	private TableView<Pair<String, Object>> exifTable;
	@FXML
	private TableColumn<Pair<String, Object>, String> exifKeyColumn;
	@FXML
	private TableColumn<Pair<String, Object>, Object> exifValueColumn;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		imageView = null;
		mediaView = null;
		mediaPlaying = false;

		//disable standard scroll and zoom content instead
		scrollPane.addEventFilter(ScrollEvent.ANY, (ScrollEvent event) -> {
			zoom(event);
		});
	}

	@FXML
	private void zoom(ScrollEvent event) {

		//only zoom images
		if (imageView!=null) {

			double zoomFactor = ZOOM_FACTOR;
			if (event.getDeltaY() <= 0) {
				// zoom out
				zoomFactor = 1 / zoomFactor;
			}

			// determine scale
			double oldScale = zoomPane.getScaleX();
			double scale = oldScale * zoomFactor;
			double f = (scale / oldScale) - 1;

			// determine offset that we will have to move the node
			Bounds bounds = zoomPane.localToScene(zoomPane.getBoundsInLocal());
			double dx = (event.getSceneX() - (bounds.getWidth() / 2 + bounds.getMinX()));
			double dy = (event.getSceneY() - (bounds.getHeight() / 2 + bounds.getMinY()));

			//do it
			zoomPane.setTranslateX(zoomPane.getTranslateX() - f * dx);
			zoomPane.setTranslateY(zoomPane.getTranslateY() - f * dy);
			zoomPane.setScaleX(scale);
			zoomPane.setScaleY(scale);
		}
		event.consume();
	}

	@FXML
	private void fit(ActionEvent event) {
		fitMediaItemIntoView();
		event.consume();
	}

	@FXML
	private void apply(ActionEvent event) {
		saveMediaItem();
	}

	@FXML
	private void saveAndClose(ActionEvent event) {
		saveMediaItem();
		getController().getStage().close();
		event.consume();
	}

	@FXML
	private void cancel(ActionEvent event) {
		getController().getStage().close();
		event.consume();
	}

	@FXML
	private void startEditing(TableColumn.CellEditEvent<String, String> event) {
		logger.debug("started editing");
		//TODO: implement
		event.consume();
	}

	@FXML
	private void restoreValue(TableColumn.CellEditEvent<String, String> event) {
		logger.debug("cancelled editing");
		//TODO: implement
		event.consume();
	}

	@FXML
	private void checkValue(TableColumn.CellEditEvent<String, String> event) {
		logger.debug("checking value");
		//TODO: implement
		event.consume();
	}

	// non-FXML methods ///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Load a media item for the dialog
	 *
	 * @param mi the media item to load
	 */
	public void loadMediaItem(MediaItem mi) {
		mediaItem = mi;
		logger.info("Showing details for media item {}, file located at {}", mediaItem.getFilename(), mediaItem.getPath());

		//video
		if (mediaItem.isVideo()) {
			mediaView = new MediaView();
			mediaView.setMediaPlayer(new MediaPlayer(new Media(new File(mediaItem.getPath()).toURI().toString())));
			mediaView.setOnMouseClicked((event) -> {
				if (mediaPlaying) {
					mediaView.getMediaPlayer().pause();
					mediaPlaying = false;
				} else {
					mediaView.getMediaPlayer().play();
					mediaPlaying = true;
				}
			});
			//TODO: add controls
			imageView = null;
		//image
		} else {
			File imageFile = new File(mediaItem.getPath());
			imageView = new ImageView();
			mediaView = null;
			imageView.setImage(new Image(imageFile.toURI().toString()));
			imageView.setPreserveRatio(true);
		}
		fitMediaItemIntoView();

		//add data
		ObservableList<Pair<String, Object>> data = FXCollections.observableArrayList();
		mediaItem.getEditableValues().entrySet().forEach(o -> data.add(new Pair<>(o.getKey(), o.getValue())));
		exifTable.getItems().setAll(data);

		exifKeyColumn.setCellValueFactory(new PairKeyFactory());
		exifValueColumn.setCellValueFactory(new PairValueFactory());
		exifValueColumn.setEditable(true);

		exifTable.getColumns().setAll(exifKeyColumn, exifValueColumn);
		exifValueColumn.setCellFactory((TableColumn<Pair<String, Object>, Object> column) -> new PairValueCell());
	}

	/**
	 * Save the media item's EXIF data back into the file itself as well as the UI and the store
	 */
	private void saveMediaItem() {
		//TODO
	}

	/**
	 * Fit the media item so that it fills the view while keeping its aspect ratio and is centered
	 */
	private void fitMediaItemIntoView() {

		if (!mediaItem.isVideo()) {

			//undo any zoom
			zoomPane.setTranslateX(0);
			zoomPane.setTranslateY(0);
			zoomPane.setScaleX(1);
			zoomPane.setScaleY(1);

			//fit into window
			imageView.setFitWidth(scrollPane.getWidth());
			imageView.setFitHeight(scrollPane.getHeight());

			//move to center
			if (mediaItem.hasLandscapeOrientation()) {
				double diff = scrollPane.getLayoutBounds().getHeight() - imageView.getLayoutBounds().getHeight();
				imageView.setTranslateY(diff/2);
			} else {
				double diff = scrollPane.getLayoutBounds().getWidth()- imageView.getLayoutBounds().getWidth();
				imageView.setTranslateX(diff/2);
			}

			zoomPane.getChildren().clear();
			zoomPane.getChildren().add(imageView);
		} else {
			//fit into window
			mediaView.setFitWidth(scrollPane.getWidth());
			mediaView.setFitHeight(scrollPane.getHeight());

			//video doesn't use the same metrics as image so need to calc aspect ratio
			double aspectratio = Double.valueOf(mediaItem.getWidth()/mediaItem.getHeight());

			//move to center
			if (mediaItem.hasLandscapeOrientation()) {
				double diff = scrollPane.getHeight() - scrollPane.getHeight()/aspectratio;
				mediaView.setTranslateY(diff/2);
			} else {
				//TODO: test for portrait format video
				double diff = scrollPane.getWidth()- scrollPane.getWidth()/aspectratio;
				mediaView.setTranslateX(diff/2);
			}

			zoomPane.getChildren().clear();
			zoomPane.getChildren().add(mediaView);
		}
	}



	private class PairKeyFactory implements Callback<TableColumn.CellDataFeatures<Pair<String, Object>, String>, ObservableValue<String>> {
		@Override
		public ObservableValue<String> call(TableColumn.CellDataFeatures<Pair<String, Object>, String> data) {
			return new ReadOnlyObjectWrapper<>(data.getValue().getKey());
		}
	}

	private class PairValueFactory implements Callback<TableColumn.CellDataFeatures<Pair<String, Object>, Object>, ObservableValue<Object>> {
		@Override
		public ObservableValue<Object> call(TableColumn.CellDataFeatures<Pair<String, Object>, Object> data) {
			Object value = data.getValue().getValue();
			return (value instanceof ObservableValue)
					? (ObservableValue<Object>) value
					: new ReadOnlyObjectWrapper<>(value);
		}
	}

	private class PairValueCell extends TableCell<Pair<String, Object>, Object> {
		@Override
		protected void updateItem(Object item, boolean empty) {
			super.updateItem(item, empty);

			if (item != null) {
				if (item instanceof String) {
					setText((String) item);
					setGraphic(null);
				} else if (item instanceof Boolean) {
					CheckBox checkBox = new CheckBox();
					checkBox.setSelected((boolean) item);
					setGraphic(checkBox);
				} else {
					setText(item.toString());
					setGraphic(null);
				}
			} else {
				setText(null);
				setGraphic(null);
			}
		}
	}
}
