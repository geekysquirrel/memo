/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a tree containing all the alignment sets of photos taken with different cameras.
 * It is assumed that it is a tree (specifically: acyclic) and the weights represent seconds,
 * thus allowing for positive and negative weights.
 *
 * @author Stefanie Wiegand
 */
public class AlignmentGraph {

	private static final Logger logger = LoggerFactory.getLogger(AlignmentGraph.class);

	//making this a set makes sure all nodes are unique and add/delete operations deal with duplication automatically
	private final Set<GraphNode> nodes;

	/**
	 * Creates a new, empty alignment graph
	 */
	public AlignmentGraph() {
		this.nodes = new HashSet<>();
	}

	/**
	 * Adds a node to the graph
	 *
	 * @param node the node to add
	 */
	public void addNode(GraphNode node) {
		//logger.debug("Adding node {}", node);
		nodes.add(node);
	}

	/**
	 * Removed a node from the graph
	 *
	 * @param node the node to remove
	 */
	public void removeNode(GraphNode node) {
		nodes.remove(node);
	}

	/**
	 * Gets the distance between two nodes
	 *
	 * @param fromID the ID of the start node
	 * @param toID the ID of the end node
	 * @return the dtstance or null if they are not connected
	 */
	public Long distance(String fromID, String toID) {
		GraphNode start = getNode(fromID);
		GraphNode end = getNode(toID);

		if (start==null || end==null) {
			//Invalid nodes, cannot possibly be connected
			return null;
		}

		//TODO: distance doesn't seem to work for a graph with only two nodes
		Set<String> visitedNodes = new HashSet<>();
		return distance(start, end, null, visitedNodes);
	}

	/**
	 * Finds the distance between two nodes in a graph.
	 * This implements a DFS as the graph to be handled can be expected to be not very big
	 * but is probably more deep than broad in most cases.
	 *
	 * @param from starting node
	 * @param to end node
	 * @param distance initial distance (null if not recursing)
	 * @param visitedNodeIDs a set of the node IDs that have already been processed (to prevent infinite loops)
	 * @return the distance between the two nodes (may be negative if the node has negative outgoing edges)
	 *			or null if the nodes are not connected
	 */
	private Long distance(GraphNode from, GraphNode to, Long distance, Set<String> visitedNodeIDs) {

		//remember original state
		boolean wasNull = distance==null;
		//logger.debug("starting distance: {}", distance);

		//found!
		if (from.linksTo(to)) {
			//logger.debug("Directly connected: {} {}", from, to);
			return from.getEdges().get(to);
		}

		Long localDistance = distance;

		//recurse
		for (GraphNode n: from.getEdges().keySet()) {
			//logger.debug("next node {}. current distance: {}", n, distance);

			//add distance of current edge in case the final node lies along this path
			//logger.debug("adding distance {}", from.getEdges().get(n));
			if (distance==null) {
				localDistance = from.getEdges().get(n);
			} else {
				localDistance += from.getEdges().get(n);
			}

			if (!visitedNodeIDs.contains(n.getId())) {
				visitedNodeIDs.add(n.getId());
				//logger.debug("visited nodes before recursion: {}", visitedNodeIDs);
				Long dist = distance(n, to, localDistance, visitedNodeIDs);
				//logger.debug("visited nodes after return: {}", visitedNodeIDs);

				if (dist!=null) {
					//logger.debug("found some distance: {}", dist);

					//result found; only add up if there was a starting distance
					if (wasNull) {
						return dist;
					} else {
						return dist + localDistance;
					}
				} else {
					//logger.debug("found no distance");

					//remove current distance as final node was not found in this subtree
					//logger.debug("removing distance {}", from.getEdges().get(n));
					if (wasNull) {
						localDistance = null;
					} else {
						localDistance -= from.getEdges().get(n);
					}
				}
			}
		}
		//only get here if not connected
		return null;
	}

	/**
	 * A method to check if the data in the node makes sense.
	 *
	 * @return true if the graph is sensible, false if it isn't
	 */
	public boolean isSensible() {

		//logger.debug("Checking whether the graph is sensible: {}", this);

		//check whether edges have corresponding values in both directions
		for (GraphNode n: nodes) {
			for (Map.Entry<GraphNode, Long> e: n.getEdges().entrySet()) {
				Long there = e.getValue();
				//check the other way round
				for (Map.Entry<GraphNode, Long> oe: e.getKey().getEdges().entrySet()) {
					if (oe.getKey().equals(n)) {
						Long back = oe.getValue();
						//logger.debug("({})->({}) = {}", e.getKey(), oe.getKey(), there);
						//logger.debug("({})->({}) = {}", oe.getKey(), e.getKey(), back);
						if (!back.equals(-there)) {
							logger.warn("Values don't match for ({})-({}): {} / {}, graph is not sensible!",
									e.getKey().getId(), oe.getKey().getId(), there, back);
							return false;
						}
					}
				}
			}
		}

		//no problematic edges found: graph must be sane
		return true;
	}

	@Override
	public String toString() {
		StringBuilder tmp = new StringBuilder("Graph:");
		if (!nodes.isEmpty()) {
			for (GraphNode n: nodes) {
				tmp.append("\n");
				tmp.append(n);
				tmp.append(" -> [ ");
				if (n.getEdges().size()>0) {
					for (Map.Entry<GraphNode, Long> e: n.getEdges().entrySet()) {
						tmp.append(e.getKey());
						tmp.append(":");
						tmp.append(e.getValue());
						tmp.append(" ");
					}
				}
				tmp.append("]");
			}
		} else {
			tmp.append(" no nodes contained.");
		}
		return tmp.toString();
	}

	/**
	 * Get the node with the given ID
	 *
	 * @param nodeID the ID of the node to get
	 * @return the node object or null if it wasn't found
	 */
	public GraphNode getNode(String nodeID) {
		for (GraphNode n: nodes) {
			if (n.getId().equals(nodeID)) {
				return n;
			}
		}
		return null;
	}

	/**
	 * Find out if this graph is empty (has no nodes)
	 *
	 * @return true if it's empty, false if not
	 */
	public boolean isEmpty() {
		return nodes.isEmpty();
	}
}
