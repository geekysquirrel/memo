/*
 * Copyright 2016 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.components;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.AComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a ThemeButton showing a menu for switching the theme of an application at runtime
 *
 * @author Stefanie Wiegand
 */
public final class ThemeButton extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(ThemeButton.class);

	@FXML
	private Button button;
	@FXML
	private ContextMenu themeMenu;

	/**
	 * Create a new ThemeButton with a controller
	 */
	public ThemeButton() {

		super();
		//create context menu
		themeMenu = new ContextMenu();
		try {
			//check css resources folder
			Enumeration<URL> resources = getClass().getClassLoader().getResources("css");
			while (resources.hasMoreElements()) {
				URL next = resources.nextElement();
				File cssDir = new File(next.getFile());
				//scan for CSS files
				for (File css: cssDir.listFiles()) {
					//if it's a theme file, display it
					if (css.getName().contains("Theme")) {
						String themeName = css.getName().replace(".css", "").replace("Theme", "");
						MenuItem menuItem = new MenuItem(themeName);
						menuItem.setOnAction((ActionEvent event) -> {
							logger.info("Applying theme {}", themeName);
							logger.info("controller: {}", getController());
							if (getController()!=null) {
								controller.applyTheme(css.getName());
								//persist theme choice
								MemoOptions.set("display.theme", css.getName());
								MemoOptions.save();
							}
							toggleMenu(event);
						});
						//check if the theme is already in the menu
						boolean found = false;
						for (MenuItem mi: themeMenu.getItems()) {
							if (mi.getText().equals(themeName)) {
								found = true;
								break;
							}
						}
						//add it
						if (!found) {
							themeMenu.getItems().add(menuItem);
						}
					}
				}
			}
		} catch (IOException ex) {
			logger.error("Could not load stylesheets from resources directory", ex);
		}
	}

	@FXML
    private void toggleMenu(ActionEvent event) {
		if (!themeMenu.isShowing()) {
			themeMenu.show(button, Side.BOTTOM, 0.0, 0.0);
		} else {
			themeMenu.hide();
		}
		event.consume();
    }

}
