/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import java.io.File;

/**
 * This class is a normal file EXCEPT that it returns the filename only when converted to string.
 *
 * @author Stefanie Wiegand
 */
public class ShortFile extends File {

	private static final long serialVersionUID = 1L;

	/**
	 * Create a new short file
	 *
	 * @param pathname the file's path
	 */
	public ShortFile(String pathname) {
		super(pathname);
	}

	@Override
	public String toString() {
		return super.getName();
	}
}
