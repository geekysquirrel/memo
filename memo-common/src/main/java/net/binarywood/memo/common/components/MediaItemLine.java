/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.components;

import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.spec.AController;

/**
 * A single MediaItem in its "line" view
 *
 * @author Stefanie Cox
 */
public class MediaItemLine extends AMediaItemComponent {

	/**
	 * Create a new MediaItemLine
	 *
	 * @param mi the MediaItem to display in this MediaItemLine
	 * @param controller the controller used for interactions with other MemO components
	 */
	public MediaItemLine(MediaItem mi, AController controller) {

		super(mi, controller);
		thumbnailImageView.setFitHeight(50);
	}

}
