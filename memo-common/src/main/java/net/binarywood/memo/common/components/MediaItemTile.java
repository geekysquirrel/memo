/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.components;

import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.AController;

/**
 * A single MediaItem in its "tile" view
 *
 * @author Stefanie Cox
 */
public class MediaItemTile extends AMediaItemComponent {

	/**
	 * Create a new MediaItemTile
	 *
	 * @param mi the MediaItem to display in this MediaItemTile
	 * @param controller the controller used for interactions with other MemO components
	 */
	public MediaItemTile(MediaItem mi, AController controller) {

		super(mi, controller);

		//scale to predefined size
		if (mediaItem.hasLandscapeOrientation()) {
			thumbnailImageView.setFitWidth(Double.valueOf(MemoOptions.get("display.thumbnailWidth")));
		} else {
			thumbnailImageView.setFitHeight(Double.valueOf(MemoOptions.get("display.thumbnailHeight")));
		}
	}

}
