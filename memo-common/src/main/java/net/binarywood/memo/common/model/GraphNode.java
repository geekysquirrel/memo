/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import java.util.HashMap;
import java.util.Map;
import net.binarywood.memo.common.exceptions.EdgeOverrideException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The node in a (directed) graph
 *
 * @author Stefanie Wiegand
 */
public class GraphNode {

	private static final Logger logger = LoggerFactory.getLogger(GraphNode.class);

	private final String id;
	private String data;
	private final Map<GraphNode, Long> edges;

	/**
	 * Create a new GraphNode with the given ID and no edges
	 *
	 * @param id a unique identifier for the new node
	 */
	public GraphNode(String id) {
		this.id = id;
		edges = new HashMap<>();
	}

	/**
	 * Create a weighted link from this node to the given node
	 * @param node the node to link to
	 * @param distance the distance (weight) of the new edge
	 * @throws net.binarywood.memo.common.exceptions.EdgeOverrideException if the link would override an existing edge
	 */
	public void linkToNode(GraphNode node, Long distance) throws EdgeOverrideException {
		//check if connection already exists
		if (edges.containsKey(node)) {
			logger.debug("Connection from {} to {} already exists!", this, node);
			if (edges.get(node).equals(distance)) {
				logger.debug("Ignoring duplicate edge");
			} else {
				throw new EdgeOverrideException(
					"Overriding old edge " + edges.get(node) + " with new edge " + distance,
					this.getId(), node.getId()
				);
			}
		}
		edges.put(node, distance);
	}

	/**
	 * Remove the link from this node to the given node
	 *
	 * @param node the node to which this node shall no longer link
	 */
	public void unlinkNode(GraphNode node) {
		edges.remove(node);
	}

	/**
	 * Ask whether this node links to the given node
	 *
	 * @param node the node it potentially links to
	 * @return true if it does, false if it doesn't
	 */
	public boolean linksTo(GraphNode node) {
		return edges.containsKey(node);
	}

	@Override
	public String toString() {
		return id + (data!=null?" (" + data + ")":"");
	}

	// Getters/Setters ////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get this node's ID
	 *
	 * @return the ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * Get all edges of this node
	 *
	 * @return a map of the edges (which node they link to and their weight)
	 */
	public Map<GraphNode, Long> getEdges() {
		return edges;
	}

	/**
	 * Get the data in this node
	 *
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * Set this node's payload
	 *
	 * @param data the date this node should hold
	 */
	public void setData(String data) {
		this.data = data;
	}

}
