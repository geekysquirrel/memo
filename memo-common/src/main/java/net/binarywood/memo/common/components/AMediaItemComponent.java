/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.components;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.presenter.MediaItemPresenter;
import net.binarywood.memo.common.spec.AComponent;
import net.binarywood.memo.common.spec.AController;
import net.binarywood.memo.common.spec.APresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A single MediaItem
 *
 * @author Stefanie Cox
 */
public abstract class AMediaItemComponent extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(AMediaItemComponent.class);

	protected MediaItem mediaItem;
	protected boolean selected;

	protected final ContextMenu contextMenu;

	@FXML protected VBox thumbnailVbox;
	@FXML protected ImageView thumbnailImageView;
	@FXML protected VBox normalisedVbox;
	@FXML protected ImageView normalisedImageView;
	@FXML protected VBox typeVbox;
	@FXML protected ImageView typeImageView;
	@FXML protected VBox geoVbox;
	@FXML protected ImageView geoImageView;
	@FXML protected VBox labelVbox;
	@FXML protected Label filenameLabel;
	@FXML protected Label datetimeLabel;
	@FXML protected Label tagsLabel;

	/**
	 * Create a new MediaItemComponent
	 *
	 * @param mi the MediaItem to display in this MediaItemComponent
	 */
	protected AMediaItemComponent(MediaItem mi, AController controller) {

		super(controller);
		mediaItem = mi;
		selected = false;

		//thumbnail
		File thumbnailFile = new File(MemoOptions.get("path.thumbnailDirectory") + mediaItem.getPath());
		thumbnailImageView.setImage(new Image(thumbnailFile.toURI().toString()));

		//mimetype
		String typeString = null;
		if (mediaItem.isStereo()) {
			typeString = "stereo.png";

		}
		//stereo and video files are mutually exclusive.
		//if this ceases to be the case in the future, this has to be reviewed
		if (mediaItem.isVideo()) {
			typeString = "video.png";
		}
		//attach type icon
		if (typeString != null) {
			File iconFile = new File(getClass().getClassLoader().getResource("icons/" + typeString).getPath());
			typeImageView.setImage(new Image(iconFile.toURI().toString()));
		} else {
			typeVbox.setVisible(false);
		}

		//geotag
		if (!mediaItem.isGeotagged()) {
			geoVbox.setVisible(false);
		} else {
			//attach tooltip with coordinates to the geotagging icon
			Tooltip.install(geoVbox, new Tooltip(mediaItem.getLatRef() + " " + mediaItem.getLatitude() + "\n"
					+ mediaItem.getLongRef() + " " + mediaItem.getLongitude()));
		}
		MenuItem showOnMap = new MenuItem("Show on map");
		showOnMap.setOnAction((ActionEvent e) ->
			//TODO: implement
			logger.debug("Showing {} on map...", mediaItem.getLatRef() + " " + mediaItem.getLatitude()
					+ mediaItem.getLongRef() + " " + mediaItem.getLongitude())
		);
		contextMenu = new ContextMenu(showOnMap);

		//normalised mediaItem
		if (!mediaItem.isNormalised()) {
			normalisedVbox.setVisible(false);
		} else {
			//add UTC time if it exists and attach tooltip to "normalised" icon
			Tooltip.install(
				normalisedVbox, new Tooltip(MediaItem.dateFormatWithTimezone.print(mediaItem.getGpsDate()))
			);
		}

		//label
		filenameLabel.setText(mediaItem.getFilename());
		//check for date
		if (mediaItem.getDatetimeOriginalExif()!=null) {
			String label = MediaItem.dateFormatWithoutTimezone.print(mediaItem.getDatetimeOriginalExif());
			//add local timezone ID if known
			if (mediaItem.getTimezoneID()!=null && !mediaItem.getTimezoneID().isEmpty()) {
				datetimeLabel.setText(label + " " + mediaItem.getTimezoneID());
			} else {
				datetimeLabel.setText(label);
			}
		} else {
			datetimeLabel.setText("");
		}
		//show keywords
		if (mediaItem.getKeywords()!=null) {
			tagsLabel.setText(mediaItem.getKeywords());
		} else {
			tagsLabel.setText("");
		}

		//click events
		mainContainer.setOnMouseClicked((MouseEvent mouseEvent) -> {
			//--double click
			if (mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2){
				logger.debug("Opening media item {}", mi.getFilename());
				APresenter presenter = getController().showDialog("fxml/MediaItemScene.fxml", mi.getFilename(), getController(), getController());
				((MediaItemPresenter) presenter).loadMediaItem(mi);
			//--right click
			} else if (mouseEvent.getButton().equals(MouseButton.SECONDARY)){
				logger.debug("Getting context menu for media item {}", mi.getFilename());
				//TODO: implement
			}
		});
	}

	/**
	 * Hide this MediaItemComponent
	 */
	public void hide() {
		setVisibility(false);
	}

	/**
	 * Show this MediaItemComponent
	 */
	public void show() {
		setVisibility(true);
	}

	/**
	 * Set the visibility for this MediaItemComponent
	 *
	 * @param visible whether to display this MediaItemComponent
	 */
	public void setVisibility(boolean visible) {
		mainContainer.setVisible(visible);
		mainContainer.setManaged(visible);
	}

	/**
	 * Toggle the visibility for this MediaItemComponent; meaning set it to the visibility value it doesn't have currently.
	 */
	public void toggleVisibility() {
		if (mainContainer.isVisible() && mainContainer.isManaged()) {
			setVisible(false);
		} else {
			setVisible(true);
		}
	}

	@FXML
    void selectMediaItem(MouseEvent event) {

		if (event.getButton().equals(MouseButton.PRIMARY)) {
			selected = true;
			if (mainContainer.getStyleClass().contains("inverted")) {
				mainContainer.getStyleClass().remove("inverted");
			} else {
				mainContainer.getStyleClass().add("inverted");
			}
		} else {
			//TODO: figure out what goes here and implement it :)
			logger.debug("Right click on media item {}", mediaItem.getFilename());
		}
		event.consume();
    }

	@FXML
    void showContextMenu(ContextMenuEvent event) {
		contextMenu.show(geoImageView, event.getScreenX(), event.getScreenY());
		event.consume();
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get the MediaItem this MediaItemComponent displays
	 *
	 * @return the MediaItem
	 */
	public MediaItem getMediaItem() {
		return mediaItem;
	}

}
