/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */
package net.binarywood.memo.common.model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Options for MemO, including some constants and some properties to be added manually or loaded from file
 *
 * @author Stefanie Wiegand
 */
public class MemoOptions {

	private static final Logger logger = LoggerFactory.getLogger(MemoOptions.class);

	//all props for read access
	private static final Properties properties = new Properties();
	//general MemO props
	private static final Properties coreProps = new Properties();
	//store props for KB
	private static final Properties storeProps = new Properties();
	//whether the options have been loaded yet
	private static boolean loaded = false;

	//public constants
	//--allowed formats
	private static final String[] FILETYPES_PICTURE = {"jpg", "jpeg", "png", "gif", "mpo", "orf", "raw"};
	private static final String[] FILETYPES_VIDEO = {"mp4", "mov", "m4v", "mts", "flv"};
	//--the name of the properties files
	private static final String CONFIG_PROPERTIES = "config.properties";
	private static final String STORE_PROPERTIES = "store.properties";
	//--the common CSS file
	public static final String CSS_COMMON = "css/MemoCommon.css";
	//--the standard expression for unknown things (specifically make/model)
	public static final String UNKNOWN = "UNKNOWN";
	//--icons
	public static final String ICON_HELP = "icons/help.png";
	public static final String ICON_YES = "icons/yes.png";
	public static final String ICON_NO = "icons/no.png";
	public static final String ICON_WARNING = "icons/warning.png";
	public static final String ICON_OPTIONS = "icons/options.png";
	public static final String ICON_CAMERA = "icons/camera.png";
	public static final String ICON_PHOTO = "icons/photo.png";
	public static final String ICON_CLOCK = "icons/clock.png";

	/**
	 * The possible triple store interfaces
	 */
	public enum storeType {
		SESAME, HTTPENDPOINT
	}

	/**
	 * Load the options from the properties file on classpath. Some properties might specify additional properties files
	 * to be loaded which also happens in this method
	 */
	public static synchronized void load() {

		//check if loaded
		if (loaded) {
			logger.debug("Properties file was already loaded, reloading now");
		}

		try {

			//load core properties
			URL coreFile = MemoOptions.class.getClassLoader().getResource(CONFIG_PROPERTIES);
			if (coreFile != null) {
				logger.debug("Loading MemO properties file from {}", coreFile.getPath());
				coreProps.load(coreFile.openStream());
				properties.putAll(coreProps);
				if (!properties.isEmpty()) {
					loaded = true;
				}
			} else {
				logger.debug("No \"config.properties\" file found on classpath");
			}

			//load store properties
			try {
				URL storeFile = MemoOptions.class.getClassLoader().getResource(STORE_PROPERTIES);
				if (storeFile!=null) {
					logger.debug("Loading store properties file from {}", storeFile.getPath());
					storeProps.load(storeFile.openStream());
					properties.putAll(storeProps);
				} else {
					logger.debug("No store configuration file specified");
				}
			} catch (IOException e) {
				logger.error("Could not load store properties file ", coreProps.getProperty(STORE_PROPERTIES), e);
			}

		} catch (IOException e) {
			logger.error("Error loading properties file \"config.properties\"", e);
		}

		logger.info("MemO properties: {}", properties);
	}

	/**
	 * Save the current properties back to the properties file(s) they were loaded from
	 */
	public static synchronized void save() {
		try {
			logger.debug("Saving properties files:");

			//core props
			URL corePropsURL = MemoOptions.class.getClassLoader().getResource("config.properties");
			if (corePropsURL != null) {
				String corePropsPath = corePropsURL.getPath();
				coreProps.store(new FileOutputStream(corePropsPath), null);
				logger.debug("  - {}", corePropsPath);
			}

			//store props
			if (coreProps.containsKey(STORE_PROPERTIES) && !coreProps.getProperty(STORE_PROPERTIES).isEmpty()) {

				URL storePropsURL = MemoOptions.class.getClassLoader().getResource(STORE_PROPERTIES);
				if (corePropsURL != null) {
					String storePropsPath = storePropsURL.getPath();
					coreProps.store(new FileOutputStream(storePropsPath), null);
					logger.debug("  - {}", storePropsPath);
				}
			}

		} catch (IOException e) {
			logger.error("Error saving properties files", e);
		}
	}

	/**
	 * Get all properties
	 *
	 * @return the current version of the properties
	 */
	public static Properties getProps() {
		return properties;
	}

	/**
	 * Get a specific property
	 *
	 * @param key the name of the property
	 * @return the value for this key or null if it's not set
	 */
	public static String get(String key) {
		return properties.getProperty(key);
	}

	/**
	 * Set a property
	 *
	 * @param key the property to set
	 * @param value the new value for this property
	 */
	public static void set(String key, String value) {

		//set property in order of importance:
		//first check in core...
		if (coreProps.containsKey(key)) {
			coreProps.setProperty(key, value);
			properties.putAll(coreProps);
		//...then in store props
		} else if (storeProps.containsKey(key)) {
			storeProps.setProperty(key, value);
			properties.putAll(storeProps);
		} else {
			throw new IllegalArgumentException("Could not set property " + key
					+ ", no suitable properties file found");
		}
	}

	/**
	 * Check whether the properties have already been loaded
	 *
	 * @return true if they have, false if they haven't
	 */
	public static boolean isLoaded() {
		return loaded;
	}

	// Getters/Setters ////////////////////////////////////////////////////////////////////////////
	/**
	 * Get all allowed picture formats
	 *
	 * @return the allowed formats
	 */
	public static String[] getPictureTypes() {
		return FILETYPES_PICTURE;
	}

	/**
	 * Get all allowed video formats
	 *
	 * @return the allowed formats
	 */
	public static String[] getVideoTypes() {
		return FILETYPES_VIDEO;
	}

}
