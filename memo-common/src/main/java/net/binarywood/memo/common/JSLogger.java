/*
 * Copyright 2017 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a Javascript logging bridge. It can be passed to a WebView engine to pass on console.log() messages to Java.
 */
public class JSLogger {

	private static final Logger logger = LoggerFactory.getLogger(JSLogger.class);

	/**
	 * Log a message
	 *
	 * @param text the message to log
	 */
    public void log(String text) {
        logger.debug(text);
    }
}