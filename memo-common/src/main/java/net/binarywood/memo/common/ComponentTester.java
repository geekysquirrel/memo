/*
 * Copyright 2016 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common;

import java.lang.reflect.Constructor;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.binarywood.memo.common.spec.AComponent;
import net.binarywood.memo.common.spec.AController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class opens a new window and displays the component in it, which is defined with its fully qualified name
 * in the COMPONENT constant.
 *
 * @author Stefanie Wiegand
 */
public class ComponentTester extends Application {

	private static final Logger logger = LoggerFactory.getLogger(ComponentTester.class);
	private static final String COMPONENT = "net.binarywood.memo.common.components.QuickMenu";

	@Override
	public void start(Stage stage) throws Exception {

		try {
			//use reflection to get component class and its controller(s)
			AComponent component = null;
			Class<?> componentClass = getClass().getClassLoader().loadClass(COMPONENT);
			Constructor<?> standardConstructor = null;
			for (Constructor<?> c: componentClass.getConstructors()) {
				if (c.getParameters().length==0) {
					standardConstructor = c;
					break;
				}
			}

			//use default (empty) constructor
			if (standardConstructor!=null) {
				logger.debug("Trying to create component from standard constructor {}", standardConstructor);
				component = (AComponent) standardConstructor.newInstance();
			//if there is a constructor with a controller use it
			} else if (componentClass.getConstructor(AController.class).getParameterCount()==1
					&& componentClass.getConstructor(AController.class).getParameterTypes()[0]==AController.class) {
				logger.debug("Trying to create component with controller");
				//although we're not actually adding a controller but passing null instead
				component = (AComponent) componentClass.getConstructor(AController.class).newInstance((Object) null);
			}

			//build scene
			if (component != null) {
				Scene scene = new Scene(component);
				stage.setTitle(ComponentTester.class.getSimpleName() + ": " + COMPONENT);
				stage.setScene(scene);
				stage.sizeToScene();
				stage.show();
			} else {
				logger.error("Could not start scene for component {}", COMPONENT);
			}
		} catch (ClassNotFoundException e) {
			logger.error("Could not load component {}", COMPONENT, e);
		} catch (NoSuchMethodException e) {
			logger.error("Could not load component {}; no suitable constructor found", COMPONENT, e);
		}
	}

	/**
	 * The runnable tool for testing the component of which the class is defined in the COMPONENT constant
	 *
	 * @param args none
	 */
	public static void main(String[] args) {
		ComponentTester.launch(args);
    }
}
