/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.spec;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import net.binarywood.memo.common.model.MemoOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This defines FXML components and their common methods.
 * They are based on AnchorPanes for better fitting into other components.
 *
 * @author Stefanie Wiegand
 */
public abstract class AComponent extends AnchorPane implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(AComponent.class);

	protected AController controller;
	protected AnchorPane mainContainer;

	/**
	 * Create a new component without a controller (self-managed)
	 */
	public AComponent() {

		//initialise
		super();

		String name = getClass().getSimpleName();

		//load component from FXML
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/" + name + ".fxml"));
		loader.setRoot(this);
		loader.setController(this);
		try {
			mainContainer = loader.load();

			//use common stylesheet
			mainContainer.getStylesheets().add(
					getClass().getClassLoader().getResource(MemoOptions.CSS_COMMON).toExternalForm()
			);

			logger.debug("Created new component {}", name);
		} catch (Exception e) {
			logger.error("Could not load component {} from fxml file", name, e);
			mainContainer = null;
		}
	}

	/**
	 * Create a new component
	 *
	 * @param controller the controller which will control this component and its parent
	 */
	public AComponent(AController controller) {

		this();
		this.controller = controller;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//required for FXML
	}

	/**
	 * Add a tooltip to a node within this component
	 *
	 * @param node the node to which to add the tooltip
	 * @param text the text to display when hovering
	 */
	public final void addTooltip(Node node, String text) {
		Tooltip.install(node, new Tooltip(text));
	}

	/**
	 * Check whether the component was loaded successfully
	 *
	 * @return true if it has been loaded, false if something went wrong and the object is invalid
	 */
	public boolean isLoaded() {
		return mainContainer!=null;
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get the main container which contains this component
	 *
	 * @return the highest level node for this component
	 */
	public AnchorPane getMainContainer() {
		return mainContainer;
	}

	/**
	 * Get the controller of this component
	 *
	 * @return the controller
	 */
	public AController getController() {
		return controller;
	}

	/**
	 * Get the controller of this component
	 *
	 * @param controller the controller
	 */
	public void setController(AController controller) {
		this.controller = controller;
	}

}
