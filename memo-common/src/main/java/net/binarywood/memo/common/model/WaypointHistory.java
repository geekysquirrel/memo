/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import net.divbyzero.gpx.Waypoint;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A history for waypoints
 *
 * @author Stefanie Wiegand
 */
public class WaypointHistory extends DataHistory {

	private static final Logger logger = LoggerFactory.getLogger(WaypointHistory.class);

	/**
	 * Get the waypoint at a specific date
	 *
	 * @param date the date for which to get the waypoint
	 * @return the waypoint
	 */
	public Waypoint getWaypointAtDate(DateTime date) {
		return (Waypoint) super.getDataAtDate(date);
	}
}
