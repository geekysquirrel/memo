/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A combo box which allows filtering by typing
 *
 * @author Stefanie Wiegand
 */
public class FilteringComboBox extends ComboBox<String> {

	private static final Logger logger = LoggerFactory.getLogger(FilteringComboBox.class);

	//the current filter
	private String filter;
	//all items
	private final ObservableList<String> items;
	//inactive items not matching the filter
	private final ArrayList<String> backupItems;

	/**
	 * Creates a new filtering combobox
	 *
	 * @param items all items in the dropdown
	 * @param defaultItem the selected item; may be null
	 */
	public FilteringComboBox(Collection<String> items, String defaultItem) {

		super();

		filter = "";
		this.items = FXCollections.observableArrayList();
		backupItems = new ArrayList<>();

		//add items to internal data structure as well as to UI element
		this.items.addAll(items);
		getItems().addAll(items);

		//select the default item if it exists
		if (defaultItem != null) {
			getSelectionModel().select(defaultItem);
		}

		this.setOnKeyPressed((KeyEvent event) -> findEntry(event));

		this.setOnAction((ActionEvent event) -> pickEntry());
	}

	/**
	 * This is triggered when an entry is selected from the combobox
	 *
	 * @param event the event of selecting an item from the dropdown
	 */
	private void pickEntry() {
		String picked = getSelectionModel().getSelectedItem();
		//update filter if an item is selected manually
		if (picked != null) {
			logger.debug("Picked {}", picked);
			filter = picked;
		}
	}

	/**
	 * This event happens upon typing into the combobox. This can be adding or removing characters. The current string
	 * will be matched against the tiems and a filter applied to hide all items that are not applicable.
	 *
	 * @param event the typing event
	 */
	private void findEntry(KeyEvent event) {

		KeyCode key = event.getCode();
		//only consider letters/numbers and / - _
		if (key.isLetterKey() || key.equals(KeyCode.SLASH) || key.equals(KeyCode.UNDERSCORE)
				|| key.equals(KeyCode.MINUS)) {
			String k = key.getName().toLowerCase();
			//translate special characters
			if (!key.isLetterKey()) {
				switch (k) {
					case "slash":
						k = "/";
						break;
					case "underscore":
						k = "_";
						break;
					case "minus":
						k = "-";
						break;
					default:
						//no default action, ignore everything but the defined characters
						break;
				}
			}
			filter += k.toLowerCase();
			//delete a character on backspace
		} else if (key.equals(KeyCode.BACK_SPACE)) {
			if (filter.length() > 0) {
				filter = filter.substring(0, filter.length() - 1);
				//bring items back (un-filter)
				Iterator<String> it = backupItems.iterator();
				while (it.hasNext()) {
					String item = it.next();
					//if the item matches add it to the box and remove from backed up items
					if (item.toLowerCase().contains(filter)) {
						getItems().add(item);
						it.remove();
					}
				}
			}
		} else {
			return;
		}

		//display current filter in combobox
		logger.debug("filter: {}", filter);
		getSelectionModel().clearSelection();
		setPromptText(filter);

		//find match
		Iterator<String> it = getItems().iterator();
		while (it.hasNext()) {
			String item = it.next();
			//remove all non-matching items from the box but preserve them separately
			if (!item.toLowerCase().contains(filter)) {
				backupItems.add(item);
				it.remove();
			}
		}
	}

	/**
	 * Selects an entry from the dropdown box
	 *
	 * @param entry the entry to be selected. May be null; non-action if it doesn't exist
	 */
	public void selectEntry(String entry) {
		getSelectionModel().select(entry);
	}

}
