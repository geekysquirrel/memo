/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.model;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A history of some sort, which maps an object to timestamps.
 * All internal dates are kept in UTC.
 *
 * @author Stefanie Wiegand
 */
public class DataHistory {

	private static final Logger logger = LoggerFactory.getLogger(DataHistory.class);
	protected SortedMap<DateTime, Object> history;

	/**
	 * Create a new, empty data history
	 */
	public DataHistory() {
		history = new TreeMap<>();
	}

	/**
	 * Removes all entries and restores this history to a virgin state
	 */
	public void clear() {
		history.clear();
	}

	/**
	 * Get the data for a certain date.
	 *
	 * @param date the date for which to get the data
	 * @return the data for this date or - if the date is not explicitly contained - the preceding contained date
	 */
	public Object getDataAtDate(DateTime date) {

		Object data = null;

		DateTime leftBoundary = null;
		DateTime rightBoundary = null;

		DateTime utc = date.withZone(DateTimeZone.UTC);

		for (Map.Entry<DateTime, Object> e : history.entrySet()) {
			if (e.getKey().isBefore(utc)) {
				leftBoundary = e.getKey().withZone(DateTimeZone.UTC);
				data = e.getValue();
			} else {
				rightBoundary = e.getKey().withZone(DateTimeZone.UTC);
				break;
			}
		}
		//logger.debug("Data between {} and {} at {} is {}",
		//	leftBoundary, rightBoundary,  utc.toString(), (data!=null?data:"null"));
		return data;
	}

	/**
	 * Get the data following the data for this date
	 *
	 * @param date the date
	 * @return the next (different) data following the data for this date; similar to "peek"
	 */
	public Object getNextDataForDate(DateTime date) {

		Object data = null;
		Object nextData = null;

		DateTime leftBoundary = null;

		for (Map.Entry<DateTime, Object> e : history.entrySet()) {
			//only start in the
			if (!e.getKey().isBefore(date)) {
				if (leftBoundary==null) {
					leftBoundary = e.getKey();
					data = e.getValue();
				} else {
					nextData = e.getValue();
					break;
				}
			}
		}
		logger.debug("Current data {}, next data {}", data, nextData);
		return data;
	}

	/**
	 * Add a new dataset to this history
	 *
	 * @param changedInto the date of teh new dataset
	 * @param data the actual data for the date
	 */
	public void add(DateTime changedInto, Object data) {
		//make sure only UTC timestamps are contained
		if (changedInto.getZone().equals(DateTimeZone.UTC)) {
			history.put(changedInto, data);
		} else {
			history.put(changedInto.toDateTime(DateTimeZone.UTC), data);
		}
	}

	/**
	 * Remove the data at the given date
	 *
	 * @param dateTime the date for which to remove the data (if it exists)
	 */
	public void remove(DateTime dateTime) {
		history.remove(dateTime);
	}

	/**
	 * Tidies up a history, meaning merge duplicate entries into one
	 */
	public void tidyUp() {
		//look at history: if two timezones follow each other, delete the second change
		Entry<DateTime, Object> prev = null;
		Iterator<Entry<DateTime, Object>> it = history.entrySet().iterator();
		while (it.hasNext()) {
			Entry<DateTime, Object> entry = it.next();
			if (prev!=null) {
				if (prev.getValue().equals(entry.getValue())) {
					it.remove();
					logger.debug("Removing duplicate history entry {} for dates {} and {}",
						entry.getValue(), prev.getKey(), entry.getKey());
				} else {
					prev = entry;
				}
			} else {
				prev = entry;
			}
		}
	}

	/**
	 * Merge the given history into this one, giving priority to this object in case of duplicate entries
	 *
	 * @param dh another data history
	 */
	public void merge(DataHistory dh) {
		dh.getHistory().entrySet().stream().forEach(entry -> {
			if (!history.containsKey(entry.getKey())) {
				history.put(entry.getKey(), entry.getValue());
			} else {
				logger.warn("Entry {} for date {} already contained in data history. Not overwriting old entry",
						entry.getValue(), entry.getKey(), history.get(entry.getKey()));
			}
		});
		tidyUp();
	}

	/**
	 * Get the size of this history
	 *
	 * @return the amount of entries in the history
	 */
	public int size() {
		return history.size();
	}

	/**
	 * Find out whether this history is empty
	 *
	 * @return true if it is, false if not
	 */
	public boolean isEmpty() {
		return history.isEmpty();
	}

	/**
	 * Get the first object from the history without removing it
	 *
	 * @return the object or null if the history was empty
	 */
	public Object peek() {
		if (!history.isEmpty()) {
			return history.get(history.firstKey());
		} else {
			return null;
		}
	}

	/**
	 * Get the last object from the history without removing it
	 *
	 * @return the object or null if the history was empty
	 */
	public Object peekLast() {
		if (!history.isEmpty()) {
			return history.get(history.lastKey());
		} else {
			return null;
		}
	}

	/**
	 * Get and remove the first object from the history
	 *
	 * @return the object or null if the history was empty
	 */
	public Object pop() {
		Object current;
		if (!history.isEmpty()) {
			current = history.get(history.firstKey());
			history.remove(history.firstKey());
		} else {
			current = null;
		}
		return current;
	}

	/**
	 * Get the actual map contained in this history object
	 *
	 * @return the history map
	 */
	public SortedMap<DateTime, Object> getHistory() {
		return history;
	}

	/**
	 * Get a part of the history, covering for as much of the given dates as possible
	 *
	 * @param from the date from which to get history entries
	 * @param to the date until which to get history entries
	 * @return the intersection of the history and the given timeframe
	 */
	public SortedMap<DateTime, Object> getSubHistory(DateTime from, DateTime to) {

		SortedMap<DateTime, Object> h = new TreeMap<>();

		//only go through the trouble if there's a chance of success!
		if (from.isBefore(to)) {

			Entry<DateTime, Object> prev = null;
			for (Entry<DateTime, Object> e: history.entrySet()) {
				//this is the first entry in the subset!
				if (prev!=null && from.isAfter(prev.getKey()) && from.isBefore(e.getKey())) {
					h.put(from, prev.getValue());
				//this is for all subsequent entries before the end date
				} else if (prev!=null && from.isBefore(e.getKey()) && to.isAfter(e.getKey())) {
					h.put(e.getKey(), e.getValue());
				}
				prev = e;
			}
		}

		return h;
	}

	/**
	 * Get the entry set of the history
	 *
	 * @return the entry set of the history map
	 */
	public Set<Entry<DateTime, Object>> entrySet() {
		return history.entrySet();
	}

	@Override
	public String toString() {
		String s = "";
		s = history.entrySet().stream().map(
			e -> "[" + e.getKey().toString() + "] -> [" + e.getValue().toString() + "], "
		).reduce(s, String::concat);
		//cut the last comma and blank
		if (s.length()>2) {
			s = s.substring(0,s.length()-2);
		} else {
			s = "[empty data history]";
		}
		return s;
	}

}
