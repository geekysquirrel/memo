/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.controller;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import javafx.application.Platform;
import javafx.scene.control.ProgressIndicator;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.APresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A file visitor to read MediaItems
 *
 * @author Stefanie Wiegand
 */
public class FindMediaVisitor extends SimpleFileVisitor<Path> {

	private static final Logger logger = LoggerFactory.getLogger(FindMediaVisitor.class);

	private final SortedMap<String, Camera> cameras;
	private final SortedMap<String, MediaItem> mediaItems;

	private final APresenter presenter;

	private int visited;
	private final int total;

	private final boolean useWebService;
	private final boolean createThumbnails;
	private final boolean useExiftool;

	private final ProgressIndicator spinner;

	/**
	 * Creates a FindMediaVisitor, initialising it with the given values.
	 *
	 * @param cameras the initial cameras
	 * @param mediaItems the initial mediaItems
	 * @param presenter the presenter for this FindMediaVisitor
	 * @param spinner the progress indicator which shows the progress for the visiting task
	 * @param totalFiles the total amount of files
	 * @param useWebService whether to use a webservice to get the timezone from the GPS tag
	 * @param createThumbnails whether to create thumbnails while visiting the media items
	 * @param useExiftool whether to permit running exiftool
	 */
	public FindMediaVisitor(SortedMap<String, Camera> cameras, SortedMap<String, MediaItem> mediaItems,
			APresenter presenter, ProgressIndicator spinner, int totalFiles,
			boolean useWebService, boolean createThumbnails, boolean useExiftool) {
		this.cameras = cameras;
		this.mediaItems = mediaItems;
		this.presenter = presenter;
		this.total = totalFiles;
		this.visited = 0;
		this.useWebService = useWebService;
		this.createThumbnails = createThumbnails;
		this.useExiftool = useExiftool;
		this.spinner = spinner;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

		String filePath = file.toAbsolutePath().toString();
		//check for file type (from ending)
		boolean validFile = false;
		List<String> filetypes = new ArrayList<>();
		filetypes.addAll(Arrays.asList(MemoOptions.getPictureTypes()));
		filetypes.addAll(Arrays.asList(MemoOptions.getVideoTypes()));
		for (String allowed: filetypes) {
			if (file.toString().toLowerCase().endsWith("." + allowed)) {
				validFile = true;
				break;
			}
		}

		//actually "visit" file
		if (validFile) {
			visited++;
			//tell presenter (via controller) about progress
			if (presenter!=null && total!=0) {
				Platform.runLater(
					() -> presenter.setStatus("Processing file " + visited + " of " + total)
				);
				presenter.updateProgress((double) visited/total, spinner);
			}
			mediaItems.put(filePath, null);

			//create media item
			MediaItem photo = new MediaItem(filePath, useWebService, createThumbnails, useExiftool);

			//add new cameras to collection
			if (!cameras.containsKey(photo.getCamera().getIdentifier())) {
				cameras.put(photo.getCamera().getIdentifier(), photo.getCamera());
			}
			//add photo to collection
			mediaItems.put(filePath, photo);

		}
		return FileVisitResult.CONTINUE;
	}

	// Getters/Setters ////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Get a list of all cameras encountered
	 *
	 * @return the cameras in a map by cameraID
	 */
	public SortedMap<String, Camera> getCameras() {
		return cameras;
	}

	/**
	 * Get a list of all media items encountered
	 *
	 * @return the media items in a map by absolute path
	 */
	public SortedMap<String, MediaItem> getMediaItems() {
		return mediaItems;
	}

	/**
	 * Get the amount of media items encountered
	 *
	 * @return the total number of media items found
	 */
	public int getVisited() {
		return visited;
	}

}
