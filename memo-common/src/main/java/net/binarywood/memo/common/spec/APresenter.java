/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.spec;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import javafx.event.EventTarget;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class manages UI interaction as a "middle man" between the view (FXML/CSS) and the controller.
 *
 * @author Stefanie Wiegand
 */
public abstract class APresenter implements EventTarget, Initializable {

	private static final Logger logger = LoggerFactory.getLogger(APresenter.class);

	protected AController controller;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		//required for FXML
	}

	/**
	 * By default this method does nothing but subclasses can choose to override it to display
	 * some information.
	 *
	 * @param status the text to display
	 */
	public void setStatus(String status) {
		logger.debug("Setting status is not implemented for {}", getClass().getSimpleName());
	}

	/**
	 * By default this method does nothing but subclasses can choose to override it to display
	 * progress.
	 *
	 * @param progress the current progress in percent
	 * @param spinner the progress indicator which will display the progress. May be null.
	 */
	public void updateProgress(double progress, ProgressIndicator spinner) {
		logger.debug("Current progress: {}", progress);
	}

	/**
	 * Set an image for an ImageView.
	 *
	 * @param iv the imageview to set the image for
	 * @param resourceImage the path of a resource file, relative to the src/main/resources directory.
	 *			This may be null to remove an image
	 */
	public void setImage(ImageView iv, String resourceImage) {
		if (resourceImage!=null) {
			iv.setImage(new Image(getClass().getClassLoader().getResourceAsStream(resourceImage)));
		} else {
			iv.setImage(null);
		}
	}

	/**
	 * Set a tooltip for a node this presenter controls
	 *
	 * @param n the node
	 * @param text the text to display as tooltip
	 * @param imagePath the path of the image to use as a tooltip graphic, null for text only
	 */
	public void setTooltip(Node n, String text, String imagePath) {
		Tooltip t = new Tooltip(text);
		t.setWrapText(true);
		if (imagePath!=null && !imagePath.isEmpty()) {
			t.setGraphic(new ImageView(imagePath));
		}
		Tooltip.install(n, t);
	}

	/**
	 * By default this method does nothing but subclasses can choose to override it to refresh the UI
	 */
	public void refresh() {
		logger.debug("Refreshing presenter {}", this.getClass().getSimpleName());
	}

	@Override
	public EventDispatchChain buildEventDispatchChain(EventDispatchChain tail) {
		tail.prepend((Event event, EventDispatchChain tail1) -> {
			// capturing phase, can handle / modify / substitute / divert the event
			boolean dispatch;
			if (!event.isConsumed()) {
				dispatch = true;
				// forward the event to the rest of the chain
				event = tail1.dispatchEvent(event);
				if (event != null) {
					// bubbling phase, can handle / modify / substitute / divert the event
					logger.debug("BUBBLE");
				}
			} else {
				dispatch = false;
			}
			return dispatch ? event : null;
		});
		return tail;
	}

	// Getters & Setters //////////////////////////////////////////////////////////////////////////

	/**
	 * Set the controller for this presenter
	 *
	 * @param controller the controller
	 */
	public void setController(AController controller) {
		this.controller = controller;
	}

	/**
	 * Get the controller for this presenter
	 *
	 * @return the controller
	 */
	public AController getController() {
		return this.controller;
	}

	/**
	 * Get the root node of the view
	 *
	 * @return the main/root node
	 */
	public Node getMainNode() {
		return controller.getCurrentScene().getRoot();
	}

}
