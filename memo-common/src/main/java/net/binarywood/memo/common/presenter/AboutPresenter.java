/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.presenter;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import net.binarywood.memo.common.spec.APresenter;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Presenter for the about dialog
 *
 * @author Stefanie Wiegand
 */
public class AboutPresenter extends APresenter implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(AboutPresenter.class);

	@FXML
    private Label copyrightLabel;
	@FXML
	private Hyperlink gplLink;
	@FXML
	private Hyperlink emailLink;
    @FXML
    private Button aboutButton;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		copyrightLabel.setText("© 2014-" + (new DateTime()).getYear() + " Stefanie Cox");
	}

	@FXML
    void followGPLLink(ActionEvent event) {
		try {
			logger.info("Opening link {}", gplLink.getText());
			getController().getHostServices().showDocument(gplLink.getText());
		} catch (Exception e) {
			logger.error("Could not open link {}", gplLink.getText(), e);
		}
    }

	@FXML
    void followEmailLink(ActionEvent event) {
		try {
			logger.info("Opening mailto link {}", emailLink.getText());
			getController().getHostServices().showDocument("mailto:" + emailLink.getText() + "?subject=MemO");
		} catch (Exception e) {
			logger.error("Could not open mailto link {}", emailLink.getText(), e);
		}
    }

	@FXML
    void closeAboutDialog(ActionEvent event) {
		getController().getStage().close();
    }

}
