/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A checked exception within the MemO project
 *
 * @author Stefanie Wiegand
 */
public class MemoException extends Exception {
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(MemoException.class);

	/**
	 * Create a new MemO exception
	 *
	 * @param msg the message to log
	 * @param cause  the cause of this exception
	 */
	public MemoException(String msg, Throwable cause) {
		super(msg, cause);
		logger.error("{}: {}", this.getClass().getSimpleName(), msg, cause);
	}

	/**
	 * Create a new MemO exception
	 *
	 * @param msg the message to log
	 */
	public MemoException(String msg) {
		super(msg);
		logger.error("{}: {}", this.getClass().getSimpleName(), msg);
	}
}
