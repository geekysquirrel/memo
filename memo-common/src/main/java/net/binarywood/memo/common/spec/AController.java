/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.spec;

import com.sun.javafx.css.StyleManager;
import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.binarywood.memo.common.exceptions.FXMLLoadingException;
import net.binarywood.memo.common.model.MemoOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the controller class for FXML applications and their components. It is meant to represent a hierarchy
 * of controllers with the root controller pointing to null as its parent. Each controller holds references to its
 * presenter and stage/scene for it to be able to control them all.
 *
 * @author Stefanie Wiegand
 */
public abstract class AController {

	private static final Logger logger = LoggerFactory.getLogger(AController.class);

	private static final int ALERT_WIDTH_PADDING = 300;
	private static final int ALERT_HEIGHT_PADDING = 200;

	protected APresenter presenter = null;
	protected Stage stage;
	protected Scene currentScene;
	protected HostServices hostServices;
	//This is optional if access to store etc is needed and null otherwise
	protected AController parentController;



	/**
	 * Create a new controller and load the options (if they haven't been loaded yet) while doing so.
	 */
	public AController() {
		if (!MemoOptions.isLoaded() || MemoOptions.getProps().isEmpty()) {
			MemoOptions.load();
		}
		this.stage = null;
	}

	/**
	 * Create a controller for a given stage
	 *
	 * @param stage the stage this controller is supposed to control
	 */
	public AController(Stage stage) {
		this();
		this.stage = stage;
	}

	/**
	 * Start an application by running its GUI (displaying its stage) and at the same time retrieve its presenter
	 *
	 * @param app the application to start
	 * @throws FXMLLoadingException if the component could not be loaded from FXML
	 * @return the application's presenter
	 */
	public APresenter showStage(AApplication app) throws FXMLLoadingException {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(app.getFxmlFile()));
			currentScene = new Scene(loader.load());
			hostServices = app.getHostServices();
			//attach common CSS
			currentScene.getStylesheets().add(getClass().getClassLoader().getResource(MemoOptions.CSS_COMMON).toExternalForm());
			presenter = loader.getController();
			presenter.setController(this);

			if (stage!=null) {
				stage.setScene(currentScene);
				stage.setTitle(app.getClass().getSimpleName());
				stage.setResizable(true);
				//check if icon file exists
				File f = new File(getClass().getClassLoader().getResource(app.getIconFile()).getPath());
				if (!f.exists()) {
					logger.warn("Icon at path {} doesn't exist in resources", app.getIconFile());
				}
				stage.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream(app.getIconFile())));
				stage.show();
			}
		} catch (IOException e) {
			throw new FXMLLoadingException("Could not load scene from FXML file " + app.getFxmlFile(), e);
		}

		return presenter;
	}

	/**
	 * Apply a theme
	 *
	 * @param theme the name of the theme to be applied; typically NameTheme.css
	 */
	public void applyTheme(String theme) {

		if (theme!=null && !theme.isEmpty()) {
			logger.debug("Applying theme {}", theme);
			Application.setUserAgentStylesheet(null);
			StyleManager.getInstance().addUserAgentStylesheet(
				getClass().getClassLoader().getResource("css/" + theme).toExternalForm()
			);
		}
	}

	/**
	 * Apply a theme from the properties file if it is defined
	 */
	public void applyTheme() {
		applyTheme(MemoOptions.get("display.theme"));
	}

	/**
	 * Shows a new dialog, which means running an AApplication in a new window
	 *
	 * @param app the app to run
	 * @param parentController the parent controller
	 */
	public void showDialog(AApplication app, AController parentController) {
		logger.debug("Showing dialog {}", app.getClass().getSimpleName());
		Platform.runLater(() -> {
			try {
				app.start(new Stage());
				app.getController().setParentController(parentController);
			} catch (Exception e) {
				logger.error("Error starting {}", app.getClass().getCanonicalName(), e);
			}
		});
	}

	/**
	 * Shows a new scene as a popup
	 *
	 * @param newscene the scene to show
	 * @param windowTitle the title of its window
	 * @return the stage in which the scene is shown
	 */
	private Stage showPopup(Scene newscene, String windowTitle) {

		Stage newStage = new Stage();
		try {
			newStage.setScene(newscene);
			newStage.setTitle(windowTitle);
			newStage.initOwner(this.stage);
			newStage.initStyle(StageStyle.DECORATED);
			newStage.initModality(Modality.WINDOW_MODAL);
			//use MemO CSS for popups too!
			newStage.getScene().getStylesheets().add(
				getClass().getClassLoader().getResource(MemoOptions.CSS_COMMON).toExternalForm()
			);
			newStage.show();
		} catch (Exception e) {
			logger.error("Error creating stage for dialog \"{}\"", windowTitle, e);
		}
		return newStage;
	}

	/**
	 * Make this application display a new dialog
	 *
	 * @param fxmlFile the FXML fle for the dialog to be displayed
	 * @param windowTitle the title of the dialog
	 * @param controller the controller for the dialog
	 * @param parentController the parent controller for the dialog
	 * @return the new dialog's presenter
	 */
	public APresenter showDialog(String fxmlFile, String windowTitle, AController controller,
			AController parentController) {

		APresenter newPresenter = null;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxmlFile));
			Parent parent = loader.load();
			Scene newscene = new Scene(parent);
			newPresenter = loader.getController();
			newPresenter.setController(controller);
			newPresenter.getController().setParentController(parentController);
			Stage s = showPopup(newscene, windowTitle);
			controller.setStage(s);
		} catch (IOException e) {
			logger.error("Error showing dialog \"{}\"", windowTitle, e);
		}
		return newPresenter;
	}

	/**
	 * Create an alert with the given properties
	 *
	 * @param type the alert type (CONFIRMATION, ERROR, INFORMATION, NONE, WARNING)
	 * @param title the title of the alert
	 * @param headerText the header text of the alert (upper text)
	 * @param contentText the content text of the alert (lower text)
	 * @return the new alert
	 * @see javafx.scene.control.Alert.AlertType
	 */
	public Alert createAlert(Alert.AlertType type, String title, String headerText, String contentText) {

		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.setResizable(true);
		//use MemO CSS for alerts too!
		alert.getDialogPane().getStylesheets().add(
			getClass().getClassLoader().getResource(MemoOptions.CSS_COMMON).toExternalForm()
		);
		//set size to fit content
		alert.getDialogPane().setMinHeight(alert.getDialogPane().getHeight() + ALERT_HEIGHT_PADDING);
		alert.getDialogPane().setMinWidth(alert.getDialogPane().getWidth() + ALERT_WIDTH_PADDING);
		return alert;
	}

	// Getters ////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get the stage this controller controls
	 *
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * Set the stage
	 *
	 * @param stage the new stage
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * Get the current scene this controller controls
	 *
	 * @return the scene
	 */
	public Scene getCurrentScene() {
		return currentScene;
	}

	/**
	 * Set the current scene
	 *
	 * @param currentScene the new scene
	 */
	public void setCurrentScene(Scene currentScene) {
		this.currentScene = currentScene;
	}

	/**
	 * Get the parent controller of this controller
	 *
	 * @return the paent controller (might be null)
	 */
	public AController getParentController() {
		return this.parentController;
	}

	/**
	 * Set the parent controller
	 *
	 * @param controller the new parent controller
	 */
	public void setParentController(AController controller) {
		this.parentController = controller;
	}

	/**
	 * This needs to be overwritten for each AController subclass in order to return the correct APresenter subclass
	 *
	 * @return the presenter for this controller
	 */
	public abstract APresenter getPresenter();

	/**
	 * Set the presenter
	 *
	 * @param presenter the new presenter
	 */
	public void setPresenter(APresenter presenter) {
		this.presenter = presenter;
	}

	/**
	 * Get the host services object for e.g. execution of external programs
	 *
	 * @return the host services object
	 */
	public HostServices getHostServices() {
		return hostServices;
	}
}
