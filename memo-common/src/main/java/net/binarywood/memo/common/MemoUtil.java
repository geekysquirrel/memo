/*
 * Copyright 2016 Stefanie Cox
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common;

import java.io.OutputStream;
import java.io.PrintStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A utility class providing static methods that are used across the MemO project.
 *
 * @author Stefanie Cox
 */
public class MemoUtil {

	private static final Logger logger = LoggerFactory.getLogger(MemoUtil.class);

	private MemoUtil() {}

	/**
	 * Format a given duration into a printable string
	 *
	 * @param milliseconds a duration given in milliseconds
	 * @return a string formatting into ms, s or min depending on the duration
	 */
	public static String getTimeStr(long milliseconds) {

		String time;
		if (milliseconds<1000l) {
			time = milliseconds + "ms";
		} else if (milliseconds<60000l) {
			time = Math.round(milliseconds/1000) + "s";
		} else {
			time = Math.round(milliseconds/60000) + "min";
		}
		return time;
	}

	/**
	 * Mute the System.out stream temporarily by relaying it to a dummy stream.
	 *
	 * @return the priginal stream so it can be replaced back after whatever operation you want o hide :)
	 */
	public static PrintStream muteSystemOut() {

		//backup original stream
		PrintStream originalStream = System.out;
		//replace output stream with dummy stream
		PrintStream dummyStream = new PrintStream(new OutputStream() {
			@Override
			public void write(int b) {/*nothing happens here*/
			}
		});
		System.setOut(dummyStream);

		return originalStream;
	}

	/**
	 * Convert a GPS coordinate string into a double
	 *
	 * @param coords the String, which looks like this: 50° 30' 18.32"
	 * @param ref the reference. This can be one of N,E,S,W
	 * @return the double representing the decimal degrees
	 */
	public static Double convertCoordinates(String coords, String ref) {

		Double result = null;

		String[] c = coords.replace("-", "").replace("°", "").replace("'", "").replace("\"", "").split(" ");

		if (c[0]!=null && c[1]!=null && c[2]!=null) {
			result = Double.valueOf(c[0]) + Double.valueOf(c[1])/60 + Double.valueOf(c[2])/3600;

			if (ref.toLowerCase().equals("s") || ref.toLowerCase().equals("w")) {
				result = result*-1;
			}
		}
		//logger.debug("Converted {} {} to {}", ref, coords, result);
		return result;
	}

	/**
	 * Convert a double GPS coordinate into a string
	 *
	 * @param coords the double, which looks like this: -50.18639542
	 * @param lat whether the coordinate is latitude or longitude. Required for the reference
	 * @return the string representing the D M S coordinates
	 */
	public static String convertCoordinates(Double coords, boolean lat) {

		String result;

		//set reference string
		if (lat) {
			if (coords>=0) {
				result = "N";
			} else {
				result = "S";
			}
		} else {
			if (coords>=0) {
				result = "E";
			} else {
				result = "W";
			}
		}

		//get degrees
		Double dd = Math.abs(Math.floor(coords));
		result += " " + dd + "° ";

		//get minutes
		Double rest = coords - dd;
		Double minutes = rest*60;
		Double mm = Math.floor(minutes);
		result += mm + "' ";

		//get seconds
		rest = minutes - mm;
		Double ss = rest*60;
		result += ss + "\"";

		//logger.debug("Converted {} to {}", coords, result);
		return result;
	}

}
