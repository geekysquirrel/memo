/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.components;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import net.binarywood.memo.common.spec.AComponent;
import net.binarywood.memo.common.spec.AController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a collection of actions that are potentially required in multiple components
 *
 * @author Stefanie Wiegand
 */
public class QuickMenu extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(QuickMenu.class);

	@FXML
	private HBox buttonBox;

	/**
	 * Create a new QuickMenu with a controller
	 *
	 * @param controller the controller for this QuickMenu. Needed to execute any actions
	 */
	public QuickMenu(AController controller) {
		super(controller);
		//add the theme ThemeButton
		ThemeButton themeButton = new ThemeButton();
		themeButton.setController(controller);
		buttonBox.getChildren().add(1, themeButton);
	}

	@FXML
    void showAboutDialog(ActionEvent event) {
		if (controller!=null) {
			controller.showDialog("fxml/AboutScene.fxml", "About MemO", controller, controller);
		} else {
			logger.info("No controller is connected, cannot show about dialog");
		}
		event.consume();
    }

	@FXML
    void quit(ActionEvent event) {
		logger.info("Exiting MemO now. Bye bye!");
		Platform.exit();
		event.consume();
    }

}
