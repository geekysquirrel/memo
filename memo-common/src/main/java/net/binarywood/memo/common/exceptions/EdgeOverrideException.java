/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.common.exceptions;

/**
 * An edge in the alignment graph has been overridden
 *
 * @author Stefanie Wiegand
 */
public class EdgeOverrideException extends MemoException {
	private static final long serialVersionUID = 1L;

	private final String fromID;
	private final String toID;

	/**
	 * Create a new EdgeOverrideException
	 *
	 * @param msg the message to log
	 * @param fromID the origin node
	 * @param toID the target node
	 * @param cause the cause for this exception
	 */
	public EdgeOverrideException(String msg, String fromID, String toID, Throwable cause) {
		super(msg, cause);
		this.fromID = fromID;
		this.toID = toID;
	}

	/**
	 * Create a new EdgeOverrideException
	 *
	 * @param msg the message to log
	 * @param fromID the origin node
	 * @param toID the target node
	 */
	public EdgeOverrideException(String msg, String fromID, String toID) {
		this(msg, fromID, toID, null);
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////
	
	public String getFromID() {
		return fromID;
	}

	public String getToID() {
		return toID;
	}

}
