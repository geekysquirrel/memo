# Roadmap

## MemO-common

* create Owner class
* fix AlignmentGraph for graphs with only two nodes
* replace JodaTime with Java8 time
* implement colour functionality for MediaItem
* calculate more composite (exiftool) tags for MediaItem
* differentiate between GPS and UTC date
* make geonames webservide configurable
* implement video thumbnail creation
* improve field of view parsing
* handle more different metadata directories
* improve IPTC, XMP & ICC parsing
* extend unit tests
* Speed up the reading of Metadata, fully replacing exiftool with a native Java library such as metadata-extractor

## MemO-Yoshihiko

* implement database i/o
* write unit tests
* write db connector

## MemO-Adrian

* order cameras alphabetically but case insensitive and model before serial number
* make loading of GPX files recursive and use initial photo dir as starting point
* implement "override normalisation" feature
! update UI while normalising

## MemO-Sherlock

* add more rules

## MemO-Leonardo

* implement tag tree functionality
* integrate Adrian
* implement media item detail view (photo & video)
* integrate tags into UI
* provide option to untag
