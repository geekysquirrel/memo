/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import javafx.embed.swing.JFXPanel;
import junit.framework.TestSuite;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.adrian.events.NormalisationFinishedEvent;
import net.binarywood.memo.adrian.events.NormalisationFinishedEventListener;
import net.binarywood.memo.common.exceptions.EdgeOverrideException;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.yoshihiko.Yoshihiko;
import org.joda.time.DateTime;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *	This test suite tests the various use-cases for photo normalisation.
 *
 * @author Stefanie Wiegand
 */
public class AdrianUseCaseTestSuite extends TestSuite implements NormalisationFinishedEventListener {

	private static final Logger logger = LoggerFactory.getLogger(AdrianUseCaseTestSuite.class);
	private static String photoLocation;
	private PhotoNormaliserController controller;
	private boolean done;

	public AdrianUseCaseTestSuite() {
		photoLocation = AdrianUseCaseTestSuite.class.getClassLoader().getResource("testpictures_usecases").getPath();
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
		logger.debug("Loading options...");
		MemoOptions.load();
		//make jfx panel to initialise Java FX
		JFXPanel workaround = new JFXPanel();
	}

	@Before
	public void setUp() throws Exception {

		logger.debug("Starting controller");
		done = false;
		controller = new PhotoNormaliserController();
		controller.addEventListener(this);
	}

	@Override
	public void eventReceived(NormalisationFinishedEvent event) {
		logger.info("Normalisation finished!");
		done = true;
	}

	// Tests //////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This test tests:
	 *
	 * - panorama pictures
	 * - video (3 different motorola phones)
	 * - flights in airplane mode
	 * - geotagging
	 * - modifying EXIF
	 * - renaming photos
	 * - moving of photos after normalisation
	 * - merging cameras
	 */
	@Test
	public void testCubaUseCase() {
		logger.info("Testing Cuba use case");
		String dir = photoLocation + "/cuba";
		Set<String> errors = new HashSet<>();

		try {
			logger.info("Loading photos");
			controller.readPhotosFromDirectory(dir, true);

			logger.info("Loading camera config file");
			SortedMap<String, Camera> cameras = Yoshihiko.importXML(dir + "/cameraConfig.xml");
			controller.getCameras().putAll(cameras);

			logger.info("Merging cameras");
			controller.mergeCameras(
				controller.getCameras().get("MOTOROLA-XT1068"),
				controller.getCameras().get("Motorola-XT1068-ABC123")
			);
			controller.mergeCameras(
				controller.getCameras().get("MOTOROLA-XT1039"),
				controller.getCameras().get("Motorola-XT1039-ABC123")
			);
			controller.mergeCameras(
				controller.getCameras().get("OLYMPUS_IMAGING_CORP.-E-510"),
				controller.getCameras().get("OLYMPUS_IMAGING_CORP.-E-510-XYZ321")
			);

			logger.info("Computing trip history");
			DateTime startDate = null;
			DateTime endDate = null;
			for (MediaItem p : controller.getPhotos().values()) {
				if (startDate == null || p.getDatetimeOriginalExif().isBefore(startDate)) {
					startDate = p.getDatetimeOriginalExif();
				}
				if (endDate == null || p.getDatetimeOriginalExif().isAfter(endDate)) {
					endDate = p.getDatetimeOriginalExif();
				}
			}
			controller.getTripDetails();

			logger.info("Aligning photos");
			controller.addAlignmentSet(new HashMap<String, String>() {{
				put("Motorola-XT1068-ABC123", dir + "/c3/IMG_20150523_200534135_HDR.jpg");
				put("Motorola-XT1039-ABC123", dir + "/c2/IMG_20150523_200534520_HDR.jpg");
			}});
			controller.addAlignmentSet(new HashMap<String, String>() {{
				put("Motorola-Phone", dir + "/c5/IMG_20150513_203042.jpg");
				put("OLYMPUS_IMAGING_CORP.-E-510-XYZ321", dir + "/c4/PP137610.JPG");
			}});
			controller.addAlignmentSet(new HashMap<String, String>() {{
				put("Motorola-XT1068-ABC123", dir + "/c3/IMG_20150518_140937453.jpg");
				put("NIKON-COOLPIX_S9300", dir + "/c1/DSCN8480.JPG");
			}});
			controller.addAlignmentSet(new HashMap<String, String>() {{
				put("OLYMPUS_IMAGING_CORP.-E-510-XYZ321", dir + "/c4/PP187921.JPG");
				put("NIKON-COOLPIX_S9300", dir + "/c1/DSCN8481.JPG");
			}});
			controller.setAlignmentReferenceCamera("Motorola-XT1068-ABC123");

			logger.info("Applying GPX");
			controller.loadGPX(dir + "/gpx");

			logger.info("Normalising photos");
			controller.normalisationStageTwo(controller.normalisationStageOne(), true, true, true, null);

			//sleep until normalisation is done
			while (!done) {
				Thread.sleep(500);
			}

		} catch (EdgeOverrideException | InterruptedException e) {
			logger.error("Could not test cuba use case from {}", dir, e);
			fail("Could not test cuba use case: " + e.getMessage());
		}

		//check results:
		//directories removed
		if (new File(dir + "/c1").isDirectory() || new File(dir + "/c2").isDirectory()
			|| new File(dir + "/c3").isDirectory() || new File(dir + "/c4").isDirectory()
			|| new File(dir + "/c5").isDirectory()) {
			errors.add("Could not remove empty directories");
		}
		//panorama pic has correct make/model
		try {
			MediaItem pano1 = new MediaItem(dir + "/UTC_2015-05-19_11-53-10-IMG_20150519_075310249.jpg",
				false, false, true);
			MediaItem pano2 = new MediaItem(dir + "/UTC_2015-05-19_23-40-14-PANO_20150520_004014.jpg",
				false, false, true);
			if ("Motorola-XT1068-ABC123".equals(pano1.getCamera().getIdentifier())
				|| "Motorola-Phone".equals(pano2.getCamera().getIdentifier())) {
				errors.add("Panorama pictures have wrong metadata");
			}
		} catch (FileNotFoundException e) {
			errors.add("Could not find normalised panorama pictures");
		}
		//TODO: aligned photos should have the same timestamp
		//TODO: photos in the correct order (flight pics) -> check for UTC
		//TODO: photos geotagged (where applicable)
		//TODO: video has make/model timestamp
		//TODO: video timestamps are correct
		//TODO: all pics have the correct timezoneID
		//TODO: photos should be in main dir & empty directories should be removed

		assertTrue(errors.toString(), errors.isEmpty());
	}

	/**
	 * This test tests:
	 *
	 * - video (nikon camera)
	 * - multiple timezone changes
	 */
	@Test
	public void testGibraltarUseCase() {
		//TODO: add data and implement test
	}

	/**
	 * This test tests:
	 *
	 * - processed flash video (nikon camera)
	 * - camera with factory date/time setting
	 * - alignment
	 */
	@Test
	public void testIrelandUseCase() {
		//TODO: add data and implement test
	}

	/**
	 * This test tests:
	 *
	 * - different travel routes for different cameras
	 * - flying without airplane mode (nautical time)
	 */
	@Test
	public void testNewZealandUseCase() {
		//TODO: add data and implement test
	}
}
