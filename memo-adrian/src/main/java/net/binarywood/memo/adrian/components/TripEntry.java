/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */
package net.binarywood.memo.adrian.components;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Set;
import java.util.TreeSet;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Labeled;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.common.components.FilteringComboBox;
import net.binarywood.memo.common.spec.AComponent;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a single trip enrty, showing the timetone, date and time fields for the user to select. It also has the
 * option to create a trip entry for selected cameras only (as opposed to for all cameras).
 *
 * @author Stefanie Wiegand
 */
public class TripEntry extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(TripEntry.class);

	private static final String WRONG = "wrong";

	private boolean dateOK;
	private boolean timeOK;

	@FXML
	private Pane timezonePlaceholder;
	private FilteringComboBox timezone;
	@FXML
	private DatePicker date;
	@FXML
	private TextField time;
	@FXML
	private CheckBox allCamerasCheckBox;
	@FXML
	private VBox camerasVBox;
	@FXML
	private Button deleteButton;

	/**
	 * Create a new trip entry component
	 *
	 * @param controller the controller which manages this component
	 */
	public TripEntry(PhotoNormaliserController controller) {

		super(controller);

		timezone = new FilteringComboBox(DateTimeZone.getAvailableIDs(), "UTC");
		timezonePlaceholder.getChildren().add(timezone);

		date.getEditor().textProperty().addListener(
			(ObservableValue<? extends String> observable, String oldValue, String newValue) -> checkDate(newValue)
		);

		time.textProperty().addListener(
			(ObservableValue<? extends String> observable, String oldValue, String newValue) -> checkTime(newValue)
		);

		//add all cameras
		controller.getPhotosByCamera().keySet().stream().forEach(c -> addCameraCheckbox(c));

		//hide checkboxes by default
		setCamerasCheckboxesVisibility(false);
	}

	@FXML
	private void toggleAllCameras(ActionEvent event) {

		setCamerasCheckboxesVisibility(!allCamerasCheckBox.isSelected());
		event.consume();
	}

	/**
	 * Tick all the checkboxes with the given IDs
	 *
	 * @param ids the ids to select; all others will be deselected.
	 */
	public void tickCheckboxes(Set<String> ids) {

		//select all cameras if no ids are given or all cameras with photos are selected
		if (ids == null || ids.isEmpty() || ids.equals(getController().getPhotosByCamera().keySet())) {
			allCamerasCheckBox.setSelected(true);
			setCamerasCheckboxesVisibility(false);
		} else {
			allCamerasCheckBox.setSelected(false);
			setCamerasCheckboxesVisibility(true);

			//untick all boxes in first run
			camerasVBox.getChildren().stream().forEach(b -> ids.stream().forEach(
				id -> ((CheckBox) b).setSelected(false)
			));

			//now tick all boxes we want ticked
			camerasVBox.getChildren().stream().forEach(cb -> ids.stream().forEach(id -> {
				if (((Labeled) cb).getText().equals(id)) {
					((CheckBox) cb).setSelected(true);
				}
			}));
		}
	}

	/**
	 * Change the visibility of the individual camera chackboxes
	 *
	 * @param visible true if visible, fals if hidden
	 */
	public final void setCamerasCheckboxesVisibility(boolean visible) {

		if (visible) {
			camerasVBox.setManaged(true);
			camerasVBox.setVisible(true);
		} else {
			camerasVBox.setManaged(false);
			camerasVBox.setVisible(false);
		}
	}

	/**
	 * Add a checkbox for a camera
	 *
	 * @param id the ID of the camera
	 */
	public final void addCameraCheckbox(String id) {

		//check if exists
		boolean exists = false;
		for (Node n : camerasVBox.getChildrenUnmodifiable()) {
			if (((Labeled) n).getText().equals(id)) {
				exists = true;
				break;
			}
		}
		//add
		if (!exists) {
			//cut camera ID to avoid large boxes
			CheckBox cb = new CheckBox(id);
			cb.setTextOverrun(OverrunStyle.CENTER_ELLIPSIS);
			//TODO: can we expand this when there is enough space?
			cb.setPrefWidth(200);
			Tooltip.install(cb, new Tooltip(id));
			camerasVBox.getChildren().add(cb);
		}
	}

	@FXML
	private void deleteTripEntry(ActionEvent event) {

		//don't delete last entry
		if (getController().getPresenter().getTripDetailsContainer().getChildren().size() < 2) {
			logger.debug("Last entry cannot be deleted!");
		} else {
			getController().getPresenter().deleteTripEntry(this);
		}
		event.consume();
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public PhotoNormaliserController getController() {
		return (PhotoNormaliserController) controller;
	}

	/**
	 * Apply a trip entry to this component
	 *
	 * @param date the date of the trip entry
	 * @param zone the timezone of the trip entry
	 */
	public void setEntry(DateTime date, DateTimeZone zone) {

		//date
		this.date.setValue(
				LocalDate.of(date.year().get(), Month.of(date.monthOfYear().get()), date.dayOfMonth().get())
		);

		//time
		DecimalFormat df = new DecimalFormat("00");
		this.time.setText(df.format(date.hourOfDay().get()) + ":"
				+ df.format(date.minuteOfHour().get()) + ":"
				+ df.format(date.secondOfMinute().get()));

		//timezone
		if (zone != null) {
			this.timezone.selectEntry(zone.getID());
		}
	}

	/**
	 * Enable/disable the delete button
	 *
	 * @param enabled true if it is to be enabled
	 */
	public void setDeleteButtonState(boolean enabled) {
		deleteButton.setDisable(!enabled);
	}

	/**
	 * Check whether a entered date is valid
	 *
	 * @param potentialDate the date to check
	 */
	public final void checkDate(String potentialDate) {

		//try parsing the date enforcing the format dd/MM/yyyy
		try {
			DateTimeFormatter dtf = new DateTimeFormatterBuilder()
				.appendFixedDecimal(DateTimeFieldType.dayOfMonth(), 2).appendLiteral('/')
				.appendFixedDecimal(DateTimeFieldType.monthOfYear(), 2).appendLiteral('/')
				.appendFixedDecimal(DateTimeFieldType.year(), 4).toFormatter();
			dtf.parseDateTime(potentialDate);
			dateOK = true;
		} catch (Exception e) {
			dateOK = false;
			logger.debug("{} is not a valid date", potentialDate);
			//deliberatly not logging/rethrowing this exception as it's only debug-level severity
		}

		//setup styles (highlighting of wrong input data)
		if (dateOK) {
			date.getEditor().getStyleClass().remove(WRONG);
		} else if (!date.getStyleClass().contains(WRONG)) {
			date.getEditor().getStyleClass().add(WRONG);
		}

		getController().getPresenter().checkTripDetails();
	}

	/**
	 * Check whether an entered time is valid
	 *
	 * @param potentialTime the time to check
	 */
	public final void checkTime(String potentialTime) {

		//try parsing the date enforcing the format dd/MM/yyyy
		try {
			DateTimeFormatter dtf = new DateTimeFormatterBuilder()
				.appendFixedDecimal(DateTimeFieldType.hourOfDay(), 2).appendLiteral(':')
				.appendFixedDecimal(DateTimeFieldType.minuteOfHour(), 2).appendLiteral(':')
				.appendFixedDecimal(DateTimeFieldType.secondOfMinute(), 2).toFormatter();
			dtf.parseDateTime(potentialTime);
			timeOK = true;
		} catch (Exception e) {
			timeOK = false;
			logger.debug("{} is not a valid time", potentialTime);
			//deliberatly not logging/rethrowing this exception as it's only debug-level severity
		}

		//setup styles (highlighting of wrong input data)
		if (timeOK) {
			time.getStyleClass().remove(WRONG);
		} else if (!time.getStyleClass().contains(WRONG)) {
			time.getStyleClass().add(WRONG);
		}
		getController().getPresenter().checkTripDetails();
	}

	/**
	 * Check whether this trip entry has been completed (contains valid date and time)
	 *
	 * @return true if it has been completed
	 */
	public boolean isComplete() {
		//TODO: also check timezone!
		return dateOK && timeOK;
	}

	/**
	 * Get the local date/time's timestamp
	 *
	 * @return the UNIX timestamp (in milliseconds)
	 */
	public Long getTimestamp() {

		Long timestamp = null;
		LocalDate ld = date.getValue();
		if (ld != null) {
			String[] t = time.getText().split(":");
			//if the time has hour/minute/second
			if (t.length == 3) {
				//date/time are in the timezone mentioned in the entry
				DateTime dt = new DateTime(ld.getYear(), ld.getMonthValue(), ld.getDayOfMonth(),
					Integer.valueOf(t[0]), Integer.valueOf(t[1]), Integer.valueOf(t[2]),
					DateTimeZone.forID(timezone.getValue()));
				timestamp = dt.getMillis();
			} else {
				logger.warn("Unexpected time found: {}, ignoring...", time);
			}
		}
		return timestamp;
	}

	/**
	 * Get the timezone that is currently selected in thes component
	 *
	 * @return the timezone ID
	 */
	public String getTimeZone() {
		return timezone.getValue();
	}

	/**
	 * Get all selected cameras in this component
	 *
	 * @return a set of cameraIDs that have been selected
	 */
	public Set<String> getCameraIDs() {
		Set<String> ids = new TreeSet<>();
		//a camera is selected if its checkbox is ticked
		camerasVBox.getChildrenUnmodifiable().stream().filter(n -> ((CheckBox) n).isSelected()).forEach(
			n -> ids.add(((Labeled) n).getText())
		);
		return ids;
	}

}
