/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.events;

/**
 * An event listener for NormalisationFinishedEvents
 *
 * @author Stefanie Wiegand
 */
@FunctionalInterface
public interface NormalisationFinishedEventListener  {

	/**
	 * Defines what happens when an event was received by a listener
	 *
	 * @param event the event that was received
	 */
	public void eventReceived(NormalisationFinishedEvent event);
}
