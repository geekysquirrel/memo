/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian;

import javafx.stage.Stage;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.AApplication;

/**
 * This is the PhotoNormaliser GUI application
 *
 * @author Stefanie Wiegand
 */
public class PhotoNormaliser extends AApplication {

	/**
	 * Creates a new instance of the PhotoNormaliser
	 */
	public PhotoNormaliser() {
		super();
		init("fxml/PhotoNormaliserScene.fxml", MemoOptions.ICON_CLOCK);
	}

	@Override
	public void start(Stage stage) throws Exception {
		controller = new PhotoNormaliserController();
		super.controller = controller;
		super.start(stage);
		controller.showStage(this);
		controller.applyTheme();
	}

	@Override
	public void run() {
		PhotoNormaliser.launch();
	}

	@Override
	public PhotoNormaliserController getController() {
		return (PhotoNormaliserController) controller;
	}
}
