/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.presenter;

import com.sun.javafx.scene.control.skin.TitledPaneSkin;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.SortedMap;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import net.binarywood.memo.adrian.Adrian;
import net.binarywood.memo.adrian.components.CameraAlignmentPane;
import net.binarywood.memo.adrian.components.Link;
import net.binarywood.memo.adrian.components.LinkCameraIcon;
import net.binarywood.memo.adrian.components.TripEntry;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.adrian.model.DateTimezoneKey;
import net.binarywood.memo.common.exceptions.EdgeOverrideException;
import net.binarywood.memo.common.model.AlignmentGraph;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.model.TimezoneHistory;
import net.binarywood.memo.common.spec.APresenter;
import net.binarywood.memo.yoshihiko.Yoshihiko;
import net.binarywood.memo.yoshihiko.components.CameraManagerPane;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class for the PhotoNormaliserPresenter.
 *
 * @author Stefanie Wiegand
 */
public class PhotoNormaliserPresenter extends APresenter {

	private static final Logger logger = LoggerFactory.getLogger(PhotoNormaliserPresenter.class);

	public static final String WRONG = "wrong";
	public static final String LINKED = "linked";
	public static final String REF_CAM = "refCam";

	private final TimezoneHistory tripHistory = new TimezoneHistory();

	//the currently expanded TitledPane. This is needed as the accordion changed the exp. pane instantly upon click
	private TitledPane activePane;
	//the order of the panels
	private LinkedList<TitledPane> succession;
	//the requirements for the panes
	private HashMap<TitledPane, Set<TitledPane>> requirements;
	//"finished", valid panels
	private Set<TitledPane> done;
	//whether titled paned are optional or compulsory
	private HashMap<TitledPane, Boolean> compulsories;

	@FXML
	private AnchorPane mainContainer;
	@FXML
	private Accordion accordion;
	@FXML
	private Label statusLabel;
	@FXML
	private TitledPane selectPhotosTitledPane;
	@FXML
	private ImageView selectPhotosCheckImage;
	@FXML
	private AnchorPane selectPhotosAnchorPane;
	@FXML
	private ProgressIndicator selectPhotosSpinner;
	@FXML
	private ImageView selectPhotosHelpImage;
	@FXML
	private TextField selectPhotosTextField;
	@FXML
	private Label selectPhotosLabel;
	@FXML
	private CheckBox selectPhotosFixCheckbox;
	@FXML
	private Button selectDirectoryButton;
	@FXML
	private Button selectPhotosNextButton;
	@FXML
	private TitledPane cameraDetailsTitledPane;
	@FXML
	private ImageView cameraDetailsCheckImage;
	@FXML
	private AnchorPane cameraDetailsAnchorPane;
	@FXML
	private Button cameraDetailsNextButton;
	@FXML
	private TitledPane tripDetailsTitledPane;
	@FXML
	private ImageView tripDetailsCheckImage;
	@FXML
	private AnchorPane tripDetailsAnchorPane;
	@FXML
	private ImageView tripDetailsHelpImage;
	@FXML
	private VBox tripDetailsContainer;
	@FXML
	private Button addNewTimezoneButton;
	@FXML
	private Button tripDetailsNextButton;
	@FXML
	private TitledPane alignPhotosTitledPane;
	@FXML
	private ImageView alignPhotosCheckImage;
	@FXML
	private AnchorPane alignPhotosAnchorPane;
	@FXML
	private ImageView alignPhotosHelpImage;
	@FXML
	private CheckBox alignmentCheckbox;
	@FXML
	private AnchorPane alignmentAnchorPane;
	@FXML
	private ChoiceBox<String> refCameraChoiceBox;
	@FXML
	private VBox camerasOverviewVBox;
	@FXML
	private VBox cameraConnectionsVBox;
	@FXML
	private Button alignPhotosNextButton;
	@FXML
	private TitledPane geotaggingTitledPane;
	@FXML
	private ImageView geotaggingCheckImage;
	@FXML
	private AnchorPane geotaggingAnchorPane;
	@FXML
	private ImageView geotaggingHelpImage;
	@FXML
	private TextField geotaggingTextField;
	@FXML
	private Label geotaggingLabel;
	@FXML
	private Label gpxFilesFoundLabel;
	@FXML
	private ProgressIndicator geotaggingSpinner;
	@FXML
	private Button geotaggingNextButton;
	@FXML
	private TitledPane sortingOptionsTitledPane;
	@FXML
	private ImageView sortingOptionsCheckImage;
	@FXML
	private AnchorPane sortingOptionsAnchorPane;
	@FXML
	private ImageView sortingOptionsHelpImage;
	@FXML
	private CheckBox writeTagsCheckbox;
	@FXML
	private CheckBox renameFilesCheckbox;
	@FXML
	private CheckBox removeSubfoldersCheckbox;
	@FXML
	private CheckBox createConfigCheckbox;
	@FXML
	private TextField configDumpTextField;
	@FXML
	private Button configDumpButton;
	@FXML
	private ProgressIndicator configSpinner;
	@FXML
	private Button sortingOptionsSortButton;

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		super.initialize(url, rb);

		//set up "line of succession"
		succession = new LinkedList<>();
		succession.add(selectPhotosTitledPane);
		succession.add(cameraDetailsTitledPane);
		succession.add(tripDetailsTitledPane);
		succession.add(alignPhotosTitledPane);
		succession.add(geotaggingTitledPane);
		succession.add(sortingOptionsTitledPane);

		//sort out requirements
		requirements = new HashMap<>();
		requirements.put(cameraDetailsTitledPane, new HashSet<>(Arrays.asList(selectPhotosTitledPane)));
		requirements.put(tripDetailsTitledPane, new HashSet<>(Arrays.asList(selectPhotosTitledPane)));
		requirements.put(alignPhotosTitledPane, new HashSet<>(Arrays.asList(
			selectPhotosTitledPane, cameraDetailsTitledPane
		)));
		requirements.put(geotaggingTitledPane, new HashSet<>(Arrays.asList(selectPhotosTitledPane)));
		requirements.put(sortingOptionsTitledPane, new HashSet<>(Arrays.asList(
			selectPhotosTitledPane, cameraDetailsTitledPane, tripDetailsTitledPane
		)));

		//determind compulsory titled panes
		compulsories = new HashMap<>();
		compulsories.put(selectPhotosTitledPane, true);
		compulsories.put(cameraDetailsTitledPane, false);
		compulsories.put(tripDetailsTitledPane, true);
		compulsories.put(alignPhotosTitledPane, false);
		compulsories.put(geotaggingTitledPane, false);
		compulsories.put(sortingOptionsTitledPane, false);

		//display icons
		done = new HashSet<>();
		activatePanes();

		//load info from config file
		selectPhotosTextField.setText(MemoOptions.get("path.normaliserDirectory"));

		//add pictures
		setImage(selectPhotosHelpImage, MemoOptions.ICON_HELP);
		setImage(tripDetailsHelpImage, MemoOptions.ICON_HELP);
		setImage(alignPhotosHelpImage, MemoOptions.ICON_HELP);
		setImage(geotaggingHelpImage, MemoOptions.ICON_HELP);
		setImage(sortingOptionsHelpImage, MemoOptions.ICON_HELP);

		//add tooltips
		setTooltip(selectPhotosHelpImage, "Choose the directory from which the photos are loaded.\n"
				+ "All photos from this trip should be in this directory or a subdirectory.", null);
		setTooltip(tripDetailsHelpImage, "The date/time should be given in the chosen timezone.\n"
				+ "If for example you drive from Spain to Portugal at 14:00 CEST,\n"
				+ "you have to put Europe/Lisbon as the timezone and 13:00 as the time.", null);
		setTooltip(alignPhotosHelpImage, "You need to pick one reference camera, which is considered\n"
				+ "to have \"the correct time\". All other selected cameras' photos will be shifted\n"
				+ "to fit that time, overriding their own timezone settings.", null);
		setTooltip(geotaggingHelpImage, "Choose the directory in which the relevant files are kept.\n"
				+ "All contained GPX files will be used to tag your photos.\n"
				+ "If you have picked a reference camera to align your photos,\n"
				+ "make sure the GPX files are in sync with it.", null);
		setTooltip(sortingOptionsHelpImage, "Caution: if the option is chosen, MemO will write/modify metadata\n"
				+ "of your files. It is always recommended to keep a backup.", null);

		//add event handlers
		selectPhotosTextField.textProperty().addListener((observable, oldValue, newValue)
			-> checkDirectoryDetails()
		);
		geotaggingTextField.textProperty().addListener((observable, oldValue, newValue)
			-> checkGeotaggingDetails()
		);

		//hide spinners
		selectPhotosSpinner.setManaged(false);
		selectPhotosSpinner.setVisible(false);
		geotaggingSpinner.setManaged(false);
		geotaggingSpinner.setVisible(false);
		configSpinner.setManaged(false);
		configSpinner.setVisible(false);

		//start on first pane
		activePane = selectPhotosTitledPane;
		accordion.setExpandedPane(selectPhotosTitledPane);
	}

	// Init TitledPanes ///////////////////////////////////////////////////////////////////////////////////////////////

	private void initCameraDetails() {

		((PhotoNormaliserController) controller).addEventHandlers();

		//display camera manager pane
		CameraManagerPane cmp = getController().getCameraManagerPane();
		cmp.setMediaButtonsVisibility(true);
		cmp.showDBArea(false);

		cmp.getController().setParentController(controller);
		AnchorPane.setTopAnchor(cmp, 0.0);
		AnchorPane.setRightAnchor(cmp, 0.0);
		AnchorPane.setBottomAnchor(cmp, 0.0);
		AnchorPane.setLeftAnchor(cmp, 0.0);

		if (!cameraDetailsAnchorPane.getChildren().contains(cmp)) {
			//needs to be executed like this as it could be called from the photo loading thread
			Platform.runLater(() -> {
				cameraDetailsAnchorPane.getChildren().add(cmp);
				//workaround to make sure button is on top
				cameraDetailsAnchorPane.getChildren().remove(cameraDetailsNextButton);
				cameraDetailsAnchorPane.getChildren().add(cameraDetailsNextButton);
			});
		}

	}

	private void initTripDetails() {

		//init
		tripDetailsContainer.getChildren().clear();

		//get trip details
		MediaItem startPhoto = getController().getTripDetails();

		//add a new UI component for each trip entry found
		getController().getTripTimezoneHistory().entrySet().stream().forEach(e -> {

			//add checkboxes to entry and tick applicable cameras
			for (String tz: e.getValue()) {
				TripEntry te = new TripEntry(getController());
				DateTimezoneKey key = new DateTimezoneKey(e.getKey(), tz);
				//logger.debug("key: {}, cams: {}", key, getController().getTripHistoryCameras().get(key));

				//set date and timezone
				te.setEntry(e.getKey(), DateTimeZone.forID(tz));
				te.setCamerasCheckboxesVisibility(true);
				//check all cameras related to this key
				te.tickCheckboxes(getController().getTripHistoryCameras().get(key));
				if (!tripDetailsContainer.getChildrenUnmodifiable().contains(te)) {
					tripDetailsContainer.getChildren().add(te);
				}
			}
		});

		//one timezone needs to be present!
		if (tripDetailsContainer.getChildren().isEmpty()) {

			//init with empty entry
			TripEntry te = new TripEntry(getController());

			//set startdate if present
			if (!getController().getTripTimezoneHistory().isEmpty()) {
				DateTime startDate = getController().getTripTimezoneHistory().firstKey();

				//try to get timezone from earliest photo
				DateTimeZone zone = DateTimeZone.UTC;
				if (startPhoto!=null) {
					Camera startCamera = getController().getCameras().get(startPhoto.getCamera().getIdentifier());
					TimezoneHistory tzh = startCamera.getSettingsHistory();
					zone = tzh.getTimezoneAtDate(startDate);
					//note that we're "losing" the timezone here though the date will be correct
				}
				te.setEntry(startDate, zone);
			}
			tripDetailsContainer.getChildren().add(te);
		}

		//make sure the last entry cannot be deleted
		if (tripDetailsContainer.getChildren().size() == 1) {
			tripDetailsContainer.getChildren().stream().forEach(te ->
				((TripEntry) te).setDeleteButtonState(false)
			);
		}

		//do initial check
		checkTripDetails();
	}

	private void initAlignmentOptions() {

		//disable by default
		//TODO: only if a value hasn't been set
		alignmentCheckbox.setSelected(false);
		alignmentAnchorPane.setDisable(true);

		//add alignment view header
		camerasOverviewVBox.getChildren().clear();
		camerasOverviewVBox.getChildren().add(
			new Link(getController(), getController().getPhotosByCamera().keySet(), null)
		);
		//TODO: also clear alignment sets?

		//add cameras
		refCameraChoiceBox.getItems().clear();
		String selectedCamera = null;
		for (Map.Entry<String, List<MediaItem>> e: getController().getPhotosByCamera().entrySet()) {
			if (e.getValue()!=null && !e.getValue().isEmpty()) {
				refCameraChoiceBox.getItems().add(e.getKey());
				//set random camera as selected so that there is always a selected camera
				//TODO: save state
				selectedCamera = e.getKey();
			}
		}
		setAlignmentRefCam(selectedCamera);

		//add event
		refCameraChoiceBox.setOnAction(event ->
			setAlignmentRefCam(refCameraChoiceBox.getSelectionModel().getSelectedItem())
		);
	}

	private void initSortingOptions() {
		//prevent to skip stages (deactivate go button)
		if (checkDirectoryDetails() && checkCameraDetails()
				&& checkTripDetails() && checkGeotaggingDetails() && checkAlignPhotos()) {
			sortingOptionsSortButton.setDisable(false);
		} else {
			sortingOptionsSortButton.setDisable(true);
		}
		toggleConfigDumpControls(new ActionEvent());
	}

	private void initTitledPane(TitledPane tp) {

		logger.debug("Initialising {}", tp.getId());

		switch (tp.getId()) {
			case "cameraDetailsTitledPane":
				initCameraDetails();
				break;
			case "tripDetailsTitledPane":
				initTripDetails();
				break;
			case "alignPhotosTitledPane":
				initAlignmentOptions();
				break;
			case "sortingOptionsTitledPane":
				initSortingOptions();
				break;
			default:
				logger.debug("No init method specified for {}", tp.getId());
		}

		if (done.contains(tp)) {
			logger.debug("Pane {} previously completed, will now do it again", tp.getId());
			done.remove(tp);
			//reset requirements and update images
			activatePanes();
			displayCheckImages();
		}
	}

	// Check details //////////////////////////////////////////////////////////////////////////////////////////////////

	private boolean checkDirectoryDetails() {
		boolean passed;
		//logger.debug("Check whether {} is a directory", this.selectPhotosTextField.getText());
		if (!new File(this.selectPhotosTextField.getText()).isDirectory()) {
			//only add styles once
			if (!selectPhotosTextField.getStyleClass().contains(WRONG)) {
				selectPhotosTextField.getStyleClass().add(WRONG);
			}
			if (!selectPhotosLabel.getStyleClass().contains(WRONG)) {
				selectPhotosLabel.getStyleClass().add(WRONG);
			}
			passed = false;
		} else {
			selectPhotosTextField.getStyleClass().remove(WRONG);
			selectPhotosLabel.getStyleClass().remove(WRONG);
			passed = true;
		}
		selectPhotosNextButton.setDisable(!passed);

		return passed;
	}

	private boolean checkCameraDetails() {
		//Don't need check whether all cameras that have photos are contained as they can't be deleted, only merged
		return true;
	}

	/**
	 * Check all trip detail entries for sanity/completeness
	 *
	 * @return true if everything is OK, false if there's a problem
	 */
	public boolean checkTripDetails() {

		//no need to check whether there's at least one stage, the last one can't be deleted
		boolean allOk = true;

		//sanity check for date/time fields
		Iterator<Node> it = tripDetailsContainer.getChildrenUnmodifiable().iterator();
		while (it.hasNext()) {
			TripEntry te = (TripEntry) it.next();
			if (!te.isComplete()) {
				allOk = false;
				break;
			}
		}

		//TODO: check trip entries are int the right order? -> sort by date?
		tripDetailsNextButton.setDisable(!allOk);
		return allOk;
	}

	private boolean checkAlignPhotos() {
		//no need to check whether all cameras actually have photos as only those are available anyway
		return true;
	}

	private boolean checkGeotaggingDetails() {
		boolean passed;
		logger.debug("Check if {} is a directory", geotaggingTextField.getText());
		File gpxDir = new File(geotaggingTextField.getText());
		//check directory
		if (gpxDir.exists() || geotaggingTextField.getText().isEmpty()) {
			geotaggingTextField.getStyleClass().remove(WRONG);
			geotaggingLabel.getStyleClass().remove(WRONG);
			passed = true;
			if (!geotaggingTextField.getText().isEmpty() && gpxDir.isDirectory()) {
				//count gpx files
				FilenameFilter gpxFilter = (File dir, String name) ->
					name.toLowerCase().endsWith(".gpx")
				;
				String[] gpxFiles = gpxDir.list(gpxFilter);
				geotaggingLabel.setText("Found " +
					(gpxFiles == null ? "0" : String.valueOf(gpxFiles.length)) + " GPX files");
			} else {
				geotaggingLabel.setText("");
			}
		} else {
			geotaggingTextField.getStyleClass().add(WRONG);
			geotaggingLabel.getStyleClass().add(WRONG);
			geotaggingLabel.setText("The directory doesn't exist. Please choose another one.");
			passed = false;
		}
		geotaggingNextButton.setDisable(!passed);

		return passed;
	}

	private boolean checkSortingOptions() {
		if (createConfigCheckbox.isSelected()) {
			//TODO: check whether the directory exists and highlight if something is wrong
		}
		return true;
	}

	// Save entered details ///////////////////////////////////////////////////////////////////////////////////////////

	private void saveDirectoryDetails() {
		MemoOptions.set("path.normaliserDirectory", selectPhotosTextField.getText());
		MemoOptions.save();

		//check for existence of given directory
		//TODO: don't continue but mark text field
		File file = new File(selectPhotosTextField.getText());
		if (file.isDirectory()) {

			selectPhotosSpinner.setManaged(true);
			selectPhotosSpinner.setVisible(true);

			//TODO: find a way of only doing this once, perhaps set a flag and only reload if the path changed
			Task<Void> task = new Task<Void>() {
				@Override
				public Void call() {
					try {
						getController().readPhotosFromDirectory(
							MemoOptions.get("path.normaliserDirectory"),
							selectPhotosFixCheckbox.isSelected()
						);
					} catch (Exception e) {
						logger.error("Error reading photos", e);
					}

					selectPhotosSpinner.setManaged(false);
					selectPhotosSpinner.setVisible(false);

					Platform.runLater(() -> setStatus(""));

					logger.debug("Finished loading photos");

					//this pane has been processed
					setDone(selectPhotosTitledPane);

					//invalidate other panes that depend on this
					invalidatePane(cameraDetailsTitledPane);
					invalidatePane(tripDetailsTitledPane);
					invalidatePane(alignPhotosTitledPane);

					//attempt switching to next pane. while this should not normally happen in the save method,
					//it needs to be called here as the original call to switch panes does not work because
					//it happens before the loading is completed hence the pane is still not "done"
					showTitledPane(cameraDetailsTitledPane);

					return null;
				}
			};

			//don't bind to task, instead have it modified by the file visitor
			Thread thread = new Thread(task);
			thread.setName("PhotoLoadingThread");
			thread.start();

		} else {
			logger.warn("The directory {} doesn't exist", selectPhotosTextField.getText());
		}
	}

	private void saveCameraDetails() {
		//saving not necessary: camera details are held in CMP

		//this pane has been processed
		setDone(cameraDetailsTitledPane);
		//invalidate other panes that depend on this
		invalidatePane(alignPhotosTitledPane);

		//TODO: updateCamera photosbycamera if cameras have changed
//		logger.debug("{} \n\n {}", ((PhotoNormaliserController) controller).getCameras(),
//				((PhotoNormaliserController) controller).getPhotosByCamera());
	}

	private void saveTripDetails() {
		tripDetailsContainer.getChildren().stream().map(n -> (TripEntry) n).forEach(te -> {
			tripHistory.add(new DateTime(te.getTimestamp()), DateTimeZone.forID(te.getTimeZone()));
			//also add to selected cameras!
			te.getCameraIDs().stream().forEach(cam -> getController().getCameras().get(cam).addLocation(
					new DateTime(te.getTimestamp()), DateTimeZone.forID(te.getTimeZone()))
			);
		});
		setDone(tripDetailsTitledPane);
	}

	private void saveAlignPhotos() {
		getController().setAlignmentReferenceCamera(refCameraChoiceBox.getValue());
		setDone(alignPhotosTitledPane);
	}

	private void saveGeotaggingDetails() {

		//skip if no GPX files selected
		if (geotaggingTextField.getText().isEmpty()) {

			//go straight to the next pane
			setDone(geotaggingTitledPane);
			showTitledPane(sortingOptionsTitledPane);

		} else {

			//check for existence of given directory
			//TODO: don't continue but mark text field
			File file = new File(geotaggingTextField.getText());
			if (file.isDirectory()) {

				geotaggingSpinner.setManaged(true);
				geotaggingSpinner.setVisible(true);

				Task<Void> task = new Task<Void>() {
					@Override
					public Void call() {
						try {
							getController().loadGPX(geotaggingTextField.getText());
						} catch (Exception e) {
							logger.error("Error reading GPX files", e);
						}

						geotaggingSpinner.setManaged(false);
						geotaggingSpinner.setVisible(false);

						Platform.runLater(() ->  setStatus("") );

						logger.debug("Finished loading GPX files");

						//this pane has been processed
						Platform.runLater(() ->  setDone(geotaggingTitledPane) );

						//attempt switching to next pane. while this should not normally happen in the save method,
						//it needs to be called here as the original call to switch panes does not work because
						//it happens before the loading is completed hence the pane is still not "done"
						showTitledPane(sortingOptionsTitledPane);

						return null;
					}
				};

				//don't bind to task, instead have it modified by the file visitor
				Thread thread = new Thread(task);
				thread.setName("GpxLoadingThread");
				thread.start();

			} else {
				logger.warn("The directory {} doesn't exist", geotaggingTextField.getText());
			}
		}
	}

	private void saveSortingOptions() {

		configSpinner.setManaged(true);
		configSpinner.setVisible(true);

		//do first normalisation phase without using the spinner because it requires user input and is very quick
		Map<String, MediaItem> normalisedMediaItems = getController().normalisationStageOne();

		Task<Void> task = new Task<Void>() {
			@Override
			public Void call() {
				try {
					logger.debug("Normalise pictures!");
					getController().normalisationStageTwo(
						normalisedMediaItems,
						writeTagsCheckbox.isSelected(),
						renameFilesCheckbox.isSelected(),
						removeSubfoldersCheckbox.isSelected(),
						createConfigCheckbox.isSelected() ? configDumpTextField.getText() + File.separator : null
					);
				} catch (Exception e) {
					logger.error("Error normalising media", e);
				}

				return null;
			}
		};

		//configSpinner.setProgress(0.0);
//		getController().getProgress().addListener((obs, oldProgress, newProgress) -> {
//			logger.debug("Updating progress: {}", newProgress);
//			updateProgress((double) newProgress, configSpinner);
//		});

		//TODO: debug
		configSpinner.progressProperty().bind(getController().getProgress());
		//don't bind to task, instead have it modified by the file visitor
		Thread thread = new Thread(task);
		thread.setName("NormalisingThread");
		thread.start();
	}

	private boolean saveTitledPaneValues(TitledPane tp) {

		logger.debug("Saving details for {}", tp.getId());

		boolean ok = true;

		switch (tp.getId()) {
			case "selectPhotosTitledPane":
				//can't switch away if this isn't filled
				if (!checkDirectoryDetails()) {
					ok = false;
				} else {
					saveDirectoryDetails();
					//set this to false as the thread will take care of switching the pane itself once it's completed
					ok = false;
				}
				break;
			case "cameraDetailsTitledPane":
				saveCameraDetails();
				break;
			case "tripDetailsTitledPane":
				saveTripDetails();
				break;
			case "geotaggingTitledPane":
				saveGeotaggingDetails();
				//set this to false as the thread will take care of switching the pane itself once it's completed
				ok = false;
				break;
			case "alignPhotosTitledPane":
				saveAlignPhotos();
				break;
			case "sortingOptionsTitledPane":
				saveSortingOptions();
				//set this to false as the thread will take care of switching the pane itself once it's completed
				ok = false;
				break;
			default:
				logger.debug("No save method specified for {}", tp.getId());
		}

		return ok;
	}

	// Jumping between TitledPanes ////////////////////////////////////////////////////////////////////////////////////

	@FXML
	private void clickedSelectPhotosPane(MouseEvent event) {
		if (event.getTarget().getClass().equals(TitledPaneSkin.class)) {
			showTitledPane(selectPhotosTitledPane);
		}
		event.consume();
	}

	@FXML
	private void clickedCameraDetailsPane(MouseEvent event) {
		if (event.getTarget().getClass().equals(TitledPaneSkin.class)) {
			showTitledPane(cameraDetailsTitledPane);
		}
		event.consume();
	}

	@FXML
	private void clickedTripDetailsPane(MouseEvent event) {
		if (event.getTarget().getClass().equals(TitledPaneSkin.class)) {
			showTitledPane(tripDetailsTitledPane);
		}
		event.consume();
	}

	@FXML
	private void clickedAlignPhotosPane(MouseEvent event) {
		if (event.getTarget().getClass().equals(TitledPaneSkin.class)) {
			showTitledPane(alignPhotosTitledPane);
		}
		event.consume();
	}

	@FXML
	private void clickedGeotaggingPane(MouseEvent event) {
		if (event.getTarget().getClass().equals(TitledPaneSkin.class)) {
			showTitledPane(geotaggingTitledPane);
		}
		event.consume();
	}

	@FXML
	private void clickedSortingOptionsPane(MouseEvent event) {
		if (event.getTarget().getClass().equals(TitledPaneSkin.class)) {
			showTitledPane(sortingOptionsTitledPane);
		}
		event.consume();
	}

	@FXML
	private void showNextTitledPane(ActionEvent event) {
		switchTitledPane(activePane, getNextInSuccession(accordion.getExpandedPane()));
		event.consume();
	}

	/**
	 * Find the next TitledPane to be displayed
	 *
	 * @param current the currently expanded TitledPane
	 * @return the following TitledPane
	 */
	private TitledPane getNextInSuccession(TitledPane current) {
		TitledPane next = null;
		Iterator<TitledPane> it = succession.iterator();
		while (it.hasNext()) {
			TitledPane candidate = it.next();
			if (candidate.equals(current)) {
				if (it.hasNext()) {
					next = it.next();
				} else {
					next = null;
				}
				break;
			}
		}
		return next;
	}

	/**
	 * Switch the currently active titled pane
	 *
	 * @param from the previous pane to hide
	 * @param to the new pane to show
	 */
	private void switchTitledPane(TitledPane from, TitledPane to) {

		if (from == null || from == to) {
			logger.debug("Collapsing/expanding a pane; not doing anything");
			return;
		}

		logger.debug("Switching from {} to {}", from, to);

		//only save if there were any changes; don't save for closing a pane
		if (!done.contains(from) && !saveTitledPaneValues(from)) {
			logger.debug("Cannot switch as action is not completed");
			//stop switching if there was a problem with saving
			return;
		}

		//TODO: reassign tick/cross for titledpanes that depend on this titledpane
		displayCheckImages();

		//close this titledpane and open the next titledpane
		activePane = to;
		accordion.setExpandedPane(to);

		//init the next pane
		initTitledPane(to);
	}

	/**
	 * Switch the currently active TitledPane
	 *
	 * @param tp the new TitledPane to expand
	 */
	private void showTitledPane(TitledPane tp) {

		logger.debug("Trying to expand {}", tp);

		//only switch if requirements satisfied
		boolean doit = true;
		if (requirements.containsKey(tp)) {
			for (TitledPane req: requirements.get(tp)) {
				if (!done.contains(req)) {
					logger.debug("Requirement {} for {} not fulfilled, not switching pane", req, tp);
					doit = false;
					break;
				}
			}
		}
		if (doit) {
			switchTitledPane(activePane, tp);
		} else {
			accordion.setExpandedPane(activePane);
		}

	}

	/**
	 * Declare a TitledPane as done, meaning whatever had it as its requirement will now be satisfied.
	 *
	 * @param tp the pane that is now done
	 */
	private void setDone(TitledPane tp) {
		done.add(tp);
		activatePanes();
		//TODO: unset the done attr whenever we jump back AND make a change
		//TODO: don't set something as done whenever the values are saved; have a separate method to sanity check and validate
	}

	/**
	 * Declare a pane "undone", i.e. remove it from the panes that are done
	 *
	 * @param pane
	 */
	private void invalidatePane(TitledPane pane) {
		if (done.contains(pane)) {
			done.remove(pane);
		}
	}

	/**
	 * Activate all panes whose requirements are satisfied; disable the others
	 */
	private void activatePanes() {
		for (TitledPane tp : succession) {

			boolean active = true;
			if (requirements.containsKey(tp)) {
				for (TitledPane req : requirements.get(tp)) {
					if (!done.contains(req)) {
						//logger.debug("Requirement {} for {} not fulfilled, deactivating pane", req, tp);
						active = false;
						break;
					}
				}
			}
			if (active) {
				tp.setDisable(false);
			} else {
				tp.setDisable(true);
			}
		}
	}

	/**
	 * Set an image for a titled pane
	 *
	 * @param tp the pane
	 * @param image the image to display or null for no image
	 */
	private void setPaneImage(TitledPane tp, String image) {

		//get the image view for this pane
		ImageView iv = null;
		switch (tp.getId()) {
			case "selectPhotosTitledPane":
				iv = selectPhotosCheckImage;
				break;
			case "cameraDetailsTitledPane":
				iv = cameraDetailsCheckImage;
				break;
			case "tripDetailsTitledPane":
				iv = tripDetailsCheckImage;
				break;
			case "geotaggingTitledPane":
				iv = geotaggingCheckImage;
				break;
			case "alignPhotosTitledPane":
				iv = alignPhotosCheckImage;
				break;
			case "sortingOptionsTitledPane":
				iv = sortingOptionsCheckImage;
				break;
			default:
				break;
		}
		if (iv != null) {
			setImage(iv, image);
		}
	}

	/**
	 * Show icons for each TitledPane depending on whether it has been processed (and is still valid)
	 */
	private void displayCheckImages() {

		succession.stream().forEach(tp -> {
			if (tp.isDisabled()) {
				setPaneImage(tp, null);
			} else if (done.contains(tp)) {
				setPaneImage(tp, MemoOptions.ICON_YES);
			} else {
				//only set image if the titled pane is actually compulsory
				if (compulsories.containsKey(tp) && compulsories.get(tp)) {
					setPaneImage(tp, MemoOptions.ICON_WARNING);
				} else {
					setPaneImage(tp, null);
				}
			}
		});
	}

	// Select directory ///////////////////////////////////////////////////////////////////////////////////////////////
	@FXML
	private void openDirectoryChooser(ActionEvent event) {
		DirectoryChooser dc = new DirectoryChooser();
		dc.setTitle("Choose photo directory");
		File dir = new File(MemoOptions.get("path.normaliserDirectory"));
		if (dir.exists() && dir.isDirectory()) {
			dc.setInitialDirectory(dir);
		}

		File file = dc.showDialog(controller.getStage());
		if (file != null) {
			String path = file.getAbsolutePath();
			//windows-specific fix - there seem to be problems with capital drive letters
			path = path.replaceFirst(path.substring(0, 1), path.substring(0, 1).toLowerCase());
			selectPhotosTextField.setText(path);
		}
		event.consume();
	}

	@Override
	public void updateProgress(double percentage, ProgressIndicator spinner) {
		spinner.setProgress(percentage % 100);
	}

	// Camera details /////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Pick a camera configuration XML file and load the contents into the controller
	 *
	 * @param event the event that triggered this
	 */
	public void chooseXMLFile(ActionEvent event) {

		FileChooser fc = new FileChooser();
		fc.setTitle("Choose camera config XML file");
		fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("XML configuration file", "xml"));
		File file = fc.showOpenDialog(controller.getStage());
		if (file != null) {
			String path = file.getAbsolutePath();
			SortedMap<String, Camera> cameras = Yoshihiko.importXML(path);
			logger.debug("loaded cams: {}", cameras);
			getController().getCameras().putAll(cameras);
			logger.debug("all cams: {}", getController().getCameras());
			selectPhotosTextField.setText(path);
		}
		event.consume();
	}

	// Trip details ///////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Delete a trip entry
	 *
	 * @param tripEntry the trip entry to delete
	 */
	public void deleteTripEntry(TripEntry tripEntry) {
		tripDetailsContainer.getChildren().remove(tripEntry);
		if (tripDetailsContainer.getChildren().size() == 1) {
			TripEntry t = (TripEntry) tripDetailsContainer.getChildren().get(0);
			t.setDeleteButtonState(false);
		}
		checkTripDetails();
	}

	@FXML
	private void addNewTimezone(ActionEvent event) {
		TripEntry newEntry = new TripEntry(getController());
		newEntry.checkDate(null);
		newEntry.checkTime(null);
		tripDetailsContainer.getChildren().add(newEntry);
		tripDetailsContainer.getChildren().stream().map(n -> (TripEntry) n).forEach(te
				-> te.setDeleteButtonState(true)
		);
		checkTripDetails();
		event.consume();
	}

	// Alignment //////////////////////////////////////////////////////////////////////////////////////////////////////
	@FXML
	private void toggleAlignment(ActionEvent event) {
		if (alignmentCheckbox.isSelected()) {
			alignmentAnchorPane.setDisable(false);
			//add existing alignment sets
			//getController().getAlignmentSets().stream().forEach(als -> {
			//	cameraConnectionsVBox.getChildren().add(new Link(this, null, als));
			//});
		} else {
			alignmentAnchorPane.setDisable(true);
			//camerasOverviewVBox.getChildren().clear();
			//cameraConnectionsVBox.getChildren().clear();
		}
		event.consume();
	}

	@FXML
	private void createNewAlignmentConnection(ActionEvent event) {

		//need to make sure a reference camera is up to date prior to this operation
		getController().setAlignmentReferenceCamera(refCameraChoiceBox.getValue());

		Dialog<Map<String, String>> dialog = new Dialog<>();

		//use MemO CSS for dialog
		dialog.getDialogPane().getStylesheets().add(
				getClass().getClassLoader().getResource(MemoOptions.CSS_COMMON).toExternalForm()
		);

		dialog.setTitle("Link cameras");
		dialog.setHeaderText("\nPlease select photos for each camera you want to link.\n"
				+ "These photos are assumed to have been taken at the same time.\n");

		// Set the button types.
		ButtonType mergeButtonType = new ButtonType("Link", ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(mergeButtonType, ButtonType.CANCEL);

		//"main" container for dialog
		VBox mainVBox = new VBox();

		//container for all cams to choose from
		HBox buttonsBox = new HBox();
		buttonsBox.setSpacing(10.0);
		for (Camera c : getController().getCameras().values()) {
			//only show cameras that have photos in this batch
			if (getController().getPhotosByCamera().containsKey(c.getIdentifier())) {
				CameraAlignmentPane cap = new CameraAlignmentPane(getController(), c);
				cap.setId(c.getIdentifier());
				buttonsBox.getChildren().add(cap);
			}
		}

		//display nodes
		mainVBox.getChildren().add(buttonsBox);
		dialog.getDialogPane().setContent(mainVBox);

		//check if values are reasonable
		dialog.setOnCloseRequest((DialogEvent event1) -> {

			//check if >2 photos
			int num = 0;
			for (Node cap : buttonsBox.getChildren()) {
				//check whether there is a photo
				File pic = new File(((CameraAlignmentPane) cap).getPath());
				if (pic.exists()) {
					num++;
				}
			}

			//if less than 2 photos are selected, this cannot work
			if (num < 2) {
				//display a warning
				Alert alert = getController().createAlert(Alert.AlertType.WARNING, "Warning", "Incomplete information",
						"Please pick at least two photos");
				alert.getButtonTypes().add(ButtonType.CANCEL);
				alert.getButtonTypes().removeAll(ButtonType.OK);
				alert.getButtonTypes().add(new ButtonType("Return and add more photos"));
				Optional<ButtonType> result = alert.showAndWait();

				//return to dialog
				if (!result.get().equals(ButtonType.CANCEL)) {
					event1.consume();
				} else {
					//cancel dialog
				}
			}
		});

		// Convert the result to the desired data structure
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == mergeButtonType) {
				Map<String, String> result = new HashMap<>();
				buttonsBox.getChildren().stream().forEach(
					cap -> result.put(cap.getId(), ((CameraAlignmentPane) cap).getPath())
				);
				return result;
			}
			return null;
		});
		Optional<Map<String, String>> result = dialog.showAndWait();
		result.ifPresent(r -> addLink(r));

		event.consume();
	}

	/**
	 * Create a new Link component based on an alignment set.
	 *
	 * @param alignmentSet the alignment set to use to create the link
	 */
	public void addLink(Map<String, String> alignmentSet) {

		//add to UI
		cameraConnectionsVBox.getChildren().add(
			new Link(getController(), getController().getPhotosByCamera().keySet(), alignmentSet)
		);

		//add to controller
		try {
			getController().addAlignmentSet(alignmentSet);
		} catch (EdgeOverrideException e) {
			Alert a = getController().createAlert(Alert.AlertType.ERROR, "Photo alignment override",
				"The alignment set you just added overrides a previously specified link.",
				"The connection between\n" + e.getFromID() +
				" and " + e.getToID() + "\nis ambiguous.\nPlease correct by deleting one of them!");
			a.showAndWait();

			//mark links as faulty
			for (Node n: cameraConnectionsVBox.getChildren()) {
				if (n.getClass().equals(Link.class)) {
					Link link = (Link) n;
					if (link.containsCamera(e.getFromID()) && link.containsCamera(e.getToID())
							&& !link.getStyleClass().contains(WRONG)) {

						link.getStyleClass().add(WRONG);
						//disable next button
						alignPhotosNextButton.setDisable(true);
					}
				}
			}
		} finally {
			updateAlignmentHeader();
		}
	}

	/**
	 * Removes a link from the UI
	 * @param alignmentSet the alignment set which contains the links to remove
	 */
	public void removeLink(Map<String, String> alignmentSet) {

		//get link from alignment set
		Link link = null;
		for (Node n: cameraConnectionsVBox.getChildren()) {
			if (n.getClass().equals(Link.class)) {
				Link l = (Link) n;
				//logger.debug("link: {}", l);
				boolean passed = true;
				for (Map.Entry<String, String> entry: alignmentSet.entrySet()) {
					if (entry.getValue()!=null && !entry.getValue().isEmpty()) {
						//logger.debug("{} found in alignment set", entry);
						if (!l.containsCamera(entry.getKey())) {
							//logger.debug("{} not found in link component", entry.getKey());
							passed = false;
							break;
						} else {
							//logger.debug("{} also contained in link component", entry.getKey());
						}
					}
				}
				if (passed) {
					link = l;
				}
			}
		}

		boolean everythingOK = true;
		if (link!=null) {
			//remove from UI
			if (cameraConnectionsVBox.getChildren().contains(link)) {
				cameraConnectionsVBox.getChildren().remove(link);
			} else {
				logger.error("The link {} was not contained in the UI", link);
			}

			//remove from controller
			getController().removeAlignmentSet(alignmentSet);

			//update highlighting: remove wrong class if everything is alright
			if (everythingOK) {
				alignPhotosNextButton.setDisable(false);
				//TODO: remove wrong highlighting
			}

		} else {
			logger.debug("link was null");
		}
	}

	/**
	 * Update the header row for the alignment sets: assign CSS classes for colour coding state
	 */
	private void updateAlignmentHeader() {
		String refCam = refCameraChoiceBox.getValue();

		if (refCam!=null) {
			//use a temporary graph for this as the real alignment graph will only be caculated when normalising
			AlignmentGraph g = null;
			try {
				g = Adrian.createAlignmentGraph(
					getController().getAlignmentSets(), getController().getAlignmentReferenceCamera(),
					getController().getPhotos()
				);
			} catch (EdgeOverrideException ex) {
				logger.warn("Unable to calculate alignment graph preview", ex);
			}
			logger.debug("graph: {}", g);

			//update header (highlight aligned cameras)
			Link l = (Link) camerasOverviewVBox.getChildren().get(0);
			for (Node camHeader : l.getChildren()) {
				if (camHeader.getClass().equals(LinkCameraIcon.class)) {
					LinkCameraIcon icon = (LinkCameraIcon) camHeader;

					if (icon.getCamera() != null) {

						//special case: ref cam
						if (refCam.equals(icon.getCamera())) {
							if (!icon.getStyleClass().contains(REF_CAM)) {
								icon.getStyleClass().add(REF_CAM);
							}
						} else {
							if (icon.getStyleClass().contains(REF_CAM)) {
								icon.getStyleClass().remove(REF_CAM);
							}
						}

						if (g!=null && g.distance(refCam, icon.getCamera()) != null) {
							logger.debug("{} and {} are connected", icon.getCamera(), refCam);
							if (!icon.getStyleClass().contains(LINKED)) {
								icon.getStyleClass().add(LINKED);
							}
						} else {
							logger.debug("{} and {} are not connected", icon.getCamera(), refCam);
							if (icon.getStyleClass().contains(LINKED)) {
								icon.getStyleClass().remove(LINKED);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Select a camera as the reference camera for alignemnt
	 *
	 * @param cameraID the ID of the new reference camera
	 */
	public void setAlignmentRefCam(String cameraID) {
		//ignore if it doesn't exist
		if (refCameraChoiceBox.getItems().contains(cameraID)) {
			//select in case this was triggered by an event rather than the selection model itself
			refCameraChoiceBox.getSelectionModel().select(cameraID);
			logger.info("Selected {} as reference camera", cameraID);
			getController().setAlignmentReferenceCamera(cameraID);
			//TODO: recalculate connected cameras in graph!
			updateAlignmentHeader();
		}
	}

	// Geotagging /////////////////////////////////////////////////////////////////////////////////////////////////////
	@FXML
	private void chooseGPXDirectory(ActionEvent event) {
		DirectoryChooser dc = new DirectoryChooser();
		dc.setTitle("Choose GPX directory");
		//chances are the GPX file is at the same location as the photos
		dc.setInitialDirectory(new File(MemoOptions.get("path.normaliserDirectory")));

		File file = dc.showDialog(controller.getStage());
		if (file != null) {
			String path = file.getAbsolutePath();
			geotaggingTextField.setText(path);
		}
		event.consume();
	}

	// Sorting options ////////////////////////////////////////////////////////////////////////////////////////////////
	@FXML
	private void toggleConfigDumpControls(ActionEvent event) {
		configDumpTextField.setVisible(!configDumpTextField.isVisible());
		configDumpButton.setVisible(!configDumpButton.isVisible());
		configDumpTextField.setManaged(!configDumpTextField.isManaged());
		configDumpButton.setManaged(!configDumpButton.isManaged());
		event.consume();
	}

	@FXML
	private void selectConfigDumpDir(ActionEvent event) {
		DirectoryChooser dc = new DirectoryChooser();
		dc.setTitle("Choose directory for config dump");

		File file = dc.showDialog(controller.getStage());
		if (file != null) {
			String path = file.getAbsolutePath();
			//windows-specific fix - there seem to be problems with capital drive letters
			path = path.replaceFirst(path.substring(0, 1), path.substring(0, 1).toLowerCase());
			configDumpTextField.setText(path);
		}
		event.consume();
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	public AnchorPane getMainContainer() {
		return mainContainer;
	}

	public TitledPane getSelectPhotosTitledPane() {
		return selectPhotosTitledPane;
	}

	@Override
	public PhotoNormaliserController getController() {
		return (PhotoNormaliserController) controller;
	}

	public void setController(PhotoNormaliserController controller) {
		this.controller = controller;
	}

	public VBox getTripDetailsContainer() {
		return tripDetailsContainer;
	}

	@Override
	public void setStatus(String msg) {
		statusLabel.setText(msg);
	}

	public AnchorPane getCameraDetailsAnchorPane() {
		return cameraDetailsAnchorPane;
	}

	@Override
	public Node getMainNode() {
		return cameraDetailsAnchorPane;
	}

	public ChoiceBox<String> getRefCameraChoiceBox() {
		return refCameraChoiceBox;
	}

	public AnchorPane getAlignPhotosAnchorPane() {
		return alignPhotosAnchorPane;
	}

	public ProgressIndicator getSelectPhotosSpinner() {
		return selectPhotosSpinner;
	}

	public ProgressIndicator getGeotaggingSpinner() {
		return geotaggingSpinner;
	}

	public ProgressIndicator getConfigSpinner() {
		return configSpinner;
	}

}
