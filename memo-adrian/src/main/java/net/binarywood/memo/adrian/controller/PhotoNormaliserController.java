/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.controller;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import net.binarywood.memo.adrian.Adrian;
import net.binarywood.memo.adrian.events.NormalisationFinishedEvent;
import net.binarywood.memo.adrian.events.NormalisationFinishedEventListener;
import net.binarywood.memo.adrian.events.SelectReferenceCameraEvent;
import net.binarywood.memo.adrian.model.DateTimezoneKey;
import net.binarywood.memo.adrian.presenter.PhotoNormaliserPresenter;
import net.binarywood.memo.common.components.MediaItemTile;
import net.binarywood.memo.common.components.QuickMenu;
import net.binarywood.memo.common.controller.FindMediaVisitor;
import net.binarywood.memo.common.exceptions.EdgeOverrideException;
import net.binarywood.memo.common.exceptions.FXMLLoadingException;
import net.binarywood.memo.common.model.AlignmentGraph;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.model.OwnerHistory;
import net.binarywood.memo.common.model.WaypointHistory;
import net.binarywood.memo.common.spec.AApplication;
import net.binarywood.memo.common.spec.AController;
import net.binarywood.memo.common.spec.APresenter;
import net.binarywood.memo.yoshihiko.Yoshihiko;
import net.binarywood.memo.yoshihiko.components.CameraManagerPane;
import net.binarywood.memo.yoshihiko.controller.CameraManagerController;
import net.binarywood.memo.yoshihiko.events.ClearCamerasEvent;
import net.binarywood.memo.yoshihiko.events.MergeCamerasEvent;
import net.binarywood.memo.yoshihiko.events.ShowMediaEvent;
import net.binarywood.memo.yoshihiko.events.UpdateCameraEvent;
import net.binarywood.memo.yoshihiko.presenter.CameraManagerPresenter;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The controller for the PhotoNormaliser
 *
 * @author Stefanie Wiegand
 */
public class PhotoNormaliserController extends AController {

	private PhotoNormaliserPresenter presenter;
	private CameraManagerPane cmp;

	private static final Logger logger = LoggerFactory.getLogger(PhotoNormaliserController.class);

	private final Set<NormalisationFinishedEventListener> _listeners = new HashSet<>();

	private FindMediaVisitor visitor;

	private String importDirectory;
	private SortedMap<String, Camera> cameras;
	//sort case-insensitive by file name
	private SortedMap<String, MediaItem> mediaItems;
	private final Map<String, List<MediaItem>> mediaItemsByCamera;
	//all the timezones this trip contains (not a TimezoneHistory as there might be duplicate keys in here)
	private final SortedMap<DateTime, Set<String>> tripTimezoneHistory;
	//the cameras used on this trip where the date corresponds to a timezone entry of tripTimezoneHistory
	private final SortedMap<DateTimezoneKey, Set<String>> tripHistoryCameras;
	private WaypointHistory waypointHistory;
	private final Set<Map<String, String>> alignmentSets;
	private AlignmentGraph dwg = null;
	private String alignmentReferenceCamera = null;
	//this is used for the progress indicator during the normalisation
	private DoubleProperty progress;

	/**
	 * Create a new PhotoNormaliserController to manage the PhotoNormaliser application
	 */
	public PhotoNormaliserController() {
		super();
		cameras = new TreeMap<>();
		mediaItems = new TreeMap<>((String o1, String o2) -> o1.toLowerCase().compareTo(o2.toLowerCase()));
		mediaItemsByCamera = new TreeMap<>();
		tripTimezoneHistory = new TreeMap<>();
		tripHistoryCameras = new TreeMap<>();
		alignmentSets = new HashSet<>();
		progress = new SimpleDoubleProperty(0.0);
	}

	/**
	 * Reset the controller to its initial state and purge all data
	 */
	private void clear() {
		cameras.clear();
		mediaItems.clear();
		mediaItemsByCamera.clear();
		tripTimezoneHistory.clear();
		tripHistoryCameras.clear();
		alignmentSets.clear();
	}

	/**
	 * Add event handlers for this controller to catch events sent by components
	 */
	public void addEventHandlers() {
		logger.debug("Adding event handler to anchor pane {} {}",
				presenter.getCameraDetailsAnchorPane().hashCode(), presenter.getCameraDetailsAnchorPane());

		presenter.getCameraDetailsAnchorPane().addEventHandler(ActionEvent.ANY, (ActionEvent event) -> {

			PhotoNormaliserController c1 = this;
			CameraManagerController c2 = c1.getCameraManagerPane().getController();

			//clear cameras
			if (event.getClass().equals(ClearCamerasEvent.class)) {

				logger.debug("Clearing cameras");
				//reload cameras found in mediaItems as they have been cleared by yoshihiko and are required
				c1.getPhotosByCamera().values().stream().filter(pics -> !pics.isEmpty()).map(
					pics -> pics.get(0).getCamera()
				).forEach(c -> c1.getCameras().put(c.getIdentifier(), c));
				c2.setCameras(c1.getCameras());
				c1.getCameraManagerPane().refresh();

			//merge cameras
			} else if (event.getClass().equals(MergeCamerasEvent.class)) {

				logger.debug("Merging cameras");
				MergeCamerasEvent me = (MergeCamerasEvent) event;
				c1.mergeCameras(me.getOldCameras(), me.getNewCamera());
				c1.getCameraManagerPane().refresh();

			//update single camera
			} else if (event.getClass().equals(UpdateCameraEvent.class)) {

				logger.debug("Updating camera");
				UpdateCameraEvent ue = (UpdateCameraEvent) event;
				c1.updateCamera(ue.getOldCamera(), ue.getNewCamera());
				c1.getCameraManagerPane().refresh();

			//show media
			} else if (event.getClass().equals(ShowMediaEvent.class)) {

				Camera cam = ((ShowMediaEvent) event).getCamera();

				if (mediaItemsByCamera.containsKey(cam.getIdentifier())) {

					Dialog<Object> dialog = new Dialog<>();
					dialog.setTitle("Show camera's media");
					dialog.setHeaderText("Media taken with camera " + cam.getIdentifier());
					dialog.setGraphic(
						new ImageView(this.getClass().getClassLoader().getResource(MemoOptions.ICON_CAMERA).toString())
					);

					//use MemO CSS for dialog
					dialog.getDialogPane().getStylesheets().add(
						getClass().getClassLoader().getResource(MemoOptions.CSS_COMMON).toExternalForm()
					);

					ButtonType loginButtonType = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);
					dialog.getDialogPane().getButtonTypes().addAll(loginButtonType);

					//create container
					HBox media = new HBox();
					media.setSpacing(10.0);
					media.setAlignment(Pos.CENTER);

					//get images (up to 3)
					int num = 0;
					for (MediaItem mi: mediaItemsByCamera.get(cam.getIdentifier())) {

						if (!mi.hasThumbnail()) {
							mi.createThumbnail();
						}
						MediaItemTile item = new MediaItemTile(mi, this);
						item.getMainContainer().getStyleClass().add("inverted");
						media.getChildren().add(item);

						//add up to three items
						if (num<2) {
							num++;
						} else {
							break;
						}
					}

					//show popup
					dialog.getDialogPane().setContent(media);
					dialog.showAndWait();

				} else {
					(c1.createAlert(Alert.AlertType.INFORMATION, "Show camera's media", "Nothing to see here :(",
						"No media has been taken with camera " + cam.getIdentifier())).showAndWait();
				}

			} else {
				logger.debug("Caught unknown event {} {} {}->{}", event.getClass().getSimpleName(),
						event.hashCode(), event.getSource(), event.getTarget());
			}

			event.consume();
		});

		presenter.getAlignPhotosAnchorPane().addEventHandler(ActionEvent.ANY, (ActionEvent event) -> {
			//select reference camera
			if (event.getClass().equals(SelectReferenceCameraEvent.class)) {
				presenter.setAlignmentRefCam(((SelectReferenceCameraEvent) event).getCameraID());
			} else {
				logger.debug("Caught unknown event {} {} {}->{}", event.getClass().getSimpleName(),
					event.hashCode(), event.getSource(), event.getTarget());
			}
			event.consume();
		});
	}

	@Override
	public APresenter showStage(AApplication app) throws FXMLLoadingException {

		setPresenter((PhotoNormaliserPresenter) super.showStage(app));

		//create embedded camera manager:
		//--create controller
		CameraManagerController cmc = new CameraManagerController();
		cmc.setParentController(this);
		//--load from FXML to get presenter
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/CameraManagerScene.fxml"));
		try {
			Scene scene = new Scene(loader.load());
			//--prepare presenter
			CameraManagerPresenter cmpres = loader.getController();
			cmpres.setController(cmc);
			cmpres.addCameraManagerPane();
			//--prepare controller
			cmc.setPresenter(cmpres);
			cmc.setCurrentScene(scene);
			//--prepare cmp
			cmp = cmpres.getCameraManagerPane();
		} catch (IOException e) {
			logger.error("Could not load embedded camera manager from fxml", e);
		}

		//add menu
		QuickMenu quickMenu = new QuickMenu(this);
		AnchorPane.setTopAnchor(quickMenu, 10.0);
		AnchorPane.setRightAnchor(quickMenu, 0.0);
		AnchorPane.setLeftAnchor(quickMenu, 0.0);
		presenter.getMainContainer().getChildren().add(quickMenu);

		return presenter;
	}

	/**
	 * Run the first iteration of the normalisation. This requires going through all photos and building the
	 * alignment graph. For this, it needs user input via dialogs for ambiguous timestamps.
	 *
	 * @return the pre-normalised items, which have "UTC" timestamps but are not yet fully normalised
	 */
	public Map<String, MediaItem> normalisationStageOne() {

		Map<String, MediaItem> normalisedMediaItems = new HashMap<>();
		//iterate over cameras to calculate "UTC" time, considering camera settings & locations
		logger.info("# First normalisation phase ###########################################################");
		for (Map.Entry<String, List<MediaItem>> e : mediaItemsByCamera.entrySet()) {
			logger.info("{}: {} photos",e.getKey(), e.getValue().size());
			for (MediaItem p : e.getValue()) {
				//get correct camera info
				p.setCamera(cameras.get(e.getKey()));
				//doit!
				normalisedMediaItems.put(p.getPath(), Adrian.normalise(p, cameras.get(alignmentReferenceCamera),
					p.getCamera().getLocationHistory(), PhotoNormaliserController.this));
			}
		}
		//calculate alignment graph
		logger.info("Calculating alignment graph");
		try {
			alignPhotos(alignmentReferenceCamera, normalisedMediaItems);
		} catch (EdgeOverrideException ex) {
			logger.error("Could not calculate alignment graph", ex);
			dwg = null;
		}
		return normalisedMediaItems;
	}

	/**
	 * Start the normalisation process
	 *
	 * @param normalisedMediaItems the mediaitems that have already gone through stage one of the normalisation
	 * @param writeTags write EXIF information back to the original file?
	 * @param renameFiles rename the files to have a UTC prefix? (recommended)
	 * @param removeSubfolders remove the original folders in which the files were found and dump them all into
	 *			a single folder
	 * @param configDumpPath the path into which the configuration from this run should be saved
	 */
	public void normalisationStageTwo(Map<String, MediaItem> normalisedMediaItems,
		boolean writeTags, boolean renameFiles, boolean removeSubfolders, String configDumpPath) {

		int processed = 0;
		progress.set(0.0);
		int total = mediaItems.size();

		//go through all normalised pictures again
		logger.info("# Second normalisation phase ##########################################################");
		for (MediaItem normalised: normalisedMediaItems.values()) {

			logger.info("########### Media item: {} ##############################", normalised.getFilename());
			logger.debug("dtNormalisedUTC:   {}", normalised.getGpsDate());
			logger.debug("dtNormalisedLocal: {}", normalised.getDatetimeOriginalExif());

			//align photos if applicable
			if (dwg!=null && !dwg.isEmpty()) {

				logger.debug("Aligning {}", normalised.getFilename());

				//calculate timeshift for this item in the context of its camera, timezone(s) and reference camera
				Long timeshift = Adrian.calculateTimeshiftForPhoto(
					dwg, normalised, cameras.get(alignmentReferenceCamera), normalisedMediaItems
				);

				//shift timestamp if applicable
				DateTime dtAlignedUTC = normalised.getGpsDate();
				DateTime dtAlignedLocal = normalised.getDatetimeOriginalExif();
				if (timeshift!=null && (timeshift!=0L)) {
					dtAlignedUTC = dtAlignedUTC.minus(timeshift);
					dtAlignedLocal = dtAlignedLocal.minus(timeshift);
				}
				logger.debug("dtAlignedUTC:      {}", dtAlignedUTC);
				logger.debug("dtAlignedLocal:    {}", dtAlignedLocal);

				//write back to image
				normalised.setGpsDate(dtAlignedUTC);
				normalised.setLocalDate(dtAlignedLocal);
			} else {
				logger.debug("Don't align media item from camera {}", normalised.getCamera().getIdentifier());
			}

			//geotag if applicable (now that we have the correct UTC time)
			if (waypointHistory!=null) {
				//find GPS position if it exists;
				//make copy of pic, OK for writing as path on disk remains unchanged
				normalised = geotagMediaItem(normalised);
			}

			//Updating of the internal media item collections is not necessary:
			//the program exits after this operation anyway

			//write necessary tags
			if (writeTags) {

				logger.debug("Preparing EXIF tags for {}", normalised.getFilename());

				//get owner from history
				OwnerHistory oh = normalised.getCamera().getOwnerHistory();
				String owner = null;
				if (oh!=null) {
					owner = oh.getOwnerAtDate(normalised.getGpsDate());
				}

				Set<String> tags = new HashSet<>();
				//local time
				tags.add("datetimeoriginal=" + normalised.getDatetimeOriginalExif().toString(MediaItem.exifDateTimeFormat));
				tags.add("datetimedigitized=" + normalised.getDatetimeOriginalExif().toString(MediaItem.exifDateTimeFormat));
				tags.add("datetime=" + normalised.getDatetimeOriginalExif().toString(MediaItem.exifDateTimeFormat));
				tags.add("createdate=" + normalised.getDatetimeOriginalExif().toString(MediaItem.exifDateTimeFormat));
				tags.add("timezoneoffset=" + normalised.getDatetimeOriginalExif().toString("Z"));
				//utc time
				tags.add("gpsdatestamp=" + normalised.getGpsDate().toString(MediaItem.exifDateFormat));
				tags.add("gpstimestamp=" + normalised.getGpsDate().toString(MediaItem.exifTimeFormat));

				//GPS
				if (normalised.isGeotagged()) {
					tags.add("gps:GPSMapDatum=WGS-84");
					tags.add("gps:GPSLatitude=" + normalised.getLatitude());
					tags.add("gps:GPSLatitudeRef=" + normalised.getLatRef());
					tags.add("gps:GPSLongitude=" + normalised.getLongitude());
					tags.add("gps:GPSLongitudeRef=" + normalised.getLongRef());
					tags.add("gps:GPSAltitude=" + normalised.getAltitude());
					//TODO: does not behave: "Warning: Not an integer for GPS:GPSAltitudeRef"
					tags.add("gps:GPSAltitudeRef#=" + normalised.getAltRef());
				}

				//other info
				//TODO: only change this if normalising for the first time
				tags.add("originalfilename=" + normalised.getFilename());
				tags.add("software=MemO");
				tags.add("usercomment=TimezoneID:" + normalised.getDatetimeOriginalExif().getZone().getID());
				//make/model
				//TODO: only write if it doesn't exist
				tags.add("make=" + normalised.getCamera().getMake());
				tags.add("model=" + normalised.getCamera().getModel());
				//TODO: serial number if it exists
				//owner
				if (owner!=null) {
					tags.add("artist=\"" + owner + "\"");
					tags.add("ownername=\"" + owner + "\"");
					tags.add("copyright=(c) " + normalised.getGpsDate().toString("yyyy") + " \"" + owner + "\"");
				}

				Adrian.writeTags(normalised.getPath(), tags);
			}

			//rename file
			if (renameFiles) {
				Adrian.renameMediaItem(normalised, removeSubfolders, importDirectory);
			}

			//display progress in UI
			processed++;
			final int pf = processed;
			final int tf = total;
			progress.set((double) pf/tf);
		}

		//remove empty folders from input directory
		if (removeSubfolders) {
			File importDir = new File(importDirectory);
			if (importDir.exists()) {
				//for each folder in the loading dir
				for (File dir: importDir.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY)) {
					//check if it's empty
					if (dir.isDirectory() && dir.listFiles().length==0) {
						//remove
						logger.debug("Removing empty directory {}", dir.getPath());
						dir.delete();
						//TODO: make recursive
					}
				}
			}
		}

		//requires JavaFX thread for dialogs
		Platform.runLater(() -> {

			//show alert for confirmation
			if (getStage()!=null) {
				Alert a = createAlert(Alert.AlertType.INFORMATION, "Finished!", "All photos processed",
					"Your photos have been normalised.");
				a.showAndWait();
				getStage().close();
			}

			//dump config file
			if (configDumpPath!=null) {
				Yoshihiko.exportXML(
					configDumpPath + "cameraConfig_" + DateTime.now().toString(MediaItem.safeFileDateFormat) + ".xml",
					cameras
				);
			}

			_fireNormalisationFinishedEvent();
		});
	}

	/**
	 * Adds a listener for this class's normalisation finished event
	 *
	 * @param listener an object which is listening for events from the photo normaliser controller
	 */
	public synchronized void addEventListener(NormalisationFinishedEventListener listener) {
        _listeners.add(listener);
    }

	/**
	 * Notify all listeners that the normalisation has finished
	 */
	private synchronized void _fireNormalisationFinishedEvent() {
        NormalisationFinishedEvent event = new NormalisationFinishedEvent(this);
        Iterator<NormalisationFinishedEventListener> listeners = _listeners.iterator();
        while(listeners.hasNext()) {
            listeners.next().eventReceived(event);
        }
    }

	/**
	 * Count the files in a directory
	 *
	 * @param directory the directory to scan
	 * @param whitelist a set of file types to count. If null all files will be counted
	 * @return the number of (matching) files found in the directory
	 */
	public int countFiles(File directory, Set<String> whitelist) {
		return countFiles(directory, whitelist, 0);
	}

	/**
	 * A recursive method to count files
	 *
	 * @param directory the directory to scan
	 * @param whitelist a set of file types to count. If null all files will be counted
	 * @param soFar the amount of files found until this call
	 * @return the number of (matching) files found in the directory
	 */
	private int countFiles(File directory, Set<String> whitelist, int soFar) {

		//making a copy of the parameter instead of reusing it
		int sf = soFar;
		for (File child: directory.listFiles()) {
			if (child.isDirectory()) {
				sf = countFiles(child, whitelist, sf);
			} else {
				//check if it's a media item
				String suffix = child.getName().substring(child.getName().lastIndexOf(".") + 1).toLowerCase();
				if (whitelist==null || whitelist.contains(suffix)) {
					sf++;
				}
			}
		}
		return sf;
	}

	/**
	 * Recursively read all media items in a directory
	 *
	 * @param path the directory to scan
	 * @param fixMetadataUsingDirectory if this is true, media items with missing EXIF data will be corrected using
	 *			information from the other files in the same directory (Camera make, model and serial number)
	 */
	public void readPhotosFromDirectory(String path, boolean fixMetadataUsingDirectory) {

		//remove old cameras/photos
		clear();

		importDirectory = path;

		try {
			//TODO: switch for using geonames webservice to get timezone id for coordinates
			Set<String> mediaTypes = new HashSet<>();
			mediaTypes.addAll(Arrays.asList(MemoOptions.getPictureTypes()));
			mediaTypes.addAll(Arrays.asList(MemoOptions.getVideoTypes()));
			visitor = new FindMediaVisitor(cameras, mediaItems, getPresenter(),
				getPresenter()!=null?getPresenter().getSelectPhotosSpinner():null,
				countFiles(new File(path), mediaTypes), false, false, true);

			//read cams
			Path startDir = Paths.get(path);
			Files.walkFileTree(startDir, visitor);

			cameras = visitor.getCameras();
			mediaItems = visitor.getMediaItems();

			//container for inferred make/model
			Map<String, String> makeByDir = new HashMap<>();
			Map<String, String> modelByDir = new HashMap<>();

			if (fixMetadataUsingDirectory) {
				//set up map of mediaItems ordered by their parent dir
				SortedMap<String, Set<MediaItem>> photosByDir = new TreeMap<>();
				for (MediaItem p: mediaItems.values()) {
					if (!photosByDir.containsKey(p.getParentDirectory())) {
						Set<MediaItem> newSet = new HashSet<>();
						newSet.add(p);
						photosByDir.put(p.getParentDirectory(), newSet);
					} else {
						photosByDir.get(p.getParentDirectory()).add(p);
					}
				}
				//for each dir try and collect sensible data for make and model
				for (String dir: photosByDir.keySet()) {
					logger.debug("Inspecting dir {}", dir);
					for (MediaItem pic: photosByDir.get(dir)) {
						if (pic.getCamera().getMake()!=null && !"UNKNOWN".equals(pic.getCamera().getMake())) {
							makeByDir.put(dir, pic.getCamera().getMake());
						}
						if (pic.getCamera().getModel()!=null && !"UNKNOWN".equals(pic.getCamera().getModel())) {
							modelByDir.put(dir, pic.getCamera().getModel());
						}
						//stop if found
						if (makeByDir.containsKey(dir) && modelByDir.containsKey(dir)) {
							break;
						}
					}
					logger.debug("{} {}", makeByDir.get(dir), modelByDir.get(dir));
				}
			}

			for (MediaItem p: mediaItems.values()) {
				//adding mediaItems from a new camera
				if (!mediaItemsByCamera.containsKey(p.getCamera().getIdentifier())) {
					//add photo with non-empty camera
					if (!p.getCamera().getIdentifier().isEmpty()) {
						mediaItemsByCamera.put(p.getCamera().getIdentifier(), new LinkedList<>());
					//remove invalid empty camera from list
					} else {
						//logger.debug("No camera set for this media item, removing empty cam from list");
						//remove empty camera from list
						cameras.remove(p.getCamera().getIdentifier());
					}
				}
				//add mediaItems from an existing camera
				if (mediaItemsByCamera.containsKey(p.getCamera().getIdentifier())) {
					//if the "existing camera is unknown, check the inferred make/model containers first
					if (MemoOptions.UNKNOWN.equals(p.getCamera().getMake())
							|| (MemoOptions.UNKNOWN.equals(p.getCamera().getModel()))) {

						String oldIdentifier = p.getCamera().getIdentifier();
						//check make
						if (MemoOptions.UNKNOWN.equals(p.getCamera().getMake())
								&& makeByDir.containsKey(p.getParentDirectory())) {
							p.getCamera().setMake(makeByDir.get(p.getParentDirectory()));
						}
						//check model
						if (MemoOptions.UNKNOWN.equals(p.getCamera().getModel())
							&& modelByDir.containsKey(p.getParentDirectory())) {

							p.getCamera().setModel(modelByDir.get(p.getParentDirectory()));
						}
						p.getCamera().makeIdentifier();
						//add "new" camera if it doesn't exist
						if (!mediaItemsByCamera.containsKey(p.getCamera().getIdentifier())) {
							mediaItemsByCamera.put(p.getCamera().getIdentifier(), new LinkedList<>());
						}
						//remove old camera if no mediaItems are left
						if (mediaItemsByCamera.get(oldIdentifier).isEmpty()) {
							//remove all mediaItems by old cam; should be none as established above
							mediaItemsByCamera.remove(oldIdentifier);
							//can remove old cam since no mediaItems and timezone history hasn't been loaded yet
							cameras.remove(oldIdentifier);
						}
					}
					//then add camera (only unknown if it wasn't fixed)
					mediaItemsByCamera.get(p.getCamera().getIdentifier()).add(p);
				//add mediaItems where the camera tag is missing/invalid
				} else {
					logger.debug("Adding media file {} with missing camera tag(s)", p.getFilename());
					if (!mediaItemsByCamera.containsKey(MemoOptions.UNKNOWN)) {
						mediaItemsByCamera.put(MemoOptions.UNKNOWN, new LinkedList<>());
					}
					mediaItemsByCamera.get(MemoOptions.UNKNOWN).add(p);
				}
			}
			logger.debug("Found {} photos from {} cameras", mediaItems.size(), mediaItemsByCamera.size());
			logger.info(mediaItemsByCamera.keySet().toString());

			//add cameras to camera manager pane
			if (cmp!=null) {
				cmp.getController().setCameras(cameras);
				Platform.runLater(() ->  cmp.refresh());
			}

		} catch (IOException e) {
			logger.error("Error reading photos from directory {}", path, e);
		}
	}

	/**
	 * Merge two cameras into one
	 *
	 * @param oldCam the camera which to "add" to the new camera
	 * @param newCam the new camera which will hold both the old and new camera information
	 */
	public void mergeCameras(Camera oldCam, Camera newCam) {
		Set<Camera> oldCams = new HashSet<>();
		oldCams.add(oldCam);
		oldCams.add(newCam);
		mergeCameras(oldCams, newCam);
	}

	/**
	 * Merge a set of cameras into a completely new camera
	 *
	 * @param oldCams the old cameras that are about to be combined
	 * @param newCam the new camera which doesn't necessarily exist yet
	 */
	public void mergeCameras(Set<Camera> oldCams, Camera newCam) {

		//remove all old cameras
		oldCams.stream().forEach(old -> {
			if (old!=null && cameras.containsKey(old.getIdentifier())) {
				cameras.remove(old.getIdentifier());
			}
		});

		//add new camera
		cameras.put(newCam.getIdentifier(), newCam);

		//update mediaItems
		Iterator<Map.Entry<String, MediaItem>> it = mediaItems.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, MediaItem> e = it.next();
			MediaItem p = e.getValue();
			String pname = e.getKey();

			//check if they are from an old cam
			oldCams.stream().forEach(old -> {
				if (old != null && p.getCamera().getIdentifier().equals(old.getIdentifier())
						&& mediaItems.containsKey(pname)) {

					//if they are, replace the camera of the photo
					mediaItems.get(pname).setCamera(newCam);
				}
			});
		}

		//update mediaItemsByCamera
		mediaItemsByCamera.clear();
		mediaItems.values().stream().forEach(p -> {
			//add new camera
			if (!mediaItemsByCamera.containsKey(p.getCamera().getIdentifier())) {
				LinkedList<MediaItem> ll = new LinkedList<>();
				ll.add(p);
				mediaItemsByCamera.put(p.getCamera().getIdentifier(), ll);
			//add photo to existing camera
			} else {
				mediaItemsByCamera.get(p.getCamera().getIdentifier()).add(p);
			}
		});
	}

	/**
	 * Update a camera
	 *
	 * @param oldCam the camera to update
	 * @param newCam what to update the camera to
	 */
	public void updateCamera(Camera oldCam, Camera newCam) {

		logger.debug("Updating {} to {}", oldCam, newCam);

		//compare ids
		if (oldCam.getIdentifier().equals(newCam.getIdentifier())) {
			//only replace old cam values, leave ID intact
			cameras.put(oldCam.getIdentifier(), newCam);
		} else {
			//remove old camera
			cameras.remove(oldCam.getIdentifier());
			//add new camera
			cameras.put(newCam.getIdentifier(), newCam);
		}

		//update mediaItems
		Iterator<Map.Entry<String, MediaItem>> it = mediaItems.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, MediaItem> e = it.next();
			MediaItem p = e.getValue();
			String pname = e.getKey();

			//check if they are from an old cam
			if (p.getCamera().getIdentifier().equals(oldCam.getIdentifier()) && mediaItems.containsKey(pname)) {
				//if they are, replace the camera of the photo
				mediaItems.get(pname).setCamera(newCam);
			}
		}

		//update mediaItemsByCamera
		mediaItemsByCamera.clear();
		mediaItems.values().stream().forEach(p -> {
			//add new camera
			if (!mediaItemsByCamera.containsKey(p.getCamera().getIdentifier())) {
				LinkedList<MediaItem> ll = new LinkedList<>();
				ll.add(p);
				mediaItemsByCamera.put(p.getCamera().getIdentifier(), ll);
			//add photo to existing camera
			} else {
				mediaItemsByCamera.get(p.getCamera().getIdentifier()).add(p);
			}
		});

		logger.debug("{}", mediaItemsByCamera);
	}

	/**
	 * Calculate the trip details based on camera information (if there is any)
	 *
	 * @return the first media item in the trip history
	 */
	public MediaItem getTripDetails() {

		tripTimezoneHistory.clear();
		tripHistoryCameras.clear();

		DateTime startDate = null;
		DateTime endDate = null;
		MediaItem startPhoto = null;
		MediaItem endPhoto = null;

		//find earliest/latest photo to determine trip timeframe
		for (MediaItem p : mediaItems.values()) {
			if (startDate == null || p.getDatetimeOriginalExif().isBefore(startDate)) {
				startDate = p.getDatetimeOriginalExif();
				startPhoto = p;
			}
			if (endDate == null || p.getDatetimeOriginalExif().isAfter(endDate)) {
				endDate = p.getDatetimeOriginalExif();
				endPhoto = p;
			}
		}
		logger.debug("Earliest photo: {}, {}", startDate, startPhoto != null ? startPhoto.getFilename() : "");
		logger.debug("Latest photo:   {}, {}", endDate, endPhoto != null ? endPhoto.getFilename() : "");

		//collect all possible timezones in the history
		//get trip details from cameras, that have mediaItems in this dataset
		for (String cid: mediaItemsByCamera.keySet()) {
			Camera c = cameras.get(cid);
			if (c!=null) {
				//logger.debug("Adding trip location history for {}: {}", c.getIdentifier(), c.getLocationHistory());
				//get only entries from camera's history that lie in the applicable timeframe for this trip
				for (Map.Entry<DateTime, Object> e: c.getLocationHistory().getSubHistory(startDate, endDate).entrySet()) {
					if (!tripTimezoneHistory.containsKey(e.getKey())) {
						tripTimezoneHistory.put(e.getKey(), new HashSet<>());
					}
					//HashSet will take care of duplicates
					tripTimezoneHistory.get(e.getKey()).add(e.getValue().toString());
				}
			} else {
				logger.warn("Could not find camera {}", cid);
			}
		}

		//grab location histories from cameras and put together for trip timezone history
		Iterator<Entry<DateTime, Set<String>>> it = tripTimezoneHistory.entrySet().iterator();
		Map.Entry<DateTime, Set<String>> stageStart = null;
		if (it.hasNext()) {
			stageStart = it.next();
			Map.Entry<DateTime, Set<String>> stageEnd = null;
			while (it.hasNext()) {
				stageEnd = it.next();
				addToTripHistory(stageStart.getKey(), stageStart.getValue(), stageEnd.getKey());
				//continue to next timestamp
				stageStart = stageEnd;
			}
			//process last entry, using the date of the last photo as the end date
			if (stageEnd!=null) {
				addToTripHistory(stageEnd.getKey(), stageEnd.getValue(), endDate);
			}
		}
		//logger.debug("Trip history after: {}", tripTimezoneHistory);
		//logger.debug("Trip cams: {}", tripHistoryCameras);

		return startPhoto;
	}

	/**
	 * Adds new information to the trip history by scanning all camera's location histories
	 *
	 * @param startDate the start of a time interval to scan
	 * @param startZones the timezones related to teh startdate for which to scan
	 * @param endDate  the end of the time interval
	 */
	private void addToTripHistory(DateTime startDate, Set<String> startZones, DateTime endDate) {

		//search all zones connected to this date
		for (String tz: startZones) {
			DateTimezoneKey key = new DateTimezoneKey(startDate, tz);
			//iterate over cameras
			for (Camera c: cameras.values()) {
				//check if the camera was in the timezone at any point during the interval
				if (c.getLocationHistory().wasPresentInTimezoneBetweenDates(
					DateTimeZone.forID(tz), startDate, endDate)
				) {
					//if this is the first trip entry for this camera in this trip create a new entry
					if (!tripHistoryCameras.containsKey(key)) {
						tripHistoryCameras.put(key, new HashSet<>());
					}
					//append trip entry (HashSet takes care of duplicates)
					tripHistoryCameras.get(key).add(c.getIdentifier());
				}
			}
		}
	}

	/**
	 * Load GPX files from a directory
	 *
	 * @param directory the directory to load the files from
	 */
	public void loadGPX(String directory) {
		waypointHistory = Adrian.loadGPX(directory, this);
	}

	/**
	 * Geotag a media item
	 *
	 * @param mediaItem the item to geotag
	 * @return the geotagged MediaItem
	 */
	public MediaItem geotagMediaItem(MediaItem mediaItem) {
		return Adrian.geotagPhoto(mediaItem, waypointHistory);
	}

	/**
	 * Remove an alignment set for a number of cameras
	 *
	 * @param alignmentSet the alignment set, which is a map in which the key is the cameraID and the value is the
	 *			path of the media item used to align the cameras
	 * @throws net.binarywood.memo.common.exceptions.EdgeOverrideException if the new alignment set would override
	 *			an existing edge
	 */
	public void addAlignmentSet(Map<String, String> alignmentSet) throws EdgeOverrideException {
		alignmentSets.add(alignmentSet);
	}

	/**
	 * Add an alignment set for a number of cameras
	 *
	 * @param alignmentSet the alignment set, which is a map in which the key is the cameraID and the value is the
	 *			path of the media item used to align the cameras
	 */
	public void removeAlignmentSet(Map<String, String> alignmentSet) {
		alignmentSets.remove(alignmentSet);
	}

	/**
	 * Align the alignemnt set mediaItems according to the alignment sets (i.e. build an alignemnt graph)
	 *
	 * @param refCameraId the root to be used for the graph
	 * @param normalisedMediaItems normalised media items; used for creating the alignment graph
	 * @throws EdgeOverrideException if different alignemnt sets override each other
	 */
	public void alignPhotos(String refCameraId, Map<String, MediaItem> normalisedMediaItems)
			throws EdgeOverrideException {

		alignmentReferenceCamera = refCameraId;

		dwg = Adrian.createAlignmentGraph(alignmentSets, refCameraId, normalisedMediaItems);

		//check if the input provided can be used to align the mediaItems correctly
		if (!dwg.isSensible()) {
			logger.error("Graph is not sensible!");
			Alert a = createAlert(Alert.AlertType.ERROR, "Could not align photos",
				"There was a problem when trying to align the photos",
				"The given information contains circular logic. Please correct before continuing...");
			ImageView iv = new ImageView();
			presenter.setImage(iv, "icons/escher.png");
			a.setGraphic(iv);
			a.showAndWait();
			//TODO: disable next button until corrected
		} else {
			//TODO: enable next button
		}
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	public SortedMap<String, Camera> getCameras() {
		return cameras;
	}

	public void setCameras(SortedMap<String, Camera> cameras) {
		this.cameras = cameras;
	}

	public SortedMap<String, MediaItem> getPhotos() {
		return mediaItems;
	}

	public void setPhotos(SortedMap<String, MediaItem> photos) {
		this.mediaItems = photos;
	}

	@Override
	public PhotoNormaliserPresenter getPresenter() {
		return (PhotoNormaliserPresenter) super.presenter;
	}

	public void setPresenter(PhotoNormaliserPresenter presenter) {
		super.setPresenter(presenter);
		this.presenter = presenter;
	}

	public SortedMap<DateTime, Set<String>> getTripTimezoneHistory() {
		return tripTimezoneHistory;
	}

	public SortedMap<DateTimezoneKey,Set<String>> getTripHistoryCameras() {
		return tripHistoryCameras;
	}

	public FindMediaVisitor getVisitor() {
		return visitor;
	}

	public CameraManagerPane getCameraManagerPane() {
		return cmp;
	}

	public Map<String, List<MediaItem>> getPhotosByCamera() {
		return mediaItemsByCamera;
	}

	public AlignmentGraph getAlignmentGraph() {
		return dwg;
	}

	public String getAlignmentReferenceCamera() {
		return alignmentReferenceCamera;
	}

	public void setAlignmentReferenceCamera(String alignmentReferenceCamera) {
		this.alignmentReferenceCamera = alignmentReferenceCamera;
	}

	public Set<Map<String, String>> getAlignmentSets() {
		return alignmentSets;
	}

	public DoubleProperty getProgress() {
		return progress;
	}

}
