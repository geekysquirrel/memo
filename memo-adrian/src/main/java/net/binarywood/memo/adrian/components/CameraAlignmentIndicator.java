/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */
package net.binarywood.memo.adrian.components;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.adrian.presenter.PhotoNormaliserPresenter;
import net.binarywood.memo.common.model.AlignmentGraph;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.AComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a label for a camera with an image to show whether it has been aligned or not.
 *
 * @author Stefanie Wiegand
 */
public class CameraAlignmentIndicator extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(CameraAlignmentIndicator.class);
	private PhotoNormaliserPresenter presenter;
	private final Camera camera;

	@FXML
	private Label indicatorLabel;
	@FXML
	private ImageView indicatorImageView;

	/**
	 * Create e new CameraAlignmentIndicator
	 *
	 * @param controller the controller for this component
	 * @param camera the camera this indicator is for
	 */
	public CameraAlignmentIndicator(PhotoNormaliserController controller, Camera camera) {

		super(controller);
		this.camera = camera;

		//set up label and colours
		indicatorLabel.setText(camera.getIdentifier());
		checkAlignment();
	}

	/**
	 * Check if this camera is connected to the reference camera and display an icon to reflect this
	 */
	public final void checkAlignment() {
		//get reference cam - without it there couldn't be a distance
		String refCam = presenter.getRefCameraChoiceBox().getValue();
		if (refCam != null) {
			//grab the graph and calculate the distance
			AlignmentGraph dwg = presenter.getController().getAlignmentGraph();
			Long distance = dwg.distance(camera.getIdentifier(), refCam);
			//if it comes back as null it means it's not connected (=aligned)
			if (distance == null) {
				indicatorImageView.setImage(
					new Image(getClass().getClassLoader().getResourceAsStream(MemoOptions.ICON_NO))
				);
			//otherwise it's aligned
			} else {
				indicatorImageView.setImage(
					new Image(getClass().getClassLoader().getResourceAsStream(MemoOptions.ICON_YES))
				);
			}
		}
	}

}
