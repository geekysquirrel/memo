/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.components;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a link component, containing a set of linked cameras, e.g.
 *
 * (cam1)-----()------(cam3)-----()-----()
 *
 * @author Stefanie Wiegand
 */
public class Link extends HBox {

	private static final Logger logger = LoggerFactory.getLogger(Link.class);

	private static final Double SPACING = 5.0;

	private PhotoNormaliserController controller;
	private final Set<String> cameras;

	/**
	 * Creates a camera icon in the link component.
	 *
	 * @param controller the parent controller
	 * @param cameras the existing cameras
	 * @param alignmentSet the alignment set this UI component represents
	 */
	public Link(PhotoNormaliserController controller, Set<String> cameras, Map<String, String> alignmentSet) {

		this.controller = controller;
		this.cameras = new HashSet<>();

		//style
		this.getStyleClass().add("Link");
		this.setSpacing(SPACING);

		//this is not a header
		if (alignmentSet!=null) {

			//iterate over cameras
			for (String c: cameras) {

				String imagePath;
				if (alignmentSet.containsKey(c) && !alignmentSet.get(c).isEmpty()) {
					//check whether thumbnail exists and create if it doesn't
					File testFile = new File(
						MemoOptions.get("path.thumbnailDirectory") + alignmentSet.get(c)
					);
					if (!testFile.exists()) {
						try {
							MediaItem mi = new MediaItem(alignmentSet.get(c), false, false, false);
							mi.createThumbnail();
						} catch (FileNotFoundException e) {
							//deliberately not logging exception as this is only a warning
							logger.warn("Failed to create thumbnail for {}", testFile.getPath());
						}
					}
					imagePath = "file:" + MemoOptions.get("path.thumbnailDirectory")
							+ alignmentSet.get(c);
				} else {
					imagePath = null;
				}

				//if this camera is in the set (=is linked) add a linked icon, otherwise add a non-linked icon
				this.getChildren().add(new LinkCameraIcon(
					controller, null, imagePath, imagePath!=null
				));
				if (imagePath!=null) {
					this.cameras.add(c);
				}
			}
		//this is a header
		} else {
			//iterate over sorted map of cameras
			cameras.stream().forEach(c ->
				//add a camera icon for the header row
				this.getChildren().add(new LinkCameraIcon(controller, c, null, null))
			);
		}

		//add delete button
		Button deleteButton = new Button("Delete");
		if (alignmentSet==null) {
			deleteButton.setVisible(false);
		} else {
			this.setAlignment(Pos.CENTER);
			deleteButton.setOnAction(event -> {
				controller.getPresenter().removeLink(alignmentSet);
				//update UI
				controller.getPresenter().setAlignmentRefCam(
					controller.getPresenter().getRefCameraChoiceBox().getValue()
				);
				event.consume();
			});
		}
		this.getChildren().add(deleteButton);
	}

	/**
	 * Test whether this link contains a particular camera
	 *
	 * @param cameraID the identifier of the camera to look for
	 * @return true if the camera is contained in the link; false if it isn't
	 */
	public boolean containsCamera(String cameraID) {
		return cameras.contains(cameraID);
	}

	@Override
	public String toString() {
		return cameras.toString();
	}
}
