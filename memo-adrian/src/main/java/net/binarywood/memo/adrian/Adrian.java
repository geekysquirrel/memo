/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */
package net.binarywood.memo.adrian;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.common.exceptions.EdgeOverrideException;
import net.binarywood.memo.common.model.AlignmentGraph;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.GraphNode;
import net.binarywood.memo.common.model.MediaItem;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.model.TimezoneHistory;
import net.binarywood.memo.common.model.WaypointHistory;
import net.binarywood.memo.common.spec.AController;
import net.divbyzero.gpx.GPX;
import net.divbyzero.gpx.JDOM;
import net.divbyzero.gpx.Parser;
import net.divbyzero.gpx.ParsingException;
import net.divbyzero.gpx.Waypoint;
import org.im4java.core.ETOperation;
import org.im4java.core.ExiftoolCmd;
import org.im4java.core.IM4JavaException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class does sorting related actions involving photos. It does not maintain media items and acts as a (static)
 * utility class.
 *
 * @author Stefanie Wiegand
 */
public class Adrian {

	private static final Logger logger = LoggerFactory.getLogger(Adrian.class);

	private Adrian() {}

	/**
	 * Loads all GPX files withing a given directory into a waypoint history object
	 *
	 * @param directory where to load the GPX files from
	 * @param controller the controller to update during this task
	 * @return the waypoint history
	 */
	public static WaypointHistory loadGPX(String directory, PhotoNormaliserController controller) {

		WaypointHistory wph = new WaypointHistory();
		List<GPX> gpxFiles = new ArrayList<>();

		File dir = new File(directory);

		//count gpx files
		FilenameFilter gpxFilter = (File d, String name) -> name.toLowerCase().endsWith(".gpx");
		String[] gpxPaths = dir.list(gpxFilter);
		int total = 0;
		if (gpxPaths != null) {
			total = gpxPaths.length;
		}

		if (dir.isDirectory()) {
			logger.debug("Scanning {} for GPX files", dir.getAbsolutePath());

			int processed = 0;
			for (File file : dir.listFiles()) {

				if (!file.getName().toLowerCase().endsWith(".gpx")) {
					continue;
				}

				processed++;
				//tell presenter (via controller) about progress
				if (controller != null && controller.getPresenter() != null && !gpxFiles.isEmpty()) {
					final int tf = total;
					final int pf = processed;
					Platform.runLater(() -> controller.getPresenter().setStatus("Loading GPX file " + pf + " of " + tf));
					Platform.runLater(() -> controller.getPresenter().updateProgress((double) pf / tf,
							controller.getPresenter().getGeotaggingSpinner()));
				}

				try {
					Parser parser = new JDOM();
					gpxFiles.add(parser.parse(new File(file.getAbsolutePath())));
				} catch (ParsingException e) {
					logger.error("Could not load GPX file {}", file.getName(), e);
				}
			}

			//get all the waypoints in the correct order (by UTC datetime) from all the gpx files
			gpxFiles.stream().forEach(gpx
					-> gpx.getTracks().stream().forEach(t
							-> t.getSegments().stream().forEach(s
									-> s.getWaypoints().stream().forEach(wp
											-> wph.add(new DateTime(wp.getTime(), DateTimeZone.UTC), wp)
									)
							)
					)
			);
			if (controller != null && controller.getPresenter() != null) {
				Platform.runLater(() -> controller.getPresenter().setStatus(""));
			}
		}
		return wph;
	}

	/**
	 * Naive method to geotag a photo using a waypoint history. If the photo's date is covered, this will walk the
	 * (sorted) history until a nearby GPS tag is found. If it's near enough (considering the logging interval), the
	 * photo is tagged.
	 *
	 * @param mediaItem the item to tag
	 * @param waypointHistory the history to use for tagging
	 * @return the geotagged photo or the original photo if goetagging was unsuccessful
	 */
	public static MediaItem geotagPhoto(MediaItem mediaItem, WaypointHistory waypointHistory) {

		if (mediaItem.isGeotagged()) {
			logger.debug("MediaItem {} is already geotagged!", mediaItem.getPath());
			return mediaItem;
		}

		if (waypointHistory.isEmpty()) {
			logger.debug("No GPS waypoints found");
			return mediaItem;
		}

		//make sure this history covers the given photo
		//TODO: check camera was at the same timezone as gpx recorder!
		DateTime first = waypointHistory.getHistory().firstKey();
		DateTime last = waypointHistory.getHistory().lastKey();
		if (first.isAfter(mediaItem.getDatetimeOriginalExif()) && last.isBefore(mediaItem.getDatetimeOriginalExif())) {
			logger.warn("Could not geotag MediaItem {}; the given GPX files do not cover the MediaItem's timestamp",
					mediaItem.getPath());
			return mediaItem;
		}

		//prepare iteration
		SortedMap<DateTime, Object> history = waypointHistory.getHistory();
		if (!history.isEmpty()) {
			DateTime currentDateTimeGPX = history.firstKey();
			Waypoint currentWaypoint = (Waypoint) history.get(history.firstKey());
			//Waypoint currentWaypoint = (Waypoint) history.firstEntry().getValue();
			int loggingInterval = 0;

			for (Map.Entry<DateTime, Object> he : history.entrySet()) {
				//forward gpx timestamps
				if (currentDateTimeGPX.isBefore(mediaItem.getGpsDate())) {
					//maintain current point and interval
					DateTime newDateTimeGPX = he.getKey();
					loggingInterval = (int) (newDateTimeGPX.minus(currentDateTimeGPX.getMillis()).getMillis() / 1000);
					currentDateTimeGPX = newDateTimeGPX;
					currentWaypoint = (Waypoint) he.getValue();
					continue;
				}
				//logger.debug("Next waypoint found at: {} {}",
				//	(new DateTime(((Waypoint) he.getValue()).getTime(), DateTimeZone.UTC)), he.getValue());
				//logger.debug("Last logging interval {}", loggingInterval);

				//Compare both datetimes, find closest match (within logging interval)
				int diffPrev = (int) (currentDateTimeGPX.minus(mediaItem.getGpsDate().getMillis()).getMillis() / 1000);
				int diffNext = (int) (he.getKey().minus(mediaItem.getGpsDate().getMillis()).getMillis() / 1000);
				//logger.debug("MediaItem diff to prev: {}, next: {}", diffPrev, diffNext);
				int diffMin;
				Waypoint myWaypoint;
				if (diffPrev < diffNext) {
					diffMin = diffPrev;
					myWaypoint = currentWaypoint;
				} else {
					diffMin = diffNext;
					myWaypoint = (Waypoint) he.getValue();
				}

				//tag photo if match found and nearest gpx point less than a minute away
				//TODO: clever stuff here like considering speed, distance etc.
				if (diffMin <= loggingInterval && diffMin <= 60) {

					logger.debug("Tagging MediaItem {} with GPS from GPX {}", mediaItem.getFilename(), myWaypoint);
					logger.debug("MediaItem at {}, diff to nearest gpx waypoint: {}", mediaItem.getGpsDate(), diffMin);
					logger.debug("Waypoint at {}: {}", new DateTime(myWaypoint.getTime(), DateTimeZone.UTC), myWaypoint);

					mediaItem.setAltitude(String.valueOf(myWaypoint.getElevation()));
					if (myWaypoint.getCoordinate().getLatitude() >= 0) {
						mediaItem.setLatRef("N");
					} else {
						mediaItem.setLatRef("S");
					}
					if (myWaypoint.getCoordinate().getLongitude() >= 0) {
						mediaItem.setLongRef("E");
					} else {
						mediaItem.setLongRef("W");
					}
					mediaItem.setLatitude(String.valueOf(myWaypoint.getCoordinate().getLatitude()));
					mediaItem.setLongitude(String.valueOf(myWaypoint.getCoordinate().getLongitude()));
					mediaItem.setFlags();

				}
				break;
			}
		}
		return mediaItem;
	}

	/**
	 * Build an AlignmentGraph using the media items' UTC timestamps
	 *
	 * @param alignmentSets the alignment sets containing the distance between nodes
	 * @param refCameraId the reference camera ID (=graph start node)
	 * @param normalisedMediaItems the items to use to build the graph. Used to access UTC time
	 * @return the finished graph
	 * @throws net.binarywood.memo.common.exceptions.EdgeOverrideException if the linking would override an existing
	 * link
	 */
	public static AlignmentGraph createAlignmentGraph(Set<Map<String, String>> alignmentSets, String refCameraId,
			Map<String, MediaItem> normalisedMediaItems) throws EdgeOverrideException {
		//TODO: debug this
		logger.debug("Creating alignment graph, using reference camera {}", refCameraId);
		//logger.debug("Alignment sets:\n{}", alignmentSets);

		//build graph
		AlignmentGraph dwg = new AlignmentGraph();
		//this structure maintains the nodes while building the graph
		Map<String, GraphNode> nodes = new HashMap<>();
		//this set holds a copy of all the nodes for the graph building algorithm
		Set<GraphNode> nodeSet = new HashSet<>();
		//iterate over all alignment sets to get nodes
		alignmentSets.stream().forEach(alss ->
				//for each alignment set look at the individual cameras contained in it
				alss.entrySet().stream().forEach(als -> {
					//only consider cameras that have a photo
					if (als.getValue() != null && !als.getValue().isEmpty()) {
						//get hold of the camera node; create if it doesn't exist
						GraphNode currentNode;
						if (!nodes.containsKey(als.getKey())) {
							currentNode = new GraphNode(als.getKey());
							currentNode.setData(als.getValue());
							//logger.debug("adding new node {}", currentNode);
						} else {
							currentNode = nodes.get(als.getKey());
							//logger.debug("using existing node {}", currentNode);
						}
						nodes.put(currentNode.getId(), currentNode);
						nodeSet.add(currentNode);
					} else {
						//logger.debug("Skipping camera without picture: {}", als.getKey());
					}
				})
		);
		//logger.debug("nodes: {}", nodeSet);

		//start with reference cam
		GraphNode nextNode = nodes.get(refCameraId);
		nodeSet.remove(nextNode);
		//iterate over alignment sets again to create links
		while (nextNode != null) {
			Iterator<Map<String, String>> iterator = alignmentSets.iterator();
			while (iterator.hasNext()) {
				Map<String, String> als = iterator.next();
				//find an alignment set this node is part of
				if (als.containsKey(nextNode.getId()) && !als.get(nextNode.getId()).isEmpty()) {
					//look at each camera within this alignment set
					for (Map.Entry<String, String> entry : als.entrySet()) {
						//if it's NOT this camera, link (this means linking will be done both sides)
						if (!entry.getKey().equals(nextNode.getId())) {
							GraphNode linkTo = nodes.get(entry.getKey());
							//only consider cameras from alignment set that have a photo
							if (linkTo != null && entry.getValue() != null && !entry.getValue().isEmpty()) {
								//start the linking

								//logger.debug("Linking {} with {}", nextNode.getId(), linkTo.getId());
								//calculate time diff weight (no need to run exiftool!)
								MediaItem from = normalisedMediaItems.get(als.get(nextNode.getId()));
								MediaItem to = normalisedMediaItems.get(entry.getValue());

								if ((new File(from.getPath())).isFile() && (new File(to.getPath())).isFile()) {
									logger.debug("{} vs {}", from.getFilename(), to.getFilename());
									logger.debug("{} vs {}", from.getGpsDate(), to.getGpsDate());
									//negative diff means to is before from
									long diff;
									//use GPS date when possible
									if (to.getGpsDate()!=null && from.getGpsDate()!=null) {
										diff = to.getGpsDate().getMillis() - from.getGpsDate().getMillis();
									//if it's not available fall back to local time
									} else {
										diff = to.getDatetimeOriginalExif().getMillis()
											- from.getDatetimeOriginalExif().getMillis();
									}
									logger.debug("difference: {}s", diff / 1000);
									nextNode.linkToNode(nodes.get(entry.getKey()), diff);
								} else {
									logger.warn("Cannot link {} and {}: at least one of the files doesn't exist",
											from.getPath(), to.getPath());
								}
							}
						}
					}
					//allow cyclic graphs, the distance() method facilitates searching of cyclic graphs
					//iterator.remove();
				}
			}
			//finish this node
			//logger.debug("Finishing node {}", nextNode.getId());
			nodeSet.remove(nextNode);
			Iterator<GraphNode> it = nodeSet.iterator();
			if (it.hasNext()) {
				nextNode = it.next();
				it.remove();
				//logger.debug("Next node: {}", nextNode.getId());
			} else {
				nextNode = null;
			}
		}

		//add nodes to actual graph
		nodes.values().stream().forEach(node -> dwg.addNode(node));
		logger.debug("Created alignment graph {}", dwg);

		return dwg;
	}

	/**
	 * Calculate the timeshift for the two given cameras
	 *
	 * @param graph the alignement graph containing (hopefully) both cameras
	 * @param mediaItem the item for which to calculate the timeshift
	 * @param referenceCamera the reference camera to use for the calculation
	 * @param normalisedMediaItems a map of already normalised media items
	 * @return the temporal distance between the cameras as read from the graph
	 */
	public static Long calculateTimeshiftForPhoto(AlignmentGraph graph, MediaItem mediaItem, Camera referenceCamera,
		Map<String, MediaItem> normalisedMediaItems) {

		Camera camera = mediaItem.getCamera();

		Long timeshift = null;
		//only do this if there is a valid graph containing information for both relevant cameras
		if (graph != null && !graph.isEmpty() && graph.getNode(camera.getIdentifier()) != null
			&& graph.getNode(referenceCamera.getIdentifier()) != null) {

			//get UTC timeshift between cameras at the time that the alignment photo was taken
			timeshift = graph.distance(referenceCamera.getIdentifier(), camera.getIdentifier());

			if (timeshift != null) {

//				//TODO: calc path in graph
//				//get all photos in chain plus their timezones
//				//retrieve original media items
//					String fileName = graph.getNode(camera.getIdentifier()).getData();
//					String referenceFileName = graph.getNode(referenceCamera.getIdentifier()).getData();
//
//					MediaItem alignmentMediaItem = normalisedMediaItems.get(fileName);
//					MediaItem referenceMediaItem = normalisedMediaItems.get(referenceFileName);
//				//get the timezones the cameras had at that time
//					DateTime cameraDate = alignmentMediaItem.getDatetimeOriginalExif();
//					DateTime referenceDate = referenceMediaItem.getDatetimeOriginalExif();
//					DateTimeZone cameraZone = camera.getSettingsHistory().getTimezoneAtDate(cameraDate);
//					DateTimeZone referenceZone = referenceCamera.getSettingsHistory().getTimezoneAtDate(referenceDate);
//					logger.debug("For the reference images, the camers's timezones are:\n"
//						+ "\t{} {} {} {}\n\t{} {} {} {}",
//						camera.getIdentifier(), alignmentMediaItem.getFilename(),
//						alignmentMediaItem.getDatetimeOriginalExif(), cameraZone,
//						referenceCamera.getIdentifier(), referenceMediaItem.getFilename(),
//						referenceMediaItem.getDatetimeOriginalExif(), referenceZone);
//				//consider distance between camera settings timezones
//					DateTime localisedDatetime1 = cameraDate.withZoneRetainFields(cameraZone);
//					DateTime localisedDatetime2 = referenceDate.withZoneRetainFields(referenceZone);
//					logger.debug("Localised dates: {}, {}", localisedDatetime1, localisedDatetime2);
//					long offset1 = cameraZone.getOffset(localisedDatetime1);
//					long offset2 = referenceZone.getOffset(localisedDatetime2);
//					long offsetDistance = offset1 - offset2;
//					logger.debug("Timezone offset distance then: {} - {} = {}", offset1, offset2, offsetDistance);
//				//calculate the timezone offset distance the cameras should have now
//					DateTime itemDate = mediaItem.getDatetimeOriginalExif();
//					DateTimeZone itemZoneNow = camera.getSettingsHistory().getTimezoneAtDate(itemDate);
//					DateTimeZone referenceZoneNow = referenceCamera.getSettingsHistory().getTimezoneAtDate(itemDate);
//					long offset1now = itemZoneNow.getOffset(itemDate);
//					long offset2now = referenceZoneNow.getOffset(itemDate);
//					long offsetDistanceNow = offset1now - offset2now;
//					logger.debug("Timezone offset distance now: {} - {} = {}",
//					offset1now, offset2now, offsetDistanceNow);
//
//					//TODO: use distance of offset to calculate "custom" media item timeshift
//					logger.debug("EXPERIMENTAL: Timeshift then: {}, timeshift now: {}", timeshift,
//					timeshift - offsetDistance + offsetDistanceNow);
			} else {
				logger.debug("The cameras {} and {} are not connected in the alignment graph",
					camera.getIdentifier(), referenceCamera.getIdentifier());
			}
		}

		//don't shift for ref mediaItemFromCamera
		if (referenceCamera.getIdentifier().equals(camera.getIdentifier())) {
			logger.debug("Don't shift media by reference camera");
			timeshift = 0L;
			//if there is a (valid) distance between the two cameras let the user know
		} else if (timeshift != null) {
			logger.debug("{} taken by {} needs to be shifted {} by {}s",
				mediaItem.getFilename(), mediaItem.getCamera().getIdentifier(),
				timeshift > 0 ? "back" : "forward", timeshift < 0 ? timeshift / -1000 : timeshift / 1000);
			//if there was anything wrong with the graph don't shift
		} else {
			logger.debug("Don't shift media by this camera");
			timeshift = 0L;
		}

		return timeshift;
	}

	/**
	 * Normalise a single media item. This considers all the timestamps and calculates UTC and local time. For now, the
	 * alignment graph is ignored; we pretend we won't align (optionally happens later)
	 *
	 * @param mi the media item to process
	 * @param referenceCamera the reference camera (most likely the camera which recorded the GPX tracks)
	 * @param tripTimezoneHistory the history of timezones visited by the various devices during the trip
	 * @param controller the controller responsible for showing alerts to capture user input
	 * @return the "normalised" media item, containing allcalculated timestamps
	 */
	public static MediaItem normalise(MediaItem mi, Camera referenceCamera,
			TimezoneHistory tripTimezoneHistory, AController controller) {

		//TODO: check if it has been normalised already
		/*
		* gpsdate and time set
		* usercomment contains timezone id
		* software is MemO (sure?)
		* renamed
		 */
		//consequences: will have GPS timestamp, datetime digitised
		//and p.getFilename() will look like UTC-2014-09-20_16-34-58-DSCN1234.JPG
		//if already processed: log but process again
		if (mi.getDatetimeOriginalExif() == null) {
			logger.info("Couldn't find sufficient metadata to process {}", mi.getFilename());
		} else if (mi.isVideo() && mi.getGpsDate() != null) {
			logger.info("########### Video: {} ####################################################", mi.getFilename());
			logger.info("Special procedure for videos, where timestamps are UTC timestamps");
			DateTime dtUTC = mi.getGpsDate();
			logger.debug("dtUTC:       {}", dtUTC);
			DateTime dtLocal = dtUTC.withZone(mi.getCamera().getLocationHistory().getTimezoneAtDate(dtUTC));
			logger.debug("dtLocal:     {}", dtLocal);
			mi.setLocalDate(dtLocal);

		} else {
			logger.info("########### Media item: {} ###############################################", mi.getFilename());

			DateTime dtRaw, dtCamera, dtLocal, dtUTC;

			//don't use GPS info if available because it's based on phone time (not GPS time) and is probably wrong!
			//instead, calculate date(s):
			//get date from photo. ignore timezone as set by OS at time of parsing, not by camera
			dtRaw = mi.getDatetimeOriginalExif();
			logger.debug("dtRaw:       {}", dtRaw);

			//get all the timezones close to the photo date
			//(i.e. using the local time combined with all existing timezones)
			Set<DateTimeZone> nearbyTimezones;
			if (mi.getCamera() == null || mi.getCamera().getSettingsHistory() == null
					|| mi.getCamera().getSettingsHistory().getTimezonesAroundDate(dtRaw) == null) {
				nearbyTimezones = new HashSet<>();
				nearbyTimezones.add(DateTimeZone.UTC);
			} else {
				nearbyTimezones = mi.getCamera().getSettingsHistory().getTimezonesAroundDate(dtRaw);
			}
			logger.debug("Close timezones: {}", nearbyTimezones);

			//now apply all the possible timezones to the camera timeline and retrieve all the possible timezones
			TimezoneHistory possibleTimezones = new TimezoneHistory();
			DateTimeZone correctTimeZone;
			final Set<DateTimeZone> correctTimeZoneSet = new HashSet<>();
			for (DateTimeZone tz : nearbyTimezones) {
				DateTime possibleDate = dtRaw.withZoneRetainFields(tz);
				DateTimeZone possibleTimezone = mi.getCamera().getSettingsHistory().getTimezoneAtDate(possibleDate);
				logger.debug("Applying possible timezone {} to date {} => UTC {}",
						tz, possibleDate, possibleDate.withZone(DateTimeZone.UTC));
				if (tz!=null && possibleTimezone!=null && tz.equals(possibleTimezone)) {
					logger.debug("Match! this could be the timezone!");
					possibleTimezones.add(possibleDate, possibleTimezone);
				} else {
					logger.debug("Timzone mismatch: if the timezone was {}, according to the history it was {}",
						tz, possibleTimezone);
				}
			}

			logger.debug("Possible timezones: {}", possibleTimezones);
			if (possibleTimezones.getDistinctTimezones().size() == 1) {
				correctTimeZone = (DateTimeZone) possibleTimezones.peek();
				logger.debug("Only one possible timezone found: {}", correctTimeZone);
			} else if (possibleTimezones.getDistinctTimezones().isEmpty()) {
				//UTC is wrong here but need something to use and UTC is as good as any
				correctTimeZone = DateTimeZone.UTC;
				logger.warn("No timezone found for raw date {}", dtRaw);
			} else {
				logger.warn("{} possible date/timezone combinations found: {}",
						possibleTimezones.size(), possibleTimezones);

				//TODO: check the following:
				//if the timezones' times describe the same UTC time for this date (because the timezone
				//offset is the same) just pick one at random - it doesn't matter for the finished photo
				//because its timezone metadata will be written using camera location, not camera setting
				Set<DateTimeZone> tmp = possibleTimezones.getDistinctTimezones();
				correctTimeZone = null;
				Iterator<DateTimeZone> it = tmp.iterator();
				DateTimeZone tz1 = it.next();
				it.remove();
				DateTimeZone tz2 = null;
				//the absolute maximum here is 3 timezones
				while (it.hasNext()) {
					DateTimeZone tz = it.next();

					if (!tz.equals(tz1)) {
						//compare tz1 and tz2 (first iteration) or tz1 and tz3 (potential second iteration)
						logger.debug("a) Compare {} {} with {} {}",
								tz, tz.getOffset(mi.getDatetimeOriginalExif()),
								tz1, tz1.getOffset(mi.getDatetimeOriginalExif()));
						if (tz.getOffset(mi.getDatetimeOriginalExif())
								== tz1.getOffset(mi.getDatetimeOriginalExif())) {
							correctTimeZone = tz;
							it.remove();
						} else {
							tz2 = tz;
						}
					}

					if (!tz.equals(tz2) && tz2 != null) {
						//compare tz2 and tz3 if there are 3 timezones
						logger.debug("b) Compare {} {} with {} {}",
								tz, tz.getOffset(mi.getDatetimeOriginalExif()),
								tz2, tz2.getOffset(mi.getDatetimeOriginalExif()));
						if (tz.getOffset(mi.getDatetimeOriginalExif())
								== tz2.getOffset(mi.getDatetimeOriginalExif())) {
							it.remove();
							correctTimeZone = tz;
						}
					}
				}

				//if after filtering timezones with the same effective date the correct timezone still
				//couldn't be determinded, ask the user
				if (correctTimeZone == null) {

					//Note: this happens when crossing timezones east to west. In that case,
					//there is a time window where local time is "wound back" and timestamps are ambiguous.
					//guessing doesn't help:
					//-GPS info (independent of camera setting)
					//-trip time (independent of camera setting)
					//The only information that can help here is the picture itself
					//TODO: - altitude (if it's very high in the air - geonames srtm3 - ,
					//then chances are airplane mode is on and the previous timezone is active)
					//intro buttonText
					String text = mi.getFilename() + "\nPhoto date: "
							+ mi.getDatetimeOriginalExif().toString(MediaItem.dateFormatWithoutTimezone) + "\n";

					//TODO: could this be more than 2 timezones?
					//the two zones
					StringBuilder placeA = new StringBuilder();
					StringBuilder placeB = new StringBuilder();

					//generate list of buttons
					ArrayList<ButtonType> types = new ArrayList<>();

					for (DateTimeZone tz : possibleTimezones.getDistinctTimezones()) {

						placeA = new StringBuilder();
						placeB = new StringBuilder();

						StringBuilder buttonText = new StringBuilder("Camera set to ");
						buttonText.append(tz.getID());
						buttonText.append("\n\n");
						//calculate possible time
						DateTime possibleUTCDateTime = mi.getDatetimeOriginalExif()
							.withZoneRetainFields(tz).withZone(DateTimeZone.UTC);

						for (DateTimeZone dtz : possibleTimezones.getDistinctTimezones()) {
							if (!tz.equals(dtz)) {

								//direction of movement must be east to west, else it wouldn't be ambiguous
								if (tz.getOffset(mi.getDatetimeOriginalExif())
									> dtz.getOffset(mi.getDatetimeOriginalExif())) {

									placeA.append(tz.getID());
									placeB.append(dtz.getID());
									buttonText.append("Still travelling\nSetting not yet changed\n");
									buttonText.append("Possibly in airplane safe mode\n\n");
								} else {
									placeA.append(dtz.getID());
									placeB.append(tz.getID());
									//TODO: is that good? time will tell xD
									if (tz.equals(DateTimeZone.UTC)) {
										buttonText.append("Possibly used network on the plane\nStill travelling\n");
										buttonText.append("Using international time\n\n");
									} else {
										buttonText.append("Already arrived\nSetting changed\nUsing local time\n\n");
									}
								}
								buttonText.append(
									possibleUTCDateTime.withZone(tz).toString(MediaItem.dateFormatWithoutTimezone)
								);
								buttonText.append(", ");
								buttonText.append(tz.getID());
								buttonText.append("\n");
								buttonText.append(
									possibleUTCDateTime.withZone(dtz).toString(MediaItem.dateFormatWithoutTimezone));
								buttonText.append(", ");
								buttonText.append(dtz.getID());
							}
						}

						//create button
						ButtonType bt = new ButtonType(buttonText.toString());
						types.add(bt);
					}

					//dialog to ask the user
					Alert a = controller.createAlert(Alert.AlertType.CONFIRMATION, "Pick a timezone", text,
						"When taking this picture using " + mi.getCamera().getIdentifier() + "\n"
						+ "the timezone was first set to " + placeA
						+ " and later changed to " + placeB + ".\n"
						+ "Which of the following is true?");

					//build a dialog
					ImageView thumb = new ImageView("file://" + mi.getPath());
					thumb.setPreserveRatio(true);
					thumb.setFitWidth(Double.valueOf(MemoOptions.get("display.thumbnailWidth")));
					a.setGraphic(thumb);

					a.getButtonTypes().setAll(types);

					//let the user pick one
					Optional<ButtonType> result = a.showAndWait();
					String cutTimezone = result.get().getText();
					//cut the timezone id from the button text (workaround for button data)
					cutTimezone = cutTimezone.substring(14, cutTimezone.indexOf("\n"));
					logger.debug("User input: {}", cutTimezone);
					correctTimeZoneSet.add(DateTimeZone.forID(cutTimezone));
				}
			}

			//attach camera timezone leaving date and time intact
			if (!correctTimeZoneSet.isEmpty()) {
				correctTimeZone = correctTimeZoneSet.iterator().next();
			}
			dtCamera = dtRaw.withZoneRetainFields(correctTimeZone);
			logger.debug("Camera time: " + dtCamera + ", timezone is " + dtCamera.getZone());

			//transform date the camera thought it was to local time
			dtLocal = dtCamera.withZone(tripTimezoneHistory.getTimezoneAtDate(dtCamera));
			logger.debug("Trip time:   " + dtLocal + ", timezone is " + dtLocal.getZone());

			//save local time back to media item
			mi.setLocalDate(dtLocal);

			//get UTC time
			dtUTC = dtLocal.toDateTime(DateTimeZone.UTC);
			logger.debug("UTC time:    " + dtUTC);
			mi.setGpsDate(dtUTC);
		}

		return mi;
	}

	/**
	 * Write EXIF tags to a file
	 *
	 * @param file the path of the file
	 * @param tags the tage to write
	 */
	public static void writeTags(String file, Set<String> tags) {
		ExiftoolCmd exif = new ExiftoolCmd();
		ETOperation op = new ETOperation();
		op.addImage(file);
		// no need to use quotes if blanks are contained - will be handled automatically
		op.setTags(tags.toArray(new String[tags.size()]));
		op.overwrite_original();
		//ignore messy/incorrect metadata and just do what is required to get it done ;)
		op.addRawArgs("-m");
		//op.verbose();
		// execute the operation
		try {
			//mute exiftool
			//PrintStream originalStream = MemoOptions.muteSystemOut();
			//run exiftool
			exif.run(op);
			//print debug if anything happened
			ArrayList<String> debug = exif.getErrorText();
			if (!debug.isEmpty()) {
				logger.debug("Exif debug: {}", exif.getErrorText());
			}
			//replace dummy stream with original stream
			//System.setOut(originalStream);
		} catch (IOException | InterruptedException | IM4JavaException e) {
			logger.error("Error writing tag(s) to file {}", file, e);
		}
	}

	/**
	 * Rename a normalised media item using the UTC date/time
	 *
	 * @param mi the item to be renamed
	 * @param removeSubfolders whether to remove the subfolders and save everything in the main folder
	 * @param importDirectory the parent directory from which the media items were loaded
	 */
	public static void renameMediaItem(MediaItem mi, boolean removeSubfolders, String importDirectory) {
		//TODO: comment back in and investigate why it fails when it shouldn't
		//if (mi.isNormalised()) {
		File oldFile = new File(mi.getPath());
		String filename = "UTC_" + mi.getGpsDate().toString("yyyy-MM-dd_HH-mm-ss-") + mi.getFilename();
		String newpath;
		if (removeSubfolders) {
			newpath = importDirectory + "/" + filename;
			//TODO: actually remove subfolders?
		} else {
			newpath = oldFile.getParent() + "/" + filename;
		}
		File newFile = createUniqueFile(newpath);
		if (oldFile.renameTo(newFile)) {
			//TODO: send event to update DB if existing
		} else {
			logger.error("Error renaming file {} to {}", mi.getPath(), newpath);
		}
		//} else {
		//	logger.error("Cannot rename media item {}, it's not normalised", mi.getFilename());
		//}
	}

	/**
	 * Creates a filename, which is guaranteed to be unique, i.e. append a number if it isn't.
	 *
	 * @param path the original/desired path
	 * @return the actual File
	 */
	private static File createUniqueFile(String path) {
		File newFile = new File(path);
		int suffix = 1;
		String potentialPath;
		while (newFile.exists()) {
			suffix++;
			potentialPath = path + "_" + suffix;
			newFile = new File(potentialPath);
		}
		return newFile;
	}

}
