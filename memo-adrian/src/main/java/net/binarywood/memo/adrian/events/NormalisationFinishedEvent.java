/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.events;

import java.util.EventObject;

/**
 * An event for letting the receiver know that the normalisation (running on a separate thread) has finished now
 *
 * @author Stefanie Wiegand
 */
public class NormalisationFinishedEvent extends EventObject {
	private static final long serialVersionUID = 1L;

	/**
	 * Create a new NormalisationFinishedEvent
	 *
	 * @param source the event source
	 */
	public NormalisationFinishedEvent(Object source) {
		super(source);
	}
}
