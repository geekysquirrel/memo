/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.components;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.adrian.events.SelectReferenceCameraEvent;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.AComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents one single camera in a link component, e.g.
 *	vvvv
 * (cam1)-----()------(cam3)-----()-----()
 *
 * @author Stefanie Wiegand
 */
public class LinkCameraIcon extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(LinkCameraIcon.class);

	private final String camera;

	@FXML
	private ImageView cameraIcon;

	/**
	 * Creates a camera icon in the link component.
	 *
	 * @param controller the parent controller
	 * @param camera the ID of the camera or null
	 * @param image the image used for this link or null
	 * @param linked whether it is connected or not
	 */
	public LinkCameraIcon(PhotoNormaliserController controller, String camera, String image, Boolean linked) {

		super(controller);
		this.camera = camera;

		//attach CSS classes depending on what this is
		if (linked==null) {
			mainContainer.getStyleClass().add("linkedHeader");
		} else if (linked) {
			mainContainer.getStyleClass().add("linked");
		} else {
			mainContainer.getStyleClass().add("notLinked");
		}

		//add icon of camera for headers
		if (camera!=null) {
			getController().getPresenter().setImage(cameraIcon, MemoOptions.ICON_CAMERA);
			getController().getPresenter().setTooltip(mainContainer, camera, null);
		//add thumbnail of image for link icon
		} else if (image!=null) {
			getController().getPresenter().setImage(cameraIcon, MemoOptions.ICON_PHOTO);
			getController().getPresenter().setTooltip(mainContainer, "", image);
		//show only empty container for non-linked camera
		} else {
			getController().getPresenter().setImage(cameraIcon, null);
		}

		//add double click event for selecting ref cam
		mainContainer.setOnMouseClicked(event -> {
			if (event.getClickCount()>1) {
				fireEvent(new SelectReferenceCameraEvent(this, getController().getPresenter().getMainNode(), camera));
			}
		});
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	public String getCamera() {
		return camera;
	}

	@Override
	public final PhotoNormaliserController getController() {
		return (PhotoNormaliserController) controller;
	}
}
