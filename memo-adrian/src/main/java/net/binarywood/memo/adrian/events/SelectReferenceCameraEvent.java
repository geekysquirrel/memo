/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.events;

import javafx.event.ActionEvent;
import javafx.event.EventTarget;

/**
 * An event for displaying camera media after a camera has been selected
 *
 * @author Stefanie Wiegand
 */
public class SelectReferenceCameraEvent extends ActionEvent {
	private static final long serialVersionUID = 1L;

	private final String cameraID;

	/**
	 * Create a new camera update event
	 *
	 * @param source the event source
	 * @param target the event target
	 * @param cam the old camera
	 */
	public SelectReferenceCameraEvent(Object source, EventTarget target, String cam) {
		super(source, target);
		this.cameraID = cam;
	}

	/**
	 * Get the new camera
	 *
	 * @return the new camera
	 */
	public String getCameraID() {
		return cameraID;
	}

}
