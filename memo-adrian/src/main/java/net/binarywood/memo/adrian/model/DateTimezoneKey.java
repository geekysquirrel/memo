/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.model;

import java.util.Objects;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple combined key for a timezone at a date
 *
 * @author Stefanie Wiegand
 */
public class DateTimezoneKey implements Comparable<DateTimezoneKey> {

	private static final Logger logger = LoggerFactory.getLogger(DateTimezoneKey.class);

	private final DateTime dateTime;
	private final String timezoneId;

	/**
	 * Create a new DateTimezoneKey with the given parameters
	 *
	 * @param dateTime the date/time
	 * @param timezoneId the String representation of the timezone ID
	 */
	public DateTimezoneKey(DateTime dateTime, String timezoneId) {
		this.dateTime = dateTime;
		this.timezoneId = timezoneId;
	}

	@Override
	public int compareTo(DateTimezoneKey o) {
		if (dateTime.equals(o.getDateTime())) {
			return timezoneId.compareTo(o.getTimezoneId());
		} else {
			return dateTime.compareTo(o.getDateTime());
		}
	}

	@Override
	public boolean equals(Object other) {
		return other.getClass().equals(DateTimezoneKey.class)
			&& dateTime.equals(((DateTimezoneKey) other).getDateTime())
			&& timezoneId.equals(((DateTimezoneKey) other).getTimezoneId());
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 59 * hash + Objects.hashCode(this.dateTime);
		hash = 59 * hash + Objects.hashCode(this.timezoneId);
		return hash;
	}

	@Override
	public String toString() {
		return dateTime + ": " + timezoneId;
	}

	// GETTERS & SETTERS //////////////////////////////////////////////////////////////////////////////////////////////

	public DateTime getDateTime() {
		return dateTime;
	}

	public String getTimezoneId() {
		return timezoneId;
	}

}
