/*
 * Copyright 2014 Stefanie Wiegand
 * This file is part of MemO which is licenced under GPL3 and comes with ABSOLUTELY NO WARRANTY.
 * For more information, see http://www.gnu.org/licenses/gpl-3.0.html
 */

package net.binarywood.memo.adrian.components;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import net.binarywood.memo.adrian.controller.PhotoNormaliserController;
import net.binarywood.memo.common.model.Camera;
import net.binarywood.memo.common.model.MemoOptions;
import net.binarywood.memo.common.spec.AComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This pane allows a photo (no video as timestamp is not a point in time) to be picked for one camera from disk
 * in order to align it with other cameras.
 *
 * @author Stefanie Wiegand
 */
public class CameraAlignmentPane extends AComponent {

	private static final Logger logger = LoggerFactory.getLogger(CameraAlignmentPane.class);
	private Camera camera;

	@FXML
	private TextField pathTextField;
	@FXML
	private Label cameraLabel;
	@FXML
	private ImageView thumbnailImageView;
	@FXML
	private Button selectButton;

	/**
	 * Create a new CameraAlignmentPane, which enables the user to pick an alignment picture for this camera
	 *
	 * @param controller the controller for this component
	 * @param camera the camera this pane represents
	 */
	public CameraAlignmentPane(PhotoNormaliserController controller, Camera camera) {

		super(controller);
		this.camera = camera;

		cameraLabel.setText(camera.getIdentifier());
	}

	/**
	 * Select a photo for an alignment set.
	 * This cannot be a video as it requires a single timestamp
	 *
	 * @param event the event that triggered this
	 */
	@FXML
	public void selectPhoto(ActionEvent event) {

		//let the user pick a photo
		FileChooser fc = new FileChooser();
		fc.setTitle("Choose alignment photo for camera " + camera.getIdentifier());
		fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Photo", "jpg,jpeg,png,gif,mpo,orf,raw"));
		//set initial diectory to import dir
		fc.setInitialDirectory(new File(MemoOptions.get("path.normaliserDirectory")));
		File file = fc.showOpenDialog(controller.getStage());
		//use the photo
		if (file != null) {
			String path = file.getAbsolutePath();
			pathTextField.setText(path);
			thumbnailImageView.setImage(new Image(file.toURI().toString()));
		}
		event.consume();
	}

	// GETTERS/SETTERS ////////////////////////////////////////////////////////////////////////////////////////////////

	public String getPath() {
		return pathTextField.getText();
	}

}
